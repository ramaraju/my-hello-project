require_relative "features/#{Device.platform}/support/env"
require_relative 'features/support/test_case'

def require_dir(path)
  Dir.glob("#{path}/*.rb") { |file| require_relative file }
end

require_dir('features/data')
require_dir('features/tests')
require_dir("features/#{Device.platform}/tests")
