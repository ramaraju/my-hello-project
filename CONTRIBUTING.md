We try to use the following workflow.
 
- Identify a problem or task that needs to be performed. It can be a non-coding activity.
- Create an issue on GitLab describing the problem, assign it to the relevant person (or keep unassigned, if it’s low priority and anyone can work on it)
- Try not to make epic issues that would require weeks of work; split those into smaller problems
- When looking for things to do, go to [Issue Board](https://vats.rakuten-it.com:444/calabash/ichiba/board), find an issue that’s assigned to you or unassigned, and drag it to the “In Progress” list. If the issue wasn’t assigned to anyone, assign it to yourself
- You can have multiple “in progress” issues assigned to you, but it’s best to keep it to one or two
- For issues that require code changes, create a new branch with the “New branch” button on the issue page. Commit any work in progress to that branch. When done, create a merge request; successful merge request will automatically close the issue
- For non-code problems, add comments about the activity related to the issue. Close the issue manually when you’re done
- Make commits and comments as often as you’d like, but try not going dark for days
 