module BrowsingHistoryUtils
  def delete_browsing_history(item)
    home = Home.new
    browsing_history = home.goto_browsing_history
    browsing_history.delete_browsing_history(item)
    # browsing_history.tap_delete_bookmark_button
    sleep(2) # wait for the spinner to disappear
    # confirm item is no longer displayed
    expect(browsing_history.item_label(item.item_name)).not_to be_visible
  end
end
