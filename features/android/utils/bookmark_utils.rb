module BookmarkUtils
  def delete_bookmark(item)
    home = Home.new
    bookmarks = home.goto_bookmarks
    bookmarks.select_bookmark(item)
    bookmarks.tap_delete_bookmark_button
    sleep(2) # need to do function to wait for the spinner to disappear
    # confirm item is no longer displayed
    expect(bookmarks.item_label(item.item_name)).not_to be_visible
  end
end
