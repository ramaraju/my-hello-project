module SearchUtils
  def search(item)
    @home = Home.new
    @home.tap_search_box
    @home.enter_search_box(item.cpc_search_keyword.nil? ? item.item_name : item.cpc_search_keyword)
    @home.tap_search_button
  end
end
