require 'calabash-android/operations'
require 'calabash-android/abase'
# require 'docsplit'

module IchibaApp
  module AndroidHelpers
    include ::Calabash::Android::Gestures

    def flick_with_params(uiquery, from, to, options = {})
      10.times {
        break if View.new(uiquery).exist?
        sleep(0.5)
      }
      gesture = Gesture.with_parameters(Gesture.generate_swipe(from, to, {flick: true}.merge(options)), {query_string: uiquery}.merge(options))
      calabash.execute_gesture(gesture)
    end

    def wait_progress_bar_gone(time = 90)
      sleep(0.5)
      TextView.new(id: 'message').wait_gone(timeout: time) {
        Button.new(text: '続行').tap_if_exist
        break if Button.new(text: 'アプリの評価をする').exist?
      }
      ProgressBar.new(id: 'ptr_progress').wait_gone(timeout: time) {
        Button.new(text: '続行').tap_if_exist
      }
      Button.new(text: '続行').wait_gone(timeout: time) {
        Button.new(text: '続行').tap
        sleep(3) unless Button.new(text: '続行').exist?
      }
    end

    def flick_and_find(temp_view, direction, maxflick = 100)
      iterator = 0
      until temp_view.exist? || iterator > maxflick
        flick_with_params("* id:'content'", {x: 50, y: 60}, {x: 50, y: 40}) if direction == :up
        flick_with_params("* id:'content'", {x: 50, y: 40}, {x: 50, y: 60}) if direction == :down
        iterator += 1
      end
      temp_view.exist?
    end

    def flick_up_and_tap(temp_view, maxflick = 100)
      flick_and_find(temp_view, :up, maxflick)
      # wait for animations
      sleep(1)
      temp_view.tap
    end

    def list_items(uiquery, direction, max_flick: 50)
      item_list = []
      if direction == :up
        y1, y2, mark = 90, 50, :last
      elsif direction == :down
        y1, y2, mark = 50, 90, :first
      end
      max_flick.times {
        items = calabash.query(uiquery.to_s, :text)
        break if item_list.include?(items.send(mark))
        item_list |= items
        flick_with_params("* id:'content'", {x: 50, y: y1}, {x: 50, y: y2})
      }
      item_list
    end

    def enter_text_simulate_keyboard(input_str)
      wait_for_keyboard
      input_str.each_char { |c| perform_action('keyboard_enter_text', c) }
    end

    def weekdays
      {
        'Sunday' => '日',
        'Monday' => '月',
        'Tuesday' => '火',
        'Wednesday' => '水',
        'Thursday' => '木',
        'Friday' => '金',
        'Saturday' => '土'
      }
    end

    def assert_text_on_page_by_screenshot(textlist, pass_rate = 0.7)
      screenshot_file = calabash.screenshot({prefix: 'temp', name: ''})[0..-5]
      sleep(1)
      system('convert ' + screenshot_file + '.png ' + screenshot_file + '.pdf')
      sleep(1)
      Docsplit.extract_text([screenshot_file + '.pdf'])
      length_before_deletion = textlist.length
      temp_list = textlist

      File.open(screenshot_file + '.txt', 'r') do |file|
        while (line = file.gets)
          temp_list.each do |text|
            if line.include?text
              temp_list.delete(text)
              print text, "  FOUND!!! \n"
            end
          end
        end
      end

      # File.delete(screenshot_file + '.png', screenshot_file + '.pdf', screenshot_file + '.txt' )
      fail 'text not included on page' unless temp_list.length <= length_before_deletion * (1 - pass_rate)
    end

    def scroll_to(element, direction = :up)
      case direction
      when :up
        y1, y2 = 70, 30
      when :down
        y1, y2 = 30, 70
      end
      send(element).wait {
        flick_with_params('*', {x: 50, y: y1}, {x: 50, y: y2})
      }
    end

    def get_element_arg(element, symbol)
      element =
        if element.is_a?(Hash)
          send(element[:method], *element.values[1..-1])
        else
          send(element).is_a?(Query) ? View.new(send(element)) : send(element)
        end
      element.wait {
        flick_with_params('*', {x: 50, y: 70}, {x: 50, y: 30})
      }
      calabash.query(element.query.to_s, symbol).first
    end

    def view_all(view)
      view.class.all(view.query.to_s)
    end
  end
end

World(IchibaApp::AndroidHelpers)
