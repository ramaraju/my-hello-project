@android @function @no_reinstall
Feature: Point

  # C1866902-C1866911
  @non_login
  Scenario Outline: Point campaign (1)
    Given Point1a(<item>)

    Examples:
      | item |
      | 通常商品2(point) before |
      | 通常商品2(point) during |
      | 通常商品3(point) before |
      | 通常商品3(point) during |
      | 通常商品3(point) after|
      | 通常商品1(point) before |
      | 通常商品1(point) after |

  @user
  Scenario Outline: Point campaign (2)
    Given Point1b(<item>)

    Examples:
      | item |
      | 通常商品2(point) after |
      | 通常商品1(point) during |

  # C9194285-C9194286
  @user
  Scenario: Verify page transfer of rakuten point card page
    Given Point2
