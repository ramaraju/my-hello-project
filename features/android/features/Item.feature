# C1855782-C1855795 @no_reinstall
# Item can be displayed and selected in Home main screen (bookmarks)
@android @function @no_reinstall @wip
Feature: Item display

  @user
  Scenario Outline: Item can be displayed and selected in Home main screen (bookmarks)
    Given Item1(<item>)

    Examples:
      | item |
      | 通常商品(項目選択肢別がない) 31 |
      | 通常商品(項目選択肢別があり) 32 |
      | 通常商品(期間限定商品) 33 |
      | 通常商品(あす楽商品) 34 |
      | 通常商品(酒類) 35 |
      | 通常商品(藥類) 36 |
      | CPC_定期購入商品(通常ジャンル) 1 |
      | CPC_頒布会商品(通常ジャンル) 2 |
      | CPC_予約商品(通常ジャンル) 3 |
      | CPC_通常＆定期購入商品(通常ジャンル) 4 |
      | CPC_定期購入商品(酒/薬事ジャンル) 5 |
      | CPC_頒布会商品(酒/薬事ジャンル) 6 |
      | CPC_予約商品(酒/薬事ジャンル) 7 |
      | CPC_通常＆定期購入商品(酒/薬事ジャンル) 8 |

  # C1855799-C1855812
  # Item can be displayed and selected in Home main screen (browsing history)
  @user
  Scenario Outline: Item can be displayed and selected in home browsing history (browsing history)
    Given Item2(<item>)

    Examples:
      | item |
      | 通常商品(項目選択肢別がない) 31 |
      | 通常商品(項目選択肢別があり) 32 |
      | 通常商品(期間限定商品) 33 |
      | 通常商品(あす楽商品) 34 |
      | 通常商品(酒類) 35 |
      | 通常商品(藥類) 36 |
      | CPC_定期購入商品(通常ジャンル) 1 |
      | CPC_頒布会商品(通常ジャンル) 2 |
      | CPC_予約商品(通常ジャンル) 3 |
      | CPC_通常＆定期購入商品(通常ジャンル) 4 |
      | CPC_定期購入商品(酒/薬事ジャンル) 5 |
      | CPC_頒布会商品(酒/薬事ジャンル) 6 |
      | CPC_予約商品(酒/薬事ジャンル) 7 |
      | CPC_通常＆定期購入商品(酒/薬事ジャンル) 8 |

  # C1866897
  @non_login
  Scenario: Verify item with link in item description
    Given Item3

  # C9194182 ~ C9194185
  @non_login
  Scenario: Verify default value of item option select and checkbox control on item details page.
    Given Item4

  # C9194186 ~ C9194187
  @user
  Scenario: Verify purchase without selecting item options
    Given Item5

  # C9194188 ~ C9194190
  @non_login
  Scenario: Verify display of special characters in selecting item options on cart page
    Given Item6
