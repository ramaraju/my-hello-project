@android @function @no_reinstall
Feature: Review

  Scenario: Number of reviews 3 or less
    Given Review1

  Scenario: Number of reviews more than 3
    Given Review2

  Scenario Outline: Filter item reviews by different conditions
    Given ReviewFilterItem(<filter>)

    Examples:
      | filter |
      | Star |
      | Reviewer |
      | Age |
      | Gender |
      | Image |
      | Sort |
      | Combined |

  Scenario: Filter by average rating
    Given Review10

  Scenario Outline: Filter shop reviews by different conditions
    Given ReviewFilterShop(<filter>)

    Examples:
      | filter |
      | Reviewer |
      | Age |
      | Gender |
      | Combined |

  Scenario: Tap navigation button
    Given Review16
