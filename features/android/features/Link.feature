@android @function @no_reinstall
Feature: Link

  # C1867059 C1867060
  @non_login
  Scenario: Help in setting
    Given Link1

  # C1866912 ~ C1866914
  @non_login
  Scenario: Verify links on item details screen
    Given Link2

  # C1867076 C1867078
  @non_login
  Scenario: Home Login link
    Given Link3

  # C1867074 C1867075 C1867077
  @user
  Scenario: Step1 login link (1)
    Given Link4

  # C1867074 C1867075 C1867077
  @user
  Scenario: Step1 login link (2)
    Given Link5

  # C1867090 ~ C1867097
  @non_login
  Scenario: Verity links on Help page
    Given Link7

  # C1867102
  @non_login
  Scenario: Verity page share+ comments on Help page
    Given Link8
