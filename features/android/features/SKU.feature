@android @function @no_reinstall

Feature: SKU & Options

  Scenario Outline: Check sku of item with different options.
    Given SkuWithDifferentOption(<item>)

    Examples:
    | item |
    | 通常商品(項目選択肢別があり) 56 |
    | 通常商品(項目選択肢別があり) 57 |
    | 通常商品(項目選択肢別があり) 58 |
    | 通常商品(項目選択肢別があり) 59 |
    | 通常商品(項目選択肢別があり) 60 |
    | 通常商品(項目選択肢別があり) 61 |

  Scenario Outline: Check options for select & checkbox.
    Given Options(<item>, <type>)

    Examples:
    | item | type |
    | 通常商品(項目選択肢別がない) 62 | select |
    | 通常商品(項目選択肢別がない) 63 | checkbox |