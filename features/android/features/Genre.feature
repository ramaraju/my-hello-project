@android @function @no_reinstall @non_login
Feature: Genre

  # C1855774, C1855775
  # Tab [ジャンル]tab in Home page
  # Confirm genre list can be shown
  # Select genre with [<] or [>]
  # Tap [ジャンル]tab in Search Result Screen
  # Tap [検索結果画面へ戻る] button
  # Tap [ジャンルを指定しない] button
  # Function 1 & 2
  Scenario: Genre tab in Home page can be selected
    Given Genre1

  # C1855776
  # cpc in genre
  Scenario: Verify CPC item can be displayed by select genre
    Given Genre2

  # C1855777
  # suggest list search
  # confirm transform page
  Scenario: Verify genre can be shown in search suggest data
    Given Genre3

  # C1855778
  # Search keyword in search box
  # Click "+" button
  # Pree the search button
  Scenario: Verify genre can be shown in search suggest data
    Given Genre4

  # C1855829 ~ C1855832
  Scenario: Verify the genre of level 1~5 and genre search result can be displayed
    Given Genre5

  # C1866872 ~ C1866876
  Scenario: Verify the genre of all levels
    Given Genre6

  # C1866872 ~ C1866876
  Scenario: Verify the genre ad
    Given Genre7
