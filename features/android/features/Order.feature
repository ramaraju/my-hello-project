@android @function @no_reinstall @user
Feature: Order

  # C1866925
  # Add cart without select option
  Scenario: Add cart without select option
    Given Order1

  # C1866929 C1866935 C1866941 C1866947 C1866953 C1866959 C1866965 C1866971 C1866977 C1866983 C1866985 C1866989
  # purchase completely
  Scenario Outline: Regular item can be purchased (1)
    Given Order2(<item>)

    Examples:
      | item |
      | 通常商品(項目選択肢別がない) 31 |
      | 通常商品(項目選択肢別があり) 32 |
      | 通常商品(期間限定商品) 33 |
      | 通常商品(あす楽商品) 34 |

  Scenario Outline: Regular item can be purchased (2)
    Given Order6(<item>)

    Examples:
      | item |
      | 通常商品(酒類) 35 |

  Scenario Outline: Regular item can be purchased (3)
    Given Order7(<item>)

    Examples:
      | item |
      | 通常商品(藥類) 36 |

  Scenario Outline: Regular item can be purchased (4)
    Given Order8(<item>)

    Examples:
      | item |
      | CPC_定期購入商品(通常ジャンル) temp1|

  Scenario Outline: Bidding item can be purchased
    Given Order9(<item>)

    Examples:
      | item |
      | CPC_頒布会商品(通常ジャンル) temp2 |

  Scenario Outline: Reserved item can be purchased
    Given Order10(<item>)

    Examples:
      | item |
      | CPC_予約商品(通常ジャンル) temp3 |

  Scenario Outline: Regular CPC item can be purchased
    Given Order11(<item>)

    Examples:
      | item |
      | CPC_通常＆定期購入商品(通常ジャンル) temp4 |

  Scenario Outline: Time wine item can be purchased
    Given Order12(<item>)

    Examples:
      | item |
      | CPC_定期購入商品(酒/薬事ジャンル) temp5 |

  Scenario Outline: Bidding wine item can be purchased
    Given Order13(<item>)

    Examples:
      | item |
      | CPC_頒布会商品(酒/薬事ジャンル) temp6 |

  Scenario Outline: Reserved wine item can be purchased
    Given Order14(<item>)

    Examples:
      | item |
      | CPC_予約商品(酒/薬事ジャンル) temp7 |

  Scenario Outline: Regular CPC wine item can be purchased
    Given Order15(<item>)
    Examples:
      | item |
      | CPC_通常＆定期購入商品(酒/薬事ジャンル) temp8 |

  # C1866895,C1866924,C1867025,C1867026
  # Check cart function check when ordering items
  Scenario: Check cart function when ordering items
    Given Order16

  # C1867027, C1867033, C1867035, C1867039,C1867046,C1867051, C1867055, C1867058
  # Order one regular item
  Scenario: Order one regular item (1)
    Given Order17
      |	when    | 商品（税込）| 送料 	| 合計金額 	| ポイント 	| お支払い方法 | 配送方法  |
      |	after	| 999円 	| 100円 	| 1,199円 	| 9ポイント 	| 代金引換    | 宅配便 |

  # C1867027, C1867033, C1867035, C1867039,C1867046,C1867051, C1867055, C1867058
  # Order one regular item
  Scenario: Order two regular items
    Given Order18
      |	when    | 商品（税込）| 送料 	| 合計金額 	| ポイント 	| お支払い方法        | 配送方法 |
      |	before	| 2,998円 	| 100円 	| 3,098円 	| 28ポイント 	| 代金引換           | 宅配便   |
      |	after	| 2,998円 	| 100円 	| 3,098円 	| 28ポイント 	| クレジットカード決済 | 宅配便   |

  # C1867029, C1867037, C1867038, C1867040, C1867048, C1867049, C1867052
  Scenario: Order one regular item (2)
    Given Order19
      |	when    | 商品（税込）| 送料 	| 合計金額 	| ポイント 	| お支払い方法        | 配送方法 |
      |	before	| 999円 	    | 100円 	| 1,099円 	| 9ポイント 	| クレジットカード決済 | 宅配便   |
      |	after	| 999円 	    | 100円 	| 1,099円  	| 9ポイント 	| クレジットカード決済 | 宅配便   |

  # C1867030, C1867041, C1867042, C1867043, C1867044, C1867045
  Scenario: Order one regular item (3)
    Given Order20
      |	when    | 商品（税込）| 送料 	| ポイント利用 | 合計金額 	| ポイント 	| お支払い方法        | 配送方法 |
      |	before	| 999円 	    | 100円 	| 利用なし    | 1,099円 	| 9ポイント 	| クレジットカード決済 | 宅配便   |
      |	after	| 999円 	    | 100円 	| -10円      | 1,189円 	| 9ポイント 	| 代金引換  | 宅配便   |

  # C1867061 ~ C1867067
  Scenario: Purchase history check
    Given Order21
