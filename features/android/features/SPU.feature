@android @function @no_reinstall

Feature: SPUx

  # [C9194191,C9194194,C9194197,C9194200,C9194203,C9194206,C9194209,C9194212,C9194215,C9194218,C9194221,C9194224,C9194227,C9194230,C9194233,C9194236,C9194239,C9194242,C9194245,C9194248]
  @non_login
  Scenario Outline: Not logged in & price of item = 100
    Given SPU1(<item_name>)

    Examples:
    | item_name                    |
    | SPUx通常商品(在庫なし)1        |
    | SPUx通常商品(項目選択肢別がない)1|
    | SPUx通常商品(項目選択肢別があり)1|
    | SPUx通常商品(期間限定商品)1     |
    | SPUx通常商品(あす楽商品)1       |
    | SPUx通常商品(酒類)1            |
    | SPUx通常商品(藥類)1            |
    | SPUx資料請求のみ1              |
    | SPUx通常商品(商品変倍)1        |
    | SPUx通常商品(店舗変倍)1        |
    | SPUx通常商品(問い合わせ)1      |
    | SPUxRakutenMart1             |
    | SPUxBooks1                   |
    | SPUxKobo1                    |
    | CPC_予約商品                  |
    | CPC_定期購入商品              |
    | CPC_頒布会商品                |

  # [C9194192,C9194195,C9194198,C9194201,C9194204,C9194207,C9194210,C9194213,C9194216,C9194219,C9194222,C9194225,C9194228,C9194231,C9194234,C9194237,C9194240,C9194243,C9194246,C9194249]
  @user
  Scenario Outline: Logged in & price of item = 99
    Given SPU2(<item_name>)

    Examples:
    | item_name                    |
    | SPUx通常商品(在庫なし)2        |
    | SPUx通常商品(項目選択肢別がない)2|
    | SPUx通常商品(項目選択肢別があり)2|
    | SPUx通常商品(期間限定商品)2     |
    | SPUx通常商品(あす楽商品)2       |
    | SPUx通常商品(酒類)2            |
    | SPUx通常商品(藥類)2            |
    | SPUx資料請求のみ2              |
    | SPUx通常商品(商品変倍)2        |
    | SPUx通常商品(店舗変倍)2        |
    | SPUx通常商品(問い合わせ)2      |
    | SPUxRakutenMart2             |
    | SPUxBooks2                   |
    | SPUxKobo2                    |
    | CPC_定期購入商品_less_100     |
    | CPC_頒布会商品_less_100       |

  # [C9194193,C9194196,C9194199,C9194202,C9194205,C9194208,C9194211,C9194214,C9194217,C9194220,C9194223,C9194226,C9194229,C9194232,C9194235,C9194238,C9194241,C9194244,C9194247,C9194250]
  @user
  Scenario Outline: Logged in & price of item = 100
    Given SPU3(<item_name>)

    Examples:
    | item_name                    |
    | SPUx通常商品(在庫なし)1         |
    | SPUx通常商品(項目選択肢別がない)1|
    | SPUx通常商品(項目選択肢別があり)1|
    | SPUx通常商品(期間限定商品)1     |
    | SPUx通常商品(あす楽商品)1       |
    | SPUx通常商品(酒類)1            |
    | SPUx通常商品(藥類)1            |
    | SPUx資料請求のみ1              |
    | SPUx通常商品(商品変倍)1        |
    | SPUx通常商品(店舗変倍)1        |
    | SPUx通常商品(問い合わせ)1      |
    | SPUxRakutenMart1             |
    | SPUxBooks1                   |
    | SPUxKobo1                    |
    | CPC_予約商品                  |
    | CPC_定期購入商品               |
    | CPC_頒布会商品                |

  @user
  Scenario: Check special item spu
    Given SPUSpecialItem

  @user
  Scenario: SPU (4)
    Given SPU4

  @user
  Scenario: SPU (5)
    Given SPU5

  @user
  Scenario: SPU (6)
    Given SPU6

  @user
  Scenario: Check recommendation item spu
    Given SPU7
