@android @function @no_reinstall @non_login
Feature: Search

  # C1855876, C1855877, C1855878
  Scenario: Verify search suggestion
    Given Search1

  # C1855881 ~ C1855886
  # no history display when no search performed
  # check history after search
  # search by select history and check display
  Scenario: Search history check and search by selecting history
    Given Search2

  # C1855888
  # Lowest price shop display
  Scenario: Lowest price shop display
    Given Search3

  # C1855890,C1855891,C1855892
  # search result  can be displayed as list mode,grid (middle) mode and grid (large) mode
  Scenario: Search results display in different modes
    Given Search4

  # C1855899,C1855901,C1855903
  # Search detail search error check on search result and searh detail page
  Scenario: Search detail search error check
    Given Search5

  # C1855904
  # Verify previous search condition can not be saved after searching by new search condition
  Scenario: Search detail condition can be saved
    Given Search6

  # C1855905 ~ C1855900 & C1855911 ~ C1855912
  Scenario: Verify search by genre and tag rules
    Given Search7

  # C1855913 ~ C1855915
  Scenario: Verify search by price range+ price range maximum is 999,999,999 + error message if input wrong price range
    Given Search8

  # C1855916 ~ C1855917 & C1866863
  Scenario: Verify functionality of search by except keyword
    Given Search9

  # C1855919 ~ C1855921 & C1866857
  Scenario: Verify functionality of search by あす楽 area keywords
    Given Search10

  # C1855918 & C1866855 & C1866856
  Scenario: Verify functionality of search by shop keywords
    Given Search11

  # C1855923 & C1855924
  Scenario: Verify functionality of search by availability keywords
    Given Search12

  # C1855925 & C1855926
  Scenario: Verify functionality of search by shipping fee keywords
    Given Search13

  # C1855927 & C1855928
  Scenario: Verify functionality of search by card keywords
    Given Search14

  # C1855929 & C1855930
  Scenario: Verify functionality of search by sort keywords
    Given Search15

  # C1855931
  Scenario: Verify functionality of search clear
    Given Search16

  # C1855913 ~ C1855915
  Scenario: Search about cpc
    Given Search17

  # C1866847 ~ C1866849
  Scenario: Search result not crash in scrolling and screen transfer
    Given Search18

  # C1866850 ~ C1866851
  Scenario: Range of item on search result screen
    Given Search19