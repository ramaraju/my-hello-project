@android @function @no_reinstall @non_login
Feature: Detail Search Tab

  # C10046648 ~ C10046650
  Scenario: Verify when no search keywords or empty search result
    Given DetailSearchTab01

  # C10046651 ~ C10046652
  Scenario: Verify search by genre and saving search condition
    Given DetailSearchTab02

  # C10046653 ~ C10046655
  Scenario: Verify search by price
    Given DetailSearchTab03

  # C10046656 ~ C10046657 & C10046670
  Scenario: Verify search by except keyword
    Given DetailSearchTab04

  # C10046658
  Scenario: Verify search by shop
    Given DetailSearchTab05

#  # C1872274
#  Scenario Outline: Data preparation for DetailSearchTab06
#    Given DetailSearchTab06Data(<genre_name>,<total>)
#    Examples:
#      |	genre_name	                  | total   |
#      |CD・DVD・楽器	                  | 28	    |
#  @no_reinstall_no_clear
#    Examples:
#      |	genre_name	                  | total   |
#      |インテリア・寝具・収納        |  56	    |
#  @no_reinstall_no_clear
#    Examples:
#      |	genre_name	                  | total   |
#      |おもちゃ・ホビー・ゲーム      | 76 	    |
#  @no_reinstall_no_clear
#    Examples:
#      |	genre_name	                  | total   |
#      |ダイエット・健康              |  92	    |
#  @no_reinstall_no_clear
#    Examples:
#      |	genre_name	                  | total   |
#      |スポーツ・アウトドア	          |  92 	|
#  @no_reinstall_no_clear
#    Examples:
#      |	genre_name	                  | total   |
#      |日用品雑貨・文房具・手芸       |  92	    |
#
#  @no_reinstall_no_clear
#  # C10046659
#  Scenario: Verify search by shop(large number of shops)
#    Given DetailSearchTab06

  # C10046660 ~ C10046663
  Scenario: Verify search by area
    Given DetailSearchTab07

  # C10046664 ~ C10046669
  Scenario: Verify search by availability/card payment/postage issues
    Given DetailSearchTab08
