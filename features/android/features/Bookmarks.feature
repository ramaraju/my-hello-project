@android @function @no_reinstall
Feature: Bookmarks

  # C1855779 & C1855780
  # Tab [お気に入り] Tab in Home page
  # Bookmark banner can not be displayed in Home main screen when non-login
  @non_login
  Scenario: [お気に入り] Tab in Home page can be selected
    Given Bookmarks1

  # C1855781
  # Check the items in bookmark banner
  @user
  Scenario: The number of items in Home main screen is the same for Bookmark tab screen
    Given Bookmarks3

  # C1855835 & C1855836
  # Verify the bookmark display from Navigation Drawer
  @user
  Scenario: Verify the bookmark display from Navigation Drawer and Home screen
    Given Bookmarks4

  # C1855837 & C1855840
  # Verify the bookmark display from Home screen and Navigation Drawer
  @user
  Scenario: Verify the bookmark display from Home screen and Navigation Drawer when bookmark item number is 0
    Given Bookmarks5a

  # C1855838 & C1855841
  # Verify the bookmark display from Home screen and Navigation Drawer
  @user
  Scenario: Verify the bookmark display from Home screen and Navigation Drawer when bookmark item number is more than 50
    Given Bookmarks5b

  # C1855839 & C1855842
  # Verify the bookmark display from Home screen and Navigation Drawer
  @user
  Scenario: Verify bookmark display from Home screen & Navigation Drawer after deleting all bookmark items
    Given Bookmarks5c

  # C1855843 & C1855844
  @user
  Scenario: Verify order of bookmark items can be changed (Home screen bookmark tab & Navigation Drawer bookmark)
    Given Bookmarks6

  # C1855845 ~ C1855848
  @user
  Scenario: Verify bookmark on order banner and grid change
    Given Bookmarks7

  # C1855849 ~ C1855864,C1866896
  @user
  Scenario: Verify different items can be displayed in bookmark
    Given Bookmarks8
      | 通常商品(項目選択肢別がない) 31 |
      | 通常商品(項目選択肢別があり) 32 |
      | 通常商品(期間限定商品) 33 |
      | 通常商品(あす楽商品) 34 |
      | 通常商品(酒類) 35 |
      | 通常商品(藥類) 36 |
      | 通常商品(在庫なし) 37 |
      | CPC_定期購入商品(通常ジャンル) 1 |
      | CPC_頒布会商品(通常ジャンル) 2 |
      | CPC_予約商品(通常ジャンル) 3 |
      | CPC_通常＆定期購入商品(通常ジャンル) 4 |
      | CPC_定期購入商品(酒/薬事ジャンル) 5 |
      | CPC_頒布会商品(酒/薬事ジャンル) 6 |
      | CPC_予約商品(酒/薬事ジャンル) 7 |
      | CPC_通常＆定期購入商品(酒/薬事ジャンル) 8 |
