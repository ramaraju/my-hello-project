@android @function @no_reinstall
Feature: Browsing history

  # C1855796
  # Tab [閲覧履歴] tab in navigation drawer
  @user
  Scenario: Browsing history tab in navigation drawer can be selected
    Given BrowsingHistory1

  # C1855797
  # Check the Browsing history banner in Home main screen
  @non_login
  Scenario: Browsing history banner can not be displayed in Home main screen when not logged in
    Given BrowsingHistory2

  # C1855798
  # browsing history in home limit is 50
  @user
  Scenario: Verify the number of items in Home main screen is the same for browsing history tab screen
    Given BrowsingHistory3

  # C1855869, C1855870
  @user
  Scenario: Verify the transition when tapping regular item in browsing history
    Given BrowsingHistory4
