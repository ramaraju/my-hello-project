@android @function @no_reinstall @non_login
Feature: Search History

  # C1855871,C1855872,C1855873, C1855874, C1855875, C1866864
  # Search history check
  Scenario: Verify search history display on various conditions
    Given SearchHistory1

  # C1866860,C1866859,C1866852,C1866853,C1866854,C1866858,C1866860,C1866862
  # Search by same search condition two times
  Scenario: Only one search history can be displayed
    Given SearchHistory2a

  # C1866861
  Scenario: search condition of search history can be remained
    Given SearchHistory2b

  # C9194181
  # search history display with 90
  Scenario: Search history is 90 (1)
    Given SearchHistory31

  @no_reinstall_no_clear
  Scenario: Search history is 90 (2)
    Given SearchHistory32

  @no_reinstall_no_clear
  Scenario: Search history is 90 (3)
    Given SearchHistory33

  @no_reinstall_no_clear
  Scenario: Search history is 90
    Given SearchHistory3
