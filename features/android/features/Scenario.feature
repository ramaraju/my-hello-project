# @android @function @no_reinstall
Feature: Scenario

  # C1872274
  Scenario: Order one regular item (1)
    Given ScenarioP00101
      |	flg	    | 商品（税込）| 送料 	| 合計金額 	| ポイント 	| お支払い方法        | 配送方法 |
      |	before	| 999円 	    | 0円 	| 999円  	| 9ポイント 	| クレジットカード決済 | 宅配便   |
      |	after	| 999円 	    | 0円 	| 999円    	| 9ポイント 	| クレジットカード決済 | 宅配便   |

  # C1872275
  Scenario: Order one regular item (2)
    Given ScenarioP00102
      |	when    | 商品（税込）| 送料 	|ポイント利用| 合計金額 	| ポイント 	| お支払い方法 | 配送方法 |
      |	before	| 1,080円   | 100円 	| 利用なし   | 1,180円 	| 10ポイント 	| 代金引換    | 宅配便   |
      |	before	| 1,080円   | 100円 	| -20円     | 1,160円 	| 10ポイント 	| 代金引換    | 宅配便   |

  # C1872276
  Scenario: Order one regular item (3)
    Given ScenarioP00103
      |	when    | 商品（税込）| 送料 	| 代引料 | 合計金額 	| ポイント 	| お支払い方法 | 配送方法 |
      |	before	| 999円     | 50円 	| 100円  | 1,149円 	| 9ポイント 	| 代金引換    | 自社配送  |

  # C1872277
  Scenario: Order one regular item (4)
    Given ScenarioP00104
      |	when    | 商品（税込）| 送料 	|ポイント利用| 合計金額 	| ポイント 	| お支払い方法  | 配送方法  |
      |	before	| 1,080円   | ---円 	| 利用なし   | 1,180円 	| 10ポイント 	| 代金引換     | 宅配便    |
      |	after	| 1,080円   | ---円 	| -20円     | 1,160円  	| 10ポイント 	| 代金引換     | 宅配便    |

  # C1872279
  Scenario: Order one time-sale item
    Given ScenarioP00106
      |	when    | 商品（税込）| 送料 	| 代引料 | 合計金額 	| ポイント 	| お支払い方法  | 配送方法  |
      |	before	| 1,080円   | 50円 	| 100円  | 1,230円 	| 10ポイント 	| 代金引換     | 宅配便    |

  # C1872280
  Scenario: Order two time-sale item
    Given ScenarioP00107
      |	when    | 商品（税込）| 送料 	| 合計金額 	| ポイント 	| お支払い方法        | 配送方法  |
      |	before	| 1,998円   | 0円 	| 1,998円  	| 19ポイント 	| クレジットカード決済 | 自社配送  |
      |	after	| 1,998円   | 0円 	| 1,998円  	| 19ポイント 	| クレジットカード決済 | 自社配送  |

  # C1872308
  Scenario: Step3 review dialog exists, and tap [アプリの評価をする]
    Given ScenarioP00401

  # C1872309
  Scenario: Step3 review dialog exist, and tap [後でする]
    Given ScenarioP00402

  # C1872310
  @no_reinstall_no_clear
  Scenario: Step3 review dialog does not exist
    Given ScenarioP00403

  # C1872314
  Scenario: Step3 review dialog exists, and tap area out of dialog
    Given ScenarioP00407

  # C1872315
  Scenario: Step3 review dialog exists, and tap device back button
    Given ScenarioP00408
