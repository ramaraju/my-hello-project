@android @function @no_reinstall
Feature: Home

  @user
  Scenario: Verify login status can be kept after restarting app & login in rakuten member service after logout & information on member service/my rakuten/pointclub page can be shown & barcode camera can start
    Given Home1

  @user
  Scenario: Verify userinformation from navigation
    Given Home2

  @user
  Scenario: Verify login
    Given Home3

  @non_login
  Scenario: Verify 注目のイベント
    Given Home4

  @non_login
  Scenario: Verify home tab
    Given Home5
