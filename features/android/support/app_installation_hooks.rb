require 'calabash-android/management/app_installation'

class Environment
  class << self
    attr_accessor :production
    alias_method :production?, :production
  end
end

AfterConfiguration do |_config|
  FeatureMemory.feature = nil
end

Before('@no_reinstall') do
  @no_reinstall = true
end

Before('@no_reinstall_no_clear') do
  @no_reinstall_no_clear = true
end

Before('@prod') do
  Environment.production = true
end

Before do |scenario|
  scenario = scenario.scenario_outline if scenario.respond_to?(:scenario_outline)
  feature = scenario.feature
  if @no_reinstall_no_clear
    # to_modify the other if conditions so that DO NOTHING is the default behavior
  elsif FeatureMemory.feature != feature || !@no_reinstall
    reinstall_apps
    clear_app_data
    FeatureMemory.feature = feature
    FeatureMemory.invocation = 1
  else
    clear_app_data
    FeatureMemory.invocation += 1
  end
end

FeatureMemory = Struct.new(:feature, :invocation).new
