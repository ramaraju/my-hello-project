class TestCase
  def setup
    App.dismiss_alerts
    set_stage
    App.logout
  end
  def set_stage
    prefs = Bridge::Package('jp.co.rakuten.android')::Class('App').sInstance.getSharedPreferences('MockedServiceImpl', 0)
    prefs.edit.putBoolean('enabledState', true).commit!
    prefs.edit.putString('environment', 'STAGING').commit!
    prefs.edit.putString('appconfig_url', 'asset://xml/ichiba_app_info_config.xml').commit!
    calabash.start_test_server_in_background
  end
end
