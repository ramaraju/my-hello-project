require_relative 'header'
require_relative 'genre'
require_relative '../utils/utils_android'
require 'rspec/expectations'
require 'rspec-benchmark'

class TabsSearch < Page
  include IchibaApp::AndroidHelpers, RSpec::Benchmark::Matchers, RSpec::Matchers
  attr_reader :search_button, :error_message, :header, :keyword, :ngkeyword, :selected_genre_title, :clear_search_button, :genre_unselected, :starting_price, :end_price, :price_range_error,
              :shop_unselected, :remove_shop, :other_search_filter, :shop_select, :area_select, :remove_area, :error_no_search_conditions, :ok_button, :back_button_on_shop_list,
              :home_tab

  def initialize
    super('android.support.v4.view.ViewPager', getCurrentItem: 1)

    @header = Header.new(Home)
    @genre = Genre.new
    @home_tab = TextView.new(id: 'tv_icon_title', text: 'ホーム')

    @list = ListView.new
    @list_item = TextView.new(id: 'ichiba_list_item_title')
    @other_search_filter = TextView.new(id: 'additional_menu_open_title')
    @keyword = EditText.new(id: 'search_refine_keyword')
    @ngkeyword = EditText.new(id: 'ngkeyword_edit')
    @genre_unselected = TextView.new(id: 'genre_unselected')
    @selected_genre_title = TextView.new(id: 'selected_genre_title')
    @starting_price = EditText.new(id: 'search_starting_price')
    @end_price = EditText.new(id: 'search_end_price')
    @shop_unselected = TextView.new(id: 'shop_unselected')
    @shop_select = TextView.new(id: 'selected_shop_title')
    @area_unselected = TextView.new(id: 'area_unselected')
    @area_select = TextView.new(id: 'selected_area_title')
    @availability_checkbox = CheckBox.new(id: 'availability_checkbox')
    @postage_checkbox = CheckBox.new(id: 'postage_checkbox')
    @credit_checkbox = CheckBox.new(id: 'credit_card_checkbox')
    @hybrid_checkbox = CheckBox.new(id: 'hybrid_search_checkbox')
    @search_button = Button.new(id: 'execute_search_button')
    @error_message = TextView.new(id: 'message')
    @remove_shop = Button.new(id: 'btn_selectNone', text: '指定を解除する')
    @remove_area = TextView.new(text: 'エリア指定を解除する')
    @error_no_search_conditions = TextView.new(text: 'キーワード、ジャンル、ショップいずれかの検索条件を指定してください。')
    @ok_button = Button.new(id: 'button1')
    @clear_search_button = Button.new(id: 'clear_search_button')
    @price_range_error = TextView.new(id: 'message', text: '価格帯の範囲指定に誤りがあります')
    @back_button_on_shop_list = ImageButton.new(Query.new(id: 'app_toolbar').descendant('android.widget.ImageButton'))
  end

  # search detail on home
  # rubocop:disable Metrics/MethodLength, Metrics/BlockLength, Metrics/CyclomaticComplexity, Metrics/PerceivedComplexity
  def make_detail_search(hash)
    @other_search_filter.tap_if_exist
    unless header.tabs.campaign.wait.rect.center.y < @keyword.rect.y
      flick_with_params("* id:'content'", {x: 1, y: 40}, {x: 1, y: 50})
    end
    hash.each do |filter, value|
      calabash.flick_down
      case filter
      when 'keyword'
        @keyword.wait.enter_text(value) unless value == 'unset'
      when 'ngkeyword'
        @ngkeyword.enter_text(value) unless value == 'unset'
      when 'genre'
        unless value == 'unset'
          @genre_unselected.tap
          genre_page = Genre.wait
          genre_page.select_genre(value)
        end
      when 'price_from'
        @starting_price.enter_text(value) unless value == 'unset'
      when 'price_to'
        @end_price.enter_text(value) unless value == 'unset'
      when 'shop'
        unless value == 'unset'
          @shop_unselected.tap
          sleep(1)
          CheckedTextView.new(id: 'text1', text: value).wait.tap
        end
      when 'area'
        unless value == 'unset'
          @area_unselected.tap
          area_view = TextView.all(id: 'ichiba_list_item_title', text: value)
          @list.scroll_to_view(area_view)
          calabash.wait_for_animations
          area_view.tap
        end
      when 'availability'
        unless value == 'unset'
          @availability_checkbox.wait do
            flick_with_params("* id:'content'", {x: 50, y: 30}, {x: 50, y: 10})
            sleep(0.5)
          end
          @availability_checkbox.tap unless @availability_checkbox.checked? == value
        end
      when 'postage'
        unless value == 'unset'
          @postage_checkbox.wait do
            flick_with_params("* id:'content'", {x: 50, y: 30}, {x: 50, y: 10})
            sleep(0.5)
          end
          @postage_checkbox.tap unless @postage_checkbox.checked? == value
        end
      when 'credit'
        unless value == 'unset'
          @credit_checkbox.wait do
            flick_with_params("* id:'content'", {x: 50, y: 30}, {x: 50, y: 10})
            sleep(0.5)
          end
          @credit_checkbox.tap unless @credit_checkbox.checked? == value
        end
      when 'hybrid'
        unless value == 'unset'
          @hybrid_checkbox.wait do
            flick_with_params("* id:'content'", {x: 50, y: 30}, {x: 50, y: 10})
            sleep(0.5)
          end
          @hybrid_checkbox unless @hybrid_checkbox.checked? == value
        end
      end
    end
    @search_button.wait {
      flick_with_params("* id:'content'", {x: 50, y: 30}, {x: 50, y: 10})
    }.tap
    SearchResult.wait
  end

  # confirm search detail conditions
  def assert_filter_value(hash)
    wait_progress_bar_gone
    @other_search_filter.tap_if_exist
    hash.each do |filter, value|
      case filter
      when 'keyword'
        expect(@keyword.text).to eq(value)
      when 'ngkeyword'
        expect(@ngkeyword.text).to eq(value)
      when 'genre'
        if value == 'unset'
          @genre_unselected.wait
        else
          expect(@selected_genre_title.text).to eq(value)
        end
      when 'shop'
        if value == 'unset'
          @shop_unselected.wait
        else
          expect(@shop_select.text).to eq(value)
        end
      when 'area'
        if value == 'unset'
          @area_unselected.wait
        else
          expect(@area_select.text).to eq(value)
        end
      when 'price_from'
        if value == 'unset'
          expect(@starting_price.text).to eq('')
        else
          expect(@starting_price.text).to eq(value)
        end
      when 'price_to'
        if value == 'unset'
          expect(@end_price.text).to eq('')
        else
          expect(@end_price.text).to eq(value)
        end
      when 'availability'
        expect(@availability_checkbox.wait.checked?).to eq(value)
      when 'postage'
        expect(@postage_checkbox.wait.checked?).to eq(value)
      when 'credit'
        expect(@credit_checkbox.wait.checked?).to eq(value)
      end
    end
  end
  # rubocop:enable all
  def make_empty_search
    @search_button.wait {
      flick_with_params("* id:'content'", {x: 50, y: 30}, {x: 50, y: 10})
    }.tap
  end

  def obtain_shops_in_list
    shop_list = []
    TextView.new(id: 'text1').wait
    shop_name_query = Query.new(id: 'text1')
    bottom_reached = false
    name_of_last_shop_on_page = ''
    until bottom_reached
      shops_on_current_page = shop_name_query.run
      shops_on_current_page.each do |shop|
        shop_list += [shop['text']] unless shop_list.include? shop['text']
      end
      if shops_on_current_page[-1]['text'].eql? name_of_last_shop_on_page
        bottom_reached = true
      else
        name_of_last_shop_on_page = shops_on_current_page[-1]['text']
      end
      flick_with_params('*', {x: 50, y: 30}, {x: 50, y: 10})
    end
    shop_list
  end

  def go_back_home
    home_tab.wait.tap
    Home.wait
  end
end
