require_relative 'header'
require_relative 'web_view/web_common'
require_relative '../utils/utils_android'

class Help < Page
  include IchibaApp::AndroidHelpers
  include RSpec::Matchers

  attr_reader :cart, :title_help, :title_information, :customer_comments
  def initialize
    super(id: 'TextView02', text: 'ヘルプ')
    @cart = TextView.new(id: 'tv_icon_title', text: '買い物かご')
    @title_help = TextView.new(id: 'TextView02', text: 'ヘルプ')
    @title_information = TextView.new(id: 'TextView01', text: 'アプリ情報')
    @back_button = ImageButton.new
    @application_share = TextView.new(id: 'helpText', text: 'アプリをシェアする')
    @label_of_complete_action = TextView.new(text: 'Complete action using')
    @customer_comments = TextView.new(text: 'ご意見・ご要望')
    @app_version = TextView.new(id: 'app_version')
  end

  def goto_webview_page(str)
    flick_up_and_tap(TextView.new(text: str))
    wait_progress_bar_gone
  end

  def assert_webview_page_reached(path_list)
    return if path_list.empty?
    path_list.each do |path|
      WebView.new(id: 'app_web', xpath: path).wait
    end
  end

  def back_to_help_page_from_webview
    @back_button.wait.tap
    TextView.new(text: '総合案内所').wait {
      flick_with_params("* id:'scrollView1'", {x: 40, y: 10}, {x: 40, y: 30})
    }
  end

  def verify_application_share_page_transfer
    flick_up_and_tap(@application_share)
    sleep(1)
    calabash.assert_text_on_page_by_screenshot(['Complete action using'])
  end

  def verify_web_page(page)
    wait_progress_bar_gone
    expect(WebCommon.send(page)).to exist
  end

  def verify_app_version
    @app_version.wait {
      flick_with_params("* id:'content'", {x: 2, y: 40}, {x: 2, y: 10})
    }
    fail 'app version incorrect' unless App.get_apk_file_name.include?@app_version.text
  end
end
