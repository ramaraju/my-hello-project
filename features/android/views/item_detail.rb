# coding: utf-8
require 'calabash-android/abase'
require_relative 'web_view/cart'
require_relative 'review'
require_relative 'header'
require_relative 'web_view/others'
require_relative '../utils/utils_android'

# rubocop:disable Metrics/ClassLength
class ItemDetail < Page
  include IchibaApp::AndroidHelpers, RSpec::Matchers

  attr_reader :progress_bar,
              :spu_point_nomal, :spu_point_rakuten_card, :spu_point_monthly_app,
              :spu_point_premium_card, :spu_point_rakuten_mobile, :spu_point_shop_point

  # SPUx popup
  linear_layout :home_menu_point_rate, id: 'home_menu_point_rate'
  text_view :home_menu_point_rate_counter, id: 'home_menu_point_rate_counter'
  frame_layout :point_rate_detail_contents, id: 'point_rate_detail_contents'
  text_view :about_the_item_point_o, text: '「今この商品はポイント○倍」について'
  text_view :point_get_history, text: 'ポイント利用獲得履歴'
  text_view :item_detail_review_count, id: 'item_detail_rating_count'
  image_view :banner_image, 'android.widget.ImageView', "{url CONTAINS 'banner'}", id: 'point_rate_image_banner'
  image_view :campaign_image, 'android.widget.ImageView', "{url CONTAINS 'campaign'}", id: 'point_rate_image_banner'
  image_view :close_spu, id: 'point_rate_close'
  relative_layout :item_detail_view_reviews, id: 'item_detail_view_reviews'
  linear_layout :inventory_candidates, id: 'inventory_candidates'
  text_view :selectable_inventory_title, id: 'selectable_inventory_title'
  text_view :all_options, id: 'all_options'
  text_view :in_stock_option, id: 'in_stock_option'
  text_view :sku_title, id: 'sku_title'
  text_view :sku_header, id: 'sku_header'
  text_view :option_title, id: 'option_title'
  text_view :alertTitle, id: 'alertTitle'

  def initialize(item = 'any_item')
    @item = item
    super('android.widget.LinearLayout', id: 'ichiba_list_item_container')

    # item detail
    @add_to_cart_button = TextView.new(id: 'add_to_cart_button')
    @add_bookmark = TextView.new(text: 'お気に入りに追加')
    @search_in_shop = TextView.new(id: 'shop_menu_in_shop_search', text: 'ショップ内から探す')
    @edit_text = EditText.new(id: 'search_src_text')
    @edit_units = EditText.new(id: 'edit_units')
    @show_all_item = TextView.new(id: 'ichiba_list_item_title', text: 'ショップの商品一覧')
    @add_shop = TextView.new(id: 'shop_menu_add_my_shop', text: '検索ショップリストに追加')
    @postage_flag = TextView.new(id: 'postage_flag')
    @creditcard_image = ImageView.new(id: 'credit_card_image')
    @shop_title = TextView.new(id: 'ichiba_list_item_title')
    @visit_pc_page = TextView.new(id: 'visit_pc_page')
    @home_menu_point_rate = LinearLayout.new(id: 'home_menu_point_rate')
    @shop_shipping_payment_method = TextView.new(id: 'ichiba_list_item_title', text: '会社概要・決済方法・配送方法')
    @shop_top_page = TextView.new(id: 'ichiba_list_item_title', text: 'ショップのトップページ')
    @cpc_details = TextView.new(id: 'item_page_button', text: '詳細を見る')
    @add_cart_cpc = WebView.new(id: 'app_web', xpath: '//section[7]/form/div/aside/p[3]/input[@type = "submit"]')
    @time_button = WebView.new(id: 'app_web', xpath: '//*[@id = "regularCart"]/input')
    @item_description_link = WebView.new(xpath: '//a[text()="商品説明文"]')

    @bidding_button = WebView.new(id: 'app_web', xpath: '//input[@id="hanpuCartBtn_ev2mo"]')
    @reserve_button = WebView.new(id: 'app_web', xpath: '//div/form/button[contains(.,"予約する")]')

    @item_num = EditText.new(id: 'edit_units')
    @progress_bar = ProgressBar.new(id: 'ptr_progress')

    @spinner_candidates = LinearLayout.new(id: 'spinner_candidates')
    @top_bar_container = LinearLayout.new(id: 'top_bar_container')

    @recommend_item_see_more = TextView.new(id: 'recommend_item_see_more', text: 'おすすめ商品一覧を見る')
    @card_title_view_title = TextView.new(id: 'card_title_view_title', text: '楽天市場のおすすめ商品')
    @recommend_item_item_title = TextView.new(id: 'recommend_item_item_title')
    @recommend_page_title = TextView.new(text: 'オススメ商品一覧')
    @recommend_page_search_condition = TextView.new(id: 'search_condition', text: 'この商品に関連するオススメ商品')
    @sku_radio_button = View.new('android.widget.RadioButton', id: 'sku_radio_button')
  end

  # confirm itemdetail screen information
  def validate
    TextView.new(id: 'item_title').wait {
      calabash.flick_with_params("* id:'content'", {x: 2, y: 40}, {x: 2, y: 60})
    }
    expect(View.new('TextView', "{text ENDSWITH '#{@item.item_name}'}")).to exist
  end

  def tap_details
    ItemDetail.new(@item).wait
  end

  # confirm put item into cart from itemdetail screen and go to cart screen
  def place_in_cart(item_style = nil)
    @add_to_cart_button.wait {
      flick_with_params("* id:'content'", {x: 2, y: 20}, {x: 2, y: 10})
    }.tap
    @add_to_cart_button.wait_gone {
      # wait for animations
      sleep(0.5)
      @add_to_cart_button.tap if View.new("* text:'詳細を選択'").exist?
    }
    wait_progress_bar_gone
    case item_style
    when :wine
      fail 'wine do not notice' unless WebView.new(id: 'app_web', xpath: '//div[@id="agreehead"]').wait['textContent'] == "\n!\n  酒類をご購入の方へ \n"
      WebView.new(id: 'app_web', xpath: '//li[@class="note" and text()="酒類の購入には、年齢制限があります。20歳未満の購入や飲酒は法律で禁止されています。"]').wait
      WebView.new(id: 'app_web', xpath: '//li[@class="note" and text()="楽天市場では20歳未満の方は、酒類の購入は出来ません。必ず生年月日を入力した上でご購入ください。"]').wait
      WebView.new(id: 'app_web', xpath: '//input[@id="nextButton" and @value="内容に同意し買い物かごへ進む"]').tap
    when :medicine
      fail 'wine do not notice' unless WebView.new(id: 'app_web', xpath: '//div[@id="agreehead"]').wait['textContent'] == "\n!\n  医薬品をご購入の方へ \n"
      WebView.new(id: 'app_web', xpath: '//li[@class="note" and text()="本商品は医薬品となります。購入時には、使用上の注意をよく読み、内容をご確認の上、注文手続きをお願い致します。"]').wait
      WebView.new(id: 'app_web', xpath: '//li[@class="note" and text()="楽天市場では、医薬品の購入には、18歳以上の会員の方のみとさせていただいております。ご了承ください。"]').wait
      WebView.new(id: 'app_web', xpath: '//li[text()="使用者は医師の治療を受けていたり、病気の診断を受けている。"]').wait
      WebView.new(id: 'app_web', xpath: '//li[text()="使用者は病院でもらったお薬や薬局で購入したお薬・サプリメントなどを使用している。"]').wait
      WebView.new(id: 'app_web', xpath: '//li[text()="使用者は乳児、幼児あるいは小児である。 "]').wait
      WebView.new(id: 'app_web', xpath: '//input[@id="nextButton" and @value="内容に同意し買い物かごへ進む"]').tap
    end
    wait_progress_bar_gone
    variant_a = WebView.new(id: 'app_web', xpath: '//header/div[text()="買い物かご"]')
    variant_b = WebView.new(id: 'app_web', xpath: '//h1[text()="買い物かご"]')
    calabash.wait_for {
      [variant_a, variant_b, WebView.new(id: 'app_web')].any?(&:exist?)
    }
    unless variant_a.exist? || variant_b.exist?
      add_to_cart_web_button = WebView.new(id: 'app_web', xpath: '//a[contains(., "ご購入手続きへ")]')
      wait_progress_bar_gone
      flick_with_params("* id:'content'", {x: 50, y: 70}, {x: 50, y: 50})
      # wait for animations
      sleep(1)
      add_to_cart_web_button.tap
      wait_progress_bar_gone
    end
    Cart.wait
  end

  # add bookmarks
  def add_bookmark(page = nil)
    @add_bookmark.wait {
      flick_with_params("* id:'scroll_view'", {x: 5, y: 70}, {x: 5, y: 40})
    }
    reveal_view(@add_bookmark).tap
    wait_progress_bar_gone
    page.wait unless page.nil?
  end

  # tap back
  def tap_back(page = SearchResult)
    wait_progress_bar_gone
    calabash.press_back_button
    calabash.wait_for {
      next true if page.open?
      flick_with_params("* id:'content'", {x: 2, y: 40}, {x: 2, y: 60})
    }
    page.wait
  end

  # tap back button
  def tap_back_button(page = Home)
    ImageButton.new.wait.tap
    calabash.wait_for {
      next true if page.open?
      flick_with_params("* id:'content'", {x: 2, y: 40}, {x: 2, y: 60})
    }
    page.wait
  end

  def item_name
    TextView.new(id: 'item_title').wait {
      flick_with_params("* id:'content'", {x: 2, y: 90}, {x: 2, y: 60})
    }.text
  end

  def assert_item_name(item_name)
    TextView.new(id: 'item_title', text: item_name).wait(timeout_message: 'item detail page is not correct')
  end

  def input_keyword_in_shop_search(keyword)
    @search_in_shop.wait {
      flick_with_params("* id:'content'", {x: 2, y: 30}, {x: 2, y: 10})
    }
    reveal_view(@search_in_shop).tap
    @edit_text.enter_text(keyword)
    calabash.press_enter_button
    sleep(5)
    SearchResult.wait
  end

  def find_and_tap_show_all_item
    @show_all_item.wait {
      flick_with_params("* id:'content'", {x: 2, y: 30}, {x: 2, y: 10})
    }
    reveal_view(@show_all_item).tap
    SearchResult.wait
  end

  def add_shop_in_shop_menu
    @shop_title.wait
    shop_name = @shop_title.text
    @add_shop.wait(timeout: 20) {
      flick_with_params("* id:'scroll_view'", {x: 2, y: 50}, {x: 2, y: 10})
    }
    add_shop_click_able_flg = @add_shop.query.first['enabled']
    reveal_view(@add_shop).tap if add_shop_click_able_flg == true
    @top_bar_container.wait {
      flick_with_params("* id:'scroll_view'", {x: 2, y: 30}, {x: 2, y: 70})
    }
    add_shop_click_able_flg ? shop_name : ''
  end

  def assert_item_availability(flag = true)
    @add_to_cart_button.wait do
      flick_with_params("* id:'content'", {x: 2, y: 20}, {x: 2, y: 10})
    end
    expect(@add_to_cart_button.query.first['enabled']).to eq(flag)
  end

  def assert_item_postage_flag(value)
    @postage_flag.wait do
      flick_with_params("* id:'content'", {x: 2, y: 20}, {x: 2, y: 10})
    end
    expect(@postage_flag.text).to eq(value)
  end

  def assert_item_creditcard_flag(value)
    wait_progress_bar_gone
    TextView.new(text: '価格').wait {
      flick_with_params("* id:'content'", {x: 2, y: 20}, {x: 2, y: 10})
    }
    expect(@creditcard_image.exist?).to eq(value)
    if !value
      expect(@creditcard_image).not_to exist
    else
      @creditcard_image.wait
    end
  end

  # rubocop:disable Metrics/AbcSize, Metrics/CyclomaticComplexity, Metrics/MethodLength, Metrics/PerceivedComplexity
  def assert_item_point(item, shop)
    @shop_title.wait(timeout: 10) {
      flick_with_params('*', {x: 2, y: 30}, {x: 2, y: 60})
    }
    if item.valid_period == 'before' && shop.shop_specific_point.valid_period == 'before'
      sleep(3)
      TextView.new(text: 'ポイントアップ期間中！').wait_gone
      @add_to_cart_button.wait(timeout: 10) {
        flick_with_params('*', {x: 2, y: 50}, {x: 2, y: 30})
      }
      TextView.new(text: 'ポイントアップ期間:').wait_gone
      TextView.new(id: 'point_rate_text').wait_gone
    end

    if item.valid_period == 'during' && shop.shop_specific_point.valid_period == 'before'
      sleep(3)
      TextView.new(text: 'ポイントアップ期間中！').wait_gone
      TextView.new(text: 'ポイントアップ期間:').wait(timeout: 10) {
        flick_with_params('*', {x: 2, y: 50}, {x: 2, y: 30})
      }
      TextView.new(id: 'point_rate_text').wait
    end

    if item.valid_period == 'after' && shop.shop_specific_point.valid_period == 'before'
      sleep(3)
      TextView.new(text: 'ポイントアップ期間中！').wait_gone
      @add_to_cart_button.wait(timeout: 10) {
        flick_with_params('*', {x: 2, y: 50}, {x: 2, y: 30})
      }
      TextView.new(text: 'ポイントアップ期間:').wait_gone
      TextView.new(id: 'point_rate_text').wait_gone
    end

    if item.valid_period == 'before' && shop.shop_specific_point.valid_period == 'during'
      sleep(3)
      TextView.new(text: 'ポイントアップ期間中！').wait
      @add_to_cart_button.wait(timeout: 10) {
        flick_with_params('*', {x: 2, y: 50}, {x: 2, y: 30})
      }
      TextView.new(text: 'ポイントアップ期間:').wait_gone
      TextView.new(id: 'point_rate_text').wait_gone
    end

    if item.valid_period == 'during' && shop.shop_specific_point.valid_period == 'during'
      sleep(3)
      TextView.new(text: 'ポイントアップ期間中！').wait_gone
      TextView.new(text: 'ポイントアップ期間:').wait(timeout: 10) {
        flick_with_params('*', {x: 2, y: 50}, {x: 2, y: 30})
      }
      TextView.new(id: 'point_rate_text').wait
    end

    if item.valid_period == 'after' && shop.shop_specific_point.valid_period == 'during'
      TextView.new(text: 'ポイントアップ期間中！').wait
      @add_to_cart_button.wait(timeout: 10) {
        flick_with_params('*', {x: 2, y: 50}, {x: 2, y: 30})
      }
      TextView.new(text: 'ポイントアップ期間:').wait_gone
      TextView.new(id: 'point_rate_text').wait_gone
    end

    if item.valid_period == 'during' && shop.shop_specific_point.valid_period == 'after'
      sleep(3)
      TextView.new(text: 'ポイントアップ期間中！').wait_gone
      TextView.new(text: 'ポイントアップ期間:').wait {
        flick_with_params('*', {x: 2, y: 50}, {x: 2, y: 30})
      }
      TextView.new(id: 'point_rate_text').wait
    end

    if item.valid_period == 'after' && shop.shop_specific_point.valid_period == 'after'
      sleep(3)
      TextView.new(text: 'ポイントアップ期間中！').wait_gone
      @add_to_cart_button.wait(timeout: 10) {
        flick_with_params('*', {x: 2, y: 50}, {x: 2, y: 30})
      }
      TextView.new(text: 'ポイントアップ期間:').wait_gone
      TextView.new(id: 'point_rate_text').wait_gone
    end

    if item.valid_period == 'before' && shop.shop_specific_point.valid_period == 'after'
      sleep(3)
      TextView.new(text: 'ポイントアップ期間中！').wait_gone
      @add_to_cart_button.wait(timeout: 10) {
        flick_with_params('*', {x: 2, y: 50}, {x: 2, y: 30})
      }
      TextView.new(text: 'ポイントアップ期間:').wait_gone
      TextView.new(id: 'point_rate_text').wait_gone
    end
  end
  # rubocop:enable all
  def assert_point_is_same(item, point)
    @shop_title.wait(timeout: 10) {
      flick_with_params('*', {x: 2, y: 30}, {x: 2, y: 60})
    }
    sleep(3)
    flag_shop = TextView.new(text: 'ポイントアップ期間中！').exist?
    if item.valid_period == 'during'
      TextView.new(id: 'point_rate_text').wait {
        flick_with_params('*', {x: 2, y: 50}, {x: 2, y: 30})
      }
      point_item = TextView.new(id: 'point_rate_text').text.scan(/\d+/).join
      expect(point_item).to eq(point)
    elsif point == 'ポイント2～5倍'
      expect(TextView.new(id: 'point_info_text').text).to eq('通常商品 ダイヤモンド5倍 プラチナ4倍 ゴールド3倍 シルバー2倍 レギュラー2倍')
    elsif point == 'ポイント2倍'
      expect(TextView.new(id: 'point_info_text').text).to eq('通常商品 全会員2倍')
    elsif flag_shop == false
      expect(point).to be_nil
    end
  end

  def goto_webview_page(str)
    case str
    when 'visit_pc_page'
      object = @visit_pc_page
    when 'shop_shipping_payment_method'
      object = @shop_shipping_payment_method
    when 'shop_top_page'
      object = @shop_top_page
    end

    flick_and_find(object, :up)
    reveal_view(object).tap
  end

  def assert_webview_page_reached(path_list)
    return if path_list.empty?
    path_list.each do |path|
      WebView.new(id: 'app_web', xpath: path).wait
    end
  end

  # rubocop:disable Metrics/CyclomaticComplexity, Metrics/MethodLength, Metrics/PerceivedComplexity
  def place_in_cart_about_cpc(item_style, button)
    @add_to_cart_button.wait {
      flick_with_params("* id:'content'", {x: 2, y: 20}, {x: 2, y: 10})
    }.tap
    wait_progress_bar_gone
    case button
    when 'regular'
      button_element = @add_cart_cpc
    when 'time'
      button_element = @time_button
    when 'bidding'
      button_element = @bidding_button
    when 'reserve'
      button_element = @reserve_button
    end
    2.times {
      break if Button.new(text: '続行').exist?
      sleep(1)
    }
    Button.new(text: '続行').wait_gone(timeout: 60) {
      Button.new(text: '続行').tap
      sleep(3) unless Button.new(text: '続行').exist?
    }
    button_element.wait(timeout: 20) {
      flick_with_params("* id:'content'", {x: 2, y: 20}, {x: 2, y: 10})
      Button.new(text: '続行').tap_if_exist
    }.tap
    wait_progress_bar_gone
    if item_style == 'wine'
      if button == 'regular'
        WebView.new(id: 'app_web', xpath: '//li[@class="note" and text()="酒類の購入には、年齢制限があります。20歳未満の購入や飲酒は法律で禁止されています。"]').wait
        WebView.new(id: 'app_web', xpath: '//li[@class="note" and text()="楽天市場では20歳未満の方は、酒類の購入は出来ません。必ず生年月日を入力した上でご購入ください。"]').wait
        WebView.new(id: 'app_web', xpath: '//input[@id="nextButton" and @value="内容に同意し買い物かごへ進む"]').tap
      else
        WebView.new(id: 'app_web', xpath: '/html/body/form/div[2]/ul/li[contains(.,"酒類の購入には、年齢制限があります。20歳未満の購入や飲酒は法律で禁止されています。")]').wait
        WebView.new(id: 'app_web', xpath: '/html/body/form/div[2]/ul/li[2][contains(.,"楽天市場では20歳未満の方は、酒類の購入は出来ません。必ず生年月日を入力した上でご購入ください。")]').wait
        WebView.new(id: 'app_web', xpath: '/html/body/form/center[2]/input[@alt="内容に同意し買い物かごへすすむ"]').tap
      end
    elsif item_style == 'medicine'
      if button == 'regular'
        WebView.new(id: 'app_web', xpath: '//li[@class="note" and text()="本商品は医薬品となります。購入時には、使用上の注意をよく読み、内容をご確認の上、注文手続きをお願い致します。"]').wait
        WebView.new(id: 'app_web', xpath: '//li[@class="note" and text()="楽天市場では、医薬品の購入には、18歳以上の会員の方のみとさせていただいております。ご了承ください。"]').wait
        WebView.new(id: 'app_web', xpath: '//li[text()="使用者は医師の治療を受けていたり、病気の診断を受けている。"]').wait
        WebView.new(id: 'app_web', xpath: '//li[text()="使用者は病院でもらったお薬や薬局で購入したお薬・サプリメントなどを使用している。"]').wait
        WebView.new(id: 'app_web', xpath: '//li[text()="使用者は乳児、幼児あるいは小児である。 "]').wait
        WebView.new(id: 'app_web', xpath: '//input[@id="nextButton" and @value="内容に同意し買い物かごへ進む"]').tap
      end
    end
    wait_progress_bar_gone
    if button == 'regular'
      Cart.wait
    else
      Others.wait
    end
  end
  # rubocop:enable all
  # rubocop:disable Style/AccessorMethodName
  def set_item_num(num)
    @item_num.wait {
      flick_with_params("* id:'content'", {x: 2, y: 20}, {x: 2, y: 10})
    }
    calabash.clear_text_in(@item_num.query.to_s)
    calabash.keyboard_enter_text(num)
    calabash.hide_soft_keyboard
    sleep(0.5)
  end
  # rubocop:enable all
  def go_to_item_description_link
    inner_html_element = WebView.new
    inner_html_element.wait {
      flick_with_params("* id:'scroll_view'", {x: 5, y: 70}, {x: 5, y: 40})
    }
    reveal_view(inner_html_element)
    @item_description_link.tap
    wait_progress_bar_gone
    View.new(id: 'home_menu_close').wait
  end

  def verity_value_of_selectable_inventory(hash = nil)
    inventory_candidates.wait {
      flick_with_params("* id:'content'", {x: 2, y: 20}, {x: 2, y: 10})
    }
    if hash.nil?
      reveal_view(inventory_candidates).tap
      inventory_text = TextView.new(id: 'sku_title').wait.query.first['text'].scan(/\w+\s[×]\s\w+/).first
      calabash.press_back_button
    elsif hash.keys.include?('inventory')
      inventory_text = hash['inventory']
    else
      fail 'error in input hash'
    end
    TextView.new(Query.new(id: 'spinner_selected_text', text: inventory_text.concat(' を選択してください'))
                     .parent("android.widget.LinearLayout id:'inventory_candidates'")).wait
  end

  def verity_value_of_option_select(i_text = nil)
    @spinner_candidates.wait {
      flick_with_params("* id:'content'", {x: 2, y: 20}, {x: 2, y: 10})
    }
    temp_text = @spinner_candidates.query.descendant(Query.new(id: 'spinner_selected_text')).first['text']

    if i_text.nil?
      reveal_view(@spinner_candidates).tap
      TextView.new(id: 'bt_spinner_text').wait
      text_to_compare = Query.new(id: 'bt_spinner_text').first['text']
      calabash.press_back_button
    else
      text_to_compare = i_text
    end
    expect(text_to_compare).to eq(temp_text)
  end

  def verity_value_of_option_checkbox
    @edit_units.wait {
      flick_with_params("* id:'content'", {x: 2, y: 20}, {x: 2, y: 10})
    }
    Query.new(id: 'option_checkbox').run.each do |checkbox|
      expect(checkbox['checked']).to be(false)
    end
  end
  # rubocop:disable Style/AccessorMethodName
  def get_select_info
    result_list = []
    @spinner_candidates.wait {
      flick_with_params("* id:'content'", {x: 2, y: 20}, {x: 2, y: 10})
    }
    option_name_array = TextView.new(id: 'selectable_inventory_title').wait.text.split(' × ')
    reveal_view(inventory_candidates).tap
    @sku_radio_button.tap
    values = TextView.new(id: 'spinner_selected_text').wait.query.run
    inventory_values = values[0]['text'].split(' × ')
    result_list += ["\n" + option_name_array[1] + ':' + inventory_values[1]]
    result_list += [option_name_array[0] + ':' + inventory_values[0]]
    TextView.new(id: 'option_title').wait {
      flick_with_params("* id:'content'", {x: 2, y: 20}, {x: 2, y: 10})
    }
    result_list += [TextView.new(id: 'option_title').text + ':' + values[1]['text']]
    result_list
  end
  # rubocop:enable all
  def set_get_checkbox_info
    result = []
    @edit_units.wait {
      flick_with_params("* id:'content'", {x: 2, y: 20}, {x: 2, y: 10})
    }
    Query.new(id: 'option_checkbox').run.each do |checkbox|
      calabash.perform_action('touch_coordinate', checkbox['rect']['center_x'], checkbox['rect']['center_y']) unless checkbox['checked']
    end
    checkbox_title = Query.new(id: 'option_name').parent('*').parent('*').parent(Query.new(id: 'option_contents_view')).sibling(Query.new(id: 'option_title')).first['text']
    Query.new(id: 'option_name').run.each do |option_name|
      result += [checkbox_title + ':' + option_name['text']]
    end
    result
  end

  def spu_point_sum(point_style_symble, point_rate = nil)
    case point_style_symble
    when :total
      point_style_label = '今この商品はポイント合計'
    when :base
      point_style_label = 'ベースポイント合計'
    when :bonus
      point_style_label = 'ボーナスポイント合計'
    end
    if point_rate.nil?
      View.new(TextView.new(text: point_style_label).query.sibling('*', text: '倍以上').parent('android.widget.RelativeLayout'))
    else
      View.new(TextView.new(text: point_style_label).query.sibling('*', text: point_rate).sibling('*', text: '倍以上')
                   .parent('android.widget.RelativeLayout'))
    end
  end

  @spu_point_nomal = View.new(TextView.new(text: '通常購入分').query.sibling('*', text: '1').sibling('*', text: '倍')
                                  .parent('android.widget.RelativeLayout'))
  @spu_point_rakuten_card = View.new(TextView.new(text: '楽天カードご利用').query.sibling('*', text: '＋').sibling('*', text: '3')
                                         .sibling('*', text: '倍').parent('android.widget.RelativeLayout'))
  @spu_point_monthly_app = View.new(TextView.new(text: 'アプリから当月1回以上ご購入').query.sibling('*', text: '＋')
                                        .sibling('*', text: '1').sibling('*', text: '倍').parent('android.widget.RelativeLayout'))
  @spu_point_premium_card = View.new(TextView.new(text: '楽天プレミアムカードご利用').query.sibling('*', text: '＋')
                                         .sibling('*', text: '1').sibling('*', text: '倍').parent('android.widget.RelativeLayout'))
  @spu_point_rakuten_mobile = View.new(TextView.new(text: '楽天モバイルご加入').query.sibling('*', text: '＋')
                                           .sibling('*', text: '1').sibling('*', text: '倍').parent('android.widget.RelativeLayout'))
  @spu_point_shop_point = View.new(TextView.new(text: 'ショップボーナスポイント分').query.sibling('*', text: '＋')
                                       .sibling('*', text: '9').sibling('*', text: '倍').parent('android.widget.RelativeLayout'))

  def spu_point(point_style_symble, point_prefix, point_rate)
    case point_style_symble
    when :normal
      point_style_label = '通常購入分'
    when :rakuten_card
      point_style_label = '楽天カードご利用'
    when :monthly_app
      point_style_label = 'アプリから当月1回以上ご購入'
    when :premium_card
      point_style_label = '楽天プレミアムカード・楽天ゴールドカードご利用'
    when :rakuten_mobile
      point_style_label = '楽天モバイルご加入'
    when :shop_point
      point_style_label = 'ショップボーナスポイント分'
    end
    View.new(TextView.new(text: point_style_label).query.sibling('*', text: point_prefix).sibling('*', text: point_rate)
                 .sibling('*', text: '倍').parent('android.widget.RelativeLayout'))
  end

  def open_spux_popup
    home_menu_point_rate_counter.wait {
      home_menu_point_rate.tap
    }
  end

  def verify_recommendation_item_shown(item)
    @recommend_item_see_more.wait {
      calabash.flick_with_params("* id:'content'", {x: 40, y: 70}, {x: 40, y: 40})
    }
    @card_title_view_title.wait
    TextView.new(id: 'recommend_item_item_title', text: item.item_name).wait.tap
    TextView.new(id: 'item_title', text: item.item_name).wait {
      calabash.flick_with_params("* id:'content'", {x: 40, y: 70}, {x: 40, y: 40})
    }
  end

  def verify_recommendation_list(item1, item2)
    @recommend_item_see_more.wait {
      calabash.flick_with_params("* id:'content'", {x: 40, y: 70}, {x: 40, y: 40})
    }
    reveal_view(@recommend_item_see_more).tap
    @recommend_page_title.wait
    @recommend_page_search_condition.wait
    TextView.new(text: item1.item_name).wait
    TextView.new(text: item2.item_name).wait
  end

  def verify_recommendation_item_not_shown
    @add_shop.wait {
      calabash.flick_with_params("* id:'content'", {x: 40, y: 70}, {x: 40, y: 40})
    }
    expect(@recommend_item_see_more).not_to exist
    expect(@card_title_view_title).not_to exist
    expect(@recommend_item_item_title).not_to exist
  end

  def open_review
    item_detail_review_count.wait(timeout: 20) {
      flick_with_params("* id:'content'", {x: 2, y: 50}, {x: 2, y: 10})
    }
    review_count = item_detail_review_count.text.to_number
    reveal_view(item_detail_view_reviews).tap
    wait_progress_bar_gone
    Review.new(@item, review_count).wait
  end

  def reveal_view(element)
    if @add_to_cart_button.exist?
      y_max = @add_to_cart_button.rect.y
      element_location = element.rect
      element_bottom_y = element_location.y + element_location.height
      flick_with_params("* id:'content'", {x: 2, y: 50}, {x: 2, y: 40}) if element_bottom_y > y_max
    end
    element
  end

  def select_inventory_if_exist(inventory_must_exist, inventory = nil)
    inventory_candidates_exist = true
    inventory_candidates.wait {
      flick_with_params("* id:'scroll_view'", {x: 5, y: 70}, {x: 5, y: 40})
      if @add_shop.exist?
        inventory_candidates_exist = false
        break
      end
    }
    expect(inventory_candidates_exist).to eq(inventory_must_exist) if inventory_must_exist
    if inventory_candidates_exist
      reveal_view(inventory_candidates).tap
      if inventory
        horizontal = inventory[:horizontal]
        vertical = inventory[:vertical]
        horizontal_parent_query = View.new(id: 'sku_header', text: horizontal).query.parent('android.widget.LinearLayout')
        vertical_query = horizontal_parent_query.sibling('*', id: 'sku_option_container').descendant('*', id: 'item_size_option', text: vertical)
        select_element = View.new(vertical_query)
      else
        select_element = View.new(id: 'sku_radio_button')
      end
      select_element.wait {
        flick_with_params("* id:'sku_pager'", {x: 50, y: 70}, {x: 50, y: 30})
      }
      sleep(0.5)
      select_element.tap
    end
  end

  def goto_inventory
    inventory_candidates.wait {
      flick_with_params("* id:'scroll_view'", {x: 5, y: 70}, {x: 5, y: 40})
    }
    spinner_selected_label = TextView.new("android.widget.LinearLayout id:'inventory_candidates' descendant * id:'spinner_selected_text'")
    selectable_inventory_title_text = @item.y_axis_name.nil? ? @item.x_axis_name : @item.x_axis_name.concat(" × #{@item.y_axis_name}")
    expect(selectable_inventory_title.text).to eq(selectable_inventory_title_text)
    expect(spinner_selected_label.text).to eq("#{selectable_inventory_title_text} を選択してください")
    reveal_view(inventory_candidates).tap
    View.new(id: 'sku_option_recycler_view').wait
    expect(calabash.query("* id:'all_options'", :getCurrentTextColor).first).to eq(-1)
    expect(sku_title.text).to eq("#{selectable_inventory_title_text}を選択してください")
    if @item.y_axis_option.nil?
      expect(sku_header).not_to exist
      assert_inventory_options
    else
      expect(sku_header).to exist
    end
  end
  # rubocop:disable Metrics/BlockLength, Metrics/PerceivedComplexity, Metrics/CyclomaticComplexity
  def assert_inventory_options
    item_count = @item.amount_in_stock.to_number
    if item_count > 0
      child = item_count > @item.in_stock_display.to_i ? [2, 1] : [3, 2]
      child = [2, 2] if @item.in_stock_display.nil?
    else
      child = @item.order_not_in_stock.eql?('Y') ? [2, 1] : [3, 1]
    end
    options_array = []
    loop {
      list = calabash.query("* id:'item_size_option'", :text)
      break if options_array.include?(list.last)
      list.each { |option|
        next if options_array.include?(option)
        options_array << option
        option_label = TextView.new(id: 'item_size_option', text: option)
        status_icon = View.new(option_label.query.sibling("* id:'status_icon'"))
        item_stock_container = View.new(option_label.query.sibling("* id:'item_stock_container'"))
        stock_label = TextView.new(item_stock_container.query.descendant("* id:'stock_text'"))
        sku_radio_button = View.new(item_stock_container.query.descendant("* id:'sku_radio_button'"))
        expect(item_stock_container.rect.x).to be > option_label.rect.x
        if child.first == 2
          if child.last == 2
            expect(stock_label.text.to_number).to be <= @item.amount_in_stock.to_i
          else
            expect(stock_label).not_to exist
          end
          expect(sku_radio_button).to appear
        elsif child.first == 3
          expect(status_icon).to appear
          if child.last == 2
            expect(stock_label.text).to eq('残りわずか')
            expect(sku_radio_button).to appear
          else
            expect(stock_label.text).to eq('在庫なし')
            expect(sku_radio_button).not_to exist
          end
        end
      }
      flick_with_params("* id:'sku_option_recycler_view'", {x: 50, y: 70}, {x: 50, y: 40})
      # wait for animations
      sleep(1)
    }
  end

  def select_options(type, element = :default)
    case type.to_sym
    when :select
      element_id = 'bt_spinner_text'
      select_element = View.new(option_title.query.sibling("* id:'option_contents_view'"))
      select_element.wait { flick_with_params("* id:'scroll_view'", {x: 5, y: 70}, {x: 5, y: 40}) }
      select_title_text = option_title.text
      # wait for animations
      sleep(0.5) && reveal_view(select_element).tap
      expect(alertTitle).to appear
      expect(alertTitle.text).to eq("#{select_title_text}を選択")
      expect(calabash.query("* id:'select_dialog_listview'", :getChildCount).first).to eq(@item.select)
    when :checkbox
      element_id = 'option_name'
      select_element = LinearLayout.new(id: 'option_contents_view')
      select_element.wait { flick_with_params("* id:'scroll_view'", {x: 5, y: 70}, {x: 5, y: 40}) }
      expect(calabash.query("* id:'option_contents_view'", :getChildCount).first).to eq(@item.check_box)
    end
    if element == :default
      element_text = TextView.new(id: element_id)
      element_default = type.to_sym == :select ? element_text : CheckBox.new(element_text.query.sibling("* id:'checkbox_container'").descendant("* id:'option_checkbox'"))
      element_default.wait { flick_with_params("* id:'scroll_view'", {x: 5, y: 70}, {x: 5, y: 40}) if View.new(id: 'scroll_view').exist? }
      select_text = element_default.text if type.to_sym == :select
      # wait for animations
      sleep(1) && element_default.tap
      if type.to_sym == :select
        expect(TextView.new(id: 'spinner_selected_text').text).to eq(select_text)
      else
        expect(element_default.checked?).to eq(true)
      end
    else
      element_list = element.is_a?(Array) ? element : [element]
      element_list = type.to_sym == :select ? element_list.last : element_list
      element_list.each { |option|
        option_text = TextView.new(id: element_id, text: option)
        option_view =  type.to_sym == :select ? option_text : CheckBox.new(option_text.query.sibling("* id:'checkbox_container'").descendant("* id:'option_checkbox'"))
        option_view.wait { flick_with_params("* id:'scroll_view'", {x: 5, y: 70}, {x: 5, y: 40}) }
        select_text = option_view.text if type.to_sym == :select
        # wait for animations
        sleep(1) && option_view.tap
        if type.to_sym == :select
          expect(TextView.new(id: 'spinner_selected_text').text).to eq(select_text)
        else
          expect(option_view.checked).to eq(true)
        end
      }
      select_element.wait
    end
  end
end
