require 'moji'
require_relative '../utils/utils_android'

# rubocop:disable Metrics/ClassLength
class SearchResult < Page
  include IchibaApp::AndroidHelpers
  include RSpec::Matchers

  attr_reader :header, :error_message, :error_message_no_result, :item_desc, :shop_name_label
  attr_reader :clear_search_textbox_icon, :search_edit_text, :same_item_confirm_link, :search_detail, :new_search, :delete_history, :search_history_list
  attr_reader :hybrid_search_dialog_title, :hybrid_search_dialog, :hybrid_search_cancel_button, :hybrid_search_close_button, :genre_search_link, :current_genre
  attr_reader :search_result_recycler_view
  # Element definition
  def initialize
    super('android.widget.LinearLayout', id: 'search_result_toolbar')

    @header = Header.new(SearchResult)
    # Output element
    # キーワード、ジャンル、ショップいずれかの検索条件を指定してください。

    # Search
    @search_edit_text = EditText.new(id: 'search_src_text')
    @go_back_home_button = ImageButton.new
    @go_to_bookmark_button = View.new('*', id: 'tab_bookmark')
    @clear_search_textbox_icon = ImageView.new(id: 'search_close_btn')

    # Input element
    @same_item_confirm_link = TextView.new(text: '同じ製品をまとめて表示しています')
    @genre_search_link = TextView.new(text: 'ジャンル')
    @close_button = ImageView.new(id: 'home_menu_close')
    @item_name_list = TextView.new(id: 'itemDesc')

    # same item confirm popup
    @same_item_confirm_no = Button.new(text: '今後は表示しない')

    # genre list popup
    @backward_genre_link = Button.new(text: '検索結果画面へ戻る')
    @delete_genre_link = Button.new(id: 'button_reset', text: 'ジャンルを指定しない')

    # search
    @search_detail = TextView.new(id: 'tv_icon_title', text: '詳細検索')
    @new_search = TextView.new(id: 'tv_icon_title', text: '新規検索')
    # Search Result
    @current_genre = TextView.new(id: 'current_genre')
    @item_number = TextView.new(id: 'current_count')
    @cd_dvd_genre = TextView.new(text: 'CD・DVD・楽器')
    @container = LinearLayout.new(id: 'container')
    @row_parent = LinearLayout.new(id: 'row_parent')
    @item_image = ImageView.new(id: 'itemImage')
    @shop_icon = ImageView.new(id: 'shopIcon')
    @shop_name = TextView.new(id: 'shopName')
    @item_desc = TextView.new(id: 'itemDesc')
    @item_price = TextView.new(id: 'itemPrice')
    @yen_sign = TextView.new(id: 'yenSign')
    @desc_container = LinearLayout.new(id: 'descContainer')
    @lowest_price_shop = TextView.new(text: '最安ショップを見る')
    @error_message_no_result = TextView.new(text: "ご指定の検索条件に該当する\n商品はありませんでした。")
    @tag_error_msg = TextView.new(text: '条件が多すぎます。[黒ぶどう]で指定したものをいくつか外してから再度この条件を指定してください。')
    @error_message = TextView.new(id: 'message')
    # @item_number_upper_bound = View.new(id: 'footer_button', text: '表示件数の上限に達しました。詳細検索より条件を指定して絞り込んでください。')
    @item_number_upper_bound = Button.new(id: 'footer_button')

    @back_button = ImageButton.new
    @delete_history = TextView.new(text: '検索履歴をすべて消去')
    # @item_subtitle = TextView.new(Query.new(text:'同じ製品をまとめて表示する')
    #                                   .parent('android.widget.LinearLayout')
    #                                   .descendant('android.widget.CheckBox',id:'ichiba_list_history_item_subtitle'))
    @item_num = TextView.new(id: 'current_count')
    @shop_menu_add_my_shop = TextView.new(id: 'shop_menu_add_my_shop')
    @hybrid_search_dialog_title = TextView.new(id: 'ichiba_base_dialog_title', text: '同じ製品をまとめて表示')
    @hybrid_search_dialog = TextView.new(id: 'ichiba_message_dialog_text')
    @hybrid_search_cancel_button = Button.new(text: '今後は表示しない')
    @hybrid_search_close_button = Button.new(text: '閉じる')

    @search_history_list = TextView.new(id: 'ichiba_list_history_item_title')
    @new_search_location = 'android.widget.PopupWindow$PopupViewContainer'
    @shop_name_label = TextView.new(id: 'shopName')
    @genre_ad_confirm_text = TextView.new(text: '注目のピックアップアイテム')
    @genre_ad_item_name = TextView.new(id: 'genre_ad_description')

    @search_result_recycler_view = View.new(id: 'search_result_recycler_view')
  end

  # confirm search result page information
  def assert_search_result_page_shown
    @search_detail.wait
  end

  # display the items and confirm the result
  def tap_the_display_label
    result = false
    if @same_item_confirm_link.exist?
      @same_item_confirm_link.tap
      @same_item_confirm_no.tap
      result = true
    end
    result
  end

  # confirm the selected genre on search result
  def assert_selected_genre(genre_name)
    TextView.new(text: genre_name).wait
    @item_name_list.wait
  end

  # goto genre search on search result page
  def tap_genre_search_button(current_genre = 'ジャンル選択')
    header.tabs.genre.wait {
      flick_with_params("* id:'content'", {x: 50, y: 50}, {x: 50, y: 70})
    }.tap
    Genre.new(current_genre).wait
  end

  # confirm selected genre name on search result page
  def assert_selected_genre_on_search_result(genre_name)
    TextView.new(text: "#{genre_name}を選択する").wait
  end

  # tap the return button on genre list
  def tap_the_return_button_on_genre_list
    @backward_genre_link.wait {
      flick_with_params("* id:'content'", {x: 50, y: 50}, {x: 50, y: 10})
    }.tap
  end

  # tap the genre cancel button and check popup can be displayed
  def tap_the_genre_cancel_button_and_check
    @delete_genre_link.wait {
      flick_with_params("* id:'content'", {x: 50, y: 50}, {x: 50, y: 10})
    }.tap
    TextView.new(id: 'message', text: 'キーワード、ジャンル、ショップいずれかの検索条件を指定してください。').wait
  end

  # confirm search result
  def assert_search_result
    @current_genre.wait
    @item_image.wait
    @shop_icon.wait
    @shop_name.wait
    @item_desc.wait
    @item_price.wait
    @yen_sign.wait
  end

  # make search
  def search(text)
    @clear_search_textbox_icon.tap_if_exist
    @search_edit_text.wait.tap
    calabash.keyboard_enter_text(text, @search_edit_text.query.to_s)
    calabash.press_enter_button
    wait_progress_bar_gone
    # assert_search_result
    @item_desc.wait
    SearchResult.wait
  end

  # go back home page
  def go_back_home_page(page = Home)
    @go_back_home_button.wait {
      flick_with_params("* id:'content'", {x: 50, y: 50}, {x: 50, y: 80})
    }.tap
    page.wait
  end

  def go_to_bookmark_page
    @go_to_bookmark_button.tap
    Bookmark.wait
  end

  def keyword
    @search_edit_text.text
  end

  # confrim keyword on search result page
  def assert_keyword_value(keyword)
    @item_desc.wait
    expect(@search_edit_text.text.strip).to eq(keyword)
  end

  # back to home page
  def tap_back(page = Home)
    ImageButton.new.wait {
      flick_with_params("* id:'content'", {x: 50, y: 20}, {x: 50, y: 10})
    }.tap
    page.wait
  end

  # tap detail search on search result page
  def goto_search_detail
    header.tabs.detail_search.wait {
      flick_with_params("* id:'content'", {x: 50, y: 50}, {x: 50, y: 70})
    }.tap
    SearchDetail.wait
  end

  # select item form search result
  def tap_item(item = nil)
    @item_name_list.wait
    item_name = item.is_a?(Item) ? [item.item_name, item.cpc_item_name] : [item]
    item_name.reject!(&:nil?)
    item_view = item_name.empty? ? TextView.new(id: 'itemDesc') : TextView.new(id: 'itemDesc', text: item_name.last)
    item_view.wait(timeout: 120) {
      flick_with_params("* id:'content'", {x: 50, y: 70}, {x: 50, y: 40})
    }
    # wait for animations
    sleep(0.5)
    item_view.tap
    wait_progress_bar_gone
    ItemDetail.new(item).wait
  end

  def touch_query_list(uiquery, position = 0)
    @item_name_list.wait
    element = calabash.query(uiquery)[position]
    x = element['rect']['center_x']
    y = element['rect']['center_y']
    item_name = element['text']
    calabash.perform_action('touch_coordinate', x, y)
    wait_progress_bar_gone
    ItemDetail.new(item_name)
  end

  def assert_cheapest_shop_link
    @lowest_price_shop.wait do
      calabash.flick_up
    end
  end

  # check search result
  def check_search_result(item)
    ProgressBar.new(id: 'ptr_progress').wait_gone
    TextView.new(id: 'itemDesc', text: item).wait do
      flick_with_params("* id:'content'", {x: 50, y: 50}, {x: 50, y: 20})
      sleep(0.5)
    end
  end

  def assert_search_result_match_single_keyword(keyword)
    ProgressBar.new(id: 'ptr_progress').wait_gone
    TextView.new(id: 'itemDesc').wait
    result = Query.new(id: 'itemDesc').run
    result.each do |item|
      expect(item['text']).to include(keyword)
    end
  end

  def select_sort(category)
    header.tabs.sort.tap
    label = '標準'
    case category
    when :standard
      label = '標準'
    when :price_low
      label = '価格が安い'
    when :price_high
      label = '価格が高い'
    when :new
      label = '新着順'
    when :review_number
      label = '感想の件数が多い'
    when :review_high
      label = '感想の評価が高い'
    end
    TextView.new(text: label).tap
    sleep(1)
  end

  def assert_sort(category, _product, sort_items)
    calabash.flick_with_params("* id:'content'", {x: 50, y: 50}, {x: 50, y: 80})
    select_sort(category)
    array_item = []
    loop do
      TextView.new(id: 'itemDesc').wait
      array = TextView.new(id: 'itemDesc').query.run
      array.each do |item|
        array_item = array_item << item['text'] unless array_item.include?(item['text'])
      end
      flick_with_params('*', {x: 50, y: 40}, {x: 50, y: 20})
      sleep(0.5)
      break if TextView.new(id: 'itemDesc').query.run.last['text'] == array_item.last
    end
    expect(sort_items).to eq(array_item)
  end

  def tap_new_search_and_confirm_search_history(items)
    @item_desc.wait(timeout_message: 'loading failed')
    @new_search.tap
    array_item = []
    sleep(2)
    array = Query.new(id: 'ichiba_list_history_item_title').run
    array.each do |item|
      array_item = array_item << item['text']
    end
    expect(items).to eq(array_item)
  end

  def assert_display_mode(mode)
    TextView.new(text: '表示切替').tap unless mode == 'list'
    image_width = ImageView.new(id: 'itemImage').rect.width
    row_width = View.new("android.support.v7.widget.RecyclerView' id:'search_result_recycler_view'").rect.width
    if mode == 'list'
      @item_desc.wait
      expect(image_width).to be < row_width * 0.4
    elsif mode == 'middle'
      expect(image_width).to be < row_width * 0.5
      expect(image_width).to be > row_width * 0.4
    else
      expect(image_width).to be > row_width * 0.9
    end
  end

  def assert_the_number_of_search_result(num)
    sleep(5)
    expect(@item_num.text.lstrip).to eq("#{num}件")
  end

  def assert_the_first_item_is_cpc
    wait_progress_bar_gone
    @item_name_list.wait
    string_item = @item_name_list.text
    expect(string_item).to include('[PR]')
  end

  def tap_back_to_item_detail(item_name = nil)
    calabash.press_back_button
    TextView.new('android.widget.TextView', id: 'item_title').wait do
      flick_with_params("* id:'content'", {x: 50, y: 20}, {x: 50, y: 50})
    end
    LinearLayout.new('android.widget.LinearLayout', id: 'ichiba_list_item_container').wait do
      flick_with_params("* id:'content'", {x: 50, y: 20}, {x: 50, y: 50})
    end
    ItemDetail.new(item_name).wait
  end

  def operate_search_with_keyword(str)
    @search_edit_text.tap
    calabash.enter_text_simulate_keyboard(str)
    calabash.press_enter_button
  end

  def assert_except_keyword_search(keyword, except_keyword, flick_time = 0)
    @item_name_list.wait
    [0..flick_time].each do
      result = @item_name_list.query.run
      result.each do |item_json|
        expect(item_json['text']).to include(keyword)
        expect(item_json['text']).not_to include(except_keyword)
      end
    end
  end

  def assert_search_histories_shown_on_new_search(first_history, last_history)
    @new_search.tap
    calabash.hide_soft_keyboard
    TextView.new(text: last_history).wait
    TextView.new(text: first_history).wait do
      flick_with_params('android.widget.ListPopupWindow$DropDownListView', {x: 50, y: 50}, {x: 50, y: 30})
    end
  end

  def assert_the_first_item_is_not_cpc
    wait_progress_bar_gone
    string_item = @item_name_list.text
    expect(string_item).not_to include('[PR]')
  end

  def tap_item_by_name(item_name)
    wait_progress_bar_gone
    item = "[PR] #{item_name}"
    TextView.new(id: 'itemDesc', text: item.to_s).wait {
      flick_with_params("* id:'content'", {x: 50, y: 20}, {x: 50, y: 10})
    }.tap
    ProgressBar.new.wait_gone
    ItemDetail.new(item_name).wait
  end
  # rubocop:disable Style/NumericLiterals
  def assert_flick_and_touch(direction, max_flick = 100)
    last_item_list = []
    y = direction == :up ? -20000 : 20000
    index = direction == :up ? 0 : -1
    max_flick.times {
      calabash.query("* id:'search_result_recycler_view'", smoothScrollBy: [0, y])
      wait_progress_bar_gone
      break if last_item_list.include?(@item_desc.wait.query[index]['text'])
      last_item_list << @item_desc.wait.query[index]['text']
      item_detail = tap_item
      item_detail.tap_back(SearchResult)
    }
  end

  def assert_item_amount_in_result(str, value)
    @item_number.wait
    item_count = @item_number.text.to_number
    if str == 'greater'
      expect(item_count).to be >= value
    elsif str == 'less'
      expect(item_count).to be <= value
    end
  end

  def assert_upperbound_msg
    calabash.flick_up
    @item_number_upper_bound.wait(timeout: 400) do
      calabash.scroll_to_row("* id:'search_result_recycler_view'", 3001)
    end
  end

  def assert_transfer_of_clicking_upperbound_button
    @item_number_upper_bound.tap
    SearchDetail.wait
  end

  def assert_item_point(item, shop)
    @item_name_list.wait
    point = nil
    point = TextView.new(id: 'pointRate').text.scan(/\d+/).join if item.valid_period == 'during' || shop.shop_specific_point.valid_period == 'during'
    point
  end
  # rubocop:disable Metrics/MethodLength, Metrics/BlockLength, Metrics/CyclomaticComplexity, Metrics/PerceivedComplexity
  def assert_new_search_history_with_subtitle(option = {})
    @search_history_list.wait
    string_num = option.size.to_s
    keyword = option['keyword']
    text_view = TextView.new(Query.new(id: 'ichiba_list_history_item_title', text: keyword.to_s)
                        .parent('android.widget.LinearLayout')
                        .descendant('android.widget.TextView', id: 'ichiba_list_history_item_subtitle'))
    text = text_view.wait {
      flick_with_params('android.widget.PopupWindow$PopupViewContainer', {x: 50, y: 50}, {x: 50, y: 30})
    }.text
    if string_num == 1
      option = ['(在庫あり・注文可能)']
      raise 'error in search history about shop' unless text.eql?(option)
    else
      option.each do |filter, value|
        case filter
        when 'ngkeyword'
          if value == 'unset'
            expect(text).not_to include(value)
          else
            expect(text).to include("(除外:#{value})")
          end
        when 'genre'
          if value == 'unset'
            expect(text).not_to include(value)
          else
            value = value.last
            expect(text).to include(value)
          end
        when 'shop'
          if value == 'unset'
            expect(text).not_to include(value)
          else
            expect(text).to include(value)
          end
        when 'area'
          if value == 'unset'
            expect(text).not_to include(value)
          else
            expect(text).to include(value)
          end
        when 'availability'
          if value
            value = '(在庫あり・注文可能)'
            expect(text).to include(value)
          else
            value = '(在庫あり・注文可能)'
            expect(text).not_to include(value)
          end
        when 'postage'
          if value
            value = '(送料無料 / 送料込み)'
            expect(text).to include(value)
          else
            expect(text).not_to include(value)
          end
        when 'credit'
          if value
            value = '(クレジットカード利用可)'
            fail 'keyword is set' unless string.wait {
              flick_with_params('android.widget.PopupWindow$PopupViewContainer', {x: 50, y: 50}, {x: 50, y: 30})
            }.text.include?(value)
          elsif text.include?(value)
            raise 'error in search history about price_form'
          end
        when 'sort'
          expect(text).not_to include(value)
        end
      end
      price_from = option['price_from']
      price_to = option['price_to']
      if price_from == 'unset' || price_from.nil?
        if price_to == 'unset' || price_to.nil?
          expect(text).not_to include('円')
        else
          expect(text).to include("#{price_to}円")
        end
      elsif price_to == 'unset' || price_to.nil?
        expect(text).to include("#{price_from}円")
      else
        price = "#{price_from}円～#{price_to}円"
        expect(text).to include(price)
      end
    end
  end
  # rubocop:enable all
  def delete_search_history_of_new_search
    raise 'error in search history about delete' unless @delete_history.wait {
      flick_with_params('android.widget.ListPopupWindow$DropDownListView', {x: 50, y: 50}, {x: 50, y: 30})
    }.tap
    sleep(2)
    search_history_list.wait_gone
    @new_search.tap
    search_history_list.wait_gone
  end

  def find_new_search_history(search_name)
    history_name = TextView.new(id: 'ichiba_list_history_item_title', text: search_name)
    calabash.wait_for_keyboard
    history_name.wait(timeout_message: "history missing: #{search_name}") {
      # flick_with_params('android.widget.PopupWindow$PopupViewContainer', {x: 50, y: 50}, {x: 50, y: 30})
      flick_with_params('android.widget.ListPopupWindow$DropDownListView', {x: 50, y: 50}, {x: 50, y: 30})
    }
  end

  def search_history_does_not_exist(search_name)
    history_name = TextView.new(id: 'ichiba_list_history_item_title', text: search_name)
    @delete_history.wait(timeout_message: "history missing: #{search_name}") {
      flick_with_params('android.widget.ListPopupWindow$DropDownListView', {x: 50, y: 50}, {x: 50, y: 30})
      # flick_with_params('android.widget.PopupWindow$PopupViewContainer', {x: 50, y: 50}, {x: 50, y: 30})
    }
    history_name.wait_gone
  end

  def assert_search_history_num(num)
    array_item = []
    loop do
      array = TextView.new(id: 'ichiba_list_history_item_title').query.run
      array.each do |item|
        array_item << item['text'] unless array_item.include?(item['text'])
      end
      flick_with_params('android.widget.ListPopupWindow$DropDownListView', {x: 50, y: 50}, {x: 10, y: 30})
      break if @delete_history.exist?
    end
    array_item_num = array_item.size.to_s
    expect(num).to eq(array_item_num)
  end

  def assert_history_selected(num)
    @search_history_list.query[num]['text']
  end

  def tap_search_history_title(history_text)
    TextView.new(id: 'ichiba_list_history_item_title', text: history_text).tap
    SearchResult.wait
  end

  def assert_search_conditon_on_search_result(keyword, genre)
    genre_name = @current_genre.text
    search_keyword = @search_edit_text.text
    expect(keyword).to eq(search_keyword)
    expect(genre).to eq(genre_name)
  end

  def item_name
    calabash.scroll_to_row("* id:'search_result_recycler_view'", 15)
    @genre_ad_item_name.wait(timeout_message: 'Genre ad not found') {
      flick_with_params("* id:'content'", {x: 50, y: 70}, {x: 50, y: 40})
      calabash.wait_for_animations
    }
    @genre_ad_item_name.text
  end

  def slide_genre_ad(item)
    genre_ad_item = TextView.new(id: 'genre_ad_description', text: item)
    genre_ad_item.wait(timeout: 10)
    genre_ad_item.wait_gone {
      flick_with_params("* id:'genre_ads_horizontally_scrollable_view'", {x: 60, y: 50}, {x: 40, y: 50})
    }
  end

  def tap_genre_ad(item)
    TextView.new(id: 'genre_ad_description', text: item).wait {
      flick_with_params("* id:'genre_ads_horizontally_scrollable_view'", {x: 40, y: 50}, {x: 60, y: 50})
    }.tap
    ItemDetail.wait
  end
end
