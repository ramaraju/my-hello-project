require_relative '../utils/utils_android'

# rubocop:disable Metrics/ClassLength
class SearchDetail < Page
  include IchibaApp::AndroidHelpers, RSpec::Matchers

  attr_reader :search_button, :error_message_no_input, :popup_ok_button, :search_history_button
  attr_reader :search_detail_button, :navigator_back, :availability_checkbox, :postage_checkbox, :credit_checkbox
  attr_reader :price_range_error_msg, :error_message_no_result, :error_message, :hybird_search, :sort
  attr_reader :hybird_search_checkbox, :clear_button, :popup_yes_button, :search_history_list
  attr_reader :keyword
  def initialize
    super('android.widget.TextView', text: '詳細検索')
    @genre = Genre.new
    # go back home button
    @go_back_home_button = ImageButton.new
    # navigator
    # navigator_back button text is different on different screen size phones
    @navigator_back = ImageButton.new(index: 0)
    # search
    @keyword = EditText.new(id: 'searchkeyword_edit')
    @ngkeyword = EditText.new(id: 'ngkeyword_edit')
    @genre_unselected = TextView.new(text: 'ジャンル')
    @starting_price = EditText.new(id: 'search_starting_price')
    @price = TextView.new(Query.new(text: '価格帯')
                              .sibling("android.widget.TextView id:'ichiba_list_item_subtitle'"))
    @price_range = TextView.new(text: '価格帯')
    @price_from = EditText.new(id: 'refine_price_range_from')
    @price_to = EditText.new(id: 'refine_price_range_to')
    @shop = TextView.new(Query.new(text: 'ショップ指定')
                             .sibling("android.widget.TextView id:'ichiba_list_item_subtitle'"))
    @shop_list = TextView.new(text: 'ショップ指定')
    @area = TextView.new(Query.new(text: '翌日配送エリア').sibling("android.widget.TextView id:'ichiba_list_item_subtitle'"))
    @availability_checkbox = CheckBox.new(Query.new(text: '在庫あり・注文可能').parent('android.widget.LinearLayout').sibling('android.widget.CheckBox'))
    @postage_checkbox = CheckBox.new(Query.new(text: '送料無料 / 送料込み').parent('android.widget.LinearLayout').sibling('android.widget.CheckBox'))
    @credit_checkbox = CheckBox.new(Query.new(text: 'カードOK').parent('android.widget.LinearLayout').sibling('android.widget.CheckBox'))
    @sort = TextView.new(Query.new(text: '並び順').sibling("android.widget.TextView id:'ichiba_list_item_subtitle'"))
    @list_item = TextView.new(id: 'execute_search_button')
    @list_item_subtitle = TextView.new(id: 'ichiba_list_item_subtitle')
    @search_button = Button.new(text: 'この条件で検索する')
    @clear_button = Button.new(id: 'button_clear')
    @tag_submit_button = Button.new(id: 'button_submit')
    # popup
    @price_set_button = Button.new(text: '価格帯を設定する')
    @popup_yes_button = Button.new(text: 'はい')
    @popup_ok_button = Button.new(id: 'button1')
    @error_message = TextView.new(id: 'message')

    @sort_normal = CheckedTextView.new(text: '標準')
    @sort_cheap = CheckedTextView.new(text: '価格が安い')
    @sort_expensive = CheckedTextView.new(text: '価格が高い')
    @sort_register = CheckedTextView.new(text: '新着順')
    @sort_review_more = CheckedTextView.new(text: '感想の件数が多い')
    @sort_review_better = CheckedTextView.new(text: '感想の評価が高い')
    # search history
    @search_history_button = TextView.new(id: 'search_history')
    @search_detail_button = TextView.new(id: 'detail_search')
    @search_history_list = TextView.new(id: 'ichiba_list_item_title')
    # message
    @error_message_no_result = TextView.new(text: 'ご指定の検索条件に該当する商品はありませんでした')
    @error_message_no_input = TextView.new(text: 'キーワード、ジャンル、ショップいずれかの検索条件を指定してください。')
    @price_range_error_msg = TextView.new(text: '価格帯の範囲指定に誤りがあります')
    # moved from search result
    @checked_tag_checkbox = CheckBox.new(tag: 'check_box', checked: true)
    @loading_progress_bar = TextView.new(text: '読み込み中...')
    # hybird search
    @hybird_search = TextView.new(id: 'ichiba_list_item_title', text: '同じ製品をまとめて表示する')
    @hybird_search_checkbox = CheckBox.new(Query.new(text: '同じ製品をまとめて表示する')
                                              .parent('android.widget.LinearLayout')
                                              .descendant('android.widget.CheckBox'))
  end

  def sub_page_title(title)
    View.new("* id:'app_toolbar' descendant * text:'#{title}'")
  end
  # confirm search result page information
  def validate
    # wait_for_element_exists(trait,timeout:3)
  end

  # confirm search result page information
  def assert_search_result_shown
    SearchDetail.wait
  end

  # confrim keyword on detail search page
  def assert_keyword(keyword)
    ScrollView.new.page_scroll(:up)
    expect(@keyword.text.strip).to eq(keyword)
  end

  def tap_search_button
    @search_button.tap
    wait_progress_bar_gone
    SearchResult.wait
  end
  # tap back button on detail search page
  def tap_back
    @navigator_back.tap
    SearchResult.wait
  end

  def tap_genre(genre)
    genre_view = TextView.new(text: genre, id: 'ichiba_list_item_title')
    if ListView.new.exist?
      ListView.new.scroll_to_view(genre_view)
    else
      view_all(genre_view).scroll_to
    end
    genre_view.tap
    wait_progress_bar_gone
  end

  # confirm search detail conditions
  # rubocop:disable Metrics/MethodLength, Metrics/BlockLength, Metrics/PerceivedComplexity, Metrics/CyclomaticComplexity, Metrics/AbcSize
  def assert_filter_value(hash)
    wait_progress_bar_gone
    price_from_exist_flg = hash.keys.include?('price_from') && !hash['price_from'].eql?('unset')
    price_to_exist_flg = hash.keys.include?('price_to') && !hash['price_to'].eql?('unset')
    price = view_all(@price).scroll_to.text.scan(/\d+|～/).join if price_from_exist_flg || price_to_exist_flg
    ScrollView.new.page_scroll(:up)
    hash.each do |filter, value|
      case filter
      when 'keyword'
        if value == 'unset'
          expect(@keyword.text).to eq('')
        else
          expect(@keyword.text).to eq(value)
        end
      when 'ngkeyword'
        if value == 'unset'
          expect(@ngkeyword.text).to eq('')
        else
          expect(@ngkeyword.text).to eq(value)
        end
      when 'genre'
        if value == 'unset'
          LinearLayout.new(id: 'genre').wait
        else
          TextView.new(id: 'ichiba_list_item_title', text: value.pop).wait(timeout_message: 'genre is not correct')
        end
      when 'shop'
        TextView.all(text: 'ショップ指定').scroll_to
        if value == 'unset'
          expect(@shop).not_to exist
        else
          expect(@shop.text).to eq(value)
        end
      when 'area'
        TextView.all(text: '翌日配送エリア').scroll_to
        if value == 'unset'
          expect(@area).not_to exist
        else
          expect(@area.text).to eq(value)
        end
      when 'price_from'
        TextView.all(text: '価格帯').scroll_to
        if value == 'unset'
          expect(price).to start_with('～') if price_to_exist_flg
        else
          price_from = price.split('～').first.to_number
          expect(price_from).to eq(value.to_i)
        end
      when 'price_to'
        TextView.all(text: '価格帯').scroll_to
        if value == 'unset'
          expect(price).to end_with('～') if price_from_exist_flg
        else
          price_to = price.split('～').last.to_number
          expect(price_to).to eq(value.to_i)
        end
      when 'availability'
        view_all(@availability_checkbox).scroll_to
        expect(@availability_checkbox.checked?).to eq(value)
      when 'postage'
        view_all(@postage_checkbox).scroll_to
        expect(@postage_checkbox.checked?).to eq(value)
      when 'credit'
        view_all(@credit_checkbox).scroll_to
        expect(@credit_checkbox.checked?).to eq(value)
      when 'sort'
        expect(view_all(@sort).scroll_to.text).to eq(value)
      end
    end
  end

  def assert_search_history_is_exist(keyword)
    View.new('android.support.v4.view.ViewPager', getCurrentItem: 1).wait {
      search_history_button.tap
    }
    expect(TextView.new(id: 'ichiba_list_item_title', text: keyword.to_s)).to appear
  end

  def assert_search_history_from_detail_page(num)
    search_history_num = calabash.query("* id:'list'", :getChildCount).first
    expect(search_history_num).to eq(num)
  end

  # clear search conditions
  def clear_search_condition
    @clear_button.tap
    @popup_yes_button.wait.tap
  end

  # make detail search on search detail page
  def make_detail_search(hash)
    wait_progress_bar_gone
    ScrollView.new.page_scroll(:up)
    hash.each do |filter, value|
      case filter
      when 'keyword'
        @keyword.enter_text(value) unless value == 'unset'
      when 'ngkeyword'
        @ngkeyword.enter_text(value) unless value == 'unset'
      when 'genre'
        unless value == 'unset'
          view_all(@genre_unselected).scroll_to
          @genre_unselected.tap
          genre_page = Genre.wait
          genre_page.select_genre(value)
          sleep(1)
        end
      when 'tag'
        goto_select_tag(value) unless value == 'unset'
      when 'price_from'
        unless value == 'unset'
          TextView.all(id: 'ichiba_list_item_title', text: '価格帯').scroll_to
          TextView.new(text: '価格帯').tap
          @price_from.enter_text(value)
          @price_set_button.tap
        end
      when 'price_to'
        unless value == 'unset'
          TextView.all(id: 'ichiba_list_item_title', text: '価格帯').scroll_to
          TextView.new(text: '価格帯').tap
          @price_to.enter_text(value)
          @price_set_button.tap
        end
      when 'shop'
        unless value == 'unset'
          TextView.all(id: 'ichiba_list_item_title', text: 'ショップ指定').scroll_to
          TextView.new(text: 'ショップ指定').tap
          sub_page_title('ショップ選択')
          CheckedTextView.new(id: 'text1', text: value).tap
        end
      when 'area'
        unless value == 'unset'
          TextView.all(id: 'ichiba_list_item_title', text: '翌日配送エリア').scroll_to
          TextView.new(text: '翌日配送エリア').tap
          expect(sub_page_title('エリア指定')).to appear
          view = View.new(id: 'ichiba_list_item_title', text: value)
          ListView.new.scroll_to_view(view)
          view.tap
        end
      when 'availability'
        @availability_checkbox.tap unless view_all(@availability_checkbox).scroll_to.checked? == value
      when 'postage'
        @postage_checkbox.tap unless view_all(@postage_checkbox).scroll_to.checked? == value
      when 'credit'
        @credit_checkbox.tap unless view_all(@credit_checkbox).scroll_to.checked? == value
      when 'sort'
        TextView.all(id: 'ichiba_list_item_title', text: '並び順').scroll_to
        TextView.new(text: '並び順').tap
        ListView.new(index: 0).wait
        CheckedTextView.new(text: value).tap
      end
    end
    @search_button.wait {
      flick_with_params("* id:'content'", {x: 50, y: 50}, {x: 50, y: 30})
    }
    @search_button.tap
    @loading_progress_bar.wait_gone
    SearchResult.wait unless @error_message_no_result.exist?
  end
  # rubocop:enable all
  # Make sure the view is visible and not covered by other elements
  # @param view View to scroll to
  def reveal_view(view)
    view = send(view) if view.is_a?(Symbol)
    view.wait {
      flick_with_params("* id:'content'", {x: 50, y: 50}, {x: 50, y: 30})
    }
    if @clear_button.exist?
      y_max = @clear_button.rect.y
      view.wait(timeout: 10) {
        flick_with_params("* id:'content'", {x: 50, y: 50}, {x: 50, y: 30})
      }
      bottom_y = view.rect.y + view.rect.height
      flick_with_params("* id:'content'", {x: 50, y: 50}, {x: 50, y: 40}) if bottom_y >= y_max
      calabash.wait_for_animations
    end
    view
  end

  def goto_select_tag(hash)
    hash.each do |key, value|
      tag_label = View.all(Query.new(id: 'taggroup_container').descendant("android.widget.TextView text:'#{key}'"))
      tag_label.scroll_to.tap
      genre_page = Genre.new(key).wait
      genre_page.select_tag(value)
    end
  end

  def assert_search_history_list(item_list, otherinfo_list = [])
    View.new('android.support.v4.view.ViewPager', getCurrentItem: 1).wait {
      search_history_button.tap
    }
    array_item = calabash.query("* id:'ichiba_list_item_title'", :text)
    expect(array_item).to eq(item_list)
    unless otherinfo_list.empty?
      array_otherinfo = calabash.query("* id:'ichiba_list_item_subtitle'", :text)
      expect(array_otherinfo).to eq(otherinfo_list)
    end
  end

  def assert_shipping_address_suggestion_first_place(area)
    @area.wait.tap
    TextView.new(text: 'エリア指定を解除する').wait
    expect(TextView.new(id: 'ichiba_list_item_title').text).to eq(area)
    expect(View.new('android.widget.RadioButton').query.first(:isChecked)).to eq(true)
  end

  def remove_shipping_area
    TextView.new(text: 'エリア指定を解除する').tap
  end

  def goto_remove_shop_suggestion
    @shop_list.wait.tap
    Button.new(text: '指定を解除する').wait.tap
  end

  def assert_no_area_checked
    TextView.new(text: '翌日配送エリア').wait.tap
    TextView.new(text: 'エリア指定を解除する').wait
    expect(Query.new('android.widget.RadioButton checked:true').run).to be_empty
    @navigator_back.tap
    @clear_button.wait
  end

  def to_edit_except_keyword(str)
    @ngkeyword.tap
    calabash.enter_text_simulate_keyboard(str)
    calabash.press_back_button
  end

  def assert_except_keyword_input_correct(str)
    expect(@ngkeyword.text).to eq(str)
  end

  def clear_search_conditions
    @clear_button.tap
    @popup_yes_button.wait.tap
  end

  def to_edit_keyword(str)
    @keyword.tap
    calabash.enter_text_simulate_keyboard(str)
    calabash.press_back_button
  end

  def to_select_shop
    @shop_list.tap
    View.new(id: 'text1').tap
  end

  def go_to_select_genre
    @genre_unselected.tap
    wait_progress_bar_gone
  end

  def select_some_tags(number)
    iterator = 0
    query_for_unchecked_tags = Query.new(tag: 'check_box', checked: false)
    query_for_unchecked_tags.wait

    until iterator == number
      result = query_for_unchecked_tags.run
      until result.empty? || iterator == number
        calabash.touch(query_for_unchecked_tags.to_s)
        iterator += 1
        result = query_for_unchecked_tags.run
      end
      flick_with_params("* id:'content'", {x: 50, y: 60}, {x: 50, y: 40})
      # wait for animations
      sleep(1)
    end
  end

  def tick_one_tag_off
    @checked_tag_checkbox.tap
  end

  def click_submit
    @tag_submit_button.tap
    sleep(1)
  end

  def input_price_range(from, to)
    @price_from.wait
    sleep(1)
    @price_from.tap unless calabash.keyboard_visible?
    calabash.enter_text_simulate_keyboard(from.to_s)
    calabash.press_back_button
    @price_to.tap
    calabash.enter_text_simulate_keyboard(to.to_s)
    sleep(1)
    calabash.press_back_button
    @price_set_button.tap_if_exist
  end

  def assert_price_input(input_text, direction = :left)
    @price_to.wait {
      @price_range.tap
    }
    text_to_assert = direction == :left ? @price_from.query[0]['text'] : @price_to.query[0]['text']
    expect(input_text).to eq(text_to_assert)
  end

  def click_genre_button
    @genre_unselected.tap
  end

  def set_search_price_range(from, to)
    @price_range.tap
    input_price_range(from, to)
  end

  def assert_genre_cleared(genre_name)
    expect(Query.new(text: genre_name).run).to be_empty
  end

  # go back home page
  def go_back_home_page
    @go_back_home_button.tap
    Home.wait
  end

  def assert_error_for_too_many_tags_shown
    @error_message.wait
  end

  def clear_error
    @popup_ok_button.tap
  end

  def find_search_history(search_name)
    history_name = TextView.new(id: 'ichiba_list_item_title', text: search_name)
    history_name.wait(timeout_message: "history missing: #{search_name}") {
      flick_with_params('*', {x: 50, y: 50}, {x: 50, y: 30})
    }
  end

  def search_history_does_not_exist(flag, search_name)
    history_name = TextView.new(id: 'ichiba_list_item_title', text: search_name)
    TextView.new(id: 'ichiba_list_item_title', text: flag).wait(timeout_message: "history missing: #{flag}") {
      flick_with_params('*', {x: 50, y: 50}, {x: 50, y: 30})
    }
    history_name.wait_gone
  end
end
