require_relative '../utils/utils_android'
require 'calabash-android/abase'

class RakutenPointCard < Page
  include IchibaApp::AndroidHelpers

  attr_reader :header
  def initialize
    super(id: 'action_bar_title', text: '楽天ポイントカード')
    @back_button = TextView.new(id: 'back_text')
  end

  def go_back_home
    @back_button.wait.tap
    Home.wait
  end
end
