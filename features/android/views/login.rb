require_relative 'web_view/web_common'
require_relative '../utils/utils_android'

class Login < Page
  include IchibaApp::AndroidHelpers

  edit_text :user_id_textbox, id: 'username_input'
  edit_text :password_textbox, id: 'password_input'
  text_view :login_current_username, id: 'login_current_username'
  button :login_other_account_button, id: 'login_other_account'
  button :login_button, id: 'login_button'
  button :forget_password_button, id: 'forgot_password_button'
  text_view :register_new_account, text: '楽天会員に登録（無料）'

  def initialize
  end

  def trait
    Query.new('android.view.ViewGroup', id: 'app_toolbar').descendant("android.widget.TextView text:'ログイン'")
  end

  def wait(*args)
    Button.new(text: 'OK').tap && sleep(1) if TextView.new(text: '自動ログイン').exist?
    # system("adb shell input tap 550 1050")
    super(*args)
  end

  def goto_user_input_page
    ProgressBar.new(id: 'ptr_progress').wait_gone(timeout: 120)
    login_other_account_button.tap if login_current_account_button.exist? && login_other_account_button.exist?
    user_id_textbox.wait
  end

  # Login with given username and password, then return to the home screen.
  # @param username [String] account user name
  # @param password [String] account password
  def login(user, page = Home)
    goto_user_input_page
    sleep(1)
    user_id_textbox.text = user.username
    password_textbox.text = user.password
    login_button.tap
    page.wait {
      flick_with_params("* id:'content'", {x: 50, y: 30}, {x: 50, y: 70})
    }
  end

  # Confirm transform to the login
  def assert_transform_to_the_login
    TextView.new(text: 'ログイン').wait
  end

  def goto_webview_page(str)
    case str
    when 'forget_password'
      object = forget_password_button
    when 'register_new_account'
      object = register_new_account
    end
    object.tap && sleep(0.5)
    wait_progress_bar_gone
  end

  def assert_webview_page_reached(query)
    fail "page #{page_sign} not reached" unless Query.new(query).empty?
  end

  def login_current_account_button(page = Home)
    RelativeLayout.new(id: 'login_current_account', tap_target: page)
  end
end
