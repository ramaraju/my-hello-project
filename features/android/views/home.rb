require_relative 'header'
require_relative 'tabs_search'
require_relative 'tabs_campaign'
require_relative 'tabs_bookmark'
require_relative 'tabs_purchase_history'
require_relative 'help'
require_relative 'rakuten_point_card'
require_relative 'web_view/my_rakuten'
require_relative 'web_view/pointclub'
require_relative 'web_view/mycoupon'
require_relative '../utils/utils_android'
require_relative 'app'

# rubocop:disable Metrics/ClassLength
class Home < Page
  include IchibaApp::AndroidHelpers
  include RSpec::Matchers

  attr_reader :header, :search, :campaign, :bookmark, :purchase_history, :search_result, :barcode_search_link
  attr_reader :voice_search, :login_button, :open_drawer, :genre_button, :genre_button_from_nav
  attr_reader :delete_search_history_link, :back_button, :pointclub_button, :member_name_label
  attr_reader :cd_dvd_genre, :dvd_genre, :member_info_frame, :rakuten_point_button, :featured_events_detail
  attr_reader :suggestions_list, :suggestions_list_genre_label, :suggestions_item_list, :dialog_login_button

  text_view :member_name_label, id: 'home_member_name'
  linear_layout :ichiba_base_dialog, id: 'ichiba_base_dialog'
  # # home page
  # view :member_info_frame, id: 'member_info_view'
  # text_view :rank_label, 'android.widget.TextView', "{text ENDSWITH 'ランキング'}", id: 'card_title_view_title'
  # text_view :fashion_label, 'android.widget.TextView', "{text ENDSWITH '一押しファッション'}", id: 'card_title_view_title'
  # text_view :attract_label, text: '注目のイベント'
  # text_view :bookmark_label, 'android.widget.TextView', "{text BEGINSWITH 'お気に入り'}", id: 'card_title_view_title'
  # text_view :login_or_empty_prompt_message, id: 'login_or_empty_prompt_message_title', text: '現在ログインしていません'
  # text_view :bookmark_shop_name, id: 'shopName'
  # text_view :festival_banner_text, id: 'home_top_festival_banner'
  # text_view :list_item, id: 'ichiba_list_item_title'
  # text_view :login_button, text: 'ログイン', tap_target: :Login
  # text_view :genre_button, text: 'ジャンル'
  # text_view :genre_button_from_nav, text: 'ジャンルで探す'
  # image_button :back_button
  # image_button :open_drawer, ImagecontentDescription: 'open_drawer'
  # text_view :all_ranking, 'android.widget.TextView', "{text ENDSWITH '総合ランキング'}", id: 'home_ranking_current_genre'
  # # search page
  # text_view :suggestions_list_genre_label, id: 'ichiba_list_suggestion_item_subtitle'
  # exit_text :search_edit_text, id: 'search_src_text'
  # text_view :barcode_search_link, text: 'バーコード検索'
  # text_view :search_history_list, id: 'ichiba_list_history_item_title'
  # list_view :suggestions_list, id: 'search_suggestions_list'
  # text_view :suggestions_item_list, id: 'ichiba_list_suggestion_item_title'
  # image_button :suggestions_add_icon_list, id: 'list_item_suggestion_view_right_icon'
  # text_view :delete_search_history_link, text: '検索履歴をすべて消去'
  # text_view :voice_search, id: 'voice_search'
  # image_button :go_back_home_button, Imageindex: 0
  # image_button :search_by_barcode_button, Imageid: 'home_code_search_button'
  # image_button :next_button_for_genre, id: 'NoResourceEntry-0'
  # # elements in navigation drawer
  # list_view :home_drawer_list, id: 'home_drawer_list'
  # text_view :setting_label, text: '設定'
  # text_view :browsing_history_label, text: '閲覧履歴'
  # text_view :bookmark_in_drawer, id: 'home_drawer_tv', text: 'お気に入り'
  # text_view :help, id: 'home_drawer_tv', text: 'ヘルプ'
  # # setting
  # text_view :mock_setting_label, text: 'Mock Settings'
  # check_box :enable_mocking_check_box, text: 'Enable Mocking'
  # text_view :prodution_label, text: 'Production'
  # # checked_text_view :staging_check_label, Checkedtext: 'Staging'
  # text_view :app_config_url_label, text: 'App Config Url'
  # checked_text_view :apk_internal_check_label, Checkedtext: 'APK-internal cached config file'
  # # camera page
  # text_view :text_on_camera_page, text: '赤い線をバーコードに合わせて読み取ります。'
  # # user information popup
  # button :pointclub_button, id: 'point_club_button'
  # button :my_rekuten_button, id: 'my_rakuten_button'
  # text_view :logout_button, text: 'ログアウト'
  # # logout confirm popup
  # button :logout_confirm_button, text: 'ログアウト'
  # # genre list popup
  # image_view :backward_genre_button, id: 'genre_tag_header_action_view_id'
  # image_view :forward_genre_list, id: 'NoResourceEntry-0'
  # button :genre_botom_button, text: '戻る'
  # text_view :cd_dvd_genre, text: 'CD・DVD・楽器'
  # text_view :dvd_genre, text: 'DVD'
  # text_view :western_wine_genre, text: 'ビール・洋酒'
  # # tab
  # text_view :bookmark_tab, id: 'tv_icon_title', text: 'お気に入り'

  # Element definition
  # rubocop:disable Metrics/MethodLength
  def initialize
    super('android.support.v4.view.ViewPager', getCurrentItem: 0)
    @header = Header.new(Home)
    @search = TabsSearch.new
    @campaign = TabsCampaign.new
    @bookmark = TabsBookmark.new
    @purchase_history = TabsPurchaseHistory.new
    @search_result = TabsSearchResult.new

    # home page
    @member_info_frame = View.new('HomeMemberInfoView', id: 'member_info_view')
    @member_name_label = TextView.new(id: 'home_member_name')
    @rank_label = TextView.new('android.widget.TextView', "{text ENDSWITH 'ランキング'}", id: 'card_title_view_title')
    @fashion_label = TextView.new('android.widget.TextView', "{text ENDSWITH '一押しファッション'}", id: 'card_title_view_title')
    @attract_label = TextView.new(text: '注目のイベント')
    @bookmark_label = TextView.new('android.widget.TextView', "{text BEGINSWITH 'お気に入り'}", id: 'card_title_view_title')

    @login_or_empty_prompt_message = TextView.new(id: 'login_or_empty_prompt_message_title', text: '現在ログインしていません')
    @bookmark_shop_name = TextView.new(id: 'shopName')
    @festival_banner_text = ImageView.new(id: 'home_top_festival_banner')
    @list_item = TextView.new(id: 'ichiba_list_item_title')
    @login_button = Button.new(text: 'ログイン', tap_target: Login)
    @dialog_login_button = View.new("android.widget.LinearLayout id:'ichiba_base_dialog' descendant android.widget.Button text:'ログイン'", tap_target: Login)
    @genre_button = TextView.new(text: 'ジャンル')
    @genre_button_from_nav = TextView.new(text: 'ジャンルで探す')
    @back_button = ImageButton.new
    @open_drawer = ImageButton.new(contentDescription: 'open_drawer')
    @all_ranking = TextView.new('android.widget.TextView', "{text ENDSWITH '総合ランキング'}", id: 'home_ranking_current_genre')
    @featured_events_detail = View.new(TextView.new(text: '注目のイベント').query.sibling('*', text: 'すべてを見る'), tap_target: TabsCampaign)

    # search page
    @suggestions_list_genre_label = TextView.new(id: 'ichiba_list_suggestion_item_subtitle')
    @search_edit_text = EditText.new(id: 'search_src_text')
    @barcode_search_link = TextView.new(text: 'バーコード検索')
    @search_history_list = TextView.new(id: 'ichiba_list_history_item_title')
    @suggestions_list = ListView.new(id: 'search_suggestions_list')
    @suggestions_item_list = TextView.new(id: 'ichiba_list_suggestion_item_title')
    @suggestions_add_icon_list = ImageView.new(id: 'list_item_suggestion_view_right_icon')
    @delete_search_history_link = TextView.new(text: '検索履歴をすべて消去')
    @voice_search = TextView.new(id: 'voice_search')
    @go_back_home_button = ImageButton.new(index: 0)
    @search_by_barcode_button = ImageButton.new(id: 'home_code_search_button')
    @next_button_for_genre = ImageView.new(id: 'NoResourceEntry-0')

    # elements in navigation drawer
    @home_drawer_list = ListView.new(id: 'home_drawer_list')
    @setting_label = TextView.new(text: '設定')
    @browsing_history_label = TextView.new(text: '閲覧履歴')
    @bookmark_in_drawer = TextView.new(id: 'home_drawer_tv', text: 'お気に入り')
    @help = TextView.new(id: 'home_drawer_tv', text: 'ヘルプ')
    # setting
    @mock_setting_label = TextView.new(text: 'Mock Settings')
    @enable_mocking_check_box = CheckBox.new(text: 'Enable Mocking')
    @prodution_label = TextView.new(text: 'Production')
    @staging_check_label = CheckedTextView.new(text: 'Staging')
    @app_config_url_label = TextView.new(text: 'App Config Url')
    @apk_internal_check_label = CheckedTextView.new(text: 'APK-internal cached config file')
    @menu_save_button = TextView.new(id: 'menu_save')
    # camera page
    @text_on_camera_page = TextView.new(text: '赤い線をバーコードに合わせて読み取ります。')
    # user information popup
    @pointclub_button = TextView.new(id: 'point_club')
    @my_rekuten_link = TextView.new(id: 'my_rakuten')
    @mycoupon_button = TextView.new(id: 'my_coupon')
    @edy_button = TextView.new(id: 'edy_button')

    @logout_button = TextView.new(text: 'ログアウト')
    # logout confirm popup
    @logout_confirm_button = Button.new(text: 'ログアウト')
    # genre list popup
    @backward_genre_button = ImageView.new(id: 'genre_tag_header_action_view_id')
    @forward_genre_list = ImageView.new(id: 'NoResourceEntry-0')
    @genre_botom_button = Button.new(text: '戻る')
    @cd_dvd_genre = TextView.new(text: 'CD・DVD・楽器')
    @dvd_genre = TextView.new(text: 'DVD')
    @western_wine_genre = TextView.new(text: 'ビール・洋酒')
    # tab
    @bookmark_tab = TextView.new(id: 'tv_icon_title', text: 'お気に入り')

    @smart_progress_loading_message = TextView.new(id: 'smart_progress_loading_message')
    @rakuten_point_button = TextView.new(id: 'tv_icon_title', text: '楽天ポイント')
  end

  def to_member_frame
    @smart_progress_loading_message.wait_gone
    member_info_frame.wait {
      flick_with_params('android.widget.ScrollView', {x: 50, y: 40}, {x: 50, y: 30})
    }
  end

  def logout
    to_member_frame
    if LinearLayout.new(id: 'home_member_info_logged_in').exist?
      goto_user_info
      @logout_button.wait {
        flick_with_params("* id:'point_rate_detail_contents'", {x: 50, y: 40}, {x: 50, y: 10})
      }
      sleep(0.5)
      @logout_button.tap
      @logout_confirm_button.tap
    end
    wait
  end

  def search_textbox
    TextView.new(id: 'fake_search_view', text: 'キーワードから検索').wait {
      flick_with_params("* id:'content'", {x: 50, y: 40}, {x: 50, y: 10})
    }
  end

  # confirm going to itemdetail screen from the bookmark tab of home screen
  def goto_itemdetail_from_bookmark(item)
    item = item.item_name if item.is_a?(Item)
    goto_bookmarks
    TabsBookmark.new.item(item).tap
  end
  # go to bookmark
  def goto_bookmarks
    View.new('*', id: 'tab_bookmark').tap
    wait_progress_bar_gone
    TabsBookmark.wait
  end

  def goto_bookmarks_from_nav
    @open_drawer.tap
    @bookmark_in_drawer.wait.tap
    wait_progress_bar_gone
    TabsBookmark.wait
  end

  # confirm going to cart screen from home screen
  def goto_cart
    header.toolbar.cart_button.tap
    sleep(0.5)
    wait_progress_bar_gone
    # TODO: refactor
    begin
      Cart.wait(timeout: 60)
    rescue StandardError
      raise unless Cart.exist?
    end
  end

  # go to genre
  def goto_genre_from_nav
    @open_drawer.tap
    @genre_button_from_nav.wait {
      flick_with_params("* id:'home_drawer_tv'", {x: 50, y: 10}, {x: 50, y: 30})
    }.tap
    # wait_progress_bar_gone
    Genre.wait
  end

  def goto_genre
    @genre_button.wait {
      flick_with_params("* id:'content'", {x: 50, y: 40}, {x: 50, y: 10})
    }
    sleep(0.5)
    @genre_button.tap
    TextView.new(id: 'ichiba_list_item_title').wait
  end

  # confirm the genre title
  def assert_genre_title(genre_title)
    TextView.new(id: 'title', text: genre_title).wait
  end

  # select genre
  def tap_genre_by_name(genre_name)
    TextView.new(text: genre_name).wait {
      flick_with_params("* id:'content'", {x: 50, y: 40}, {x: 50, y: 10})
    }
    # wait for animations
    sleep(1)
    TextView.new(text: genre_name).tap
  end

  def tap_this_genre(genre_name)
    LinearLayout.new(id: genre_name).tap
    SearchResult.wait
  end

  # select genre label
  def tap_genre_by_index(number)
    if number == -1
      @backward_genre_button.tap
    else
      @forward_genre_list.wait
      calabash.touch(@forward_genre_list.query[number])
    end
  end

  # search by genre
  def search_by_genre(genre_name)
    TextView.new(id: 'ichiba_list_item_title', text: genre_name).tap
    SearchResult.wait
  end

  # confirm user information
  def assert_user_info(user)
    TextView.new(text: user.last_name + ' ' + user.first_name).wait {
      flick_with_params('*', {x: 50, y: 50}, {x: 50, y: 30})
    }
  end

  def goto_user_info
    sleep(1)
    home_member_element = LinearLayout.new(id: 'home_member_title')
    home_member_element.wait {
      @smart_progress_loading_message.wait_gone
      flick_with_params("* id:'content'", {x: 50, y: 70}, {x: 50, y: 50})
    }
    unless header.tabs.campaign.wait.rect.center.y < home_member_element.rect.y
      flick_with_params("* id:'home_scroll'", {x: 1, y: 40}, {x: 1, y: 50})
    end
    home_member_element.tap
    View.new('android.widget.FrameLayout', id: 'point_rate_detail_contents').wait
  end

  def back_to_user_info
    @back_button.wait.tap
    TextView.new(text: '会員情報').wait
  end

  def goto_point_club(user = nil)
    user ||= App.user
    @pointclub_button.wait {
      flick_with_params("* id:'content'", {x: 50, y: 70}, {x: 50, y: 50})
    }.tap
    wait_progress_bar_gone(300)
    web_view = WebView.new(id: 'app_web', xpath: '//h1[@class="name"]/em').wait(timeout: 200)
    fail 'display of user name is not correct' if user.nil? || web_view.query.first['textContent'] != user.last_name + user.first_name
    PointClub.wait
  end

  def goto_mycoupon
    @mycoupon_button.wait {
      flick_with_params("* id:'content'", {x: 50, y: 70}, {x: 50, y: 50})
    }.tap
    sleep(1)
    wait_progress_bar_gone(300)
    # WebView.new(id: 'app_web', xpath: '//*[@id="likeArea"]/h2').wait(timeout: 200)
    MyCoupon.wait
  end

  def goto_my_rakuten
    @my_rekuten_link.wait {
      flick_with_params("* id:'content'", {x: 50, y: 70}, {x: 50, y: 50})
    }.tap
    sleep(1)
    wait_progress_bar_gone(300)
    MyRakuten.wait
  end

  def goto_edy
    @edy_button.wait {
      flick_with_params("* id:'content'", {x: 50, y: 40}, {x: 50, y: 10})
    }.tap
    sleep(1)
  end

  # confirm the bookmark cannot be displayed without login
  def assert_bookmark_not_shown
    member_info_frame.wait {
      flick_with_params("* id:'home_scroll'", {x: 50, y: 40}, {x: 50, y: 10})
    }
    @login_button.wait
    @attract_label.wait do
      flick_with_params("* id:'home_scroll'", {x: 50, y: 40}, {x: 50, y: 10})
      sleep(3)
    end
    # element_exists("android.widget.TextView id:'card_title_view_title' text:'お気に入り'")
    @bookmark_label.wait_gone
  end

  # confirm bookmark exist and can be tapped
  def assert_bookmark_not_shown_message
    header.tabs.bookmarks.tap
    @login_or_empty_prompt_message.wait
  end

  # tap search box and confirm
  def tap_search_box
    TextView.new(id: 'message').wait_gone && sleep(0.5)
    TextView.new(id: 'fake_search_view', text: 'キーワードから検索').wait {
      flick_with_params("* id:'home_scroll'", {x: 1, y: 40}, {x: 1, y: 30})
      sleep(0.5)
    }
    unless header.tabs.campaign.wait.rect.center.y < search_textbox.rect.y
      flick_with_params("* id:'home_scroll'", {x: 1, y: 40}, {x: 1, y: 50})
    end
    @search_edit_text.wait {
      search_textbox.tap
      calabash.hide_soft_keyboard
    }
    @voice_search.wait
  end

  # input text into search box
  def enter_search_box(keyword)
    @search_edit_text.text = keyword
  end

  # confirm genre on suggest list
  def assert_genre_shown_on_suggest_list
    @suggestions_list.wait
    @suggestions_list_genre_label.wait
  end

  # add suggest list into search box and check
  def tap_suggestion_by_index(suggestion_index)
    suggestion_text = @suggestions_item_list.text
    calabash.touch(@suggestions_add_icon_list.query.run[suggestion_index])
    edit_text = @search_edit_text.text
    sleep(5)
    expect(suggestion_text).to eq(edit_text.strip)
  end

  # confirm there is no history
  def assert_history_not_shown
    @delete_search_history_link.wait_gone
  end

  # confirm codebar is diaplyed
  def assert_barcode_shown
    @barcode_search_link.wait
    assert_history_not_shown
  end

  # make searches more than once
  def make_multiple_searches(number)
    1.upto(number) { |i|
      @search_edit_text.text = format('a%02d', i)
      calabash.press_enter_button
      wait_progress_bar_gone
      TextView.new(id: 'itemDesc').wait
      ImageView.new(id: 'search_close_btn').tap
    }
    SearchResult.wait
  end

  # confirm search histories
  def assert_search_histories_shown(first_history, last_history)
    flick_with_params('*', {x: 50, y: 50}, {x: 50, y: 80}) until search_textbox.exist?
    search_textbox.tap
    @barcode_search_link.wait_gone
    TextView.new(text: last_history).wait
    history_query = Query.new('android.widget.FrameLayout', id: 'fragment_container').to_s
    flick_with_params(history_query, {x: 50, y: 50}, {x: 50, y: 30}) until TextView.new(text: first_history).exist?
    flick_with_params(history_query, {x: 50, y: 50}, {x: 50, y: 30}) until @delete_search_history_link.exist?
  end

  # input many characters for searches
  def enter_characters_for_searches(number)
    input_text =
      case number
      when 1 then 'd'
      when 2 then 'dh'
      when 3 then 'dhc'
      when 4 then 'dhc '
      when 5 then 'dhc m'
      when 6 then 'dhc me'
      else 'dhc men '
      end
    @search_edit_text.text = input_text
  end

  # confirm suggest list under search textbox
  def assert_suggestion_list_shown
    @suggestions_item_list.wait
    @suggestions_add_icon_list.wait
  end

  # assert_error_message_shown in search page
  def assert_error_message_shown
    calabash.press_enter_button
    TextView.new(id: 'message', text: "漢字または2文字以上のキーワードを指定してください。\n1文字(英数ひらがなカタカナ)での検索はできません。").wait
    Button.new(id: 'button1', text: 'OK').tap
  end

  # select suggested option
  def tap_suggested_option
    @suggestions_item_list.tap
  end

  def tap_barcode
    @search_by_barcode_button.tap
  end

  def assert_message_camera_shown
    @text_on_camera_page.wait
  end

  def assert_message_festival_banner_shown
    @festival_banner_text.wait
  end

  def assert_message_genre_label_shown
    TextView.new(id: 'title', text: 'ジャンル選択').wait
  end

  def assert_the_first_genre_of_level_1_shown
    @cd_dvd_genre.wait
  end

  def flick_up_to_bottom_button
    calabash.flick_up until @genre_botom_button.exist?
  end

  def flick_down_to_top_button
    flick_with_params('*', {x: 50, y: 50}, {x: 50, y: 80}) until @cd_dvd_genre.exist?
  end

  def assert_genre_bottom_button_shown
    @genre_botom_button.wait
  end

  def tap_genre_on_level_1(genre = @cd_dvd_genre)
    TextView.new(id: 'ichiba_list_item_title', index: 0).wait
    genre_name = genre.text
    genre.wait {
      flick_with_params("* id:'content'", {x: 50, y: 40}, {x: 50, y: 10})
    }
    TextView.new(text: genre_name + 'を選択する').wait {
      genre.tap
    }
  end

  def tap_genre_on_next_level
    @next_button_for_genre.tap
    sleep(0.5)
    TextView.new(id: 'ichiba_list_item_title').wait
  end

  def tap_genre_on_last_level(genre_name)
    TextView.new(id: 'ichiba_list_item_title').wait
    TextView.new(text: genre_name).wait {
      flick_with_params("* id:'content'", {x: 50, y: 40}, {x: 50, y: 10})
    }.tap
    # TextView.new(text: genre_name).tap
    SearchResult.wait
  end

  def tap_genre_on_level_2
    @dvd_genre.tap
  end

  def assert_genre_of_second_level_shown
    @cd_dvd_genre.wait
  end

  def assert_current_genre_shown
    TextView.new(id: 'current_genre').wait
  end

  # search suggest item and transform to the rearch result page
  def tap_search_button
    calabash.press_enter_button
    sleep(5)
    SearchResult.wait
  end

  # make search
  def search(text)
    @search_edit_text.text = text
    calabash.press_enter_button
    SearchResult.wait
  end

  # clear_search_textbox
  def clear_search_textbox
    ImageView.new(id: 'search_close_btn').tap
  end

  # confirm search history
  def assert_search_history_part(keyword)
    flick_with_params('*', {x: 50, y: 40}, {x: 50, y: 80}) until search_textbox.exist?
    search_textbox.tap
    sleep(5)
    expect(@search_history_list.wait.text).to eq(keyword)
  end

  # go back home page
  def goto_home
    @go_back_home_button.tap
    Home.wait
  end

  # confirm bookmark item in bookmark
  def tap_item_on_bookmarks_banner(item_name)
    until @rank_label.exist?
      flick_with_params('*', {x: 50, y: 40}, {x: 50, y: 10})
    end
    TextView.new(text: item_name).tap
  end

  # select suggested option for search
  def make_suggestion_search(item_name)
    TextView.new(id: 'ichiba_list_suggestion_item_title', text: item_name).tap
    SearchResult.wait
  end

  # find item in bookmarks
  def find_item_in_bookmarks(item)
    TextView.new(id: 'home_bookmark_item_title', text: item.item_name, tap_target: ItemDetail.new(item.item_name)).wait {
      flick_with_params("* id:'content'", {x: 50, y: 40}, {x: 50, y: 10})
      @smart_progress_loading_message.wait_gone unless TextView.new(id: 'home_bookmark_item_title').exist?
    }
  end

  # tap the navigation drawer
  def tap_navigation_drawer
    @open_drawer.tap
    @genre_button_from_nav.wait
  end

  # confirm view history exist and can be tapped
  def tap_view_history
    @home_drawer_list.wait
    TextView.new(text: '閲覧履歴').wait {
      calabash.flick_up
    }
    sleep(0.5)
    TextView.new(text: '閲覧履歴').tap
    Login.wait
  end

  # confirm the browsing history cannot be displayed without login
  def assert_browsing_history_not_shown
    header.tabs.wait
    @attract_label.wait do
      flick_with_params('*', {x: 50, y: 40}, {x: 50, y: 10})
    end
    @browsing_history_label.wait_gone
  end

  # goto browsing histories
  def goto_browsing_history
    @home_drawer_list.wait {
      @open_drawer.tap
    }
    TextView.new(id: 'home_drawer_tv', text: '閲覧履歴').wait {
      flick_with_params(@home_drawer_list.query.to_s, {x: 50, y: 40}, {x: 50, y: 10})
    }
    BrowsingHistory.wait {
      TextView.new(id: 'home_drawer_tv', text: '閲覧履歴').tap
    }
  end

  # search by selecting history
  def search_by_history
    search_textbox.tap
    calabash.wait_for_keyboard
    calabash.hide_soft_keyboard
    @delete_search_history_link.wait {
      calabash.flick_up
    }
    TextView.new(id: 'ichiba_list_history_item_title').tap
    SearchResult.wait
  end

  def goto_the_particular_genre(uiquery)
    View.new(uiquery).tap
    wait_progress_bar_gone
    SearchResult.wait
  end

  def click_a_genre(uiquery)
    temp_view = View.new(uiquery)
    # until temp_view.wait(timeout: 3, timeout_message:"wine genre not exist")
    until temp_view.exist?
      flick_with_params("* id:'content'", {x: 50, y: 60}, {x: 50, y: 50})
    end
    temp_view.tap
    wait_progress_bar_gone
  end

  # confirm item in browsing
  def assert_item_shown_in_browsing_banner(item)
    sleep(3)
    # until TextView.new(id: 'card_title_view_title', text: '最近チェックした商品').exist? do
    #   flick_with_params('*', {x: 50, y: 40}, {x: 50, y: 10})
    #   sleep(1)
    # end
    TextView.new(id: 'home_browsing_history_item_title', text: item.item_name).wait(timeout: 30) {
      flick_with_params('*', {x: 50, y: 40}, {x: 50, y: 20})
      sleep(0.5)
    }
  end

  # tap and transform to the item detail from browsing history
  def tap_item_on_browsing_banner(item)
    TextView.new(text: item.item_name).tap
    wait_progress_bar_gone
    # query("android.widget.TextView id:'home_browsing_history_item_title', text:'cala商品f3'")
    ItemDetail.new(item.item_name).wait
  end

  # tap show all the browsing
  def tap_show_all_the_browsing
    query = Query.new('android.widget.TextView', id: 'home_browsing_history_item_title')
    query = query.parent('android.widget.RelativeLayout')
    query = query.descendant('android.widget.TextView', id: 'card_action_view_title')
    View.new(query).wait {
      flick_with_params('*', {x: 50, y: 60}, {x: 50, y: 40})
    }.tap
    BrowsingHistory.wait
  end

  def assert_navigation_drawer_shown
    expect(Query.new(id: 'home_drawer_tv').size).to be >= 3
  end

  def tap_bookmark_from_drawer
    @bookmark_in_drawer.tap
    wait_progress_bar_gone
    TabsBookmark.wait
  end

  def goto_search_tab
    header.tabs.search.tap
    TabsSearch.wait
  end

  def assert_search_history(items)
    array_item = []
    array = TextView.new(id: 'ichiba_list_history_item_title').query.run
    array.each do |item|
      array_item = array_item << item['text']
    end
    expect(items).to eq(array_item)
  end

  def assert_bookmark_item_and_number
    horizontal_query = Query.new('android.widget.TextView', "{text BEGINSWITH 'お気に入り'}", id: 'card_title_view_title')
    horizontal_query = horizontal_query.parent('android.widget.RelativeLayout').descendant('*', marked: 'horizontally_scrollable_buffer_view')
    View.new(horizontal_query).wait {
      flick_with_params('*', {x: 50, y: 40}, {x: 50, y: 20})
      @smart_progress_loading_message.wait_gone unless View.new(horizontal_query).exist?
    }
    array_item = []
    loop do
      array = TextView.new(id: 'home_bookmark_item_title').query.run
      array.each do |item|
        array_item << item['text'] unless array_item.include?(item['text'])
      end
      flick_with_params(horizontal_query.to_s, {x: 50, y: 50}, {x: 15, y: 50})
      sleep(0.5)
      break if View.new('android.support.v4.view.ViewPager', getCurrentItem: 1).exist?
    end
    expect(array_item.size).to eq(30)
    array_item
  end

  def go_to_western_wine_genre
    @cd_dvd_genre.wait
    flick_up_and_tap(@western_wine_genre)
  end

  def assert_browsing_history_item_and_number
    horizontal_query = Query.new('android.widget.TextView', "{text BEGINSWITH '閲覧履歴'}", id: 'card_title_view_title')
    horizontal_query = horizontal_query.parent('android.widget.RelativeLayout').descendant('*', marked: 'horizontally_scrollable_buffer_view')
    View.new(horizontal_query).wait {
      flick_with_params('*', {x: 50, y: 40}, {x: 50, y: 20})
    }
    array_item = []
    loop do
      array = TextView.new(id: 'home_browsing_history_item_title').query.run
      array.each do |item|
        array_item << item['text'] unless array_item.include?(item['text'])
      end
      flick_with_params(horizontal_query.to_s, {x: 70, y: 50}, {x: 30, y: 50})
      sleep(0.5)
      break if View.new('android.support.v4.view.ViewPager', getCurrentItem: 1).exist?
    end
    expect(array_item.size).to eq(50)
    array_item
  end

  def press_enter_button_confirm_current_screen
    calabash.press_enter_button
    @search_edit_text.wait
  end

  def search_by_search_history(search_keyword)
    TextView.new(id: 'ichiba_list_history_item_title', text: search_keyword).tap
    TextView.new(id: 'itemDesc').wait
    SearchResult.wait
  end

  def tap_help_in_navigator
    @help.wait(timeout: 10) {
      calabash.flick_up
    }
    @help.tap
    sleep(2)
    Help.wait
  end

  def get_genre_list(genre_name = '')
    result = []
    @genre_botom_button.wait do
      temp_result = @list_item.query.run
      temp_result.each do |genre_json|
        result += [genre_json['text']] unless result.include?genre_json['text']
      end
      flick_with_params("* id:'content'", {x: 50, y: 70}, {x: 50, y: 20})
    end
    temp_result = @list_item.query.run
    temp_result.each do |genre_json|
      result += [genre_json['text']] unless result.include?genre_json['text']
    end

    new_text = genre_name + 'を選択する'
    result.delete(new_text) if result.include?new_text
    flick_and_find(TextView.new(text: new_text), :down) unless genre_name == ''
    result
  end

  def click_genre_arrow(genre_name)
    arrow_feature = find_features_of_genre_arrow(genre_name)
    expect(arrow_feature['enabled']).to be_truthy
    calabash.perform_action('touch_coordinate', arrow_feature['x'], arrow_feature['y'])
    View.new(text: genre_name + 'を選択する').wait
  end

  def member_info_from_nav
    @open_drawer.tap
    TextView.new(Query.new("android.widget.ListView descendant * id:'member_info_detail_button'")).wait.tap
    TextView.new(id: 'member_future_granted_points_label').wait
  end

  def goto_login
    @login_button.wait {
      flick_with_params("* id:'content'", {x: 50, y: 40}, {x: 50, y: 30})
    }
    sleep(0.5)
    @login_button.tap
    Login.wait
  end

  def goto_help
    @login_button.wait {
      flick_with_params("* id:'content'", {x: 50, y: 40}, {x: 50, y: 10})
    }
    sleep(0.5)
    @login_button.tap
    Login.wait
  end

  def assert_member_info_on_nav(hash)
    hash.each do |id, text|
      TextView.new(id: id, text: text).wait
    end
  end

  def assert_page_transfer_on_nav_a(hash)
    hash.each do |text, textview_id|
      ImageButton.new(contentDescription: 'close_drawer').wait
      TextView.new(id: 'home_drawer_tv', text: text).wait {
        calabash.flick_up
      }.tap
      highlighted_icon_x = View.new(id: 'highlight_ani').rect.center.x
      icon_x = View.new(id: textview_id).rect.center.x
      expect(highlighted_icon_x).to eq(icon_x)
      @open_drawer.tap
    end
  end

  def assert_page_transfer_on_nav_b(hash)
    hash.each_key do |text|
      ImageButton.new(contentDescription: 'close_drawer').wait
      TextView.new(id: 'home_drawer_tv', text: text).wait {
        calabash.flick_up
      }.tap
      wait_progress_bar_gone
      View.new(text: text).wait
      calabash.press_back_button
      @open_drawer.wait.tap
    end
  end

  def assert_link_display_on_nav(hash)
    calabash.flick_up
    hash.each_key do |text|
      TextView.new(text: text).wait
    end
    TextView.new(id: 'home_member_info_coupon_title').wait {
      flick_with_params("* id:'home_drawer_list'", {x: 50, y: 10}, {x: 50, y: 40})
    }
    calabash.press_back_button
  end

  def assert_links_on_member_page(hash)
    TextView.new(text: '機能一覧').wait
    TextView.new(Query.new("android.widget.ListView descendant * id:'member_info_detail_button'")).tap
    TextView.new(id: 'logout_all_apps').wait {
      flick_with_params('android.widget.ScrollView', {x: 50, y: 40}, {x: 50, y: 10})
    }
    assert_info_on_page(hash)
    calabash.press_back_button
    @open_drawer.tap
  end

  def assert_info_on_page(hash)
    hash.each do |id, text|
      View.new(id: id, text: text).wait
    end
  end

  def featured_events_account
    featured_events_container_query = TextView.new(text: '注目のイベント').query.parent('android.widget.RelativeLayout').descendant('*', id: 'home_scrollable_content_view')
    featured_events_query = featured_events_container_query.descendant('android.widget.ImageView')
    View.new(featured_events_container_query).wait {
      @smart_progress_loading_message.wait_gone
      flick_with_params("* id:'content'", {x: 50, y: 40}, {x: 50, y: 30})
    }
    featured_events_array = []
    loop {
      calabash.query(featured_events_query.to_s, :url).each { |featured_event|
        featured_events_array << featured_event unless featured_events_array.include?(featured_event)
      }
      flick_with_params(featured_events_container_query.to_s, {x: 80, y: 50}, {x: 20, y: 50})
      break if View.new('android.support.v4.view.ViewPager', getCurrentItem: 1).exist?
    }
    header.tabs.home.tap
    Home.wait
    featured_events_array.size
  end

  def assert_suggestion_selected(num)
    @suggestions_item_list.query[num]['text']
  end

  def tap_suggestion_title(suggestion_text)
    TextView.new(id: 'ichiba_list_suggestion_item_title', text: suggestion_text).tap
    SearchResult.wait
  end

  def search_history_does_not_exist(search_name)
    history_name = TextView.new(id: 'ichiba_list_history_item_title', text: search_name)
    @delete_search_history_link.wait(timeout_message: "History missing: #{search_name}") {
      flick_with_params('android.widget.ListView', {x: 50, y: 50}, {x: 50, y: 30})
    }
    history_name.wait_gone
  end

  def assert_search_history_exist(item)
    history_name = TextView.new(id: 'ichiba_list_history_item_title', text: item)
    history_name.wait(timeout_message: "History missing: #{item}") {
      flick_with_params('*', {x: 50, y: 40}, {x: 50, y: 20})
    }
  end
end
