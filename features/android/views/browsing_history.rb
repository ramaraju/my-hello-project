require_relative '../utils/utils_android'

class BrowsingHistory < Page
  include IchibaApp::AndroidHelpers

  def initialize
    super('android.widget.TextView', id: 'search_condition', text: '閲覧履歴')
    # current page
    @not_checked_check_box = CheckBox.new(id: 'deleteCheckBox', checked: false)
    @delete_button = Button.new(id: 'deleteBrowsingHistoryButton')
    @item_name_list = TextView.new(id: 'itemName')
    @search_count = TextView.new(id: 'search_count')

    # delete confirm popup
    @delete_confirm_yes = Button.new(text: 'はい')
  end

  def clear_browsing_history
    wait_progress_bar_gone
    sleep(1)
    if @search_count.exist?
      @not_checked_check_box.tap
      y_max = @delete_button.rect.y
      while @not_checked_check_box.exist?
        check_box_group = @not_checked_check_box.query.run
        check_box_group.each do |check_box|
          calabash.touch(check_box) if check_box['rect']['y'] < y_max
        end
        flick_with_params("* id:'content'", {x: 50, y: 40}, {x: 50, y: 20})
        sleep(0.5)
      end
      @delete_button.tap
      @delete_confirm_yes.tap
      wait_progress_bar_gone
    end
    ImageButton.new.tap
    Home.wait
  end

  def tap_item(item)
    view = TextView.new(id: 'itemName', text: item.item_name, tap_target: ItemDetail.new(item.item_name))
    view.wait {
      flick_with_params("* id:'content'", {x: 50, y: 20}, {x: 50, y: 10})
    }
    # wait for animations
    sleep(0.5)
    view.tap!
  end

  # tap delete browsing history
  def delete_browsing_history(item)
    slide_up_to_confirm_item_browsing(item)
    string_name = TextView.new(id: 'itemName').text
    # string_name = query("android.widget.TextView id:'itemName'")FF[0]['text']
    if item.item_name == string_name
      @delete_button.wait {
        calabash.touch("* marked:'#{item.item_name}' parent * marked:'linearLayout2' descendant * marked:'deleteCheckBox'")
      }.tap
      @delete_confirm_yes.tap
    end
  end

  # confirm item cannot be displayed from browsing history
  def assert_item_gone(item)
    # sleep(5)
    # fail 'Cannot be deleted' unless element_does_not_exist("android.widget.TextView text:'#{item_name}'")
    TextView.new(text: item.item_name).wait_gone
  end

  def assert_item_list_same_as_home_browsing(query_item)
    wait_progress_bar_gone
    array_item = []
    50.times {
      array = TextView.new(id: 'itemName').query.run
      array.each do |item|
        array_item << item['text'] unless array_item.include?(item['text'])
      end
      flick_with_params('*', {x: 50, y: 50}, {x: 50, y: 30})
      sleep(0.5)
      break if array_item.length >= 50
    }
    fail "bookmark is not matched\n :#{query_item} \n #{array_item}:" unless query_item == array_item[0, 50]
  end

  def slide_up_to_confirm_item_browsing(item)
    TextView.new(text: item.item_name).wait(timeout: 120) {
      flick_with_params('*', {x: 50, y: 30}, {x: 50, y: 70})
    }
  end

  def item_label(item_name)
    TextView.new(text: item_name)
  end
end
