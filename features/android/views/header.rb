class Header
  attr_reader :toolbar, :tabs

  def initialize(page)
    @toolbar = Kernel.const_get("Toolbar#{page}").new
    @tabs = Kernel.const_get("Tabs#{page}").new
  end
end

# home page
class ToolbarHome < View
  attr_reader :cart_button

  def initialize
    super('android.view.View', id: 'app_toolbar')
    @cart_button = TextView.new(id: 'tv_icon_title', text: '買い物かご')
  end
end

class TabsHome < Widget('RelativeLayout')
  attr_reader :home, :search, :campaign, :bookmarks, :history

  def initialize
    super(id: 'home_tabs')
    @home = TextView.new(id: 'tv_icon_title', text: 'ホーム')
    @search = TextView.new(id: 'tv_icon_title', text: '検索', tap_target: TabsSearch)
    @campaign = TextView.new(id: 'tv_icon_title', text: 'キャンペーン', tap_target: TabsCampaign)
    @bookmarks = TextView.new(id: 'tv_icon_title', text: 'お気に入り', tap_target: TabsBookmark)
    @history = TextView.new(id: 'tv_icon_title', text: '購入履歴', tap_target: TabsPurchaseHistory)
  end
end

# search result page
class ToolbarSearchResult < LinearLayout
  attr_reader :back_home, :cart_button, :search_box, :clear_text

  def initialize
    super(id: 'search_result_toolbar')
    @back_home = ImageButton.new(Query.new('android.widget.LinearLayout', id: 'search_result_toolbar').descendant('android.widget.ImageButton'))
    @cart_button = TextView.new(id: 'tv_icon_title', text: '買い物かご')
    @search_box = EditText.new(id: 'search_src_text')
    @clear_text = ImageView.new(id: 'search_close_btn')
  end
end

class TabsSearchResult < View
  attr_reader :genre, :detail_search, :new_search, :sort, :display_style

  def initialize
    super('FrameLayout', id: 'search_result_header')
    @genre = TextView.new(id: 'tv_icon_title', text: 'ジャンル')
    @detail_search = TextView.new(id: 'tv_icon_title', text: '詳細検索')
    @new_search = TextView.new(id: 'tv_icon_title', text: '新規検索')
    @sort = TextView.new(id: 'tv_icon_title', text: '並び順')
    @display_style = TextView.new(id: 'tv_icon_title', text: '表示切替')
  end
end
