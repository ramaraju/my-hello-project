require 'calabash-android/abase'
require_relative 'header'
require_relative '../utils/utils_android'

# rubocop:disable Metrics/ClassLength
class TabsBookmark < Page
  include IchibaApp::AndroidHelpers
  include RSpec::Matchers

  frame_layout :view_mode, id: 'button_viewmode'
  attr_reader :header
  def initialize
    super('android.support.v4.view.ViewPager', getCurrentItem: 3)
    @header = Header.new(Home)
    @go_back_home_button = ImageButton.new(index: '0')
    @items_exist_in_bookmark = Button.new(id: 'spinner_sort')
    @show_all_bookmark = TextView.new(text: 'すべてを見る')
    @bookmark_item_name = TextView.new(id: 'itemDesc')
    @change_grid = TextView.new(id: 'grid')
    # view_mode = View.new(id: 'button_viewmode' )
    @bookmark_label = TextView.new(id: 'card_title_view_title', text: 'お気に入り')
    @login_button = Button.new(text: 'ログイン')
    @item_query = Query.new(id: 'itemDesc')
    @progress_bar = ProgressBar.new(id: 'ptr_progress')
  end

  def assert_bookmark_page_reached
    q1 = Query.new(id: 'spinner_sort')
    q2 = Query.new(id: 'login_or_empty_prompt_message_title', text: 'お気に入り商品が登録されていません')
    fail 'bookmark page not reached' if q1.empty? && q2.empty?
  end

  def assert_text_on_page_as_id(input_text)
    Query.new(id: input_text).wait
  end

  def assert_bookmark_page_with_no_bookmark_reached
    Query.new(id: 'login_or_empty_prompt_message_title', text: 'お気に入り商品が登録されていません').wait
  end

  def delete_all_items_in_bookmark
    unless Query.new(id: 'itemDesc').empty?
      3.times { flick_with_params("* id:'content'", {x: 50, y: 50}, {x: 50, y: 90}) }
      query_for_item_to_delete = Query.new(text: '', checked: false)
      query_for_item_to_delete.wait
      result = query_for_item_to_delete.run
      until result.empty?
        until result.empty?
          calabash.touch(query_for_item_to_delete.to_s)
          result = query_for_item_to_delete.run
        end
        View.new('*', id: 'btn_delete_bookmark_items').tap
        Button.new(text: 'はい').wait.tap
        wait_progress_bar_gone
        View.new('android.support.v4.view.ViewPager', getCurrentItem: 3).wait
        result = query_for_item_to_delete.run
      end
    end
  end

  def go_back_home_page
    View.new(id: 'tab_home').wait.tap
    Home.wait
  end

  def add_items_into_bookmark(i_number)
    go_back_home_page.goto_genre_from_nav
    TextView.new(id: 'ichiba_list_item_title').tap
    View.new(id: 'genre_title_and_button').wait.tap
    wait_progress_bar_gone
    i_number.times {
      calabash.flick_up
      calabash.wait_for_animations
      item_detail = SearchResult.new.tap_item
      item_detail.add_bookmark
      item_detail.tap_back
    }
    SearchResult.new.go_back_home_page.goto_bookmarks
  end

  def item(item)
    item = item.item_name if item.is_a?(Item)
    @bookmark_item_name.wait
    TextView.new(text: item.to_s).wait {
      flick_with_params("* id:'content'", {x: 50, y: 50}, {x: 50, y: 30})
    }
    sleep(0.5)
    TextView.new(text: item, tap_target: ItemDetail.new(item))
  end

  alias_method :bookmark, :item

  def item_label(item_name)
    TextView.new(text: item_name)
  end

  def tap_item(item)
    item(item).tap
    wait_progress_bar_gone
    ItemDetail.new(item).wait
  end

  # tap show all the bookmark
  def tap_show_all_bookmark
    @fashion_label.wait {
      flick_with_params('*', {x: 50, y: 40}, {x: 50, y: 30})
    }
    @show_all_bookmark.wait.tap
  end

  # check bookmark items on home page and item detail page
  def check_bookmark_items(items)
    @bookmark_item_name.wait
    items.each { |item|
      item_object = ITEMS.search(item: item.first)
      shop_object = SHOPS.search(shop: item_object.shop)
      @bookmark_label.tap_if_exist
      View.new("#{@bookmark_item_name.query} text:'#{item_object.item_name}'").wait do
        flick_with_params("* id:'content'", {x: 50, y: 50}, {x: 50, y: 30})
      end
      shopname_local = TextView.new(text: item_object.item_name).query.parent('*', marked: 'descContainer').descendant('*', marked: 'shopName')
      shopname_list = shopname_local.run
      if shopname_list == []
        flick_with_params("* id:'content'", {x: 50, y: 50}, {x: 50, y: 40})
        shopname_list = shopname_local.run
      end
      shopname_actual = shopname_list.first
      expect(shopname_actual['text']).to eq(shop_object.shop_name)
      calabash.wait_for {
        calabash.wait_for_animations
        next true if ItemDetail.new(item_object.item_name).open?
        TextView.new(text: item_object.item_name).tap_if_exist
      }
      wait_progress_bar_gone
      calabash.press_back_button
      calabash.flick_down
    }
  end

  def select_bookmark(item)
    item_check_box = View.new(TextView.new(text: item.item_name).query.parent('*', id: 'container').descendant('android.widget.CheckBox'))
    slide_up_to_confirm_item(item)
    @bookmark_item_name.wait
    if item_check_box.exist?
      y1 = item_check_box.rect.y
      y2 = view_mode.rect.y + view_mode.rect.height
      flick_with_params("* id:'content'", {x: 50, y: 60}, {x: 50, y: 70}) if y1 < y2
    end
    item_check_box.tap
  end

  # tap delete bookmark button
  def tap_delete_bookmark_button
    Button.new(text: 'はい').wait {
      Button.new(text: 'お気に入り削除').tap
    }.tap
    wait_progress_bar_gone
  end

  # confirm item disappears
  def assert_item_not_shown(item)
    sleep(1)
    TextView.new(text: item.item_name).wait_gone
  end

  def assert_bookmark_display_order_are_changeable
    wait_progress_bar_gone
    @items_exist_in_bookmark.wait.tap
    Query.new("* id:'text1'").wait
    query_result = calabash.query("* id:'text1'")

    4.times do |i|
      j = (i + 1) % 4
      btn = CheckBox.new('*', text: query_result[j]['text'])
      next if btn.checked?
      btn.tap
      wait_progress_bar_gone
      @bookmark_item_name.wait
      btn.wait
      @items_exist_in_bookmark.tap
      Query.new("* id:'text1'").wait
    end
    calabash.press_back_button
  end

  def wait_for_item_to_display(query_for_item)
    Query.new(query_for_item).wait
  end

  def assert_the_user_is_logged_out
    @login_button.wait
  end

  def goto_login
    @login_button.tap
    Login.wait
  end

  def verify_order_banner_exist
    @items_exist_in_bookmark.wait
  end

  def verify_order_banner_not_exist
    @items_exist_in_bookmark.wait_gone
  end

  def verify_grid_button_exist
    view_mode.wait
  end

  def verify_grid_button_not_exist
    view_mode.wait_gone
  end

  def change_view
    view_mode.wait {
      calabash.flick_down
    }.tap
  end

  def assert_now_is_list_view
    item_list = Query.new(id: 'itemImage').run
    expect(item_list.size).to be > 1
    item_list.each do |item|
      expect(item['rect']['x']).to eq(item_list[0]['rect']['x'])
    end
  end

  def assert_now_is_double_colomn_view
    item_list = Query.new(id: 'itemImage').run
    expect(item_list.size).to be > 1
    error_msg = 'the current item view is not double colomn view'
    1.upto(item_list.size - 1) do |i|
      raise error_msg if i.odd? && (item_list[i]['rect']['center_y'] != item_list[i - 1]['rect']['center_y'] || (i > 1 && item_list[i]['rect']['center_x'] != item_list[i - 2]['rect']['center_x']))
      raise error_msg if i.even? && item_list[i]['rect']['center_x'] != item_list[i - 2]['rect']['center_x']
    end
  end

  def assert_now_is_single_image_view
    item_list = Query.new(id: 'itemImage').run
    expect(item_list.size).to eq(1)
  end

  def assert_different_views
    assert_now_is_list_view
    change_view

    assert_now_is_double_colomn_view
    change_view

    assert_now_is_single_image_view

    change_view
    assert_now_is_list_view
  end

  def assert_item_list_for_over_50_items_can_be_displayed
    @bookmark_item_name.wait
    expect(list_items(@item_query, :up).size).to be > 50
    sleep(1)
    expect(list_items(@item_query, :down).size).to be > 50
  end

  def assert_item_list_same_as_home(query_item)
    array_item = []
    30.times {
      array = TextView.new(id: 'itemDesc').query.run
      array.each do |item|
        array_item << item['text'] unless array_item.include?(item['text'])
      end
      flick_with_params('*', {x: 50, y: 50}, {x: 50, y: 30})
      sleep(0.5)
      string_num = array_item.size.to_s
      break if Integer(string_num) >= 30
    }
    expect(query_item).to eq(array_item[0, 30])
  end

  def slide_up_to_confirm_item(item)
    TextView.new(text: item.item_name).wait(timeout: 120) {
      flick_with_params("* id:'content'", {x: 50, y: 40}, {x: 50, y: 70})
    }
  end
end
