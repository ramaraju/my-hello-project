require 'rspec/expectations'
require 'rspec-benchmark'
require_relative '../utils/utils_android'

# rubocop:disable Metrics/ClassLength
class Review < Page
  include IchibaApp::AndroidHelpers, RSpec::Benchmark::Matchers, RSpec::Matchers

  button :item_review, text: '商品レビュー'
  button :shop_review, text: 'ショップレビュー'
  button :filter_item_review, text: '絞込/並び順', id: 'Btn_FocusOrSort'
  button :filter_shop_review, text: '絞込', id: 'Btn_FocusOrSort'
  rating_bar :review_average_star, id: 'reviewAverage'
  linear_layout :review_info_bar, id: 'review_info_bar'
  text_view :item_name, id: 'itemDesc'
  text_view :shop_name, id: 'shop_main'
  text_view :review_average_label, id: 'reviewAverage_string'
  text_view :review_average_not_enough, id: 'not_enough'
  text_view :review_average_not_enough_message, id: 'message', text: 'レビュー件数が不足しています。'
  text_view :review_count_label, id: 'reviewCount'
  text_view :reviewer_name, id: 'reviewer_name'
  text_view :review_date, id: 'review_date'
  text_view :reviewer_age_sex, id: 'reviewer_age_sex'
  text_view :bought_role, id: 'bought'
  text_view :review_rate, id: 'reviewrate'
  text_view :review_rate_string, id: 'reviewrate_string'
  text_view :cart, text: '買い物かご'
  image_button :back
  # Review Filter
  text_view :filter_star, text: '☆の数'
  text_view :filter_reviewer, text: 'レビュアー'
  text_view :filter_age, text: 'レビュアーの年齢'
  text_view :filter_gender, text: 'レビュアーの性別'
  text_view :filter_image, text: '投稿画像'
  text_view :filter_sort, text: '並び順'
  button :search, text: '上の条件で検索'
  button :clear, text: 'クリア'
  # alert:★の数で絞り込み
  text_view :alert_title_star, id: 'alertTitle'
  # alert:レビュアーを選択
  text_view :alert_title_reviewer, id: 'alertTitle', text: 'レビュアーを選択'
  # alert:レビュアーの年代を選択
  text_view :alert_title_age, id: 'alertTitle', text: 'レビュアーの年代を選択'
  # alert:レビュアーの性別を選択
  text_view :alert_title_gender, id: 'alertTitle', text: 'レビュアーの性別を選択'
  # alert:投稿画像の有無を選択
  text_view :alert_title_image, id: 'alertTitle', text: '投稿画像の有無を選択'
  # alert:並び順を選択
  text_view :alert_title_sort, id: 'alertTitle', text: '並び順を選択'
  CONDITION = {
    star: {
      all: 'すべて',
      five: '5',
      four: '4',
      three: '3',
      two: '2',
      one: '1'
    },
    reviewer: {
      bought: '購入者のみ',
      all: '未購入者含む'
    },
    age: {
      all: '全年代',
      ten: '10代',
      twenty: '20代',
      thirty: '30代',
      forty: '40代',
      fifty: '50代以上'
    },
    gender: {
      all: '全性別',
      male: '男性',
      female: '女性'
    },
    image: {
      all: 'すべて',
      image: '画像あり'
    },
    sort: {
      normal: '標準',
      new: '新着レビュー順',
      better: '商品評価が高い順'
    }
  }.freeze

  def initialize(item, review_count)
    super('android.widget.TextView', text: 'レビュー・口コミ')
    @item = item
    @review_count = review_count
  end

  def open_filter_settings(page)
    tap_element = page.equal?(:item) ? filter_item_review : filter_shop_review
    tap_element.tap
    View.new("android.view.ViewGroup id:'app_toolbar' descendant * {text BEGINSWITH '絞込'}").wait
  end

  def verify_item_review
    wait_progress_bar_gone
    item_review.wait
    current_title_color = calabash.query("android.widget.Button id:'Btn_ItemReview'", :getTextColors, :getDefaultColor)
    expect(current_title_color.first).to eq(-1)
    if @review_count > 3
      expect(review_average_star.rating).to be > 0
      review_average_element_string = review_average_label
    else
      expect(review_average_star.rating).to eq(0)
      review_average_element_string = review_average_not_enough
      review_average_not_enough_message.wait {
        review_info_bar.tap
      }
    end
    expect(item_name.text).to end_with(@item.item_name)
    expect(review_average_element_string).to exist
    expect(review_count_label.text).to eq("#{@review_count}件")
  end

  def verify_shop_review
    wait_progress_bar_gone
    shop_review.wait
    current_title_color = calabash.query("android.widget.Button id:'Btn_ShopReview'", :getTextColors, :getDefaultColor)
    expect(current_title_color.first).to eq(-1)
    shop_review_count = review_count_label.text.to_number
    if shop_review_count > 3
      expect(review_average_star.rating).to be > 0
      review_average_element_string = review_average_label
    else
      expect(review_average_star.rating).to eq(0)
      review_average_element_string = review_average_not_enough
      review_average_not_enough_message.wait {
        review_info_bar.tap
      }
    end
    shop = SHOPS.search(shop: @item.shop)
    expect(shop_name.text).to eq("#{shop.shop_name} の総合評価")
    expect(review_average_element_string).to exist
  end

  def verify_search_condition(condition, page)
    current_condition = calabash.query("android.widget.TextView id:'selected'", :text)
    current_condition << calabash.query("android.widget.TextView id:'sort_selected'", :text).first
    return if condition != :default
    expect_condition = page == :item ? ['すべて', '購入者のみ', '全年代', '全性別', 'すべて', '標準'] : ['購入者のみ', '全年代', '全性別']
    expect(current_condition).to eq(expect_condition)
  end

  # params: condition_hash = {star: 'all', ...}
  # condition_hash keys: [:star, :reviewer, :age, :gender, :image, :sort]
  # condition_hash values:
  #         star: CONDITION[:star].keys
  #         reviewer: CONDITION[:reviewer].keys
  #         age: CONDITION[:age].keys
  #         gender: CONDITION[:gender].keys
  #         image: CONDITION[:image].keys
  #         sort: CONDITION[:sort].keys
  def select_search_condition(condition)
    @filter_condition = condition
    condition.each { |key, value|
      key = key.to_sym if key.is_a?(String)
      if key.equal?(:star) && TextView.new(text: 'レビュー・口コミ').exist?
        page = TextView.new(text: 'レビュー・口コミ')
        filter_condition = review_info_bar
      else
        filter_condition = send("filter_#{key}")
        page = TextView.new(text: '絞込条件')
      end
      filter_condition.tap
      send("alert_title_#{key}").wait
      condition_index = CONDITION[key].keys.index(value.to_sym)
      CheckedTextView.new(index: condition_index).tap
      page.wait
    }
  end
  # rubocop:disable Metrics/MethodLength, Metrics/BlockLength, Metrics/CyclomaticComplexity, Metrics/PerceivedComplexity
  def verify_result_count(result_num, page)
    if result_num.zero?
      TextView.new(id: 'message', text: '指定の条件でのレビューは登録されていません。').wait
    else
      TextView.new(id: 'message', text: "#{result_num}件のレビューが見つかりました。").wait
      TextView.new(text: 'レビュー・口コミ').wait
      result_list = []
      100.times {
        review_detail = calabash.query("android.widget.LinearLayout id:'review_contents' descendant android.widget.TextView", :text)
        id_array = calabash.query("android.widget.LinearLayout id:'review_contents' descendant android.widget.TextView", :id)
        review_detail = review_detail.drop(id_array.index('reviewer_name')) if id_array.first != 'reviewer_name'
        element_num_per_review = page == :item ? 7 : 19
        review_detail_array = review_detail.reject(&:empty?).each_slice(element_num_per_review).to_a
        break if review_detail_array.last == result_list.last
        review_detail_array.each { |review_detail_data|
          break if review_detail_data.size != element_num_per_review
          result_list << review_detail_data unless result_list.include?(review_detail_data)
        }
        flick_with_params("* id:'reviewListView'", {x: 50, y: 50}, {x: 50, y: 30})
      }
      @filter_condition.each { |key, value|
        key = key.to_sym if key.is_a?(String)
        if key == :sort
          case value.to_sym
          when :new
            index = 2
          when :better
            index = 5
          end
          sort_list = []
          result_list.each { |result| sort_list << result[index].scan(/\d+/).join }
          sort_list.reject!(&:empty?)
          expect(sort_list).to eq(sort_list.sort.reverse)
        else
          result_list.each { |result|
            index = nil
            case key
            when :star
              index = 5
            when :age, :gender
              index = 3
            when :reviewer
              index = 4
            end
            break if index.nil?
            if key == :reviewer
              expect(CONDITION[key][value.to_sym]).to start_with(result[index])
            elsif key == :age && CONDITION[key][value.to_sym].scan(/\d+/).join == '50'
              expect(result[index].scan(/\d+/).join).to be >= CONDITION[key][value.to_sym].scan(/\d+/).join
            else
              expect(result[index]).to include(CONDITION[key][value.to_sym])
            end
          }
        end
      }
    end
  end
  # rubocop:enable all
  def open_cart
    cart.tap
    wait_progress_bar_gone
    Cart.wait
  end
end
