require_relative '../utils/utils_android'

class Genre < Page
  include IchibaApp::AndroidHelpers

  def initialize(title = 'ジャンル選択')
    super('android.widget.TextView', text: title)

    @button_submit = Button.new(text: '上の条件を選択する')
    @clear_button = Button.new(id: 'button_clear')
  end

  def select_genre(value)
    value = value.is_a?(Hash) ? value.values : [value] unless value.is_a?(Array)
    genre_first = value.shift
    genre_last = value.pop
    TextView.new(id: 'ichiba_list_item_title').wait
    TextView.new(id: 'ichiba_list_item_title', text: genre_first).wait {
      flick_with_params("* id:'content'", {x: 50, y: 30}, {x: 50, y: 10})
    }
    # wait for animations
    sleep(0.5)
    TextView.new(id: 'ichiba_list_item_title', text: genre_first).tap
    if genre_last.nil?
      LinearLayout.new(id: 'genre_title_and_button').tap
    else
      unless value.empty?
        value.each do |genre|
          TextView.new(id: 'ichiba_list_item_title').wait
          genre_name = TextView.new(id: 'ichiba_list_item_title', text: genre)
          genre_name = genre_name.query.parent("* id:'list_item_action_view_layout'").descendant('android.widget.ImageView').to_s
          TextView.new(id: 'ichiba_list_item_title').wait
          View.new(genre_name).wait {
            flick_with_params("* id:'content'", {x: 50, y: 30}, {x: 50, y: 10})
          }
          # wait for animations
          sleep(0.5)
          View.new(genre_name).tap
        end
      end
      TextView.new(id: 'ichiba_list_item_title').wait
      TextView.new(id: 'ichiba_list_item_title', text: genre_last).wait {
        flick_with_params("* id:'content'", {x: 50, y: 30}, {x: 50, y: 10})
      }.tap
    end
  end

  def select_tag(tags_array)
    last_tag_label = TextView.new(text: tags_array.pop)
    unless tags_array.empty?
      tags_array.each do |tag|
        tag_label = TextView.new(text: tag)
        forward_image = View.new(tag_label.query.parent("android.widget.LinearLayout id:'list_item_action_view_layout'")
                                     .descendant('android.widget.ImageView'))
        TextView.new(id: 'ichiba_list_item_title').wait
        tag_label.wait do
          flick_with_params("* id:'content'", {x: 50, y: 30}, {x: 50, y: 10})
        end
        if forward_image.exist?
          forward_image.tap
        else
          reveal_view(tag_label).tap
        end
        sleep(0.1)
      end
    end
    last_tag_label.wait do
      flick_with_params("* id:'content'", {x: 50, y: 30}, {x: 50, y: 10})
    end
    reveal_view(last_tag_label).tap
    @button_submit.tap_if_exist
  end

  def reveal_view(element)
    if @clear_button.exist?
      y_max = @clear_button.rect.y
      element_y = element.rect.y + element.rect.height
      flick_with_params("* id:'content'", {x: 50, y: 50}, {x: 50, y: 40}) if element_y >= y_max
    end
    element
  end
end
