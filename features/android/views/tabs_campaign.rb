require_relative 'header'

class TabsCampaign < Page
  attr_reader :campaign_events, :header

  def initialize
    super('android.support.v4.view.ViewPager', getCurrentItem: 2)

    @header = Header.new(Home)
    @campaign_events = View.new(TextView.new(id: 'campaign_tab_header_title', text: '注目のイベント')
    .query.parent('android.widget.ListView').descendant('android.widget.ImageView', '{tag CONTAINS "scid=wi_ich_androidapp_homeCampaignTab_event"}'))
  end
end
