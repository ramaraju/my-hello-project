require_relative 'web_page_utils'

class PointClub < WebPageUtils
  attr_reader :no_point, :all_point, :part_point, :num_point
  attr_reader :submit_button
  def initialize
    super(id: 'app_web')

    # # ポイントを利用しない
    # @no_point = WebView.new(id: 'app_web', xpath: '//input[@value="-1"]')
    # # 全てのポイントを使う
    # @all_point = WebView.new(id: 'app_web', xpath: '//input[@value="0"]')
    # # 一部のポイントを使う
    # @part_point = WebView.new(id: 'app_web', xpath: '//input[@value="1"]')
    # @num_point = WebView.new(id: 'app_web', xpath: '//input[@name="num_point"]')
    # @submit_button = WebView.new(id: 'app_web', xpath: '//input[@value="内容を変更する"]', tap_target: Step2)
  end

  def assert_point_club_page_reached
    Query.new(id: 'app_web', xpath: '//*[@id="header"]/div[1]/div/strong/a').first['textContent'] == '楽天PointClub'
  end

  def back_to_user_info
    ImageButton.new.wait.tap
    TextView.new(text: '会員情報').wait
    Home.wait
  end
end
