require_relative 'web_page_utils'

class WebCommon < WebPageUtils
  def self.forget_password
    WebView.new(id: 'app_web', xpath: '//*[@id="member"]')
  end
  def self.register_new_account
    WebView.new(id: 'app_web', xpath: '//h1[@id="member" and text()="楽天会員登録"]')
  end
  def self.opinions_and_requests
    WebView.new(id: 'app_web', xpath: '//div[@id="hHeader" and contains(.,"楽天市場アプリについてのご意見・ご要望")]')
  end
  def self.close_web(page)
    if ImageView.new(id: 'home_menu_close').exist?
      ImageView.new(id: 'home_menu_close').tap
    else
      calabash.press_back_button
    end
    page.wait {
      flick_with_params("* id:'content'", {x: 2, y: 30}, {x: 2, y: 70})
    }
  end
end
