require_relative 'web_page_utils'

class Address < WebPageUtils
  ab_test

  def update_address(address)
    update_address_name(address)
    next_button.tap
    update_address_zip(address)
    next_button.tap
    update_address_detail(address)
  end

  def update_address_name(address)
    [:lname, :fname, :lname_kana, :fname_kana].each { |key|
      case key
      when :lname
        value = address.lastname
      when :fname
        value = address.firstname
      when :lname_kana
        value = address.lastname_kana
      when :fname_kana
        value = address.firstname_kana
      end
      send(key).wait
      calabash.clear_text_in(send(key).query.to_s)
      calabash.keyboard_enter_text(value)
      calabash.hide_soft_keyboard
      sleep(0.5)
    }
  end

  def update_address_zip(address)
    [:zip1, :zip2].each { |key|
      value = address.send(key)
      send(key).wait
      calabash.clear_text_in(send(key).query.to_s)
      calabash.keyboard_enter_text(value)
      calabash.hide_soft_keyboard
      sleep(0.5)
    }
  end

  def update_address_detail(address)
    [:city, :street, :tel1, :tel2, :tel3].each { |key|
      case key
      when :city
        value = address.city
      when :street
        value = address.street
      when :tel1
        value = address.telephone1
      when :tel2
        value = address.telephone2
      when :tel3
        value = address.telephone3
      end
      send(key).wait
      calabash.clear_text_in(send(key).query.to_s)
      calabash.keyboard_enter_text(value)
      calabash.hide_soft_keyboard
      sleep(0.5)
    }
  end
end

class AddressA < Address
  # お名前
  web_view :lname, id: 'app_web', xpath: '//input[@id="lname"]'
  web_view :fname, id: 'app_web', xpath: '//input[@id="fname"]'
  # フリガナ
  web_view :lname_kana, id: 'app_web', xpath: '//input[@id="lname_kana"]'
  web_view :fname_kana, id: 'app_web', xpath: '//input[@id="fname_kana"]'
  # 郵便番号
  web_view :zip1, id: 'app_web', xpath: '//input[@id="zip1"]'
  web_view :zip2, id: 'app_web', xpath: '//input[@id="zip2"]'
  # 都道府県
  web_view :province, id: 'app_web', xpath: '//span[@id="prefDisp"]'
  # 郡市区
  web_view :city, id: 'app_web', xpath: '//input[@id="city"]'
  # 都道府県 > 国外 > 郡市区
  web_view :city_select, id: 'app_web', xpath: '//select[@id="city_select"]'
  # それ以降の住所
  web_view :street, id: 'app_web', xpath: '//input[@id="street"]'
  # 電話番号
  web_view :tel1, id: 'app_web', xpath: '//input[@id="tel1"]'
  web_view :tel2, id: 'app_web', xpath: '//input[@id="tel2"]'
  web_view :tel3, id: 'app_web', xpath: '//input[@id="tel3"]'
  # Link & Button
  web_view :back_to_order, id: 'app_web', xpath: '//input[@value="注文画面に戻る"]'
  web_view :address_edit_button, id: 'app_web', xpath: '//input[@type="button" and @value="修正" and @name="address_edit"]'
  web_view :new_address, id: 'app_web', xpath: '//a[text()="新しい送付先に送る"]'
  web_view :next_button, id: 'app_web', xpath: '//input[@value="次へ"]'
  web_view :add_address_list, id: 'app_web', xpath: '//input[@name="add_address_list"]'
  web_view :submit_button, id: 'app_web', xpath: '//input[@value="内容を変更する"]', tap_target: 'Step2'
  def initialize(type)
    text = {order: '注文者情報', send_to: '送付先'}[type]
    super(id: 'app_web', xpath: "//div[@id=\"pageTitle\" and starts-with(text(),\"#{text}\")]")
  end
end

class AddressB < Address
  def initialize(type)
    # not ready
  end
end
