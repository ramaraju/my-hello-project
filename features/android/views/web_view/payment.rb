require_relative 'web_page_utils'

class Payment < WebPageUtils
  include IchibaApp::AndroidHelpers
  list_view :list, index: 0
  ERROR_MESSAGE = {
    exp_wrong_msg: '「カード有効期限」が過ぎています。',
    brand_blank_msg: '「カード会社」を入力してください。',
    number_blank_msg: '「カード番号」を入力してください。',
    exp_blank_msg: '「有効期限」を入力してください。',
    owner_blank_msg: '「名義人」を入力してください。'
  }.freeze
  ab_test

  def self.selected_card
    @card
  end

  def current_card_info(condition)
    card_info_string = WebView.new(xpath: '//fieldset').query.first['textContent']
    card_info_array = card_info_string.split("\n")
    card_info_array[card_info_array.index(condition) + 1]
  end

  def select_card(card_object)
    fail "argument style is not correct: expect[Card] actual[#{card_object.class}]" unless card_object.is_a?(Card)
    list.wait {
      card_brand.tap
    }
    list_cell(card_object.card.split('_').first).tap
    Button.new(id: 'WebSelectDone').tap_if_exist
    Payment.new(page_mark).wait
  end

  def input_card_number(card_object)
    fail "argument style is not correct: expect[Card] actual[#{card_object.class}]" unless card_object.is_a?(Card)
    1.upto(card_object.number.split('-').length) { |index|
      send("card_number_#{index}").enter_text(card_object.number.split('-')[index - 1])
    }
    Payment.new(page_mark).wait {
      calabash.flick_down
    }
  end

  def select_date(data_hash = {y: 'future', m: 'future'})
    fail "argument style is not correct: expect[Hash] actual[#{data_hash.class}]" unless data_hash.is_a?(Hash)
    unless data_hash[:y].nil?
      list.wait {
        exp_year.tap_if_exist
      }
      year = data_hash[:y] == 'future' ? format('%02d', Time.now.strftime('%y').to_i + 9) : Time.now.strftime('%y')
      list_cell(year)
      TextView.new(text: year).tap
      list.wait_gone {
        Button.new(id: 'WebSelectDone').tap_if_exist
      }
    end
    unless data_hash[:m].nil?
      list.wait {
        exp_month.tap_if_exist
      }
      month = data_hash[:y] == 'future' ? '12' : '01'
      list_cell(month)
      TextView.new(text: month).tap
      list.wait_gone {
        Button.new(id: 'WebSelectDone').tap_if_exist
      }
    end
    calabash.flick_down
    Payment.new(page_mark)
  end

  def to_new_card(card_exist_flg = true)
    collection('クレジットカード決済').tap
    collection('新規カードを使う').tap if page_mark == :Step2 && card_exist_flg == true
    Payment.new(page_mark).wait {
      calabash.flick_down
    }
  end

  # rubocop:disable Style/AccessorMethodName
  def set_new_card_info(card_object)
    @card = card_object
    select_card(card_object)
    input_card_number(card_object)
    select_date
    card_owner.wait {
      flick_with_params('webView', {x: 50, y: 70}, {x: 50, y: 30})
    }.enter_text('rakuten')
    next_button.tap && Step2.wait
  end
  # rubocop:enable all
  def assert_error_message
    error_message_string = ''
    if card_brand.query.first['value'] == ''
      error_message_string = error_message_string << ERROR_MESSAGE[:brand_blank_msg]
    end
    if card_number_1.query.first['value'] == ''
      error_message_string = error_message_string << ERROR_MESSAGE[:number_blank_msg]
      number_1_blank_flg = true
    else
      number_1_blank_flg = false
    end
    (2..3).each { |index|
      if send("card_number_#{index}").query.first['value'] == ''
        error_message_string = error_message_string << ERROR_MESSAGE[:number_blank_msg]
      end
    }
    if card_number_4.query.first['value'] == '' && number_1_blank_flg == true
      error_message_string = error_message_string << ERROR_MESSAGE[:number_blank_msg]
    end
    if exp_month.query.first['value'] == ''
      error_message_string = error_message_string << ERROR_MESSAGE[:exp_blank_msg]
    end
    if exp_year.query.first['value'] == ''
      error_message_string = error_message_string << ERROR_MESSAGE[:exp_blank_msg]
    end
    card_owner.wait {
      flick_with_params('webView', {x: 50, y: 70}, {x: 50, y: 30})
    }
    if card_owner.query.first['value'] == ''
      error_message_string = error_message_string << ERROR_MESSAGE[:owner_blank_msg]
    end
    error_message_label.wait {
      flick_with_params('webView', {x: 50, y: 30}, {x: 50, y: 70})
    }.query.first['textContent'].include?(error_message_string)
  end

  def select_payment(payment)
    collection(payment).tap
    @next_button.tap if Cart.variant == :variant_b
    Step2.wait
  end
end

class PaymentA < Payment
  attr_reader :page_mark
  web_view :next_button, id: 'app_web', xpath: '//input[@value="次へ"]'
  web_view :card_brand, id: 'app_web', xpath: '//select[@name="card_brand"]'
  web_view :exp_month, id: 'app_web', xpath: '//select[@name="card_exp_month"]'
  web_view :exp_year, id: 'app_web', xpath: '//select[@name="card_exp_year"]'
  web_view :ci, id: 'app_web', xpath: '//select[@name="ci"]'
  web_view :card_number_1, id: 'app_web', xpath: '//input[@name="card_number1"]'
  web_view :card_number_2, id: 'app_web', xpath: '//input[@name="card_number2"]'
  web_view :card_number_3, id: 'app_web', xpath: '//input[@name="card_number3"]'
  web_view :card_number_4, id: 'app_web', xpath: '//input[@name="card_number4"]'
  web_view :card_owner, id: 'app_web', xpath: '//input[@id="card_owner"]'
  web_view :error_message_label, id: 'app_web', xpath: '//div[@class="errorMsg"]'
  def initialize(page)
    @page_mark = page
    page_title = page == :Step2 ? 'お支払い方法の変更' : 'お支払い方法'
    super(id: 'app_web', xpath: "//div[@id=\"pageTitle\" and text()=\"#{page_title}\"]")
  end
end

class PaymentB < Payment
  attr_reader :page_mark
  web_view :next_button, id: 'app_web', xpath: '//button[@id="next-button"]'
  def initialize(page)
    @page_mark = page
    page_title = page == :Step2 ? 'お支払い方法の変更' : 'お支払い方法の選択'
    super(id: 'app_web', xpath: "//h2[text()=\"#{page_title}\"]")
  end
end
