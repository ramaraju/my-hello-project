require_relative 'web_page_utils'

class MyCoupon < WebPageUtils
  def initialize
    super(id: 'app_web')
    # # ポイントを利用しない
    # @no_point = WebView.new(id: 'app_web', xpath: '//input[@value="-1"]')
    # # 全てのポイントを使う
    # @all_point = WebView.new(id: 'app_web', xpath: '//input[@value="0"]')
    # # 一部のポイントを使う
    # @part_point = WebView.new(id: 'app_web', xpath: '//input[@value="1"]')
    # @num_point = WebView.new(id: 'app_web', xpath: '//input[@name="num_point"]')
    # @submit_button = WebView.new(id: 'app_web', xpath: '//input[@value="内容を変更する"]', tap_target: Step2)
  end

  def retrieve_mycoupon_page_trait
    js = "return document.getElementById('topicPath').getElementsByTagName('b')[0].textContent"
    str = calabash.evaluate_javascript('webView', js)
    fail 'retriveval of page sign failed' if str.empty?
    str[0]
  end

  def back_to_user_info
    ImageButton.new.wait.tap
    TextView.new(text: '会員情報').wait
    Home.wait
  end
end
