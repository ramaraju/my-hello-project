require_relative 'step1'
require 'rspec/expectations'

class Cart < WebView
  include IchibaApp::AndroidHelpers
  include RSpec::Matchers
  image_view :close_button, id: 'home_menu_close'
  ab_test

  # confirm going to step1 screen from cart screen
  def tap_process_order
    wait_progress_bar_gone
    order_button.wait {
      flick_with_params("* id:'content'", {x: 50, y: 70}, {x: 50, y: 50})
      calabash.wait_for_animation
    }
    order_button.tap
    wait_progress_bar_gone
    Step1.wait
  end

  # confirm remove all items from cart and go back to home screen
  def clear_cart
    wait_progress_bar_gone
    while delete_link.exist?
      delete_link.tap
      sleep(1)
      wait_progress_bar_gone
    end
    close_button.tap
    Home.wait
  end

  def tap_close_button(page = ItemDetail)
    wait_progress_bar_gone
    close_button.tap && sleep(0.5)
    flick_with_params("* id:'content'", {x: 2, y: 30}, {x: 2, y: 70})
    page.wait
  end

  def assert_item_add_into_cart(item)
    wait_progress_bar_gone
    WebView.new(id: 'app_web', xpath: "//fieldset/div[1]/div[1]/div[2]/h3/a[text()=\"#{item.item_name}\"]").wait
  end

  def slide_to_the_bottom
    Cart.wait(timeout: 15) {
      flick_with_params("* id:'content'", {x: 50, y: 50}, {x: 50, y: 70})
    }
    if login_label.exist?
      top_page_out.wait(timeout: 15) {
        flick_with_params("* id:'content'", {x: 50, y: 70}, {x: 50, y: 50})
      }
    else
      top_page.wait(timeout: 15) {
        flick_with_params("* id:'content'", {x: 50, y: 70}, {x: 50, y: 50})
      }
    end
  end

  def close_link_tap
    close_link.wait(timeout: 10) {
      flick_with_params("* id:'content'", {x: 50, y: 70}, {x: 50, y: 50})
    }
    close_link.tap
  end

  def verify_option_info_display(info_list)
    (1..info_list.size).each do |i|
      xpath = '//*[@id="cartForm"]/fieldset/div[1]/div[2]/text()[' + i.to_s + ']'
      res = Query.new(id: 'app_web', xpath: xpath).run
      expect(res[0]['textContent']).to eq(info_list[i - 1])
    end
  end
end

class CartA < Cart
  web_view :order_button, id: 'app_web', xpath: '//input[@id="orderbutton"]', tap_target: 'Step1'
  web_view :delete_link, id: 'app_web', xpath: '//div[@class="itemdetail"]/a'
  # login
  web_view :top_page, id: 'app_web', xpath: '/html/body/div/ul[3]/li/a[text()="トップページへ"]'
  web_view :telephone_link, id: 'app_web', xpath: '/html/body/div/ul[1]/li[1]/a/div[text()="パソコンや他の携帯電話で買い物かごに入れた商品の確認方法"]'
  web_view :stock_link, id: 'app_web', xpath: '/html/body/div/ul[1]/li[2]/a/div[text()="既に購入済みの商品が「買い物かご」に残っている"]'
  web_view :payment_link, id: 'app_web', xpath: '/html/body/div/ul[1]/li[3]/a/div[text()="支払いについて"]'
  web_view :delivery_link, id: 'app_web', xpath: '/html/body/div/ul[1]/li[4]/a/div[text()="配送について"]'
  web_view :cancel_link, id: 'app_web', xpath: '/html/body/div/ul[1]/li[5]/a/div[text()="注文のキャンセルについて"]'
  # confirm
  web_view :close_link, id: 'app_web', xpath: '//div[5]/input[@value="閉じる"]'
  web_view :telephone_confirm, id: 'app_web', xpath: '//div[contains(.,"パソコンや他の携帯電話で買い物かごに入れた商品の確認方法")]'
  web_view :stock_confirm, id: 'app_web', xpath: '//div[contains(.,"既に購入済みの商品が「買い物かご」に残っている")]'
  web_view :payment_confirm, id: 'app_web', xpath: '//div[contains(.,"支払いについて")]'
  web_view :delivery_confirm, id: 'app_web', xpath: '//div[contains(.,"配送について")]'
  web_view :cancel_confirm, id: 'app_web', xpath: '//div[contains(.,"注文のキャンセルについて")]'
  # logout
  web_view :login_label, id: 'app_web', xpath: '/html/body/div/div[2]/div[text()="現在ログインしていません"]'
  web_view :top_page_out, id: 'app_web', xpath: '/html/body/div/ul[4]/li/a[text()="トップページへ"]'
  web_view :telephone_link_out, id: 'app_web', xpath: '/html/body/div/ul[2]/li[1]/a/div[text()="パソコンや他の携帯電話で買い物かごに入れた商品の確認方法"]'
  web_view :stock_link_out, id: 'app_web', xpath: '/html/body/div/ul[2]/li[2]/a/div[text()="既に購入済みの商品が「買い物かご」に残っている"]'
  web_view :payment_link_out, id: 'app_web', xpath: '/html/body/div/ul[2]/li[3]/a/div[text()="支払いについて"]'
  web_view :delivery_link_out, id: 'app_web', xpath: '/html/body/div/ul[2]/li[4]/a/div[text()="配送について"]'
  web_view :cancel_link_out, id: 'app_web', xpath: '/html/body/div/ul[2]/li[5]/a/div[text()="注文のキャンセルについて"]'
  # message
  web_view :empty_cart_message, id: 'app_web', xpath: '//h3[text()="現在、買い物かごには商品が入っていません。"]'
  def initialize
    super(id: 'app_web', xpath: '//header/div[text()="買い物かご"]')
  end
end

class CartB < Cart
  web_view :delete_link, id: 'app_web', xpath: '//a[text()="削除する"]'
  web_view :order_button, id: 'app_web', xpath: '//button[contains(.,"ご購入手続き")]', tap_target: 'Step1'
  def initialize
    super(id: 'app_web', xpath: '//h1[text()="買い物かご"]')
  end
end
