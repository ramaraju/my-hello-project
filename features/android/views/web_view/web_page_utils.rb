require 'goya/view'

class WebPageUtils < WebView
  include IchibaApp::AndroidHelpers
  def list_cell(text)
    list_view = ListView.new(index: 0)
    calabash.scroll_to_row(list_view.query.to_s, 0) && sleep(0.5)
    View.new(list_view.query.descendant('*', text: text)).wait(timeout: 60) {
      flick_with_params('android.widget.ListView', {x: 50, y: 70}, {x: 50, y: 30})
      calabash.wait_for_animations
    }
  end

  def collection(text, target = nil)
    variant_a = WebView.new(id: 'app_web', xpath: '//div[@class="allowbox"]').exist?
    xpath_a = "//div[@class=\"allowbox\" and starts-with(text(),\"#{text}\")]"
    xpath_b = "//span[text()=\"#{text}\"]"
    collection = WebView.new(id: 'app_web', xpath: variant_a ? xpath_a : xpath_b, tap_target: target)
    collection.wait {
      flick_with_params('webView', {x: 50, y: 70}, {x: 50, y: 30})
    }
  end

  def collection_disabled(text)
    WebView.new(id: 'app_web', xpath: "//div[@class='allowbox' and starts-with(text(),\"#{text}\")]/parent::li[@class='disabled']").wait {
      flick_with_params('webView', {x: 50, y: 70}, {x: 50, y: 30})
    }
  end

  def scroll_to(element)
    send(element).wait(timeout: 20) {
      flick_with_params('webView', {x: 50, y: 70}, {x: 50, y: 30})
    }
    sleep(0.1)
    send(element)
  end

  def get_by_js(symbol, filter)
    element = filter.keys.first == :id ? 'Element' : 'Elements'
    calabash.evaluate_javascript('webView', "return document.get#{element}By#{filter.keys.first.to_s.capitalize}('#{filter.values.first}').#{symbol}").first
  end
end
