require_relative 'web_page_utils'

class PointUse < WebPageUtils
  ab_test
end

class PointUseA < PointUse
  # ポイントを利用しない
  web_view :no_point, id: 'app_web', xpath: '//input[@value="-1"]'
  # 全てのポイントを使う
  web_view :all_point, id: 'app_web', xpath: '//input[@value="0"]'
  # 一部のポイントを使う
  web_view :part_point, id: 'app_web', xpath: '//input[@value="1"]'
  web_view :num_point, id: 'app_web', xpath: '//input[@name="num_point"]'
  web_view :submit_button, id: 'app_web', xpath: '//input[@value="内容を変更する"]', tap_target: 'Step2'
  def initialize
    super(id: 'app_web', xpath: '//div[@id="pageTitle" and text()="ポイント利用選択の変更"]')
  end
end

class PointUseB < PointUse
  def initialize
    # not ready
  end
end
