require_relative 'web_page_utils'
class Others < WebPageUtils
  include IchibaApp::AndroidHelpers
  attr_reader :step6_title, :submit_s3_r, :step3_title, :payment_select, :delivery_select, :step4_title_r, :submit_s5, :step2_title

  def initialize
    super(id: 'app_web', xpath: '//html/body/header/div[text()="申込情報　STEP1"]')
    # step1
    @user_textbox = WebView.new(id: 'app_web', xpath: '//input[@id="u"]')
    @password_textbox = WebView.new(id: 'app_web', xpath: '//input[@id="p"]')
    @submit_s1 = WebView.new(id: 'app_web', xpath: '//input[@value="次へ"]')
    @other_address = WebView.new(id: 'app_web', xpath: '//input[@id="deliver_to02"]')
    # step2
    @step2_title = WebView.new(id: 'app_web', xpath: '//html/body/header/div[text()="送付先　STEP2"]')
    # step3
    @set_times_1 = WebView.new(id: 'app_web', xpath: '//select[@id="select_span_day1"]')
    @set_day_1 = WebView.new(id: 'app_web', xpath: '//select[@id="select_day1" and option="1日"]')
    @set_day_2 = WebView.new(id: 'app_web', xpath: '//select[@id="select_day2" and option="月末"]')
    @set_week = WebView.new(id: 'app_web', xpath: '//select[@id="select_week"]')
    @set_week_day = WebView.new(id: 'app_web', xpath: '//select[@id="select_week_day"]')
    @submit_s3_1 = WebView.new(id: 'app_web', xpath: '//div[4]/div/input[@value="お届け日を確認する"]')
    @submit_s3_1_b = WebView.new(id: 'app_web', xpath: '//div[3]/div/input[@value="お届け日を確認する"]')
    @submit_s3_2 = WebView.new(id: 'app_web', xpath: '//input[@value="次へ"]')
    @submit_s3_3 = WebView.new(id: 'app_web', xpath: '//input[@value="次へ" and @type="submit"]')
    @submit_s3_r = WebView.new(id: 'app_web', xpath: '//center/a/input[@value="次へ"]')
    @step3_title = WebView.new(id: 'app_web', xpath: '//header/div[text()="お届日　STEP3"]')
    # step4
    @step4_title = WebView.new(id: 'app_web', xpath: '//html/body/header/div[text()="支払/配送　STEP4"]')
    @step4_title_r = WebView.new(id: 'app_web', xpath: '//html/body/header/div[text()="確認　STEP4"]')
    @payment_select = WebView.new(id: 'app_web', xpath: '//div[@class="allowbox" and text()!="クレジットカード決済"]')
    @delivery_select = WebView.new(id: 'app_web', xpath: '//div[text()="宅配便"]')
    # step5
    @step5_title = WebView.new(id: 'app_web', xpath: '//html/body/header/div[text()="確認　STEP5"]')
    @submit_s5 = WebView.new(id: 'app_web', xpath: '//html/body/form/div[1]/input[@value="この内容で申込する"]')
    # step6
    @step6_title = WebView.new(id: 'app_web', xpath: '//div[text()="申込完了"]')
  end

  def enter_password_and_user
    password = App.password
    user = App.username
    @password_textbox.enter_text(password)
    @user_textbox.enter_text(user)
    @other_address.tap
    @submit_s1.tap
    wait_progress_bar_gone
  end

  def select_address(user)
    address_label = user.addresses.first.address.split(' ')[1]
    WebView.new(id: 'app_web', xpath: "//div[text()=\"送付先リストから選択\"]/following-sibling::ul/li/a/div[contains(text(),#{address_label})]").tap
  end

  def select_date
    @step3_title.wait
    # ListView.new(index: 0).wait {
    #   @set_times_1.tap
    # }
    # list_cell('毎月').tap
    ListView.new(index: 0).wait_gone
    ListView.new(index: 0).wait {
      @set_day_1.tap
    }
    list_cell('1日').tap
    ListView.new(index: 0).wait_gone
    ListView.new(index: 0).wait {
      @set_day_2.tap
    }
    list_cell('月末').tap
    ListView.new(index: 0).wait_gone
    @submit_s3_1.tap
    @submit_s3_2.wait(timeout: 10) {
      flick_with_params("* id:'content'", {x: 50, y: 70}, {x: 50, y: 50})
    }
    @submit_s3_2.tap
    wait_progress_bar_gone
    @submit_s3_3.tap_if_exist
  end

  def select_delivery_and_payment
    @step4_title.wait
    @payment_select.tap
    sleep(3)
    @delivery_select.tap
  end

  def purchase_completely
    @step5_title.wait
    @submit_s5.tap
    wait_progress_bar_gone
  end

  def select_date_about_bidding
    list = ListView.new(index: 0)
    list.wait {
      @set_day_1.tap
    }
    list_cell('1日').tap
    list.wait_gone
    @submit_s3_1_b.tap
    @submit_s3_2.wait(timeout: 10) {
      flick_with_params("* id:'content'", {x: 50, y: 70}, {x: 50, y: 50})
    }
    @submit_s3_2.tap
    wait_progress_bar_gone
    @submit_s3_3.tap_if_exist
    @step4_title.wait
  end
end
