class Step3 < WebView
  ab_test
end

class Step3A < Step3
  web_view :price_with_tax_label, id: 'app_web', xpath: '//html/body/div/form/div/div/table/tbody/tr[1]/td[@class="charge"]'
  def initialize
    super(id: 'app_web', xpath: '//header/div[text()="注文完了"]')
  end
end

class Step3B < Step3
  def initialize
    super(id: 'app_web', xpath: '//div[text()="お客様の注文番号"]')
  end
end
