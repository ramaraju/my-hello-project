require_relative 'web_page_utils'

class Delivery < WebPageUtils
  include IchibaApp::AndroidHelpers
  class << self
    attr_accessor :selected_time
    attr_accessor :selected_date_value
  end
  list_view :list, index: 0
  ab_test

  # @param target_date Date to set
  def select_date(target_date)
    list.wait {
      select_day.tap
    }
    text = target_date.strftime('%Y年%m月%d日').concat("(#{weekdays[target_date.strftime('%A')]})")
    list_cell(text).tap
    Delivery.selected_date_value = calabash.evaluate_javascript('webView', "return document.getElementById('day').value;").first.insert(-3, '/').insert(4, '/')
    wait
  end

  # @return Currently selected Date
  def self.selected_date
    match = Delivery.selected_date_value.split('/')
    Date.new(match[0].to_i, match[1].to_i, match[2].to_i)
  end

  def time_option(_text)
    WebView.new(id: 'app_web', xpath: '//input[@name="deliveryTimeList"]/parent::label/text()}').wait {
      flick_with_params('webView', {x: 50, y: 70}, {x: 50, y: 30})
    }
  end

  def select_time(*args)
    Delivery.selected_time = []
    args.each { |time|
      time_option(time).tap!
      Delivery.selected_time << time_option(time).query.first['textContent'].strip
    }
  end

  def select_tomorrow_delivery
    tomorrow_delivery_checkbox.wait
    tomorrow_delivery_checkbox.tap if calabash.evaluate_javascript('webView', "return document.getElementById('asuraku_check').checked;").first != 'true'
  end

  def goto_step2
    calabash.press_back_button
    tomorrow_delivery_link.wait
    calabash.press_back_button
    delivery_date_label.wait
    WebView.new(id: 'app_web', xpath: '//div[@class="pos current"]/div/div[text()=2]').wait {
      flick_with_params('webView', {x: 50, y: 30}, {x: 50, y: 70})
    }
    Step2.wait
  end
end

class DeliveryA < Delivery
  # 日付指定
  web_view :set_day, id: 'app_web', xpath: '//ul[@id="day_link"]', tap_target: 'Delivery'
  # 指定なし
  web_view :not_set, id: 'app_web', xpath: '//ul[@id="none_link"]', tap_target: 'Delivery'
  # 日付指定 > お届け日
  web_view :select_day, id: 'app_web', xpath: '//select[@name="day"]'
  # 日付指定 > お届け時間
  web_view :time_list, id: 'app_web', xpath: '//input[@name="deliveryTimeList"]'
  # 次回からのお届け曜日も[]に指定する。
  web_view :regist_delivery_day, id: 'app_web', xpath: '//label[@for="regist_my_delivery_date"]'
  web_view :text_area, id: 'app_web', xpath: '//textarea'
  web_view :submit_button, id: 'app_web', xpath: '//input[@value="内容を変更する"]', tap_target: 'Step2'
  web_view :tomorrow_delivery_link, id: 'app_web', xpath: '//ul[@id="asuraku_link"]'
  web_view :tomorrow_delivery_checkbox, id: 'app_web', xpath: '//input[@id="asuraku_check" and @name="asuraku_check"]'
  web_view :delivery_date_label, id: 'app_web', xpath: '//div[text()="▼お届け日指定"]/following-sibling::div/descendant::div[@class="ctMaB10 ctBold"]'
  def initialize
    super(id: 'app_web', xpath: '//div[@id="pageTitle" and text()="配送方法の変更"]')
  end
end

class DeliveryB < Delivery
  def initialize
    # not ready
  end
end
