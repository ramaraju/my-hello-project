require_relative 'web_page_utils'

class MyRakuten < WebPageUtils
  include IchibaApp::AndroidHelpers
  attr_reader :cart_info_button, :delete_info_button, :update_delete_button, :delete_info_button
  attr_reader :submit_delete_button, :password_textbox
  def initialize
    super(id: 'app_web')

    @basic_info_button = WebView.new(id: 'app_web', xpath: '//p[contains(text(),"基本情報")]/following-sibling::p/a[text()="確認・変更"]')
    @delivery_info_button = WebView.new(id: 'app_web', xpath: '//p[contains(text(),"本人連絡先")]/following-sibling::p/a[text()="確認・変更"] ')
    @cart_info_button = WebView.new(id: 'app_web', xpath: '//p[contains(text(),"クレジットカード情報")]/following-sibling::p/a[text()="確認・変更"] ')

    @update_delete_button = WebView.new(id: 'app_web', xpath: '//button[@type="submit" and @value="変更・削除" and @name="submit"]')
    @delete_info_button = WebView.new(id: 'app_web', xpath: '//input[@type="submit" and @value="このクレジットカード情報を削除する" and @name="execMethod"]')
    @submit_delete_button = WebView.new(id: 'app_web', xpath: '//button[@type="submit" and @value="クレジットカード情報をnullする" and @name="SelectProfile"]')
    @password_textbox = WebView.new(id: 'app_web', xpath: '//input[@type="password" and @name="p"]')
    @next_button = WebView.new(id: 'app_web', xpath: '//li[@class="buttons2 subButton first"]')
  end

  def tap_member_info_update
    WebView.new(id: 'app_web', css: 'dl#personal').tap
    # js = "document.getElementById('personal').click()"
    # calabash.evaluate_javascript('webView', js)
  end

  def tap_login_info_update
    # js = "document.getElementsByTagName('span')[0].click()"
    # calabash.evaluate_javascript('webView', js)
    WebView.new(id: 'app_web', xpath: '//a[text()="登録内容の確認・変更"]').tap
    wait_progress_bar_gone
  end

  def goto_update(button)
    temp_button = @basic_info_button
    case button
    when 'basic_info'
      temp_button = @basic_info_button
    when 'delivery_info'
      temp_button = @delivery_info_button
    when 'cart_info'
      temp_button = @cart_info_button
    end
    scroll_to(temp_button)
  end

  def retrieve_my_rakuten_page_trait
    js = "return document.getElementById('personal').getElementsByTagName('dt')[0].textContent"
    str = calabash.evaluate_javascript('webView', js)
    fail 'retriveval of page sign failed' if str.empty?
    str[0]
  end

  def back_to_user_info
    ImageButton.new.wait.tap
    TextView.new(text: '会員情報').wait
    Home.wait
  end

  def delete_cart_information(user)
    card_exist = true
    count = 1
    until @update_delete_button.exist? || @next_button.exist? || count == 20
      flick_with_params('webView', {x: 50, y: 70}, {x: 50, y: 50})
      count += 1
      card_exist = false if @next_button.exist?
    end
    if card_exist == true
      scroll_to(:update_delete_button).tap
      scroll_to(:delete_info_button).tap
      scroll_to(:password_textbox).enter_text(user.password)
      scroll_to(:submit_delete_button).tap
      wait_progress_bar_gone
    end
  end
end
