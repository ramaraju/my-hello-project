require_relative 'web_page_utils'
require_relative 'address'

class Step2 < WebPageUtils
  include IchibaApp::AndroidHelpers
  ab_test

  def value(key)
    case key
    when '商品（税込）'
      label = price_with_tax_label
    when '送料'
      label = delivery_fee_label
    when '代引料'
      label = cash_on_delivery_fee_label
    when 'ポイント利用'
      label = point_use_label
    when '合計金額'
      label = total_price_label
    when 'ポイント'
      label = receive_point_label
    when 'お支払方法'
      label = payment_type_label
    when '配送方法'
      label = delivery_type_label
    end
    label.wait {
      flick_with_params("* id:'content'", {x: 50, y: 50}, {x: 50, y: 40})
    }.query.first['textContent'].strip
  end

  # confirm order information on step2 screen
  def validate(table, flg = 'before')
    commit_button.wait {
      calabash.flick_down
    }
    header = table.first
    row = table[1..-1].find { |r| r.first == flg.to_s }
    arrays = header[1..-1].zip(row[1..-1])
    arrays.each do |array|
      name, value = array[0], array[1]
      case name
      when '商品（税込）'
        label = price_with_tax_label
      when '送料'
        label = delivery_fee_label
      when '代引料'
        label = cash_on_delivery_fee_label
      when 'ポイント利用'
        label = point_use_label
      when '合計金額'
        label = total_price_label
      when 'ポイント'
        label = gain_point_normal
      when 'お支払い方法'
        label = payment_type_label
      when '配送方法'
        label = delivery_type_label
      end
      assert_value(name, value, label)
    end
    commit_button.wait {
      calabash.flick_down
    }
  end

  # check that the expected value is equal to the actual value
  def assert_value(name, value, label)
    if name == 'ポイント'
      receive_point_label.wait {
        flick_with_params("* id:'content'", {x: 50, y: 50}, {x: 50, y: 40})
      }.tap
    end
    if name == '代引料' && value == ''
      fail '代引料 should not display' if cash_on_delivery_fee_label.exist?
    else
      label.wait {
        flick_with_params("* id:'content'", {x: 50, y: 50}, {x: 50, y: 40})
      }
      first = label.query.first
      result = first['textContent'].strip
      fail "mismatch for element: #{name}: got #{result}, expected #{value}" if result != value
    end
  end

  # commit order on step2 screen and go to step3 screen
  def tap_place_order(clear_dialog = true)
    commit_button.wait {
      calabash.flick_down
    }.tap
    sleep(2)
    wait_progress_bar_gone
    if View.new('DialogTitle', id: 'alertTitle', text: 'アプリの評価').exist? && clear_dialog
      wait_progress_bar_gone
      Button.new(text: '後でする').tap_if_exist
    end
    Step3.wait
  end

  # enter user password then go to step2 screen
  def enter_password
    password = App.password
    password_textbox.enter_text(password)
    login_button.tap
    ProgressBar.new(index: 0).wait_gone
  end

  def assert_card_detail(card_object = Payment.selected_card)
    new_card_brand = "カード会社：#{card_object.card.split('_').first}"
    new_card_number = card_object.number.split('-').last
    new_card_label = card_detail_label.wait {
      flick_with_params("* id:'content'", {x: 50, y: 70}, {x: 50, y: 30})
    }.query.first['textContent']
    fail "card brand is not correct, got[#{new_card_brand}]" unless new_card_label.include?(new_card_brand)
    fail "card number is not correct, got[#{new_card_number}]" unless new_card_label.include?(new_card_number)
    commit_button.wait {
      calabash.flick_down
    }
  end

  def selected_date
    WebView.new(id: 'app_web', xpath: '//div[text()="日付指定"]/following-sibling::div').wait {
      flick_with_params("* id:'content'", {x: 50, y: 70}, {x: 50, y: 30})
    }
    text = calabash.query("webView id:'app_web' xpath:'//div[text()=\"日付指定\"]/following-sibling::div'", :textContent)
    match = text.first.match(/(\d+)年(\d+)月(\d+)日(.)/)
    Date.new(match[1].to_i, match[2].to_i, match[3].to_i)
  end

  def selected_time
    WebView.new(id: 'app_web', xpath: '//div[text()="日付指定"]/following-sibling::div').wait {
      flick_with_params("* id:'content'", {x: 50, y: 70}, {x: 50, y: 30})
    }
    text = []
    loop {
      text_new = calabash.query("webView id:'app_web' xpath:'//div[text()=\"日付指定\"]/following-sibling::div'", :textContent)
      break if !text.empty? && text.last == text_new.last || text_new.empty?
      text.concat(text_new)
      flick_with_params("* id:'content'", {x: 50, y: 70}, {x: 50, y: 30})
    }
    text = text.uniq
    text.shift && text
  end

  def selected_value(label)
    send(label.to_s).wait {
      flick_with_params("* id:'content'", {x: 50, y: 70}, {x: 50, y: 30})
    }.query.first['textContent'].strip
  end

  def select_checkbox(*symbol)
    group = [:rmail_check, :shop_rating_check, :newscheck_267, :newscheck_72, :newscheck_236, :newscheck_59]
    select_list = symbol
    group.each { |check_box|
      flg = select_list.include?(check_box) ? 'true' : 'false'
      send(check_box).wait {
        flick_with_params("* id:'content'", {x: 50, y: 70}, {x: 50, y: 30})
      }
      send(check_box).tap unless get_by_js(:checked, id: check_box.to_s) == flg
    }
  end

  def tap_dialog_button(button)
    wait_progress_bar_gone
    message = "いつもご利用いただきありがとうございます。よろしければアプリの評価をお願いします。\nレビューするには、Playストアで「このアプリを評価」の星をタップしてください。"
    TextView.new(text: message).wait
    if button == :back
      calabash.press_back_button
      Step3.wait
    elsif button == :out_dialog
      system('adb shell input tap 300 300')
      Step3.wait
    else
      send(button).tap
    end
  end
end

class Step2A < Step2
  attr_reader :send_to_button
  # submit button
  web_view :send_to_edit_button, id: 'app_web', xpath: '//input[@type="submit" and @value="修正" and @name="sendtoedit"]'
  web_view :delivery_button, id: 'app_web', xpath: '//input[@type="submit" and @value="変更" and @name="delivery"]', tap_target: 'Delivery'
  web_view :delivery_detail_button, id: 'app_web', xpath: '//input[@type="submit" and @value="変更" and @name="delivery_detail"]', tap_target: 'Delivery'
  web_view :order_button, id: 'app_web', xpath: '//input[@type="submit" and @value="変更" and @name="order"]'
  web_view :send_to_button, id: 'app_web', xpath: '//input[@type="submit" and @value="変更" and @name="sendto"]', tap_target: ['Address', :send_to]
  web_view :payment_button, id: 'app_web', xpath: '//input[@type="submit" and @value="変更" and @name="payment"]', tap_target: ['Payment', :Step2]
  web_view :point_button, id: 'app_web', xpath: '//input[@type="submit" and @value="変更" and @name="point"]', tap_target: 'PointUse'
  web_view :login_button, id: 'app_web', xpath: '//input[@id="login_button"]'
  web_view :checkbox_cancel, id: 'app_web', xpath: '//a[text()="全ての選択を解除"]'
  # label
  web_view :price_with_tax_label, id: 'app_web', xpath: '//td[@class="title" and contains(text(),"商品（税込）")]/following-sibling::td'
  web_view :delivery_fee_label, id: 'app_web', xpath: '//td[@class="title" and contains(text(),"送料")]/following-sibling::td'
  web_view :point_use_label, id: 'app_web', xpath: '//td[@class="title" and contains(text(),"ポイント利用")]/following-sibling::td'
  web_view :cash_on_delivery_fee_label, id: 'app_web', xpath: '//td[@class="title" and contains(text(),"代引料")]/following-sibling::td'
  web_view :total_price_label, id: 'app_web', xpath: '//h2[contains(text(),"合計金額")]/span'
  web_view :receive_point_label, id: 'app_web', xpath: '//div[@id="gainePointDetailTitle" and contains(.,"ポイント獲得予定")]/div/span/span[@class="ctBold ctRedPoint"]'
  web_view :payment_type_label, id: 'app_web', xpath: '//div[text()="▼決済方法"]/following-sibling::div[1]/descendant::div[@class="ctMaB10 ctBold"]'
  web_view :delivery_type_label, id: 'app_web', xpath: '//div[text()="▼配送方法"]/following-sibling::div/descendant::div[@class="ctMaB10 ctBold"]'
  web_view :card_detail_label, id: 'app_web', xpath: '//div[text()="クレジットカード決済"]/following-sibling::ul'
  web_view :send_to_address_label, id: 'app_web', xpath: '//div[text()="送付先"]/following-sibling::div/div[@class="ctAttention"]'
  web_view :delivery_date_label, id: 'app_web', xpath: '//div[text()="▼お届け日指定"]/following-sibling::div/descendant::div[@class="ctMaB10 ctBold"]'
  web_view :info_msg_label, id: 'app_web', xpath: '//div[@class="infoMsg"]'
  # [
  #     [0] "10ポイント",
  #     [1] "",
  #     [2] "",
  #     [3] "ご利用可能なポイント残高：",
  #     [4] "792ポイント"
  # ]
  web_view :point_detail_label, id: 'app_web', xpath: '//div[text()="▼ポイント利用"]/following-sibling::div'
  web_view :point_num_label, id: 'app_web', xpath: '//div[text()="▼ポイント利用"]/following-sibling::div/div/div[@class="ctBoxAC ctMaB5"]'
  web_view :gain_point_normal, id: 'app_web', xpath: '//div[@class="eachGainPoint" and contains(.,"通常購入分")]/font'
  # Check box
  web_view :rmail_check, id: 'app_web', xpath: '//input[@id="rmail_check"]'
  web_view :shop_rating_check, id: 'app_web', xpath: '//input[@id="shop_rating_check"]'
  # あなたにぴったり！お役立ち情報（楽天市場からの「お得な」お知らせ）
  web_view :newscheck_267, id: 'app_web', xpath: '//input[@id="newscheck_267"]'
  # バーゲン情報（楽天市場バーゲンニュース）
  web_view :newscheck_72, id: 'app_web', xpath: '//input[@id="newscheck_72"]'
  # まち楽　愛媛県の情報通信（まち楽　愛媛県ファンクラブ通信）
  web_view :newscheck_236, id: 'app_web', xpath: '//input[@id="newscheck_236"]'
  # 家電・カメラ・パソコン（楽天家電・パソコンニュース）
  web_view :newscheck_59, id: 'app_web', xpath: '//input[@id="newscheck_59"]'
  # Input element
  web_view :commit_button, id: 'app_web', xpath: '//form/div/input[@class="button_red" and @value="注文を確定する"]'
  web_view :password_textbox, id: 'app_web', xpath: '//input[@id="p" and @name="p"]'
  web_view :login_button, id: 'app_web', xpath: '//input[@id="login_button"]'
  # Step3 dialog button
  button :dialog_cancel, text: 'キャンセル', tap_target: 'Step3'
  button :dialog_later, text: '後でする', tap_target: 'Step3'
  button :dialog_review, text: 'アプリの評価をする'
  def initialize
    super(id: 'app_web', xpath: '//form/div/input[@class="button_red" and @value="注文を確定する"]')
  end
  def commit_button_top
    commit_button.wait {
      calabash.flick_down
    }
    WebView.new(id: 'app_web', xpath: '//form/div/input[@class="button_red" and @value="注文を確定する"]', tap_target: Step3)
  end
end

class Step2B < Step2
  web_view :price_with_tax_label, id: 'app_web', xpath: '//div[text()="小計"]/following-sibling::div'
  web_view :delivery_fee_label, id: 'app_web', xpath: '//div[text()="送料"]/following-sibling::div'
  web_view :cash_on_delivery_fee_label, id: 'app_web', xpath: '//div[text()="代引料"]/following-sibling::div'
  web_view :point_use_label, id: 'app_web', xpath: '//div[text()="ポイント利用"]/following-sibling::div'
  web_view :total_price_label, id: 'app_web', xpath: '//div[text()="総合計"]/following-sibling::div'
  web_view :receive_point_label, id: 'app_web', xpath: '//span[@class="ri-fs-18 ri-fw-bold total-point"]'
  web_view :payment_type_label, id: 'app_web', xpath: '//div[text()="お支払い方法"]/parent::div/following-sibling::div/div[2]'
  web_view :delivery_type_label, id: 'app_web', xpath: '//div[text()="配送方法"]/parent::div/following-sibling::div/div[2]'
  # button
  web_view :commit_button, id: 'app_web', xpath: '//button[text()="注文を確定する"]'
  def initialize
    super(id: 'app_web', xpath: '//li[@class="active" and text()="注文確認"]')
  end
  def commit_button_top
    commit_button.wait {
      calabash.flick_down
    }
    WebView.new(id: 'app_web', xpath: '//button[text()="注文を確定する"]', tap_target: Step3)
  end
end
