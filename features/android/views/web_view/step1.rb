class Step1 < WebView
  include IchibaApp::AndroidHelpers
  web_view :password_textbox, id: 'app_web', xpath: '//input[@id="p"]'
  web_view :login_button, id: 'app_web', xpath: '//input[@id="login_button"]'
  ab_test

  # enter user password then go to step2 screen
  def enter_password(page = Step2)
    wait_progress_bar_gone
    calabash.wait_for_animations
    password = App.password
    password_textbox.enter_text(password)
    login_button.tap
    wait_progress_bar_gone
    target = page.is_a?(Array) ? page : [page.to_s]
    Kernel.const_get(target.first).new(*target[1..-1])
  end

  def assert_protest_information
    protect_way.wait(timeout: 10) {
      calabash.flick_up
    }.tap
    wait_progress_bar_gone
    WebView.new(id: 'app_web', xpath: '//h1[text()="個人情報保護方針"]').wait
    calabash.press_back_button
    wait_progress_bar_gone
  end

  def assert_change_information
    wait_progress_bar_gone
    MyRakuten.wait
    # @confirm_change_information.wait
  end

  def assert_forget_information
    password_forget.tap
    wait_progress_bar_gone
    confirm_forget_password_j.wait
    switch_english.tap
    confirm_forget_password_e.wait
    switch_chinese.tap
    confirm_forget_password_c.wait
    switch_japanese.tap
    confirm_forget_password_j.wait
    enter_email.enter_text('cala_f_12@example.com')
    enter_lname.enter_text('楽天')
    enter_fname.enter_text('太郎')
  end
end

class Step1A < Step1
  web_view :change_information, id: 'app_web', xpath: '//fieldset/legend[3]/a[2][text()="楽天会員情報の変更"]'
  web_view :password_forget, id: 'app_web', xpath: '//fieldset/legend[3]/a[1][text()="ユーザID、パスワードを忘れた方"]'
  web_view :protect_way, id: 'app_web', xpath: '//footer/p/a[text()="個人情報保護方針"]'
  # web_view :confirm_change_information, id: 'app_web', xpath: '//h2[text()="楽天太郎f02 さんのご利用情報です"]'
  web_view :switch_english, id: 'app_web', xpath: '//dd[2]/a[text()="English"]'
  web_view :switch_chinese, id: 'app_web', xpath: '//dd[3]/a[text()="简体中文"]'
  web_view :switch_japanese, id: 'app_web', xpath: '//dd[1]/a[text()="日本語"]'
  web_view :confirm_forget_password_j, id: 'app_web', xpath: '//a[2]/img[@alt="楽天会員情報管理"]'
  web_view :confirm_forget_password_e, id: 'app_web', xpath: '//a[2]/img[@alt="idservice"]'
  web_view :confirm_forget_password_c, id: 'app_web', xpath: '//a[2]/img[@alt="帐户信息"]'
  web_view :enter_email, id: 'app_web', xpath: '//form/ul/li/input[@name="email"]'
  web_view :enter_lname, id: 'app_web', xpath: '//input[@id="name01"]'
  web_view :enter_fname, id: 'app_web', xpath: '//input[@id="name02"]'
  def initialize
    super(id: 'app_web', xpath: '//div[@class="pos current"]/div/div[text()=1]')
  end
end

class Step1B < Step1
  def initialize
    super(id: 'app_web', xpath: '//li[@class="active" and text()="ログイン"]')
  end
end
