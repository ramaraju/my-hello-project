class Welcome < Page
  def initialize
    super('android.widget.LinearLayout', id: 'ichiba_base_dialog')
  end
  # verify welcome dialog title
  def validate
    text = 'ようこそ楽天市場へ！'
    TextView.new(id: 'ichiba_base_dialog_title', text: text).wait(timeout_message: "The welcome popup text is not '#{text}'")
  end
  def dismiss
    Button.new(text: 'いいえ').tap
    Home.wait
  end
end
