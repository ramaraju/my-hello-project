# coding: utf-8
require_relative '../utils/utils_android'
require_relative 'browsing_history'
require_relative 'home'
require_relative 'item_detail'
require_relative 'login'
require_relative 'search_detail'
require_relative 'search_result'
require_relative 'welcome'
require_relative 'tabs_bookmark'
require_relative 'web_view/cart'
require_relative 'web_view/step1'
require_relative 'web_view/step2'
require_relative 'web_view/step3'
require_relative 'web_view/delivery'
require_relative 'web_view/payment'
require_relative 'web_view/point_use'
require_relative 'web_view/web_page_utils'
require_relative '../utils/search_utils'
require_relative '../utils/bookmark_utils'
require_relative '../utils/browsing_history_utils'

class ScrollView
  getter :scroll_x
  getter :scroll_y
  def scroll_to(x: 0, y: 0)
    query.run(scrollTo: [x.to_i, y.to_i])
  end
  def smooth_scroll_to(x: 0, y: 0)
    query.run(smoothScrollTo: [x.to_i, y.to_i])
  end
  def scroll_by(x: 0, y: 0)
    query.run(scrollBy: [x.to_i, y.to_i])
  end
  def smooth_scroll_by(x: 0, y: 0)
    query.run(smoothScrollBy: [x.to_i, y.to_i])
  end
  def page_scroll(direction = :up)
    query.run(pageScroll: direction == :up ? 33 : 130)
  end
end

class ListView
  getter :max_scroll_amount
  def item_count
    adapter(:getCount).first
  end
  def adapter(*args)
    query.run(:getAdapter, *args)
  end
  def scroll_to_row(row)
    query.run(smoothScrollToPosition: row)
  end
  def scroll_by_offset(offset)
    query.run(smoothScrollByOffset: offset)
  end
  def scroll_to_view(view)
    scroll_to_row(0)
    item_count.times {
      if view.exist?
        mid_y = rect.y + rect.height / 2
        row_y = view.rect.y
        old_y = nil
        while row_y > mid_y && row_y != old_y
          scroll_by_offset(1)
          sleep(0.1)
          old_y = row_y
          row_y = view.rect.y
        end
        return view
      end
      scroll_by_offset(1)
      sleep(0.1)
    }
    nil
  end
end

class View
  def scroll_to
    scroll_view = ScrollView.new(query.parent('android.widget.ScrollView'))
    raise 'ScrollView not found' if !scroll_view
    scroll_view.scroll_to(y: scroll_view.scroll_y + rect.y - rect.height / 2 - scroll_view.rect.y - scroll_view.rect.height / 2)
    self
  end
  def page_scroll(direction = :up)
    scroll_view = ScrollView.new(query.parent('android.widget.ScrollView'))
    raise 'ScrollView not found' if !scroll_view
    scroll_view.page_scroll(direction)
  end
end

class App
  @home = Home.new

  Bridge.load_mapping('features/android/support/mapping.txt')
  Package = Bridge::Package('jp.co.rakuten.android')

  class << self
    attr_reader :home, :user, :username, :password
  end

  def self.dismiss_alerts
    calabash.hide_soft_keyboard if calabash.keyboard_visible?
    calabash.wait_for {
      [Welcome, Home].any?(&:open?)
    }
    if Welcome.open?
      welcome = Welcome.wait
      welcome.validate
      welcome.dismiss
      # home.setup_mocking
    end
  end

  def self.logout
    home.logout
  end

  def self.login(user, page = Home)
    calabash.flick_down
    home.to_member_frame
    user = USERS.search(user: user) if user.is_a?(String)
    @user = user
    @username = user.username
    @password = user.password
    if Home.new.member_name_label.text != "#{user.last_name} #{user.first_name}" # don't do anything if already logged in with right user
      logout if Home.new.member_name_label.exist? # logout if a different user is already logged in
      home.login_button.tap!
      Login.new.login(user, page)
    end
  end

  def self.clear_cart
    home.goto_cart.clear_cart
  end

  def self.open_bookmarks
    home.goto_bookmarks
    TabsBookmark.wait
  end

  def self.goto_home
    Home.wait
  end
  # rubocop:disable Style/AccessorMethodName
  def self.set_stage(stage = true)
    home.setup_mocking if stage
  end
  # rubocop:enable all
  def self.restart
    begin
      calabash.start_test_server_in_background
    rescue
      calabash.start_test_server_in_background
    end
    home.wait
  end
  # rubocop:disable Style/AccessorMethodName
  def self.get_apk_file_name
    ENV['APP_PATH'].match(/app-(.+)-qa\.apk$/)[0]
    # `adb shell dumpsys package jp.co.rakuten.android | grep versionName`.split('=')[1]
  end
  # rubocop:enable all
end
