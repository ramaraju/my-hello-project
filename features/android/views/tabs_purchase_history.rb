require_relative '../utils/utils_android'
require_relative 'header'

class TabsPurchaseHistory < Page
  include IchibaApp::AndroidHelpers

  attr_reader :close_button, :header

  def initialize
    super('android.support.v4.view.ViewPager', getCurrentItem: 4)

    @header = Header.new(Home)

    # button
    @login_button = Button.new(text: 'ログイン')
    @close_button = ImageView.new(id: 'home_menu_close')
    @purchase_again_button = Button.new(id: 'purchaseagain_btn')

    @shop_name_link = LinearLayout.new(id: 'shopNameLayout')
    @order_details_button = LinearLayout.new(id: 'order_details_btn')
    @shop_review_btn = LinearLayout.new(id: 'shop_review_btn')

    # web view
    @shop_search = WebView.new(id: 'app_web', xpath: '//input[@type="submit" and @value="商品検索"]')
    @order_details = WebView.new(id: 'app_web', xpath: '//h1[text()="購入履歴詳細"]')
    @member_info = WebView.new(id: 'app_web', xpath: '//h1[@id="member" and text()="会員情報の追加登録"]')
  end

  def goto_login
    @login_button.tap
    Login.wait
  end

  def goto_shop
    @shop_name_link.tap
    wait_progress_bar_gone
    @shop_search.wait
  end

  def purchase_item(item)
    View.new("* id:'itemDesc' text:'#{item.item_name}' sibling * id:'purchaseagain_btn'").wait {
      flick_with_params('*', {x: 50, y: 40}, {x: 50, y: 20})
    }.tap
    wait_progress_bar_gone
    ItemDetail.new(item.item_name).wait
  end

  def goto_order_details
    @order_details_button.tap
    wait_progress_bar_gone
    @order_details.wait
  end

  def goto_shop_reviews
    @shop_review_btn.tap
    wait_progress_bar_gone
    @member_info.wait
  end
end
