require_relative '../../utils/search_utils'
require_relative '../../utils/browsing_history_utils'

class BrowsingHistory4 < TestCase
  include SearchUtils
  def run
    user = find_user('userdefault_%d', 1..7)
    item1 = ITEMS.search(item: 'CPC_定期購入商品(通常ジャンル) 1')
    item2 = ITEMS.search(item: '通常商品(項目選択肢別がない) 31')
    App.login(user)
    search(item1).tap_item(item1).add_bookmark
    browsing_history = App.restart.goto_browsing_history
    home = browsing_history.clear_browsing_history
    item_page = home.goto_itemdetail_from_bookmark(item1)
    item_page.tap_back(TabsBookmark).header.tabs.home.tap
    search(item2).tap_item(item2)
    browsing_history = App.restart.goto_browsing_history
    3.times {
      item_detail = browsing_history.tap_item(item1)
      browsing_history = item_detail.tap_back(BrowsingHistory)
      item_detail = browsing_history.tap_item(item2)
      browsing_history = item_detail.tap_back(BrowsingHistory)
    }
  end
end
