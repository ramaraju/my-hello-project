class BrowsingHistory2 < TestCase
  def run
    home = App.goto_home
    # Then The browsing banner cannot be displayed
    home.assert_browsing_history_not_shown
  end
end
