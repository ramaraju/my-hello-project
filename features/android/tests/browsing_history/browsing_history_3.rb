require_relative '../../utils/search_utils'

class BrowsingHistory3 < TestCase
  include SearchUtils
  def run
    # TODO: why only user #3?
    user = find_user('userbrowsinghistory1_%d', 1..7)
    item = ITEMS.search(item: '通常商品(藥類) 36')
    App.goto_home
    App.login(user)
    # add NO.31 bookmark
    item_detail = search(item).tap_item(item)
    item_detail.progress_bar.wait_gone(timeout: 10)
    home = App.restart
    home.assert_item_shown_in_browsing_banner(item)
    # get bookmark item name and compare
    array_item = home.assert_browsing_history_item_and_number
    home = App.restart
    browsing = home.tap_show_all_the_browsing
    browsing.assert_item_list_same_as_home_browsing(array_item)
    # delete bookmark which is added
    # browsing.slide_up_to_confirm_item_browsing(@item)
    browsing.delete_browsing_history(item)
  end
end
