require_relative '../../utils/search_utils'

class BrowsingHistory1 < TestCase
  include SearchUtils
  def run
    user = find_user('userbrowsinghistory1_%d', 1..7)
    item = ITEMS.search(item: '通常商品(項目選択肢別がない) 31')
    App.goto_home
    1.upto(5) do
      App.login(user)
      item_detail = search(item).tap_item(item)
      # sleep(3)
      item_detail.progress_bar.wait_gone(timeout: 10)
      App.restart
      home = App.logout
      home.tap_navigation_drawer
      login = home.tap_view_history
      login.assert_transform_to_the_login
      browsing = login.login(user, BrowsingHistory)
      browsing.delete_browsing_history(item)
      browsing.assert_item_gone(item)
      App.restart
      App.logout
    end
  end
end
