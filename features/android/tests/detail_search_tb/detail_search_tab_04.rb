class DetailSearchTab04 < TestCase
  def run
    home = App.goto_home

    ngkeyword1 = 'abcdefghijklmnopqrstuvwxyz111222333444555666777888' # 50 chars
    ngkeyword2 = 'abcdefghijklmnopqrstuvwxyz1112223334445556667778889' # 51 chars
    ngkeyword3 = 'abcd12345'
    keyword1 = 'cala'

    search_tab = home.goto_search_tab
    search_tab.ngkeyword.wait.tap
    calabash.enter_text_simulate_keyboard(ngkeyword1)
    calabash.press_back_button
    expect(search_tab.ngkeyword.query.first['text']).to eq(ngkeyword1)

    search_result = search_tab.make_detail_search({'keyword' => keyword1})
    search_result.item_desc.wait

    search_tab = search_result.go_back_home_page(TabsSearch)
    search_tab.clear_search_button.tap
    search_tab.ngkeyword.wait.tap
    calabash.enter_text_simulate_keyboard(ngkeyword2)
    calabash.press_back_button
    expect(search_tab.ngkeyword.query.first['text']).to eq(ngkeyword1)

    search_tab.clear_search_button.tap
    search_result = search_tab.make_detail_search({'keyword' => keyword1, 'ngkeyword' => ngkeyword3})
    search_result.item_desc.wait
  end
end
