class DetailSearchTab06 < TestCase
  def run
    home = App.goto_home
    puts 'main case  ', DetailSearchTab06Data.shop_list.length.to_s
    search_tab = home.goto_search_tab
    search_tab.other_search_filter.wait.tap
    search_tab.shop_unselected.wait.tap
    shop_list = search_tab.obtain_shops_in_list
    expect(shop_list.length).to eq(90)
    i = 0
    while i < 2

      search_tab.back_button_on_shop_list.wait.tap
      home = search_tab.go_back_home
      genre_name = '日本酒・焼酎'
      home.goto_genre_from_nav.select_genre(genre_name)
      search_result = SearchResult.wait
      search_result.item_desc.wait
      calabash.flick_up
      calabash.flick_up
      search_result.shop_name_label.wait
      shop_name = search_result.shop_name_label.text
      next if shop_list.include? shop_name
      search_result.shop_name_label.tap
      item_detail = ItemDetail.wait
      next if item_detail.add_shop_in_shop_menu.empty?
      search_result = item_detail.tap_back_button(SearchResult)
      home = search_result.go_back_home_page
      search_tab = home.goto_search_tab
      search_tab.clear_search_button.wait
      search_tab.other_search_filter.tap_if_exist
      search_tab.shop_unselected.wait.tap
      shop_list = search_tab.obtain_shops_in_list
      expect(shop_list.length).to eq(90)
      i += 1
    end
  end
end
