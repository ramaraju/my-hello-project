class DetailSearchTab05 < TestCase
  def run
    keyword1 = 'cala商品'
    home = App.goto_home
    search_tab = home.goto_search_tab
    search_result = search_tab.make_detail_search({'keyword' => keyword1})
    item_detail = search_result.tap_item('cala商品f8')
    item_detail.add_shop_in_shop_menu
    search_result = item_detail.tap_back_button(SearchResult)
    search_tab = search_result.go_back_home_page(TabsSearch)
    search_tab.clear_search_button.tap
    search_result = search_tab.make_detail_search({'keyword' => keyword1, 'shop' => 'キューエーアンドロゼロイチ'})
    search_result.assert_search_result_match_single_keyword('cala')
  end
end
