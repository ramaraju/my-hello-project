class DetailSearchTab07 < TestCase
  def run
    home = App.goto_home
    search_tab = home.goto_search_tab
    search_result = search_tab.make_detail_search({'keyword' => 'ca', 'genre' => 'CD・DVD・楽器', 'area' => '東京'})
    search_detail = search_result.goto_search_detail
    search_detail.assert_filter_value({'keyword' => 'ca', 'genre' => ['CD・DVD・楽器'], 'area' => '東京'})
    search_detail.assert_shipping_address_suggestion_first_place('東京')
    search_detail.remove_shipping_area
    search_result = search_detail.tap_search_button
    search_result.item_desc.wait
    label_tapped = search_result.tap_the_display_label
    search_detail = search_result.goto_search_detail
    search_detail.assert_no_area_checked
    search_detail.search_history_button.tap
    if label_tapped
      search_detail.assert_search_history_list(['ca'], ['(CD・DVD・楽器)  (在庫あり・注文可能)  (同じ製品をまとめて表示)'])
    else
      search_detail.assert_search_history_list(['ca'], ['(CD・DVD・楽器)  (在庫あり・注文可能)'])
    end
  end
end
