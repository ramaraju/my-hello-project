class DetailSearchTab03 < TestCase
  # rubocop:disable Metrics/AbcSize
  def run
    home = App.goto_home

    keyword1 = 'cala'
    keyword2 = 'ca'
    genre_name1 = 'すべてのジャンルで'

    search_tab = home.goto_search_tab
    search_tab.starting_price.wait.tap
    calabash.enter_text_simulate_keyboard('0')
    search_tab.end_price.wait.tap
    calabash.enter_text_simulate_keyboard('999999999')
    calabash.press_back_button
    expect(search_tab.starting_price.query.first['text']).to eq('0')
    expect(search_tab.end_price.query.first['text']).to eq('999999999')

    search_result = search_tab.make_detail_search({'keyword' => keyword1})
    expect(search_result.current_genre.query.first['text']).to eq(genre_name1)

    search_tab = search_result.go_back_home_page(TabsSearch)
    search_tab.clear_search_button.tap

    search_tab.starting_price.wait.tap
    calabash.enter_text_simulate_keyboard('0')
    search_tab.end_price.wait.tap
    calabash.enter_text_simulate_keyboard('1000000000')
    calabash.press_back_button
    expect(search_tab.starting_price.query.first['text']).to eq('0')
    expect(search_tab.end_price.query.first['text']).to eq('100000000')
    search_tab.clear_search_button.tap

    search_tab.starting_price.wait.tap
    calabash.enter_text_simulate_keyboard('100')
    search_tab.end_price.wait.tap
    calabash.enter_text_simulate_keyboard('50')
    calabash.press_back_button
    expect(search_tab.starting_price.query.first['text']).to eq('100')
    expect(search_tab.end_price.query.first['text']).to eq('50')
    search_tab.search_button.wait.tap
    search_tab.price_range_error.wait
    search_tab.ok_button.tap

    search_tab.clear_search_button.tap
    search_tab.starting_price.wait.tap
    calabash.enter_text_simulate_keyboard('123')
    search_tab.end_price.wait.tap
    calabash.enter_text_simulate_keyboard('45678')
    calabash.press_back_button
    expect(search_tab.starting_price.query.first['text']).to eq('123')
    expect(search_tab.end_price.query.first['text']).to eq('45678')
    search_result = search_tab.make_detail_search({'keyword' => keyword2})
    expect(search_result.current_genre.query.first['text']).to eq(genre_name1)
    search_result.item_desc.wait
  end
end
