class DetailSearchTab02 < TestCase
  def run
    home = App.goto_home

    keyword1 = 'cala'
    keyword2 = 'ca'
    genre_name1 = 'CD・DVD・楽器'
    genre_name2 = '日本酒・焼酎'

    search_tab = home.goto_search_tab
    search_result = search_tab.make_detail_search('keyword' => keyword1, 'genre' => genre_name1)
    expect(search_result.current_genre.query.first['text']).to eq(genre_name1)

    search_tab = search_result.go_back_home_page(TabsSearch)
    expect(search_tab.scroll_to(:keyword, :down).text).to eq(keyword1)
    expect(search_tab.selected_genre_title.text).to eq(genre_name1)

    search_tab.scroll_to(:clear_search_button).tap
    expect(search_tab.keyword.query.first['text']).to eq('')
    search_tab.genre_unselected.wait

    search_result = search_tab.make_detail_search('keyword' => keyword2, 'genre' => genre_name2)
    expect(search_result.current_genre.query.first['text']).to eq(genre_name2)
    search_tab = search_result.go_back_home_page(TabsSearch)
    expect(search_tab.scroll_to(:keyword, :down).text).to eq(keyword2)
    expect(search_tab.selected_genre_title.text).to eq(genre_name2)
  end
end
