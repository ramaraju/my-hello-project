class DetailSearchTab08 < TestCase
  def run
    home = App.goto_home
    search_tab = home.goto_search_tab
    search_tab.assert_filter_value({'availability' => true, 'postage' => false, 'credit' => false})
    search_result = search_tab.make_detail_search({'keyword' => 'cala商品', 'availability' => true, 'postage' => true, 'credit' => true})
    2.times do
      search_result.item_desc.wait
      item_detail = search_result.tap_item
      item_detail.assert_item_availability
      item_detail.assert_item_postage_flag('送料無料')
      item_detail.assert_item_creditcard_flag(true)
      search_result = item_detail.tap_back
    end
    calabash.flick_with_params("* id:'content'", {x: 50, y: 50}, {x: 50, y: 80})
    search_tab = search_result.go_back_home_page(TabsSearch)
    search_result = search_tab.make_detail_search({'availability' => false, 'postage' => false, 'credit' => false})
    search_result.item_desc.wait
    item_detail = search_result.tap_item('cala商品f7')
    item_detail.assert_item_availability(false)
    search_result = item_detail.tap_back
    calabash.scroll_to_row(search_result.search_result_recycler_view.query.to_s, 0)

    item_detail = search_result.tap_item('cala商品f17')
    item_detail.assert_item_postage_flag('送料別')
    search_result = item_detail.tap_back
    calabash.scroll_to_row(search_result.search_result_recycler_view.query.to_s, 0)

    item_detail = search_result.tap_item('cala商品no_card')
    item_detail.assert_item_creditcard_flag(false)
    item_detail.tap_back
  end
end
