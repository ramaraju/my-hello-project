class DetailSearchTab06Data < TestCase
  class << self; attr_accessor :shop_list end
  def run(genre_name, total)
    home = App.goto_home
    self.class.shop_list ||= []
    puts self.class.shop_list
    home.goto_genre_from_nav.select_genre(genre_name)
    search_result = SearchResult.wait
    search_result.item_desc.wait
    calabash.flick_up
    calabash.flick_up
    while self.class.shop_list.length < total.to_i
      calabash.flick_with_params("* id:'content'", {x: 50, y: 40}, {x: 50, y: 10})
      search_result.shop_name_label.wait
      shop_name = search_result.shop_name_label.text
      next if self.class.shop_list.include? shop_name
      search_result.shop_name_label.tap
      item_detail = ItemDetail.wait
      self.class.shop_list << shop_name unless item_detail.add_shop_in_shop_menu.eql?('')
      search_result = item_detail.tap_back_button(SearchResult)
    end
  end
end
