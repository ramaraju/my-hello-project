class DetailSearchTab01 < TestCase
  def run
    home = App.goto_home
    search_tab = home.goto_search_tab
    search_tab.make_empty_search
    expect(search_tab.error_no_search_conditions).to appear
    search_tab.ok_button.tap
    search_result = search_tab.make_detail_search('keyword' => 'asdfghjklkjhgfdsasdfghjklkjhgfdsasdfghjkl')
    expect(search_result.error_message_no_result).to appear
  end
end
