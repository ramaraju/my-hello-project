class Navigation1 < TestCase
  def run
    user = find_user('userhome2_%d', 1..7)
    home = App.login(user)
    home.open_drawer.tap
    home.assert_member_info_on_nav(
      'home_member_name' => user.display_username,
      'member_info_detail_button' => '会員情報',
      'home_member_total_points' => '999,999',
      'home_member_timed_points' => '0',
      'home_member_coupon_total' => '0'
    )
    home.assert_links_on_member_page(
      'my_rakuten' => 'my Rakuten',
      'my_coupon' => 'my クーポン',
      'point_club' => 'Point Club',
      'edy_button' => 'ポイントをEdyに交換',
      'logout_this_app' => 'ログアウト'
    )
    home.assert_page_transfer_on_nav_a(
      'ホーム' => 'tab_home',
      '商品検索' => 'tab_search',
      'キャンペーン' => 'tab_campaign',
      'お気に入り' => 'tab_bookmark',
      '購入履歴' => 'tab_purchase_history'
    )
    home.assert_page_transfer_on_nav_b(
      'ランキング' => '',
      '閲覧履歴' => '',
      'お知らせ' => '',
      '設定' => '',
      'ヘルプ' => '',
      'ログアウト' => ''
    )
    home.assert_link_display_on_nav('楽天グループアプリ' => '', 'ご意見・ご要望' => '')
    home.goto_genre_from_nav
  end
end
