class SPU7 < TestCase
  def run
    bonus_rate = 0
    user = find_user('userbookmark3_%d', 1..7)
    item1 = ITEMS.search(item: 'Recommendation_1')
    item2 = ITEMS.search(item: 'Recommendation_2')
    home = App.login(user)
    home.tap_search_box
    search_result = home.search(item1.item_name)
    item_detail = search_result.tap_item(item1)
    expect(item_detail.home_menu_point_rate_counter.text.to_i).to eq(item1.spux_rate + bonus_rate)
    item_detail.verify_recommendation_item_shown(item2).tap
    expect(item_detail.home_menu_point_rate_counter.text.to_i).to eq(item2.spux_rate + bonus_rate)
    item_detail = item_detail.tap_back(ItemDetail.new(item1))
    expect(item_detail.home_menu_point_rate_counter.text.to_i).to eq(item1.spux_rate + bonus_rate)
  end
end
