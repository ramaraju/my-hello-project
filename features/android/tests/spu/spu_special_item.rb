class SPUSpecialItem < TestCase
  def run
    user = USERS.search(user: 'manualteam')
    item1 = ITEMS.search(item: 'SPUxAn1')
    item2 = ITEMS.search(item: 'SPUxAn2')
    home = App.login(user)
    bookmark = home.goto_bookmarks
    item_detail = bookmark.tap_item(item1)
    expect(item_detail.home_menu_point_rate).not_to exist
    bookmark = item_detail.tap_back(TabsBookmark)
    item_detail = bookmark.tap_item(item2)
    expect(item_detail.home_menu_point_rate).to exist
  end
end
