require_relative '../../utils/search_utils'

class SPU5 < TestCase
  include SearchUtils
  def run
    user = find_user('userspuxonlypremiumcard_%d', 1..7)
    expect(user).to_not be_nil
    item = ITEMS.search(item: 'SPUx通常商品(項目選択肢別がない)1')
    App.login(user)
    item_detail = search(item).tap_item(item)
    item_detail.open_spux_popup.tap
    item_detail.point_rate_detail_contents.wait
    expect(item_detail.spu_point_sum(:total, '6')).to exist
    expect(item_detail.spu_point_sum(:base, '6')).to exist
    expect(item_detail.get_element_arg({method: :spu_point, style: :normal, prefix: '', rate: '1'}, :clickable)).to eq(false)
    expect(item_detail.spu_point(:rakuten_card, '＋', '3')).not_to exist
    expect(item_detail.get_element_arg({method: :spu_point, style: :premium_card, prefix: '＋', rate: '4'}, :clickable)).to eq(false)
    expect(item_detail.get_element_arg({method: :spu_point, style: :monthly_app, prefix: '＋', rate: '1'}, :clickable)).to eq(false)
    expect(item_detail.get_element_arg({method: :spu_point, style: :rakuten_mobile, prefix: '＋', rate: '1'}, :clickable)).to eq(true)
    expect(item_detail.spu_point_sum(:bonus)).not_to exist
  end
end
