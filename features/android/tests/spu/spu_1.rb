class SPU1 < TestCase
  def run(item_name)
    item = ITEMS.search(item: item_name)
    home = App.goto_home
    search_tab = home.goto_search_tab
    item_name = item.cpc_search_keyword.nil? ? item.item_name : item.cpc_search_keyword
    search_result = search_tab.make_detail_search('keyword' => item_name, 'availability' => false)
    item_page = search_result.tap_item(item)
    expect(item_page.home_menu_point_rate).not_to exist
  end
end
