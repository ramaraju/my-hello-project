require_relative '../../utils/search_utils'

class SPU6 < TestCase
  include SearchUtils
  def run
    bonus_rate = 0
    user = find_user('userbookmark3_%d', 1..7)
    item = ITEMS.search(item: 'SPUx通常商品(項目選択肢別がない)1')
    home = App.login(user)
    search_tab = home.goto_search_tab
    search_result = search_tab.make_detail_search('keyword' => item.item_name, 'availability' => false)
    item_detail = search_result.tap_item(item)
    expect(item_detail.home_menu_point_rate_counter).to appear
    expect(item_detail.home_menu_point_rate_counter.text.to_i).to eq(item.spux_rate + bonus_rate)
    App.restart.logout
    item_detail = search(item).tap_item(item)
    expect(item_detail.home_menu_point_rate).not_to exist
  end
end
