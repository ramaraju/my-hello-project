require_relative '../../utils/search_utils'

class SPU4 < TestCase
  include SearchUtils
  def run
    user1 = find_user('userdefault_%d', 1..7)
    user2 = find_user('userspuxspecial_%d', 1..7)
    item = ITEMS.search(item: 'SPUx通常商品(店舗変倍)3')
    App.login(user1)
    item_detail = search(item).tap_item(item)
    item_detail.open_spux_popup.tap
    item_detail.point_rate_detail_contents.wait
    # expect(item_detail.spu_point_sum(:total, '11')).to exist
    expect(item_detail.spu_point_sum(:base, '2')).to exist
    expect(item_detail.get_element_arg({method: :spu_point, style: :normal, prefix: '', rate: '1'}, :clickable)).to eq(false)
    expect(item_detail.get_element_arg({method: :spu_point, style: :rakuten_card, prefix: '＋', rate: '3'}, :clickable)).to eq(true)
    expect(item_detail.get_element_arg({method: :spu_point, style: :monthly_app, prefix: '＋', rate: '1'}, :clickable)).to eq(false)
    expect(item_detail.get_element_arg({method: :spu_point, style: :premium_card, prefix: '＋', rate: '1'}, :clickable)).to eq(true)
    expect(item_detail.get_element_arg({method: :spu_point, style: :rakuten_mobile, prefix: '＋', rate: '1'}, :clickable)).to eq(true)
    # expect(item_detail.spu_point_sum(:bonus, '9')).to exist
    expect(item_detail.get_element_arg({method: :spu_point, style: :shop_point, prefix: '＋', rate: '9'}, :clickable)).to eq(false)
    item_detail.close_spu.tap
    item_detail.point_rate_detail_contents.wait_gone
    App.restart
    App.logout
    App.login(user2)
    item_detail = search(item).tap_item(item)
    item_detail.open_spux_popup.tap
    item_detail.point_rate_detail_contents.wait
    # expect(item_detail.spu_point_sum(:total, '15')).to exist
    expect(item_detail.spu_point_sum(:base, '6')).to exist
    expect(item_detail.get_element_arg({method: :spu_point, style: :normal, prefix: '', rate: '1'}, :clickable)).to eq(false)
    expect(item_detail.get_element_arg({method: :spu_point, style: :rakuten_card, prefix: '＋', rate: '3'}, :clickable)).to eq(false)
    expect(item_detail.get_element_arg({method: :spu_point, style: :monthly_app, prefix: '＋', rate: '1'}, :clickable)).to eq(false)
    expect(item_detail.get_element_arg({method: :spu_point, style: :premium_card, prefix: '＋', rate: '1'}, :clickable)).to eq(true)
    expect(item_detail.get_element_arg({method: :spu_point, style: :rakuten_mobile, prefix: '＋', rate: '1'}, :clickable)).to eq(false)
    # expect(item_detail.spu_point_sum(:bonus, '9')).to exist
    expect(item_detail.get_element_arg({method: :spu_point, style: :shop_point, prefix: '＋', rate: '9'}, :clickable)).to eq(false)
  end
end
