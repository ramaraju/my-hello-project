class SPU3 < TestCase
  def run(item_name)
    bonus_rate = 0
    user = find_user('userbookmark3_%d', 1..7)
    item = ITEMS.search(item: item_name)
    home = App.login(user)
    search_tab = home.goto_search_tab
    item_name = item.cpc_search_keyword.nil? ? item.item_name : item.cpc_search_keyword
    search_result = search_tab.make_detail_search('keyword' => item_name, 'availability' => false)
    item_page = search_result.tap_item(item)
    if item.cpc_item_name.nil?
      expect(item_page.home_menu_point_rate_counter).to appear
      expect(item_page.home_menu_point_rate_counter.text.to_i).to eq(item.spux_rate + bonus_rate)
    else
      expect(item_page.home_menu_point_rate).not_to exist
    end
  end
end
