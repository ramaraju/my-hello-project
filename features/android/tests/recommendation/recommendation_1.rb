class Recommendation1 < TestCase
  def run
    item1 = ITEMS.search(item: 'Recommendation_1')
    item2 = ITEMS.search(item: 'Recommendation_2')
    home = App.goto_home
    home.tap_search_box
    search_result = home.search(item1.item_name)
    item_detail = search_result.tap_item(item1)
    item_detail.verify_recommendation_item_shown(item2)
    item_detail.tap_back(ItemDetail)
    item_detail.verify_recommendation_list(item1, item2)
    item_detail.tap_back(ItemDetail)
    search_result = item_detail.tap_back(SearchResult)
    search_result.search('dhc_recommendation1')
    item_detail = search_result.tap_item('dhc_recommendation1')
    item_detail.verify_recommendation_item_not_shown
  end
end
