class Recommendation2 < TestCase
  def run
    item1 = ITEMS.search(item: 'Recommendation_1')
    item2 = ITEMS.search(item: 'Recommendation_2')
    home = App.goto_home
    home.tap_search_box
    search_result = home.search(item1.item_name)
    item_detail = search_result.tap_item(item1)
    30.times do
      item_detail.verify_recommendation_list(item1, item2)
      item_detail.tap_back(ItemDetail)
    end
  end
end
