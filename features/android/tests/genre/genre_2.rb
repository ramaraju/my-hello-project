class Genre2 < TestCase
  def run
    home = App.goto_home
    home.goto_genre
    home.tap_genre_by_name('CD・DVD・楽器')
    home.assert_genre_title('CD・DVD・楽器')
    search_result = home.search_by_genre('CD・DVD・楽器を選択する')
    search_result.assert_selected_genre('CD・DVD・楽器')
    search_result.assert_the_first_item_is_cpc
    home = search_result.go_back_home_page
    home.tap_search_box
    home.barcode_search_link.wait(timeout_message: 'error in genre search')
  end
end
