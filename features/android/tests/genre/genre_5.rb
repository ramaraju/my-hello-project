class Genre5 < TestCase
  def run
    # function 059
    home = App.goto_home
    home.goto_genre
    home.assert_message_genre_label_shown
    home.assert_the_first_genre_of_level_1_shown
    home.flick_up_to_bottom_button
    home.assert_message_genre_label_shown
    home.assert_genre_bottom_button_shown
    home.flick_down_to_top_button
    home.assert_the_first_genre_of_level_1_shown

    # function 060 061
    home.tap_genre_on_level_1
    home.assert_genre_of_second_level_shown
    home.tap_genre_on_next_level
    home.tap_genre_on_next_level
    genre_on_last_level = 'ホラー'
    search_result = home.tap_genre_on_last_level(genre_on_last_level)
    search_result.assert_selected_genre(genre_on_last_level)

    # function 062
    search_result.tap_the_display_label
    item_detail = search_result.tap_item

    search_result = item_detail.tap_back
    10.times {
      calabash.flick_up
    }
    sleep(0.5)
    search_result.tap_item
  end
end
