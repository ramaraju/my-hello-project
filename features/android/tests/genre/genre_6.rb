class Genre6 < TestCase
  # rubocop:disable Metrics/AbcSize
  def run
    home = App.goto_home
    genre_name_level1 = 'CD・DVD・楽器'
    genre_name_level2 = 'DVD'
    genre_name_level3 = 'アニメ'
    genre_name_level4 = 'オリジナルアニメ'
    genre_name_level5 = 'その他'

    # level 1
    home.goto_genre
    TextView.new(text: genre_name_level1).wait.tap
    TextView.new(text: genre_name_level1 + 'を選択する').wait.tap
    search_result = SearchResult.wait
    search_result.assert_selected_genre(genre_name_level1)
    home = search_result.go_back_home_page

    # level 2
    home.goto_genre
    TextView.new(text: genre_name_level1).wait.tap
    ImageView.new(Query.new(id: 'ichiba_list_item_title', text: genre_name_level2).parent('*').sibling('android.widget.ImageView')).wait.tap
    View.new(text: genre_name_level2 + 'を選択する').wait.tap
    search_result = SearchResult.wait
    search_result.assert_selected_genre(genre_name_level2)
    home = search_result.go_back_home_page

    # level 3
    home.goto_genre
    TextView.new(text: genre_name_level1).wait.tap
    ImageView.new(Query.new(id: 'ichiba_list_item_title', text: genre_name_level2).parent('*').sibling('android.widget.ImageView')).wait.tap
    ImageView.new(Query.new(id: 'ichiba_list_item_title', text: genre_name_level3).parent('*').sibling('android.widget.ImageView')).wait.tap
    View.new(text: genre_name_level3 + 'を選択する').wait.tap
    search_result = SearchResult.wait
    search_result.assert_selected_genre(genre_name_level3)
    home = search_result.go_back_home_page

    # level 4
    home.goto_genre
    TextView.new(text: genre_name_level1).wait.tap
    ImageView.new(Query.new(id: 'ichiba_list_item_title', text: genre_name_level2).parent('*').sibling('android.widget.ImageView')).wait.tap
    ImageView.new(Query.new(id: 'ichiba_list_item_title', text: genre_name_level3).parent('*').sibling('android.widget.ImageView')).wait.tap
    ImageView.new(Query.new(id: 'ichiba_list_item_title', text: genre_name_level4).parent('*').sibling('android.widget.ImageView')).wait.tap
    View.new(text: genre_name_level4 + 'を選択する').wait.tap
    search_result = SearchResult.wait
    search_result.assert_selected_genre(genre_name_level4)
    home = search_result.go_back_home_page

    # level 5
    home.goto_genre
    TextView.new(text: genre_name_level1).wait.tap
    ImageView.new(Query.new(id: 'ichiba_list_item_title', text: genre_name_level2).parent('*').sibling('android.widget.ImageView')).wait.tap
    ImageView.new(Query.new(id: 'ichiba_list_item_title', text: genre_name_level3).parent('*').sibling('android.widget.ImageView')).wait.tap
    ImageView.new(Query.new(id: 'ichiba_list_item_title', text: genre_name_level4).parent('*').sibling('android.widget.ImageView')).wait.tap
    ImageView.new(Query.new(id: 'ichiba_list_item_title', text: genre_name_level5).parent('*').sibling('android.widget.ImageView')).wait.tap
    View.new(text: genre_name_level5).wait.tap
    search_result = SearchResult.wait
    search_result.assert_selected_genre(genre_name_level5)
    search_result.go_back_home_page
  end
end
