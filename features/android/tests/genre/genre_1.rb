class Genre1 < TestCase
  def run
    home = App.goto_home
    home.goto_genre
    home.assert_genre_title('ジャンル選択')
    home.tap_genre_by_name('CD・DVD・楽器')
    home.assert_genre_title('CD・DVD・楽器')
    home.tap_genre_by_index(1)
    home.assert_genre_title('楽器')
    home.tap_genre_by_index(-1)
    home.assert_genre_title('CD・DVD・楽器')
    genre = home.search_by_genre('CD')
    genre.assert_selected_genre('CD')
    genre.tap_genre_search_button
    genre.assert_selected_genre_on_search_result('CD')
    genre.tap_the_return_button_on_genre_list
    genre.tap_genre_search_button
    genre.assert_selected_genre_on_search_result('CD')
    genre.tap_the_genre_cancel_button_and_check
  end
end
