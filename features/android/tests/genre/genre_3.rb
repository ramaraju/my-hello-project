class Genre3 < TestCase
  def run
    home = App.goto_home
    # When I tap search box and confirm transform
    home.tap_search_box
    # Then I input "b" into search box
    home.enter_search_box('b')
    # Then confirm genre on suggest list
    home.assert_genre_shown_on_suggest_list
    # Then I add suggest No."0" into box and check
    home.tap_suggestion_by_index(0)
    # And tap the search button
    search_result = home.tap_search_button
    # Then confirm the transformed page is search result page
    search_result.assert_search_result_page_shown
  end
end
