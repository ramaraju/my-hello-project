class Genre4 < TestCase
  def run
    home = App.goto_home
    # When I tap search box and confirm transform
    home.tap_search_box
    # And I input "tiger" into search box
    home.enter_search_box('tiger')
    # And tap the search button
    search_result = home.tap_search_button
    # And confirm the transformed page is search result page
    search_result.assert_search_result_page_shown
    # Then I confirm "tiger" on search result page
    search_result.assert_keyword_value('tiger')
    # When tap detail search on search result page
    search_detail = search_result.goto_search_detail
    # Then  I confirm "tiger" on detail search page
    search_detail.assert_keyword('tiger')
    # When I tap the back button back to the search result
    search_result = search_detail.tap_back
    # And I back to the home page
    home = search_result.go_back_home_page
    # Then confirm search history "tiger"
    home.assert_search_history_part('tiger')
    home.back_button.tap
  end
end
