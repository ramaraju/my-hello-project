class Genre7 < TestCase
  def run
    user = find_user('userdefault_%d', 1..7)
    App.login(user)
    App.clear_cart
    home = App.goto_home
    genre = home.goto_genre_from_nav
    genre.select_genre('車用品・バイク用品')
    search_result = SearchResult.wait
    search_result.item_desc.wait(timeout_message: 'search result not found')
    item_name = search_result.item_name
    search_result.slide_genre_ad(item_name)
    details = search_result.tap_genre_ad(item_name)
    cart = details.place_in_cart
    step1 = cart.tap_process_order
    payment = step1.enter_password(Payment.new(:Step1))
    step2 = payment.select_payment('代金引換')
    step2.tap_place_order
  end
end
