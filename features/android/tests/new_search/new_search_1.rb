class NewSearch1 < TestCase
  def run
    home = App.goto_home
    home_tabs = home.goto_search_tab
    expect(home_tabs.reveal_view('search_button').tap)
    home_tabs.error_message.wait(timeout_message: 'loading fail')
    home_tabs.error_message.text.eql?('キーワード、ジャンル、ショップいずれかの検索条件を指定してください。')
    calabash.press_back_button
    search_result = home_tabs.make_detail_search('keyword' => 'tiger')
    search_result.tap_new_search_and_confirm_search_history(['tiger'])
    search_result.search('cat')
    search_result.tap_new_search_and_confirm_search_history(%w(cat tiger))
    home = App.restart
    home.tap_search_box
    home.enter_search_box('item_with_link')
    search_result = home.tap_search_button
    details = search_result.tap_item('item_with_link')
    shop_name = details.add_shop_in_shop_menu
    home = App.restart
    home_tabs = home.goto_search_tab
    search_result = home_tabs.make_detail_search('keyword' => 'item_with_link', 'shop' => shop_name)
    search_result.item_desc.wait
    search_result.new_search.tap
    search_result.assert_new_search_history_with_subtitle('keyword' => 'item_with_link', 'shop' => shop_name)
    home_tabs = search_result.go_back_home_page(TabsSearch)
    home_tabs.shop_select.tap
    home_tabs.remove_shop.tap
    expect(home_tabs.reveal_view('search_button').tap)
    SearchResult.wait.item_desc.wait
    SearchResult.wait.new_search.tap
    SearchResult.wait.assert_new_search_history_with_subtitle('keyword' => 'item_with_link')
    SearchResult.wait.go_back_home_page(TabsSearch)
    home = App.restart
    home_tabs = home.goto_search_tab
    search_result = home_tabs.make_detail_search('keyword' => 'item', 'area' => '秋田')
    search_result.item_desc.wait
    search_result.new_search.tap
    search_result.assert_new_search_history_with_subtitle('keyword' => 'item', 'area' => '秋田')
    home_tabs = search_result.go_back_home_page(TabsSearch)
    home_tabs.area_select.tap
    home_tabs.remove_area.tap
    expect(home_tabs.reveal_view('search_button').tap)
    SearchResult.wait.item_desc.wait
    SearchResult.wait.new_search.tap
    SearchResult.wait.assert_new_search_history_with_subtitle('keyword' => 'item')
    SearchResult.wait.delete_search_history_of_new_search
    SearchResult.wait.search_history_list.wait_gone
  end
end
