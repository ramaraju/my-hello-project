class NewSearch2 < TestCase
  include SearchUtils
  def run
    item = ITEMS.search(item: '通常商品(項目選択肢別がない) 31')
    App.goto_home
    details = search(item).tap_item(item)
    expect(details.item_name).to eq(item.display_name)
    details.add_shop_in_shop_menu
    home = App.restart
    home_tabs = home.goto_search_tab
    search_result = home_tabs.make_detail_search(load_data('NewSearch_2')['filter1'])
    search_result.item_desc.wait
    search_result.new_search.tap
    search_result.assert_new_search_history_with_subtitle(load_data('NewSearch_2')['filter1'])
    search_result.find_new_search_history('cala').tap
    search_result.item_desc.wait
    search_detail = search_result.goto_search_detail
    search_detail.assert_filter_value(load_data('NewSearch_2')['filter1'])
    search_detail.clear_button.tap
    search_detail.popup_yes_button.tap
    search_result = search_detail.make_detail_search(load_data('NewSearch_2')['filter1'])
    search_result.item_desc.wait
    search_result.new_search.tap
    search_result.assert_search_history_num('2')
  end
end
