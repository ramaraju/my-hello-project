class Home4 < TestCase
  def run
    home = App.goto_home
    featured_events_sum = home.featured_events_account
    campaign = home.featured_events_detail.tap!
    expect(campaign.campaign_events).to appear
    expect(campaign.campaign_events.query.size).to eq(featured_events_sum)
  end
end
