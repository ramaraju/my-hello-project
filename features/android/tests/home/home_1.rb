class Home1 < TestCase
  def run
    user = find_user('userhome1_%d', 1..7)
    # function 043
    App.login(user)
    home = App.restart
    home.assert_user_info(user)

    # function 044
    home.logout
    home = App.login(user)

    # function 045
    home.goto_user_info
    home.assert_info_on_page(
      'user_name' => 'user home1 さん',
      'member_total_points' => '999,999',
      'member_timed_points' => '  0',
      'member_future_granted_points' => '  0'
    )

    myrakuten = home.goto_my_rakuten
    expect(myrakuten.retrieve_my_rakuten_page_trait).to eq('会員登録情報変更・確認')
    home = myrakuten.back_to_user_info

    pointclub = home.goto_point_club
    pointclub.assert_point_club_page_reached
    home = pointclub.back_to_user_info

    # function 048
    calabash.press_back_button
    # page = App.goto_home
    home.tap_barcode
    home.assert_message_camera_shown
  end
end
