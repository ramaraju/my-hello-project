class Home2 < TestCase
  def run
    user = find_user('userhome2_%d', 1..7)
    home = App.login(user)
    home.member_info_from_nav
    home.assert_info_on_page({'user_name' => 'user home2 さん', 'member_total_points' => '999,999', 'member_timed_points' => '  0', 'member_future_granted_points' => '  0'})

    myrakuten = home.goto_my_rakuten
    expect(myrakuten.retrieve_my_rakuten_page_trait).to eq('会員登録情報変更・確認')
    home = myrakuten.back_to_user_info

    mykoupon = home.goto_mycoupon
    expect(mykoupon.retrieve_mycoupon_page_trait).to eq('myクーポン')
    home = mykoupon.back_to_user_info

    pointclub = home.goto_point_club
    pointclub.assert_point_club_page_reached
    pointclub.back_to_user_info
  end
end
