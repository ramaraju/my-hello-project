class Home3 < TestCase
  def run
    user = find_user('userdefault_%d', 1..7)
    home = Home.wait
    home.to_member_frame
    login = home.login_button.tap!
    # C9194291 can not automated
    # expect(login.user_id_textbox).to exist
    # expect(login.password_textbox).to exist
    home = login.login(user)
    home.logout
    home.to_member_frame
    login = home.login_button.tap!
    expect(login.login_current_username).to appear
    expect(login.login_current_username.text).to eq(user.display_username)
    home = login.login_current_account_button.tap
    home.logout
    home.to_member_frame
    login = home.login_button.tap!
    expect(login.login_current_username).to appear
    expect(login.login_current_username.text).to eq(user.display_username)
    login.login_other_account_button.tap
    home = login.login(user)
    home.to_member_frame
    expect(home.member_name_label).to appear
    expect(home.member_name_label.text).to eq(user.display_username)
  end
end
