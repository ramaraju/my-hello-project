class Home5 < TestCase
  def run
    home = App.goto_home
    tab_search = home.header.tabs.search.tap!
    tab_campaign = tab_search.header.tabs.campaign.tap!
    tab_bookmark = tab_campaign.header.tabs.bookmarks.tap!
    tab_bookmark.header.tabs.history.tap!
  end
end
