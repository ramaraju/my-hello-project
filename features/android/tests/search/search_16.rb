class Search16 < TestCase
  def run
    home = App.goto_home
    data = load_data('Search_16')
    search_tab = home.goto_search_tab
    search_result = search_tab.make_detail_search(data['search_condition_1'])
    item_detail = search_result.tap_item(data['item_name_1'])
    item_detail.add_shop_in_shop_menu
    search_result = item_detail.tap_back_button(SearchResult)
    search_detail = search_result.goto_search_detail

    search_detail.make_detail_search(data['search_condition_2'])
    search_detail.error_message_no_result.wait
    search_detail.clear_error

    search_detail.assert_filter_value(data['result1'])

    search_detail.clear_search_condition
    search_detail.assert_filter_value(data['result2'])
  end
end
