class Search18 < TestCase
  def run
    home = App.goto_home
    search_tab = home.goto_search_tab
    search_result = search_tab.make_detail_search('keyword' => 'cala')
    search_result.assert_item_amount_in_result('less', 300)
    search_result.assert_flick_and_touch(:up)
    search_result.assert_flick_and_touch(:down)
  end
end
