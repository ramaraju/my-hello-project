class Search5 < TestCase
  def run
    home = App.goto_home
    home.tap_search_box
    search_result = home.search('qa')
    search_result.assert_search_result
    search_detail = search_result.goto_search_detail
    expect(search_detail.keyword).to appear
    expect(search_detail.keyword.text).to eq('qa')
    search_detail.clear_search_condition
    search_detail.search_button.tap!
    search_detail.error_message_no_input.wait
    search_detail.popup_ok_button.tap
    search_result = search_detail.tap_back
    home = search_result.go_back_home_page
    home.tap_search_box
    search_result = home.search('qazwsxedc@')
    search_result.error_message_no_result.wait
    search_detail = search_result.goto_search_detail
    search_detail.search_button.tap!
    search_detail.error_message_no_result.wait
    search_detail.popup_ok_button.tap
    search_result = search_detail.tap_back
    home = search_result.go_back_home_page
    home.tap_search_box
    search_result = home.search('qa')
    search_result.assert_search_result
    genre_page = search_result.tap_genre_search_button
    genre_page.select_genre(['CD・DVD・楽器', 'DVD'])
    SearchResult.wait.assert_search_result
    SearchResult.wait.assert_search_conditon_on_search_result('qa', 'DVD')
  end
end
