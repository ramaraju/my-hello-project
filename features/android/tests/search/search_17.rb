require_relative '../../utils/search_utils'

class Search17 < TestCase
  include SearchUtils
  # rubocop:disable Metrics/MethodLength
  def run
    home = App.goto_home
    home.tap_search_box
    home.assert_barcode_shown
    search_result = home.search('cpc')
    sleep(2)
    search_result.assert_the_first_item_is_cpc

    # search_result.tap_back
    search_result.search('auto')
    search_result.assert_the_first_item_is_not_cpc

    home = search_result.go_back_home_page
    home.goto_genre
    home.tap_genre_by_name('CD・DVD・楽器')
    search_result = home.search_by_genre('DVD')
    search_result.assert_the_first_item_is_cpc

    home = search_result.go_back_home_page
    home.goto_genre
    home.tap_genre_by_name('ダイエット・健康')
    search_result = home.search_by_genre('サプリメント')
    search_result.assert_the_first_item_is_not_cpc

    home = search_result.go_back_home_page
    home.goto_genre
    home.tap_genre_by_name('CD・DVD・楽器')
    search_result = home.search_by_genre('CD・DVD・楽器を選択する')
    search_result.search('cpc')
    search_result.assert_the_first_item_is_cpc

    home = search_result.go_back_home_page
    home.goto_genre
    home.tap_genre_by_name('ビール・洋酒')
    search_result = home.search_by_genre('ビール・洋酒を選択する')
    search_result.search('cpc')
    search_result.assert_the_first_item_is_not_cpc

    home = search_result.go_back_home_page
    home.goto_genre
    home.tap_genre_by_name('CD・DVD・楽器')
    search_result = home.search_by_genre('CD・DVD・楽器を選択する')
    search_result.search('auto')
    search_result.assert_the_first_item_is_not_cpc

    home = search_result.go_back_home_page
    home.tap_search_box
    # home.assert_barcode_shown
    search_result = home.search('cpc')
    sleep(2)
    search_result.assert_the_first_item_is_cpc
    search_detail = search_result.goto_search_detail
    search_result = search_detail.make_detail_search(load_data('Function_40')['filter1'])
    search_result.assert_the_first_item_is_not_cpc

    home = search_result.go_back_home_page
    home.tap_search_box
    # home.assert_barcode_shown
    search_result = home.search('cpc')
    item_detail = search_result.tap_item_by_name('CPC Release Check 01')
    item_detail.add_shop_in_shop_menu
    search_result = item_detail.tap_back
    sleep(2)
    search_result.assert_the_first_item_is_cpc
    search_detail = search_result.goto_search_detail
    search_result = search_detail.make_detail_search(load_data('Function_40')['filter2'])
    search_result.assert_the_first_item_is_not_cpc
  end
end
