class Search2 < TestCase
  def run
    home = App.goto_home
    home.tap_search_box
    home.assert_barcode_shown
    search_result = home.search('qa')
    search_result.assert_search_result
    search_result.search('dhc')
    search_result.assert_search_result
    home = search_result.go_back_home_page
    home.assert_search_histories_shown('qa', 'dhc')
    home.goto_home
    search_result = home.search_by_history
    search_result.assert_search_result
    # added
    home = search_result.go_back_home_page
    home.tap_search_box
    home.enter_search_box('b')
    home.suggestions_list.wait(timeout_message: 'suggestion cannot be found')
    home.suggestions_item_list.wait(timeout_message: 'suggestion item cannot be found')
    search_text = home.assert_suggestion_selected(3)
    search_result = home.tap_suggestion_title(search_text)
    search_result.assert_keyword_value(search_text)
  end
end
