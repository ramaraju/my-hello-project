class Search11 < TestCase
  def run
    home = App.goto_home
    search_tab = home.goto_search_tab
    search_result = search_tab.make_detail_search('keyword' => 'cala商品f8')
    item_detail = search_result.tap_item('cala商品f8')
    item_detail.add_shop_in_shop_menu
    search_result = item_detail.tap_back_button(SearchResult)
    search_detail = search_result.goto_search_detail
    search_detail.clear_search_condition
    search_result = search_detail.make_detail_search('keyword' => 'cala商品f8', 'shop' => 'キューエーアンドロゼロイチ')
    search_result.assert_search_result_match_single_keyword('cala')
    search_detail = search_result.goto_search_detail
    search_detail.search_history_button.tap
    search_detail.assert_search_history_list(['cala商品f8'], ["(ショップ名:キューエーアンドロゼロイチ) \n(在庫あり・注文可能)"])

    search_detail.search_detail_button.tap
    search_detail.goto_remove_shop_suggestion
    search_result = search_detail.tap_search_button
    search_result.assert_search_result_match_single_keyword('cala商品f8')
    search_detail = search_result.goto_search_detail
    search_detail.search_history_button.tap
    search_detail.assert_search_history_list(['cala商品f8'], ['(在庫あり・注文可能)'])
  end
end
