class Search19 < TestCase
  def run
    home = App.goto_home
    search_tab = home.goto_search_tab
    search_result = search_tab.make_detail_search('keyword' => 'ha')
    search_result.assert_item_amount_in_result('greater', 3000)
    search_result.assert_upperbound_msg
    search_result.assert_transfer_of_clicking_upperbound_button
  end
end
