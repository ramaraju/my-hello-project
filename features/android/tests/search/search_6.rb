class Search6 < TestCase
  def run
    page = App.goto_home
    page = page.goto_search_tab
    page = page.make_detail_search(load_data('Function_134')['filter1'])
    page.check_search_result(load_data('Function_134')['result1'])
    page = page.goto_search_detail
    page.assert_filter_value(load_data('Function_134')['filter1'])
    page.clear_search_condition
    page = page.make_detail_search(load_data('Function_134')['filter2'])
    page.check_search_result(load_data('Function_134')['result2'])
    page = page.goto_search_detail
    page.assert_filter_value(load_data('Function_134')['filter2'])
  end
end
