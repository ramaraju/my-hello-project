# Regression_function_P004_37_Verify error message when specify more than ten tags in one tag group
class Search8 < TestCase
  def run
    home = App.goto_home
    search_tab = home.goto_search_tab
    search_result = search_tab.make_detail_search('keyword' => 'cala')

    search_detail = search_result.goto_search_detail
    search_detail.set_search_price_range('123', '45678')
    search_result = search_detail.tap_search_button
    search_result.assert_search_result_page_shown

    search_detail = search_result.goto_search_detail
    search_detail.set_search_price_range('0', '999999999')
    search_result = search_detail.tap_search_button
    search_result.assert_search_result_page_shown

    search_detail = search_result.goto_search_detail
    search_detail.set_search_price_range('100', '50')
    expect(search_detail.price_range_error_msg).to be_visible
    search_detail.popup_ok_button.tap

    search_detail.input_price_range('0', '10000000000')
    search_detail.assert_price_input('100000000', :right)
    calabash.press_back_button
    calabash.press_back_button

    search_detail.clear_search_condition
    sleep(0.5)

    search_detail.assert_genre_cleared('CD・DVD・楽器')
    search_detail.assert_price_input('', :left)
    search_detail.assert_price_input('', :right)
  end
end
