# Regression_function_P004_37_Verify error message when specify more than ten tags in one tag group
class Search9 < TestCase
  def run
    home = App.goto_home
    home.goto_genre
    home.tap_genre_on_level_1
    search_result = home.goto_the_particular_genre("* id:'ichiba_list_item_title' text:'CD・DVD・楽器を選択する'")
    calabash.wait_progress_bar_gone
    search_result.tap_the_display_label
    search_detail = search_result.goto_search_detail
    search_detail.to_edit_except_keyword('abcdefghijklmnopqrstuvwxyz12345678909876543210aaaa')
    search_result = search_detail.tap_search_button
    search_result.assert_search_result_page_shown

    search_detail = search_result.goto_search_detail
    search_detail.to_edit_except_keyword('abcdefghijklmnopqrstuvwxyz12345678909876543210aaaab')
    search_detail.assert_except_keyword_input_correct('abcdefghijklmnopqrstuvwxyz12345678909876543210aaaa')

    search_detail.clear_search_conditions
    search_detail.to_edit_keyword('cala f8')
    search_result = search_detail.tap_search_button
    item_detail = search_result.tap_item('cala商品f8')
    item_detail.add_shop_in_shop_menu
    search_result = item_detail.tap_back_button(SearchResult)
    search_detail = search_result.goto_search_detail
    search_detail.clear_search_conditions
    search_detail.to_select_shop
    search_detail.to_edit_keyword('商品')
    search_detail.to_edit_except_keyword('cala')
    search_result = search_detail.tap_search_button
    search_result.assert_except_keyword_search('商品', 'cala')
  end
end
