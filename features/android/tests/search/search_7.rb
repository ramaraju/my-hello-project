# Regression_function_P004_37_Verify error message when specify more than ten tags in one tag group
class Search7 < TestCase
  def run
    home = App.goto_home
    # Given the home screen is displayed "first"
    number_of_tags_to_select = 11
    # When I tap on genre button
    home.goto_genre
    home.go_to_western_wine_genre
    search_result = home.goto_the_particular_genre("* text:'ワイン'")
    search_result.tap_the_display_label
    search_detail = search_result.goto_search_detail
    search_detail.go_to_select_genre
    search_detail.tap_genre('赤ワイン')
    search_detail.tap_genre('黒ぶどう')
    search_detail.select_some_tags(number_of_tags_to_select)
    expect(search_detail.error_message).to appear

    search_detail.popup_ok_button.tap
    search_detail.tick_one_tag_off
    search_detail.click_submit
    search_detail.click_submit
    search_detail.assert_search_result_shown
    search_detail.go_back_home_page

    home = App.restart
    home.goto_genre
    home.go_to_western_wine_genre
    search_result = home.goto_the_particular_genre("* text:'ワイン'")
    search_result.tap_the_display_label
    search_detail = search_result.goto_search_detail
    search_detail.go_to_select_genre
    search_detail.tap_genre('赤ワイン')
    search_detail.tap_genre('白ぶどう')
    search_detail.select_some_tags(6)
    search_detail.click_submit
    search_detail.tap_genre('黒ぶどう')
    search_detail.select_some_tags(5)
    sleep(2)
    search_detail.assert_error_for_too_many_tags_shown
    search_detail.popup_ok_button.tap
    search_detail.go_back_home_page
  end
end
