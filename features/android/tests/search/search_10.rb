class Search10 < TestCase
  def run
    home = App.goto_home
    search_tab = home.goto_search_tab
    search_result = search_tab.make_detail_search('keyword' => 'cala')
    search_detail = search_result.goto_search_detail
    search_detail.clear_search_condition
    search_result = search_detail.make_detail_search('keyword' => 'cala', 'area' => '東京')
    search_result.check_search_result('cala商品f4')
    search_detail = search_result.goto_search_detail
    search_detail.search_history_button.tap
    search_detail.assert_search_history_list(['cala'], ['(東京)  (在庫あり・注文可能)'])
    search_detail.search_detail_button.tap
    search_detail.assert_shipping_address_suggestion_first_place('東京')
    search_detail.remove_shipping_area
    search_result = search_detail.tap_search_button
    search_result.assert_search_result_match_single_keyword('cala')
    search_detail = search_result.goto_search_detail
    search_detail.assert_no_area_checked
    search_detail.search_history_button.tap
    search_detail.assert_search_history_list(['cala'], ['(在庫あり・注文可能)'])
  end
end
