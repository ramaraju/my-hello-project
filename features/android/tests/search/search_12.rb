class Search12 < TestCase
  def run
    home = App.goto_home
    search_tab = home.goto_search_tab
    search_result = search_tab.make_detail_search('keyword' => 'cala')
    search_detail = search_result.goto_search_detail
    search_detail.assert_filter_value('availability' => true)
    search_result = search_detail.tap_search_button
    3.times do
      item_detail = search_result.tap_item
      item_detail.assert_item_availability
      search_result = item_detail.tap_back
      calabash.flick_up
      sleep(1)
    end
    search_detail = search_result.goto_search_detail
    search_detail.assert_filter_value('availability' => true)
    search_detail.reveal_view(:availability_checkbox).tap
    search_detail.assert_filter_value('availability' => false)
    search_result = search_detail.tap_search_button
    item_detail = search_result.tap_item
    item_detail.assert_item_availability
    search_result = item_detail.tap_back
    item_detail = search_result.tap_item('cala商品f7')
    item_detail.assert_item_availability(false)
  end
end
