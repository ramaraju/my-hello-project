class Search14 < TestCase
  def run
    home = App.goto_home
    search_tab = home.goto_search_tab
    search_result = search_tab.make_detail_search('keyword' => 'cala')
    search_detail = search_result.goto_search_detail
    search_detail.assert_filter_value('credit' => false)

    search_result = search_detail.tap_search_button

    item_detail = search_result.tap_item('cala商品f17')
    item_detail.assert_item_creditcard_flag(true)
    search_result = item_detail.tap_back

    item_detail = search_result.tap_item('cala商品no_card')
    item_detail.assert_item_creditcard_flag(false)
    search_result = item_detail.tap_back

    search_detail = search_result.goto_search_detail
    calabash.flick_up
    search_detail.assert_filter_value('credit' => false)
    search_detail.credit_checkbox.tap
    search_detail.assert_filter_value('credit' => true)
    search_result = search_detail.tap_search_button

    4.times do
      item_detail = search_result.tap_item
      item_detail.assert_item_creditcard_flag(true)
      search_result = item_detail.tap_back
      calabash.flick_up
      sleep(1)
    end
  end
end
