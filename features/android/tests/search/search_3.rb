class Search3 < TestCase
  def run
    home = App.goto_home
    home.tap_search_box
    home.assert_barcode_shown
    search = home.search('PS2_Saeki')
    search.assert_search_result
    search.assert_cheapest_shop_link
  end
end
