class Search13 < TestCase
  def run
    home = App.goto_home
    search_tab = home.goto_search_tab
    search_result = search_tab.make_detail_search('keyword' => 'cala商品')
    search_detail = search_result.goto_search_detail
    search_detail.assert_filter_value('postage' => false)
    search_result = search_detail.tap_search_button

    item_detail = search_result.tap_item('cala商品f17')
    item_detail.assert_item_postage_flag('送料別')
    search_result = item_detail.tap_back
    calabash.list_items(Query.new(id: 'itemDesc'), :down)

    item_detail = search_result.tap_item('cala商品f4')
    item_detail.assert_item_postage_flag('送料無料')
    search_result = item_detail.tap_back

    search_detail = search_result.goto_search_detail
    calabash.flick_up
    search_detail.assert_filter_value('postage' => false)
    search_detail.postage_checkbox.tap
    search_detail.assert_filter_value('postage' => true)
    search_result = search_detail.tap_search_button

    3.times do
      item_detail = search_result.tap_item
      item_detail.assert_item_postage_flag('送料無料')
      search_result = item_detail.tap_back
      calabash.flick_up
      sleep(1)
    end
  end
end
