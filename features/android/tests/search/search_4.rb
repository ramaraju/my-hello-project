class Search4 < TestCase
  def run
    home = App.goto_home
    home.tap_search_box
    home.assert_barcode_shown
    search = home.search('cala_sort_product')
    search.assert_search_result
    search.assert_display_mode('list')
    search.assert_display_mode('middle')
    search.assert_display_mode('large')
  end
end
