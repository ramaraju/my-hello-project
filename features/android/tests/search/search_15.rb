class Search15 < TestCase
  def run
    home = App.goto_home
    search_tab = home.goto_search_tab
    search_result = search_tab.make_detail_search('keyword' => 'cala')
    search_detail = search_result.goto_search_detail
    search_detail.assert_filter_value('sort' => '標準')
    search_result = search_detail.tap_search_button
    search_result.assert_search_result_page_shown

    search_detail = search_result.goto_search_detail
    search_result = search_detail.make_detail_search('sort' => '価格が安い')
    search_result.assert_search_result_page_shown

    search_detail = search_result.goto_search_detail
    search_result = search_detail.make_detail_search('sort' => '価格が高い')
    search_result.assert_search_result_page_shown

    search_detail = search_result.goto_search_detail
    search_result = search_detail.make_detail_search('sort' => '新着順')
    search_result.assert_search_result_page_shown

    search_detail = search_result.goto_search_detail
    search_result = search_detail.make_detail_search('sort' => '感想の件数が多い')
    search_result.assert_search_result_page_shown

    search_detail = search_result.goto_search_detail
    search_result = search_detail.make_detail_search('sort' => '感想の評価が高い')
    search_result.assert_search_result_page_shown

    search_detail = search_result.goto_search_detail
    search_result = search_detail.make_detail_search('sort' => '価格が安い')
    search_result.assert_search_result_page_shown

    search_detail = search_result.goto_search_detail
    search_result = search_detail.make_detail_search('sort' => '価格が高い')
    search_result.assert_search_result_page_shown
  end
end
