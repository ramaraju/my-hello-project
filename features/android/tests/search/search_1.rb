class Search1 < TestCase
  def run
    home = App.goto_home
    home.tap_search_box
    home.enter_characters_for_searches(1)
    home.assert_error_message_shown
    home.goto_home
    home.tap_search_box
    home.enter_characters_for_searches(3)
    home.assert_suggestion_list_shown
    search_result = home.make_suggestion_search('dhc')
    search_result.assert_search_result
  end
end
