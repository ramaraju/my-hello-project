require_relative '../../utils/search_utils'

class Shop2 < TestCase
  include SearchUtils
  def run
    item = ITEMS.search(item: '通常商品(藥類) 36')
    App.goto_home
    item_detail = search(item).tap_item(item)
    5.times {
      search_result = item_detail.find_and_tap_show_all_item
      search_result.assert_the_number_of_search_result('64')
      item_detail = search_result.tap_back_to_item_detail
    }
  end
end
