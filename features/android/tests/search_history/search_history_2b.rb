require_relative '../../utils/search_utils'

class SearchHistory2b < TestCase
  include SearchUtils
  def run
    item = ITEMS.search(item: 'searchitem')
    App.goto_home
    details = search(item).tap_item(item)
    expect(details.item_name).to eq(item.display_name)
    details.add_shop_in_shop_menu
    search_result = details.tap_back
    search_detail = search_result.goto_search_detail
    search_detail.clear_search_condition
    search_result = search_detail.make_detail_search(load_data('Function_43')['filter1'])
    home = search_result.go_back_home_page
    search_result = home.search_by_history
    search_detail = search_result.goto_search_detail
    search_detail.assert_filter_value(load_data('Function_43')['filter1'])
  end
end
