class SearchHistory32 < TestCase
  def run
    home = App.goto_home
    home.tap_search_box
    search_result = nil
    31.upto(60) do |i|
      id = "a#{i}"
      if i == 31
        search_result = home.search(id)
      else
        search_result.search(id)
      end
      expect(search_result.item_desc).to appear
    end
  end
end
