class SearchHistory2a < TestCase
  # rubocop:disable Metrics/MethodLength
  def run
    home = App.goto_home
    home.tap_search_box
    home.press_enter_button_confirm_current_screen

    home.enter_search_box('kkkkkkkkkkkkkkk')
    search_result = home.tap_search_button
    search_result.error_message_no_result.wait

    home = search_result.go_back_home_page

    home.tap_search_box
    home.enter_search_box('cala商品f4')
    search_result = home.tap_search_button
    search_result.assert_keyword_value('cala商品f4')
    search_detail = search_result.goto_search_detail
    search_detail.search_history_button.tap
    search_detail.assert_search_history_is_exist('cala商品f4')
    search_detail.assert_search_history_from_detail_page(1)
    search_result = search_detail.tap_back
    search_result.tap_new_search_and_confirm_search_history(['cala商品f4'])
    home = search_result.go_back_home_page
    home.tap_search_box
    home.assert_search_history(['cala商品f4'])

    # second search
    home.enter_search_box('cala商品f4')
    search_result = home.tap_search_button
    search_result.assert_keyword_value('cala商品f4')
    search_detail = search_result.goto_search_detail
    search_detail.search_history_button.tap
    search_detail.assert_search_history_is_exist('cala商品f4')
    search_detail.assert_search_history_from_detail_page(1)
    search_result = search_detail.tap_back
    search_result.tap_new_search_and_confirm_search_history(['cala商品f4'])
    home = search_result.go_back_home_page
    home.tap_search_box
    home.assert_search_history(['cala商品f4'])

    home.enter_search_box('bookmark')
    search_result = home.tap_search_button
    search_result.assert_keyword_value('bookmark')
    search_result.goto_search_detail
    search_detail.search_history_button.tap
    search_detail.assert_search_history_is_exist('bookmark')
    search_detail.assert_search_history_list(['bookmark', 'cala商品f4'])
    search_result = search_detail.tap_back
    search_result.tap_new_search_and_confirm_search_history(['bookmark', 'cala商品f4'])
    home = search_result.go_back_home_page
    home.tap_search_box
    home.assert_search_history(['bookmark', 'cala商品f4'])

    search_result = home.search_by_search_history('cala商品f4')
    search_detail = search_result.goto_search_detail
    search_detail.search_history_button.tap
    search_detail.assert_search_history_list(['cala商品f4', 'bookmark'])
    search_detail.assert_search_history_from_detail_page(2)
    search_result = search_detail.tap_back
    search_result.tap_new_search_and_confirm_search_history(['cala商品f4', 'bookmark'])
    home = search_result.go_back_home_page
    home.tap_search_box
    home.assert_search_history(['cala商品f4', 'bookmark'])
    home.delete_search_history_link.tap
    home.assert_history_not_shown
  end
end
