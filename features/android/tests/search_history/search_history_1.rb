class SearchHistory1 < TestCase
  def run
    home = App.goto_home
    home.tap_search_box
    # add
    search_result = home.search('qaaaq')
    search_result.new_search.tap
    search_result.search_history_list.wait_gone
    home = search_result.go_back_home_page
    home.tap_search_box
    search_result = home.make_multiple_searches(21)
    search_result.new_search.tap
    search_result.search_history_list.wait
    search_history = search_result.assert_history_selected(0)
    search_result.tap_search_history_title(search_history)
    search_result.assert_keyword_value(search_history)
    search_result.new_search.tap
    21.downto(2) { |i|
      search_result.find_new_search_history(format('a%02d', i))
    }
    search_result.search_history_does_not_exist('a01')
    home = search_result.go_back_home_page
    home.tap_search_box
    21.downto(2) { |i|
      home.assert_search_history_exist(format('a%02d', i))
    }
    home.search_history_does_not_exist('a01')
  end
end
