class SearchHistory3 < TestCase
  def run
    home = App.goto_home
    home.tap_search_box
    search_result = home.search('a91')
    search_detail = search_result.goto_search_detail
    search_detail.search_history_button.tap
    search_detail.search_history_list.wait
    91.downto(3) { |i|
      search_detail.find_search_history("a#{i}")
    }
    search_detail.search_history_does_not_exist('a2', 'a1')
  end
end
