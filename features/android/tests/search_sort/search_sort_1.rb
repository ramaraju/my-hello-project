class SearchSort1 < TestCase
  def run
    home = App.goto_home
    home.tap_search_box
    home.assert_barcode_shown
    search = home.search('calaitem_sort')
    search.assert_search_result
    # 標準 : standard
    search.assert_sort(
      :standard, 'calaitem_sort',
      ['calaitem_sort_05', 'calaitem_sort_04', 'calaitem_sort_02', 'calaitem_sort_03', 'calaitem_sort_01']
    )
    # 並び順"価格安い : lowest_price
    search.assert_sort(
      :price_low, 'calaitem_sort',
      ['calaitem_sort_01', 'calaitem_sort_02', 'calaitem_sort_03', 'calaitem_sort_04', 'calaitem_sort_05']
    )
    # 並び順"価格高い : highest_price
    search.assert_sort(
      :price_high, 'calaitem_sort',
      ['calaitem_sort_05', 'calaitem_sort_04', 'calaitem_sort_03', 'calaitem_sort_02', 'calaitem_sort_01']
    )
    # 並び順"新着順 : newest_created
    search.assert_sort(
      :new, 'calaitem_sort',
      ['calaitem_sort_05', 'calaitem_sort_04', 'calaitem_sort_03', 'calaitem_sort_02', 'calaitem_sort_01']
    )
    # 並び順"感想の件数が多い: most_evaluation
    search.assert_sort(
      :review_number, 'calaitem_sort',
      ['calaitem_sort_05', 'calaitem_sort_04', 'calaitem_sort_03', 'calaitem_sort_02', 'calaitem_sort_01']
    )
    # 並び順"評価の件数が高い"\ : highest_evaluation
    search.assert_sort(
      :review_high, 'calaitem_sort',
      ['calaitem_sort_03', 'calaitem_sort_05', 'calaitem_sort_04', 'calaitem_sort_01', 'calaitem_sort_02']
    )
  end
end
