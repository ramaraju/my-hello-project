class Review15 < TestCase
  def run
    item = ITEMS.search(item: '通常商品(項目選択肢別がない) 31')
    home = App.goto_home
    home.tap_search_box
    home.enter_search_box(item.item_name)
    search_result = home.tap_search_button
    item_detail = search_result.tap_item(item)
    review = item_detail.open_review
    review.shop_review.tap!
    review.select_search_condition(star: 'five')
    review.verify_result_count(1, :shop)
    review.open_filter_settings(:item)
    review.clear.tap!
    review.verify_search_condition(:default, :shop)
  end
end
