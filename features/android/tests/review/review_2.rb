class Review2 < TestCase
  def run
    item = ITEMS.search(item: 'spu')
    user = USERS.search(user: 'userone')
    home = App.goto_home
    home.tap_search_box
    home.enter_search_box(item.item_name)
    search_result = home.tap_search_button
    item_detail = search_result.tap_item(item)
    review = item_detail.open_review
    # repeat 3 times (can crash)
    3.times {
      review.open_filter_settings(:item)
      review.search.tap!
      review.verify_item_review
    }
    review.shop_review.tap!
    # repeat 3 times (can crash)
    3.times {
      review.open_filter_settings(:shop)
      review.search.tap!
      review.verify_shop_review
    }
    App.restart
    home = App.login(user)
    home.tap_search_box
    home.enter_search_box(item.item_name)
    search_result = home.tap_search_button
    item_detail = search_result.tap_item(item)
    review = item_detail.open_review
    # repeat 3 times (can crash)
    3.times {
      review.open_filter_settings(:item)
      review.search.tap!
      review.verify_item_review
    }
    review.shop_review.tap!
    # repeat 3 times (can crash)
    3.times {
      review.open_filter_settings(:shop)
      review.search.tap!
      review.verify_shop_review
    }
  end
end
