class ReviewFilterItem < TestCase
  def run(filter)
    item = ITEMS.search(item: '通常商品(項目選択肢別がない) 31')
    user = USERS.search(user: 'userone')
    filter = load_data('Review')[filter]
    filter_condition = filter['condition']
    result = filter['result']
    home = App.goto_home
    home.tap_search_box
    home.enter_search_box(item.item_name)
    search_result = home.tap_search_button
    item_detail = search_result.tap_item(item)
    review = item_detail.open_review
    review.verify_item_review
    review.open_filter_settings(:item)
    review.select_search_condition(filter_condition)
    review.search.tap
    review.verify_result_count(result, :item)
    # Check above logic with logged user
    App.restart
    App.login(user)
    home.tap_search_box
    home.enter_search_box(item.item_name)
    search_result = home.tap_search_button
    item_detail = search_result.tap_item(item)
    review = item_detail.open_review
    review.verify_item_review
    review.open_filter_settings(:item)
    review.select_search_condition(filter_condition)
    review.search.tap
    review.verify_result_count(result, :item)
  end
end
