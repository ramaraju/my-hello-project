class Review1 < TestCase
  def run
    item = ITEMS.search(item: 'spu')
    home = App.goto_home
    home.tap_search_box
    home.enter_search_box(item.item_name)
    search_result = home.tap_search_button
    item_detail = search_result.tap_item(item)
    review = item_detail.open_review
    # repeat 3 times (can crash)
    3.times {
      review.verify_item_review
      review.shop_review.tap!
      review.verify_shop_review
      review.item_review.tap!
    }
  end
end
