class ReviewFilterShop < TestCase
  def run(filter)
    item = ITEMS.search(item: '通常商品(項目選択肢別がない) 31')
    user = USERS.search(user: 'userone')
    filter = load_data('Review')[filter]
    filter_condition = filter['condition']
    result = filter_condition.keys.first == 'reviewer' ? 21 : filter['result']
    home = App.goto_home
    home.tap_search_box
    home.enter_search_box(item.item_name)
    search_result = home.tap_search_button
    item_detail = search_result.tap_item(item)
    review = item_detail.open_review
    review.shop_review.tap
    review.open_filter_settings(:shop)
    review.select_search_condition(filter_condition)
    review.search.tap
    review.verify_result_count(result, :shop)
    # Check above logic with logged user
    App.restart
    App.login(user)
    home.tap_search_box
    home.enter_search_box(item.item_name)
    search_result = home.tap_search_button
    item_detail = search_result.tap_item(item)
    review = item_detail.open_review
    review.shop_review.tap
    review.open_filter_settings(:shop)
    review.select_search_condition(filter_condition)
    review.search.tap
    review.verify_result_count(result, :shop)
  end
end
