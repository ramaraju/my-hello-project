class Review16 < TestCase
  def run
    item = ITEMS.search(item: '通常商品(項目選択肢別がない) 31')
    user = USERS.search(user: 'userone')
    home = App.goto_home
    home.tap_search_box
    home.enter_search_box(item.item_name)
    search_result = home.tap_search_button
    item_detail = search_result.tap_item(item)
    review = item_detail.open_review
    review.shop_review.tap!
    review.open_filter_settings(:shop)
    review.back.tap!
    review.back.tap!
    review.verify_item_review
    review.open_filter_settings(:item)
    review.back.tap!
    review.verify_item_review
    review.back.tap!
    review = ItemDetail.new(item).open_review
    review.open_cart
    # Check above logic with logged user
    App.restart
    home = App.login(user)
    home.tap_search_box
    home.enter_search_box(item.item_name)
    search_result = home.tap_search_button
    item_detail = search_result.tap_item(item)
    review = item_detail.open_review
    review.shop_review.tap
    review.open_filter_settings(:shop)
    review.back.tap!
    review.back.tap!
    review.verify_item_review
    review.open_filter_settings(:item)
    review.back.tap!
    review.verify_item_review
    review.back.tap!
    review = ItemDetail.new(item).open_review
    review.open_cart
  end
end
