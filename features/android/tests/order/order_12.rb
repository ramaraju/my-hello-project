require_relative '../../utils/search_utils'

class Order12 < TestCase
  include SearchUtils
  def run(item)
    user = find_user('userdefault_%d', 1..7)
    item = ITEMS.search(item: item)
    App.login(user)
    App.clear_cart
    details = search(item).tap_item(item)
    expect(details.item_name).to eq(item.display_name)
    step = details.place_in_cart_about_cpc('wine', 'time')
    step.enter_password_and_user
    step.step2_title.wait
    step.select_address(user)
    step.select_date
    step.select_delivery_and_payment
    step.purchase_completely
    step.step6_title.wait
  end
end
