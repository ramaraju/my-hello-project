# 1. Click "購入履歴" in navigation screen
# 2. Back to home login and Click "購入履歴" in navigation
# 3. Click shop name in webview "購入履歴" screen
# 4. Click "もう一度購入"button in "購入履歴" screen
# 5. Click "履歴詳細を表示"button in "購入履歴" screen
# 6. Click "商品レビューを書く" button in "購入履歴" screen
# Expected Result:
#
# 1. Webview login screen can be transferred
# 2. Webview "購入履歴" screen can be transferred
# 3. Shop top screen can be transferred
# 4. Webview item detail screen can be transferred
# 5. Webview"購入履歴一覧" screen can be transferred
# 6. Webview"shop review" screen can be transferred
# 7. Webview" item review" screen can be transferred. Webview" item review" screen can be transferred

class Order21 < TestCase
  def run
    user = find_user('userorder4_%d', 1..7)
    item = ITEMS.search(item: '通常商品(項目選択肢別がない) 53')
    home = App.goto_home
    purchase_history = home.header.tabs.history.tap!
    login = purchase_history.goto_login
    purchase_history = login.login(user, TabsPurchaseHistory)
    purchase_history.goto_shop
    purchase_history.close_button.tap
    item_detail = purchase_history.purchase_item(item)
    purchase_history = item_detail.tap_back_button(TabsPurchaseHistory)
    purchase_history.goto_order_details
    purchase_history.close_button.tap
    purchase_history.goto_shop_reviews
  end
end
