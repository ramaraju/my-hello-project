class Order16 < TestCase
  def run
    user = find_user('userdefault_%d', 1..7)
    item = ITEMS.search(item: '通常商品(項目選択肢別がない) 51')
    App.login(user)
    home = App.clear_cart
    cart = home.goto_cart
    cart.empty_cart_message.wait
    home = cart.close_button.tap && Home.wait
    home.tap_search_box
    home.enter_search_box(item.item_name)
    search_result = home.tap_search_button
    item_detail = search_result.tap_item(item)
    cart = item_detail.place_in_cart
    item_detail = cart.tap_close_button
    search_result = item_detail.tap_back
    home = search_result.go_back_home_page
    home.goto_cart.clear_cart
    cart = home.goto_cart
    cart.empty_cart_message.wait
  end
end
