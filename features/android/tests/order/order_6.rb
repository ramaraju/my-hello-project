require_relative '../../utils/search_utils'

class Order6 < TestCase
  include SearchUtils
  def run(item)
    user = find_user('userdefault_%d', 1..7)
    item = ITEMS.search(item: item)
    App.login(user)
    App.clear_cart
    details = search(item).tap_item(item)
    expect(details.item_name).to eq(item.display_name)
    cart = details.place_in_cart(:wine)
    step1 = cart.tap_process_order
    step2 = step1.enter_password
    step2.tap_place_order
    # Step3.wait
  end
end
