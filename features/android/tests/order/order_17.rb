# 1. Search and Purchase of one regular item
#
# 2. Login when STEP1「ログイン」
# 3. Select payment in STEP1「お支払い方法の変更①（お支払い方法一覧）」
# 4. Click "注文情報確認" button in shopping cart screen
# 5. Select  代金引換 payment
# 6. Specify 配送日時 in 配送日時指定の変更 screen
# 7. Change user 送付先 in "送付先の変更" screen
# 8. Click "注文を確認する"button
# Expected Result:
#
#              1. One regular item can be add in cart and purchase
# 2. User can login when STEP1「ログイン」
# 3. Payment can be selected correctly
# 4. "注文情報確認" screen can be displayed correctly
# 5. Specified 配送方法 can be checked automatically when selects 代金引換（配送指定）
# 6. 配送日時 can be specified correctly
# 7. User 送付先 can be changed
# 8. Order completion screen can be transferred and Order number can be displayed correctly

class Order17 < TestCase
  def run(hash)
    user = find_user('userorder2_%d', 1..7)
    item = ITEMS.search(item: '通常商品(項目選択肢別がない) 51')
    address_delivery = ADDRESS.search(address: 'Tokyo')
    App.login(user)
    home = App.clear_cart
    home.tap_search_box
    home.enter_search_box(item.item_name)
    search_result = home.tap_search_button
    item_detail = search_result.tap_item(item)
    cart = item_detail.place_in_cart
    step1 = cart.tap_process_order
    payment = step1.enter_password(['Payment', :Step1])
    step2 = payment.collection('代金引換', Step2).tap!
    delivery = step2.scroll_to(:delivery_detail_button).tap!
    delivery = delivery.set_day.tap
    delivery.select_date(Date.today + 15)
    step2 = delivery.scroll_to(:submit_button).tap!
    expect(step2.selected_date).to eq(Delivery.selected_date)
    address = step2.scroll_to(:send_to_button).tap!
    address.address_edit_button.tap
    address.update_address(address_delivery)
    step2 = address.submit_button.tap!
    expect(step2.selected_value(:send_to_address_label)).to include('東京都世田谷区新町')
    step2.validate(hash, :after)
    # step2.enter_password
    step2.tap_place_order
  end
end
