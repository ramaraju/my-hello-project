# 1. Search and purchase of 酒類商品
# 2. Login when STEP1「ログイン」
# 3. Click every option button in "注文情報確認" screen
# 4. Register a new creditcard on step2 お支払い方法の変更
# 5. Edit registerred creditcard information on step2 お支払い方法の変更
# 6. Click " 内容を変更"button without inputting anything in  new creditcard creation screen
# 7. Confirm  配送日時 can be specified when setting only 配送日can be specified in RMS on 配送日時
# 8. Click "注文を確認する"button
# Expected Result:
# 1. 酒類商品 can be add in cart and purchase
# 2. User can login when STEP1「ログイン」
# 3. Option screen from "注文情報確認" screen can be transferred correctly
# 4. New creditcard can be registerred and used correctly
# 5. Edit registerred creditcard information
# 6. Error message can be displayed correctly
# 7.. 配送日時 can be specified correctly
# 8.. Order completion screen can be transferred and Order number can be displayed correctly

class Order19 < TestCase
  def run(data)
    user = find_user('userorder1_%d', 1..7)
    item = ITEMS.search(item: '通常商品(酒類) 43')
    card = CARDS.search(card: 'JCB_1')
    App.login(user)
    home = App.clear_cart
    home.tap_search_box
    home.enter_search_box(item.item_name)
    search_result = home.tap_search_button
    item_detail = search_result.tap_item(item)
    cart = item_detail.place_in_cart(:wine)
    step1 = cart.tap_process_order
    step2 = step1.enter_password
    step2.validate(data) ## check default order information
    payment = step2.scroll_to(:payment_button).tap!
    payment.to_new_card.scroll_to(:next_button).tap
    payment.assert_error_message ## check card error message
    step2 = payment.set_new_card_info(card)
    step2.assert_card_detail(card) ## check new card information
    delivery = step2.scroll_to(:delivery_detail_button).tap!
    delivery.set_day.tap
    expect(delivery.select_day).to appear ## check 配送日 be able to be selected
    expect(delivery.time_list.query.run).to be_empty ## check 配送時間 be able to be selected
    delivery.select_date(Date.today + 4) ## select date
    step2 = delivery.scroll_to(:submit_button).tap!
    expect(step2.selected_date).to eq(Delivery.selected_date) ## check 配送日 is displayed correctly
    step2.validate(data, :after) ## check order information after modify
    step2.tap_place_order ## place order
  end
end
