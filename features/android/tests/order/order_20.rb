# 1. Search and purchase of 薬類商品
# 2. Login when STEP1「ログイン」
# 3. Confirm display of point usage frame in  "注文情報確認" screen
# 4. Use 99,999,99 points on STEP2「ポイント利用の変更」
# 5. Order by using less than 50 points
# 6. Select any payment on STEP2「お支払い方法の変更②（お支払い方法一覧）」
# 7. Click "注文を確認する"button
# Expected Result:
# 1. 薬類商品 can be add in cart and purchase
# 2. User can login when STEP1「ログイン」
# 3. point usage frame in  "注文情報確認" screen can be displayed
# 4. Error message can be shown
# 5. Item can be purchased by using less than 50 points
# 6. Payment can be selected correctly
# 7. Order completion screen can be transferred and Order number can be displayed correctly

class Order20 < TestCase
  def run(data)
    user = find_user('userdefault_%d', 1..7)
    item = ITEMS.search(item: '通常商品(藥類) 53')
    App.login(user)
    home = App.clear_cart
    home.tap_search_box
    home.enter_search_box(item.item_name)
    search_result = home.tap_search_button
    item_detail = search_result.tap_item(item)
    cart = item_detail.place_in_cart(:medicine) ## check medicine item
    step1 = cart.tap_process_order ## order medicine item
    step2 = step1.enter_password
    step2.validate(data)
    point = step2.scroll_to(:point_button).tap!
    point.scroll_to(:num_point).enter_text('10') ## use 10 point
    step2 = point.scroll_to(:submit_button).tap!
    expect(step2.selected_value(:point_num_label)).to eq('10ポイント') ## checked use 10 point
    payment = step2.scroll_to(:payment_button).tap!
    step2 = payment.collection('代金引換', Step2).tap! ## select payment [その他決済方法(１)]
    step2.validate(data, :after) ## check order detail
    step2.tap_place_order ## place order
  end
end
