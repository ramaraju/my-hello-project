# 1. Search and purchase of two regular items
# 2. Login when STEP1「ログイン」
# 3. Confirm create card can be used on step1
# 4. Select 利用可能なカード on step2
# 5. Select あす楽 on 配送方法の変更
# 6. Confirm あす楽can be specified on 配送日時
# 7. Select あす楽 unsupported area  in "送付先の変更" screen
# 8. Cancel "あす楽" and Select あす楽 unsupported area  in "送付先の変更" screen
# 9. Click "注文を確認する"button
# Expected Result:
#
# 1. Two regular item can be add in cart and purchase
# 2. User can login when STEP1「ログイン」
# 3. new card can be used correctly
# 4. User can make purchase by selecting 利用可能なカード
# 5. あす楽 can be selected and make purchase
# 6. あす楽can be specified
# 7. Error message can be displayed
# 8. Receiver's address can be selected
# 9. Order completion screen can be transferred and Order number can be displayed correctly

class Order18 < TestCase
  def run(data)
    user = find_user('userorder3_%d', 1..7)
    item1 = ITEMS.search(item: '通常商品(項目選択肢別がない) 51')
    item2 = ITEMS.search(item: '通常商品(項目選択肢別がない) 52')
    address_delivery = ADDRESS.search(address: 'Tokyo')
    card = CARDS.search(card: 'VISA_1')
    home = App.login(user)
    home.goto_user_info
    my_rakuten = home.goto_my_rakuten
    my_rakuten.tap_member_info_update
    my_rakuten.tap_login_info_update
    my_rakuten.scroll_to(:cart_info_button).tap
    my_rakuten.delete_cart_information(user)
    App.restart
    home = App.clear_cart
    home.tap_search_box
    home.enter_search_box(item1.item_name)
    search_result = home.tap_search_button
    search_result.assert_search_result
    item_detail = search_result.tap_item(item1)
    cart = item_detail.place_in_cart
    item_detail = cart.tap_close_button(ItemDetail.new(item1.item_name))
    search_result = item_detail.tap_back
    search_result.search(item2.item_name)
    search_result.assert_search_result
    item_detail = search_result.tap_item(item2)
    cart = item_detail.place_in_cart
    step1 = cart.tap_process_order
    payment = step1.enter_password(['Payment', :Step1])
    payment.to_new_card
    step2 = payment.set_new_card_info(card)
    delivery = step2.scroll_to(:delivery_detail_button).tap!
    delivery.tomorrow_delivery_link.tap!
    delivery.select_tomorrow_delivery
    step2 = delivery.goto_step2
    expect(step2.selected_value(:delivery_date_label)).to eq('翌日配送「あす楽」')
    address = step2.scroll_to(:send_to_button).tap!
    address.address_edit_button.tap
    address.update_address(address_delivery)
    step2 = address.submit_button.tap!
    expect(step2.selected_value(:info_msg_label)).to eq('配送先が翌日配送「あす楽」の対象エリア外のため、「あす楽」を指定することはできません。')
    expect(step2.selected_value(:delivery_date_label)).to eq('日付指定なし')
    step2.validate(data, :after)
    step2.tap_place_order
  end
end
