class Order1 < TestCase
  def run
    user = find_user('userhome1_%d', 1..7)
    item = ITEMS.search(item: '通常商品(項目選択肢別) 50')
    App.login(user)
    home = App.clear_cart
    home.tap_search_box
    home.enter_search_box(item.item_name)
    search_result = home.tap_search_button
    item_detail = search_result.tap_item(item)
    item_detail.select_inventory_if_exist(true)
    cart = item_detail.place_in_cart
    cart.assert_item_add_into_cart(item)
  end
end
