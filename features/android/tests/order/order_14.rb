require_relative '../../utils/search_utils'

class Order14 < TestCase
  include SearchUtils
  def run(item)
    user = find_user('userdefault_%d', 1..7)
    item = ITEMS.search(item: item)
    App.login(user)
    App.clear_cart
    details = search(item).tap_item(item)
    expect(details.item_name).to eq(item.display_name)
    step = details.place_in_cart_about_cpc('wine', 'reserve')
    step.enter_password_and_user
    step.step2_title.wait
    step.select_address(user)
    step.step3_title.wait
    step.submit_s3_r.wait(timeout: 10) {
      calabash.flick_up
    }
    step.submit_s3_r.tap
    step.step3_title.wait
    # step.select_delivery_and_payment
    step.payment_select.tap
    sleep(1)
    step.delivery_select.tap
    step.step4_title_r.wait
    step.submit_s5.tap
    step.step6_title.wait
  end
end
