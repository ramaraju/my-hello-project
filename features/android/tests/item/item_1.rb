require_relative '../../utils/search_utils'
require_relative '../../utils/bookmark_utils'

class Item1 < TestCase
  include SearchUtils, BookmarkUtils
  def run(item)
    user = find_user('userdefault_%d', 1..7)
    item = ITEMS.search(item: item)
    App.login(user)
    # params @item.search_id is search keyword
    # params @item is item object(include item information in it)
    details = search(item).tap_item(item)
    # details.item_name is action[item_name] in item detail page
    # params @display_name is item name in item detail page
    expect(details.item_name).to eq(item.display_name)
    details.add_bookmark
    home = details.tap_back.tap_back
    # current page is Home
    expect(home).to be_a(Home)
    # confirm bookmark item is in bookmark
    # params @item.item_name is item name in list(eg. search result list, bookmark list & banner etc.)
    details = home.find_item_in_bookmarks(item).tap!
    # details.item_name is action[item_name] in item detail page
    # params @display_name is item name in item detail page
    expect(details.item_name).to eq(item.display_name)
    App.restart
    # delete the bookmark
    delete_bookmark(item)
  end
end
