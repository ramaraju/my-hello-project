class Item6 < TestCase
  def run
    item = ITEMS.search(item: 'cala select checkbox special characters')
    home = App.clear_cart
    search_tab = home.goto_search_tab
    search_result = search_tab.make_detail_search('keyword' => item.item_name)
    itemdetail = search_result.tap_item(item.item_name)
    select_info = itemdetail.get_select_info
    checkbox_info = itemdetail.set_get_checkbox_info
    cart = itemdetail.place_in_cart
    cart.verify_option_info_display(select_info + checkbox_info)
  end
end
