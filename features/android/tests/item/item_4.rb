class Item4 < TestCase
  def run
    item = ITEMS.search(item: 'cala select checkbox (項目選択肢+選択肢別)')
    home = App.goto_home
    search_tab = home.goto_search_tab
    search_result = search_tab.make_detail_search('keyword' => item.item_name)
    item_detail = search_result.tap_item(item)
    item_detail.verity_value_of_selectable_inventory
    item_detail.verity_value_of_option_select
    item_detail.verity_value_of_option_checkbox
  end
end
