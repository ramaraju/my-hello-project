class Item5 < TestCase
  def run
    # TODO: Fix user #1 in users.yml
    user = find_user('uservisacard_%d', 2..7)
    item = ITEMS.search(item: 'cala select checkbox (項目選択肢別)')
    App.login(user)
    home = App.clear_cart
    search_tab = home.goto_search_tab
    search_result = search_tab.make_detail_search('keyword' => item.item_name)
    itemdetail = search_result.tap_item(item.item_name)
    cart = itemdetail.place_in_cart
    step1 = cart.tap_process_order
    step2 = step1.enter_password
    step2.tap_place_order ## place order
  end
end
