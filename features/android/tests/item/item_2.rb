require_relative '../../utils/search_utils'
require_relative '../../utils/browsing_history_utils'

class Item2 < TestCase
  include SearchUtils, BrowsingHistoryUtils
  def run(item)
    user = find_user('userdefault_%d', 1..7)
    # search item object in ITEMS by param[item_number]
    item = ITEMS.search(item: item)
    App.login(user)
    # params @item is search keyword
    # params @item is item object(include item information in it)
    item_page = search(item).tap_item(item)
    # details.item_name is action[item_name] in item detail page
    # params @display_name is item name in item detail page
    expect(item_page.item_name).to eq(item.display_name)
    home = item_page.tap_back.tap_back
    # current page is Home
    expect(home).to be_a(Home)
    # confirm bookmark item is in bookmark
    # params @item is item name in list(eg. search result list, bookmark list & banner etc.)
    home.assert_item_shown_in_browsing_banner(item)
    item_page = home.tap_item_on_browsing_banner(item)
    # details.item_name is action[item_name] in item detail page
    # params @display_name is item name in item detail page
    expect(item_page.item_name).to eq(item.display_name)
    App.restart
    delete_browsing_history(item)
  end
end
