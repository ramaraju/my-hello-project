class Item3 < TestCase
  def run
    home = App.goto_home
    search_tab = home.goto_search_tab
    search_result = search_tab.make_detail_search('keyword' => 'item_with_link')
    item_detail = search_result.tap_item('item_with_link')
    item_detail.go_to_item_description_link
    item_detail.assert_webview_page_reached(['//*[@id="itemName"]/h1', '//*[@id="itemModelNumber"]/div/dl/dd'])
  end
end
