class Point1b < TestCase
  def run(item)
    item = ITEMS.search(item: item)
    shop = SHOPS.search(shop: item.shop)
    user = find_user('userdefault_%d', 1..7)
    home = App.goto_home
    home.tap_search_box
    home.enter_search_box(item.item_name)
    search_result = home.tap_search_button
    point = search_result.assert_item_point(item, shop)
    item_detail = search_result.tap_item(item)
    item_detail.assert_item_point(item, shop)
    item_detail.assert_point_is_same(item, point)
    App.restart
    home = App.login(user)
    home.tap_search_box
    home.enter_search_box(item.item_name)
    search_result = home.tap_search_button
    point = search_result.assert_item_point(item, shop)
    item_detail = search_result.tap_item(item)
    item_detail.assert_item_point(item, shop)
    item_detail.assert_point_is_same(item, point)
  end
end
