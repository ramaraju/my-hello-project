class Point2 < TestCase
  def run
    user = find_user('userdefault_%d', 1..7)
    home = App.goto_home
    home.rakuten_point_button.wait.tap
    home.ichiba_base_dialog.wait
    login = home.dialog_login_button.tap
    rakutenpointcard = login.login(user, RakutenPointCard)
    home = rakutenpointcard.go_back_home
    home.rakuten_point_button.wait.tap
    RakutenPointCard.wait
  end
end
