class Link5 < TestCase
  def run
    user = find_user('userdefault_%d', 1..7)
    home = App.login(user)
    cart = home.goto_cart
    cart.slide_to_the_bottom
    cart.telephone_link.tap
    cart.telephone_confirm.wait
    cart.close_link_tap
    cart.slide_to_the_bottom
    cart.stock_link.tap
    cart.stock_confirm.wait
    cart.close_link_tap
    cart.slide_to_the_bottom
    cart.payment_link.tap
    cart.payment_confirm.wait
    cart.close_link_tap
    cart.slide_to_the_bottom
    cart.delivery_link.tap
    cart.delivery_confirm.wait
    cart.close_link_tap
    cart.slide_to_the_bottom
    cart.cancel_link.tap
    cart.cancel_confirm.wait
    cart.close_link_tap
    cart.slide_to_the_bottom
    App.restart
    home = App.logout
    cart = home.goto_cart
    sleep(3)
    cart.slide_to_the_bottom
    cart.telephone_link_out.tap
    cart.telephone_confirm.wait
    cart.close_link_tap
    cart.slide_to_the_bottom
    cart.stock_link_out.tap
    cart.stock_confirm.wait
    cart.close_link_tap
    cart.slide_to_the_bottom
    cart.payment_link_out.tap
    cart.payment_confirm.wait
    cart.close_link_tap
    cart.slide_to_the_bottom
    cart.delivery_link_out.tap
    cart.delivery_confirm.wait
    cart.close_link_tap
    cart.slide_to_the_bottom
    cart.cancel_link_out.tap
    cart.cancel_confirm.wait
    cart.close_link_tap
    cart.slide_to_the_bottom
  end
end
