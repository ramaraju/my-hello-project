class Link1 < TestCase
  def run
    home = App.goto_home
    home.tap_navigation_drawer
    home.assert_navigation_drawer_shown
    help = home.tap_help_in_navigator
    help.cart.wait
    help.title_information.wait(timeout: 10) {
      calabash.flick_up
    }
  end
end
