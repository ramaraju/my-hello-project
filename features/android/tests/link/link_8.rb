class Link8 < TestCase
  def run
    home = App.goto_home
    home.tap_navigation_drawer
    help = home.tap_help_in_navigator
    help.scroll_to(:customer_comments).tap
    help.verify_web_page(:opinions_and_requests)
  end
end
