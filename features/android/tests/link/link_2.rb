class Link2 < TestCase
  def run
    home = App.goto_home
    data = load_data('Link_2')
    search_tab = home.goto_search_tab
    search_result = search_tab.make_detail_search(data['search_condition_1'])
    item_detail = search_result.tap_item(data['item_name_1'])

    item_detail.goto_webview_page(data['shop_shipping_payment_method'])
    item_detail.assert_webview_page_reached(data['shop_shipping_payment_method_sign'])

    calabash.press_back_button
    item_detail.goto_webview_page(data['shop_top_page'])
    item_detail.assert_webview_page_reached(data['shop_top_page_sign'])
    calabash.press_back_button

    item_detail.goto_webview_page(data['visit_pc_page'])
    # xpath not working
    # item_detail.assert_webview_page_reached(data['visit_pc_page_sign'])
  end
end
