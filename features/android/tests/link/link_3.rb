class Link3 < TestCase
  def run
    home = App.goto_home
    # data = load_data('Link_3')
    login = home.login_button.tap!
    login.goto_user_input_page

    # login.goto_webview_page(data['page_name2'])
    # login.assert_webview_page_reached(data['page_sign2'])
    #
    # calabash.press_back_button
    # login.goto_webview_page(data['page_name1'])
    # login.assert_webview_page_reached(data['page_sign1'])
    login.goto_webview_page('register_new_account')
    WebCommon.register_new_account.wait
    WebCommon.close_web(Login)
    login.goto_webview_page('forget_password')
    WebCommon.forget_password.wait
    WebCommon.close_web(Login)
  end
end
