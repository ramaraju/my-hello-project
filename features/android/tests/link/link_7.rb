class Link7 < TestCase
  def run
    data = load_data('Link_7')
    home = App.goto_home
    home.tap_navigation_drawer
    help = home.tap_help_in_navigator

    help.verify_app_version

    calabash.flick_up_and_tap(TextView.new(text: '著作権情報'))
    TextView.new(text: 'Copyright © Rakuten, Inc.').wait
    help.back_to_help_page_from_webview
    calabash.flick_and_find(TextView.new(text: 'ヘルプ'), :down)

    data['links'].each_with_index do |link, index|
      help.goto_webview_page(link)
      help.assert_webview_page_reached(data["sign_#{index}"])
      help.back_to_help_page_from_webview
      calabash.flick_and_find(TextView.new(text: 'ヘルプ'), :down)
    end
  end
end
