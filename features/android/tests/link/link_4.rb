class Link4 < TestCase
  def run
    user = find_user('userhome1_%d', 1..7)
    item = ITEMS.search(item: '通常商品(項目選択肢別がない) 31')
    home = App.login(user)
    home.tap_search_box
    home.enter_search_box(item.item_name)
    search_result = home.tap_search_button
    item_detail = search_result.tap_item(item)
    cart = item_detail.place_in_cart
    step1 = cart.tap_process_order
    step1.assert_protest_information
    step1.change_information.tap
    myrakuten = step1.assert_change_information
    expect(myrakuten.retrieve_my_rakuten_page_trait).to eq('会員登録情報変更・確認')
    calabash.press_back_button
    Step1.wait {
      calabash.flick_down
    }.assert_forget_information
  end
end
