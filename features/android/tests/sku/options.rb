class Options < TestCase
  def run(item_name, type)
    item = ITEMS.search(item: item_name)
    home = App.goto_home
    search_tab = home.goto_search_tab
    search_result = search_tab.make_detail_search('keyword' => item.item_name, 'availability' => false)
    item_page = search_result.tap_item(item)
    item_page.select_options(type)
  end
end
