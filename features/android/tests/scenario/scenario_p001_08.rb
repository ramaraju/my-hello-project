class ScenarioP00108 < TestCase
  def run(data)
    user = USERS.search(user: 'usernocard')
    item1 = ITEMS.search(item: '通常商品 scenario P001 8a')
    item2 = ITEMS.search(item: '通常商品 scenario P001 8b')
    App.login(user)
    home = App.clear_cart
    home.tap_search_box
    home.enter_search_box(item1.item_name)
    search_result = home.tap_search_button
    item_detail = search_result.tap_item(item1)
    item_detail.place_in_cart ## order 商品
    home = App.restart
    home.tap_search_box
    home.enter_search_box(item2.item_name)
    search_result = home.tap_search_button
    item_detail = search_result.tap_item(item2)
    cart = item_detail.place_in_cart ## order 商品
    step1 = cart.tap_process_order
    payment = step1.enter_password(Payment.new(:Step1))
    step2 = payment.collection('代金引換', Step2).tap!
    step2.validate(data) ## confirm order information
    ## 決済方法を変更する→再変更→再再変更
    payment = step2.reveal_view(:payment_button).tap!
    step2 = payment.collection('その他決済方法_1', Step2).tap!
    expect(step2.selected_value(:payment_type_label)).to eq('その他決済方法_1')
    payment = step2.reveal_view(:payment_button).tap!
    step2 = payment.collection('その他決済方法_2', Step2).tap!
    ## 配送方法を変更する→初期に戻す→再変更
    delivery = step2.reveal_view(:delivery_button).tap!
    step2 = delivery.collection('ゆうパック', Step2).tap!
    expect(step2.selected_value(:delivery_type_label)).to eq('ゆうパック')
    delivery = step2.reveal_view(:delivery_button).tap!
    delivery.collection('自社配送', Step2).tap!
    delivery.select_date(Date.today + 22) ## select date
    step2 = delivery.reveal_view(:submit_button).tap!
    step2.validate(data, :after) ## confirm order information
    step2.tap_place_order
  end
end
