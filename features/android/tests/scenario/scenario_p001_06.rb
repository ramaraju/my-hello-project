class ScenarioP00106 < TestCase
  def run(data)
    user = USERS.search(user: 'userone')
    item = ITEMS.search(item: '通常商品(timesale) scenario P001 6')
    App.login(user)
    home = App.clear_cart
    home.tap_search_box
    home.enter_search_box(item.item_name)
    search_result = home.tap_search_button
    item_detail = search_result.tap_item(item)
    item_detail.add_bookmark
    home = App.restart
    bookmark = home.goto_bookmarks
    item_detail = bookmark.tap_item(item.item_name)
    cart = item_detail.place_in_cart ## order 商品（税別/送料別＋項目選択肢別在庫）from bookmark
    step1 = cart.tap_process_order
    step2 = step1.enter_password
    step2.validate(data) ## confirm order information
    step2.tap_place_order
  end
end
