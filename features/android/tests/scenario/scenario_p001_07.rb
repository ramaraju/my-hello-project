class ScenarioP00107 < TestCase
  def run(data)
    user = USERS.search(user: 'userallcard')
    item = ITEMS.search(item: '通常商品(timesale) scenario P001 7')
    App.login(user)
    home = App.clear_cart
    home.tap_search_box
    home.enter_search_box(item.item_name)
    search_result = home.tap_search_button
    item_detail = search_result.tap_item(item)
    item_detail.set_item_num('2')
    cart = item_detail.place_in_cart ## order 商品（項目選択肢）*2
    step1 = cart.tap_process_order
    step2 = step1.enter_password
    step2.validate(data) ## confirm order information
    ## 決済方法を変更する→初期に戻す→再変更
    payment = step2.reveal_view(:payment_button).tap!
    step2 = payment.collection('その他決済方法_1', Step2).tap!
    expect(step2.selected_value(:payment_type_label)).to eq('その他決済方法_1')
    payment = step2.reveal_view(:payment_button).tap!
    payment.collection('クレジットカード決済', Payment).tap!.reveal_view(:next_button).tap
    step2 = Step2.wait
    ## 配送日時を変更する→再変更→再再変更
    delivery = step2.reveal_view(:delivery_detail_button).tap!
    delivery.set_day.tap
    delivery.select_date(Date.today + 22) ## select date
    step2 = delivery.reveal_view(:submit_button).tap!
    expect(step2.selected_date).to eq(Delivery.selected_date) ## check 配送日 is displayed correctly
    delivery = step2.reveal_view(:delivery_detail_button).tap!
    delivery.set_day.tap
    delivery.select_date(Date.today + 23) ## select date
    step2 = delivery.reveal_view(:submit_button).tap!
    expect(step2.selected_date).to eq(Delivery.selected_date) ## check 配送日 is displayed correctly
    step2.validate(data, :after) ## confirm order information
    step2.tap_place_order
  end
end
