class ScenarioP00102 < TestCase
  def run(hash)
    user = USERS.search(user: 'userone')
    item = ITEMS.search(item: '通常商品 scenario p001 2')
    address1 = ADDRESS.search(address: 'Tokyo')
    address2 = ADDRESS.search(address: 'Ehime')
    App.login(user)
    home = App.clear_cart
    home.tap_search_box
    home.enter_search_box(item.item_name)
    search_result = home.tap_search_button
    item_detail = search_result.tap_item(item) ## order 商品（税込/送料込/代引料別）
    cart = item_detail.place_in_cart
    step1 = cart.tap_process_order
    step2 = step1.enter_password
    step2.validate(hash) ## confirm order information
    ## ポイント利用を変更する→初期に戻す→再変更
    point = step2.reveal_view(:point_button).tap!
    point.reveal_view(:num_point).enter_text('10') ## use 10 point
    step2 = point.scroll_to(:submit_button).tap!
    expect(step2.selected_value(:point_num_label)).to eq('10ポイント')
    point = step2.reveal_view(:point_button).tap!
    point.reveal_view(:no_point).tap ## do not use point
    step2 = point.scroll_to(:submit_button).tap!
    point = step2.reveal_view(:point_button).tap!
    point.reveal_view(:num_point).enter_text('20') ## use 20 point
    step2 = point.scroll_to(:submit_button).tap!
    expect(step2.selected_value(:point_num_label)).to eq('20ポイント')
    ## 配送日時を変更する→初期に戻す→再変更
    ##
    send_to = step2.reveal_view(:send_to_button).tap!
    send_to.address_edit_button.tap
    send_to.update_address(address1) ## modify user delivery address
    step2 = send_to.reveal_view(:submit_button).tap!
    expect(step2.selected_value(:send_to_address_label)).to start_with(address1.detail_address)
    send_to = step2.reveal_view(:send_to_button).tap!
    send_to.address_edit_button.tap
    send_to.update_address(address2) ## modify user delivery address
    step2 = send_to.reveal_view(:submit_button).tap!
    expect(step2.selected_value(:send_to_address_label)).to start_with(address2.detail_address)
    step2.reveal_view(:checkbox_cancel).tap
    step2.tap_place_order
  end
end
