class ScenarioP00104 < TestCase
  def run(data)
    user = USERS.search(user: 'userone')
    item = ITEMS.search(item: '通常商品 scenario P001 4')
    address = ADDRESS.search(address: 'Tokyo')
    App.login(user)
    home = App.clear_cart
    home.tap_search_box
    home.enter_search_box(item.item_name)
    search_result = home.tap_search_button
    item_detail = search_result.tap_item(item)
    item_detail.add_bookmark
    home = App.restart
    bookmark = home.goto_bookmarks
    item_detail = bookmark.tap_item(item.item_name)
    cart = item_detail.place_in_cart ## order 商品（税別/送料区分/代引料別）from bookmark
    step1 = cart.tap_process_order
    step2 = step1.enter_password
    step2.validate(data) ## confirm order information
    ## ポイント利用を変更する→初期に戻す→再変更
    point = step2.reveal_view(:point_button).tap!
    point.reveal_view(:num_point).enter_text('10') ## use 10 point
    step2 = point.scroll_to(:submit_button).tap!
    expect(step2.selected_value(:point_num_label)).to eq('10ポイント')
    point = step2.reveal_view(:point_button).tap!
    point.reveal_view(:no_point).tap ## do not use point
    step2 = point.scroll_to(:submit_button).tap!
    point = step2.reveal_view(:point_button).tap!
    point.reveal_view(:num_point).enter_text('20') ## use 20 point
    step2 = point.scroll_to(:submit_button).tap!
    expect(step2.selected_value(:point_num_label)).to eq('20ポイント')
    ## 送付先を変更する→注文者と異なる送付先が変更する
    send_to = step2.reveal_view(:send_to_button).tap!
    send_to.address_edit_button.tap
    send_to.update_address(address) ## modify user delivery address
    step2 = send_to.reveal_view(:submit_button).tap!
    expect(step2.selected_value(:send_to_address_label)).to start_with(address.detail_address)
    step2.reveal_view(:checkbox_cancel).tap
    step2.validate(data, :after) ## confirm order information
    step2.tap_place_order
  end
end
