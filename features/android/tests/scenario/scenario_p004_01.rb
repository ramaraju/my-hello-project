class ScenarioP00401 < TestCase
  def run
    user = USERS.search(user: 'userone')
    item = ITEMS.search(item: '通常商品 scenario p001 1')
    App.login(user)
    home = App.clear_cart
    home.tap_search_box
    home.enter_search_box(item.item_name)
    search_result = home.tap_search_button
    item_detail = search_result.tap_item(item)
    cart = item_detail.place_in_cart
    step1 = cart.tap_process_order
    step2 = step1.enter_password
    step2.commit_button.tap
    step2.tap_dialog_button(:dialog_review)
    expect(Step3).not_to exist
  end
end
