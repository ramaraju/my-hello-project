class ScenarioP00101 < TestCase
  def run(hash)
    user = USERS.search(user: 'uservisacard')
    item = ITEMS.search(item: '通常商品 scenario p001 1')
    address = ADDRESS.search(address: 'Tokyo')
    App.login(user)
    home = App.clear_cart
    home.tap_search_box
    home.enter_search_box(item.item_name)
    search_result = home.tap_search_button
    item_detail = search_result.tap_item(item) ## order 商品（税込/送料込/代引料別）
    cart = item_detail.place_in_cart
    step1 = cart.tap_process_order
    step2 = step1.enter_password
    step2.validate(hash) ## confirm order information
    send_to = step2.reveal_view(:send_to_button).tap!
    send_to.address_edit_button.tap
    send_to.update_address(address) ## modify user delivery address
    step2 = send_to.reveal_view(:submit_button).tap!
    expect(step2.selected_value(:send_to_address_label)).to start_with(address.detail_address)
    step2.validate(hash, :after) ## confirm order information
    step2.tap_place_order ## place order
  end
end
