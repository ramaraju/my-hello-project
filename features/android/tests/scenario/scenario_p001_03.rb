class ScenarioP00103 < TestCase
  def run(hash)
    user = USERS.search(user: 'userone')
    item = ITEMS.search(item: '通常商品 scenario P001 3')
    App.login(user)
    home = App.clear_cart
    home.tap_search_box
    home.enter_search_box(item.item_name)
    search_result = home.tap_search_button
    item_detail = search_result.tap_item(item) ## order 商品（税込/送料込/代引料別）
    cart = item_detail.place_in_cart
    step1 = cart.tap_process_order
    step2 = step1.enter_password
    step2.validate(hash) ## confirm order information
    step2.tap_place_order
  end
end
