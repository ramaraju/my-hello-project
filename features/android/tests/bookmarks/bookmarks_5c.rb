class Bookmarks5c < TestCase
  def run
    user = find_user('userbookmark2_%d', 1..7)
    home = App.login(user)
    bookmark = home.goto_bookmarks
    bookmark.add_items_into_bookmark(5)
    bookmark.delete_all_items_in_bookmark
    bookmark.assert_bookmark_page_with_no_bookmark_reached
    home = bookmark.go_back_home_page
    bookmark = home.goto_bookmarks_from_nav
    bookmark.assert_bookmark_page_with_no_bookmark_reached
    home = bookmark.go_back_home_page
    home.logout
  end
end
