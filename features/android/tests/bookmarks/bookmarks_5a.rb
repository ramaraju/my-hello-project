class Bookmarks5a < TestCase
  def run
    user = find_user('userbookmark1_%d', 1..7)
    home = App.goto_home
    bookmark = home.goto_bookmarks
    bookmark.assert_the_user_is_logged_out
    login = bookmark.goto_login
    bookmark = login.login(user, TabsBookmark)
    bookmark.assert_bookmark_page_with_no_bookmark_reached

    home = bookmark.go_back_home_page
    bookmark = home.goto_bookmarks_from_nav
    bookmark.assert_bookmark_page_with_no_bookmark_reached
    home = bookmark.go_back_home_page
    home.logout
  end
end
