class Bookmarks5b < TestCase
  def run
    user = find_user('userhome2_%d', 1..7)
    home = App.login(user)
    bookmark = home.goto_bookmarks
    bookmark.assert_item_list_for_over_50_items_can_be_displayed

    home = bookmark.go_back_home_page
    bookmark = home.goto_bookmarks_from_nav
    bookmark.assert_item_list_for_over_50_items_can_be_displayed

    home = bookmark.go_back_home_page
    home.logout
  end
end
