class Bookmarks4 < TestCase
  def run
    user = find_user('userhome2_%d', 1..7)
    home = App.goto_home
    bookmark = home.goto_bookmarks
    bookmark.assert_the_user_is_logged_out
    login = bookmark.goto_login
    bookmark = login.login(user, TabsBookmark)
    bookmark.assert_bookmark_page_reached
    home = bookmark.go_back_home_page
    home.logout

    home.tap_navigation_drawer
    home.assert_navigation_drawer_shown
    bookmark = home.tap_bookmark_from_drawer
    bookmark.assert_the_user_is_logged_out
    login = bookmark.goto_login
    bookmark = login.login(user, TabsBookmark)
    bookmark.assert_bookmark_page_reached
  end
end
