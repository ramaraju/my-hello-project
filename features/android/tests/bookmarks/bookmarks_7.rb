class Bookmarks7 < TestCase
  def run
    user = find_user('userdefault_%d', 1..7)
    flick_time = 1
    page = App.login(user)
    page = page.goto_bookmarks
    page.delete_all_items_in_bookmark
    page = page.add_items_into_bookmark(5)
    (0..flick_time).each do
      calabash.flick_up
      sleep(0.5)
      page.verify_order_banner_not_exist
      page.verify_grid_button_not_exist
    end
    (0..flick_time).each do
      calabash.flick_with_params("* id:'content'", {x: 50, y: 40}, {x: 50, y: 80})
      sleep(0.5)
      page.verify_order_banner_exist
      page.verify_grid_button_exist
    end
    page.assert_different_views
    page = page.go_back_home_page
    page = page.goto_bookmarks_from_nav
    page.assert_different_views
  end
end
