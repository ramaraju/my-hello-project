class Bookmarks6 < TestCase
  def run
    user = find_user('userbookmark2_%d', 1..7)
    page = App.login(user)
    bookmark = page.goto_bookmarks
    bookmark.add_items_into_bookmark(5)
    bookmark.assert_bookmark_display_order_are_changeable
    home = bookmark.go_back_home_page
    bookmark = home.goto_bookmarks_from_nav
    bookmark.assert_bookmark_display_order_are_changeable
    bookmark.delete_all_items_in_bookmark
  end
end
