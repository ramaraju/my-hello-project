require_relative '../../utils/search_utils'
require_relative '../../utils/bookmark_utils'

class Bookmarks3 < TestCase
  include SearchUtils, BookmarkUtils
  def run
    user = find_user('userbrowsinghistory1_%d', 1..7)
    item = ITEMS.search(item: '通常商品(藥類) 36')
    App.goto_home
    App.login(user)
    # add NO.31 bookmark
    item_page = search(item).tap_item(item)
    item_page.add_bookmark
    home = App.restart
    home.find_item_in_bookmarks(item)
    # get bookmark item name and compare
    array_item = home.assert_bookmark_item_and_number
    bookmark_tabs = home.goto_bookmarks
    bookmark_tabs.assert_item_list_same_as_home(array_item)
    delete_bookmark(item)
  end
end
