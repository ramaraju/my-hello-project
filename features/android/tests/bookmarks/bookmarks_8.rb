class Bookmarks8 < TestCase
  def run(table)
    user = find_user('userbookmark3_%d', 1..7)
    page = App.login(user)
    page = page.goto_bookmarks
    page.check_bookmark_items(table)
  end
end
