class Bookmarks1 < TestCase
  def run
    home = App.goto_home
    home.assert_bookmark_not_shown
    home.assert_bookmark_not_shown_message
  end
end
