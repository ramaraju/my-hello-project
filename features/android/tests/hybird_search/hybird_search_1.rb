class HybirdSearch1 < TestCase
  def run
    home = App.goto_home
    home.tap_search_box
    home.enter_search_box('item_with_link')
    search_result = home.tap_search_button
    details = search_result.tap_item('item_with_link')
    details.add_shop_in_shop_menu
    home = App.restart
    home.goto_genre
    home.tap_genre_by_name('CD・DVD・楽器')
    home.assert_genre_title('CD・DVD・楽器')
    search_result = home.search_by_genre('CD・DVD・楽器を選択する')
    search_result.assert_selected_genre('CD・DVD・楽器')
    search_result.same_item_confirm_link.wait
    search_detail = search_result.goto_search_detail
    # search_detail.testa
    expect(search_detail.reveal_view(:hybird_search_checkbox).query.first['checked']).to be(true)
    search_result.calabash.press_back_button
    search_detail = search_result.goto_search_detail
    search_detail.to_select_shop
    expect(search_detail.reveal_view(:sort).wait)
    search_detail.hybird_search.wait_gone
    search_result = search_detail.tap_search_button
    search_result.item_desc.wait
    search_result.same_item_confirm_link.wait_gone
    search_detail = search_result.goto_search_detail
    search_detail.hybird_search.wait_gone
    search_detail.goto_remove_shop_suggestion
    search_result = search_detail.tap_search_button
    search_result.item_desc.wait
    search_result.same_item_confirm_link.wait_gone
    search_detail = search_result.goto_search_detail
    expect(search_detail.reveal_view(:hybird_search_checkbox).query.first['checked']).to be(false)
    search_result = search_detail.tap_search_button
    search_result.item_desc.wait
    search_result.same_item_confirm_link.wait_gone
    home = App.restart
    home.goto_genre
    home.tap_genre_by_name('インテリア・寝具・収納')
    home.assert_genre_title('インテリア・寝具・収納')
    search_result = home.search_by_genre('インテリア・寝具・収納を選択する')
    search_result.assert_selected_genre('インテリア・寝具・収納')
    search_result.item_desc.wait
    search_result.same_item_confirm_link.wait_gone
  end
end
