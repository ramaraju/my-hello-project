class HybirdSearch2 < TestCase
  def run
    home = App.goto_home
    home.goto_genre
    home.tap_genre_by_name('CD・DVD・楽器')
    home.assert_genre_title('CD・DVD・楽器')
    search_result = home.search_by_genre('CD・DVD・楽器を選択する')
    search_result.assert_selected_genre('CD・DVD・楽器')
    search_result.same_item_confirm_link.wait.tap
    search_result.hybrid_search_dialog_title.wait(timeout_message: 'cannot be found')
    search_result.hybrid_search_dialog.wait(timeout_message: 'cannot be found')
    search_result.hybrid_search_close_button.tap
    search_result.same_item_confirm_link.wait(timeout_message: 'cannot be found')
    search_result.same_item_confirm_link.wait.tap
    search_result.hybrid_search_dialog_title.wait(timeout_message: 'cannot be found')
    search_result.hybrid_search_dialog.wait(timeout_message: 'cannot be found')
    search_result.hybrid_search_cancel_button.tap
    search_result.same_item_confirm_link.wait_gone
    home = App.restart
    home.goto_genre
    home.tap_genre_by_name('CD・DVD・楽器')
    home.assert_genre_title('CD・DVD・楽器')
    search_result = home.search_by_genre('CD・DVD・楽器を選択する')
    search_result.same_item_confirm_link.wait_gone
  end
end
