@ios @android @24h @prod
Feature: 24h monitor
  Scenario: 24h monitor
    Given Monitor24h
      | key | value |
      | 商品（税込） | 1,000円 |
      | 送料 | 200円 |
      | 合計金額 | 1,200円 |
      | ポイント | 56ポイント |
      | お支払方法 | クレジットカード決済 |
      | 配送方法 | 宅配便 |
