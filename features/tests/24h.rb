class Monitor24h < TestCase
  def setup
    App.dismiss_alerts
    App.set_stage(false)
  end
  def run(data = nil)
    item = ITEMS.search(item_name: '新かご監視商品')
    user = find_user('prod_user_%d', 1..3)
    data = data && Hash[data[1..-1]] || {}
    App.logout ## logout
    screenshot('Home')
    App.login(user) ## login
    App.clear_cart ## clear cart
    bookmarks = App.open_bookmarks ## open bookmarks
    item_detail = bookmarks.bookmark(item).tap! ## tap bookmarked item
    item_detail = item_detail.tap_details ## tap details button
    cart = item_detail.place_in_cart ## add item to cart
    screenshot('In cart')
    step1 = cart.order_button.tap! ## tap order button
    step2 = step1.enter_password ## enter user password
    data.each { |key, value| ## validate page
      #expect(step2.value(key).to_number).to eq(3) #For failure
      expect(step2.value(key).to_number).to eq(value.to_number)
    #
    }
    step3 = step2.commit_button_top.tap! ## tap commit order button
    screenshot('Commit')
    expect(step3).to be_visible ## validate last page
  end
end
