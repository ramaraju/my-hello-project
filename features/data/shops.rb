require 'yaml'

class Shop
  attr_accessor :no, :shop, :shop_name, :url, :case_id
  attr_accessor :shop_specific_point
  attr_accessor :shop_coupon
  attr_accessor :notes
  attr_accessor :tax
  attr_accessor :credit_card
  attr_accessor :other_payment_methods
  attr_accessor :delivery_methods
  attr_accessor :payment_fee
  attr_accessor :delivery_date
  attr_accessor :next_day_delivery
  attr_accessor :delivery_info
  attr_accessor :coupon
  class ShopSpecificPoint
    attr_accessor :all_members, :diamond, :platinum, :gold, :silver, :regular, :valid_period
  end
  class Tax
    attr_accessor :tax_rate, :below_one_rule
  end
  class CreditCard
    attr_accessor :visa, :master, :jcb, :diners, :amex, :other
  end
  class OtherPaymentMethods
    attr_accessor :cod, :period_setting, :large_amount_discount, :other1, :other2, :other3, :rakuten_bank, :seven_eleven, :seven_eleven_deferred
  end
  class DeliveryMethods
    attr_accessor :home, :pelican, :kangaroo, :company, :yuu_pack, :bike, :overseas, :other1, :other2, :other3
  end
  class PaymentFee
    attr_accessor :hundred_all, :horizontal_axis, :vertical_axis, :postage_inclusive, :bulk_handling, :remote_island, :period, :large_amount_discount
  end
  class DeliveryDate
    attr_accessor :show_in_steps, :info_pc, :info_mobile, :time_setting, :possible_time_period, :time_selection, :selection_terms, :visitor_free_entry, :show_date_in_steps
  end
  class NextDayDelivery
    attr_accessor :application, :period1, :period2, :delivery_by
  end
  class DeliveryInfo
    attr_accessor :send_aim, :delivery_time
  end
  def initialize
    @shop_specific_point = ShopSpecificPoint.new
    @tax = Tax.new
    @credit_card = CreditCard.new
    @other_payment_methods = OtherPaymentMethods.new
    @delivery_methods = DeliveryMethods.new
    @payment_fee = PaymentFee.new
    @delivery_date = DeliveryDate.new
    @next_day_delivery = NextDayDelivery.new
    @delivery_info = DeliveryInfo.new
  end
end

SHOPS = YAML.load(File.open("#{File.expand_path(File.dirname(__FILE__))}/shops.yml"))

class << SHOPS
  def search(**filter)
    find { |shop|
      found = true
      filter.each { |key, value|
        if shop.instance_variable_get("@#{key}") != value
          found = false
          next
        end
      }
      found
    }
  end
end