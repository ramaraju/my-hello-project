require 'yaml'

class Address
  attr_accessor :firstname, :lastname, :firstname_kana, :lastname_kana
  attr_accessor :zip1, :zip2
  attr_accessor :province, :city, :street, :telephone1, :telephone2, :telephone3
  def detail_address(label = :order)
    detail = lastname << ' ' << firstname << "\n" << '〒 ' << zip1 << '-' << zip2 << "\n" << province << city << street
    if label == :sendto
    detail = detail << "\n" << telephone1 << telephone2 << telephone3
    end
    detail
  end
end

ADDRESS = YAML.load(File.open("#{File.expand_path(File.dirname(__FILE__))}/address.yml"))

class << ADDRESS
  def search(**filter)
    find { |address|
      found = true
      filter.each { |key, value|
        if address.instance_variable_get("@#{key}") != value
          found = false
          next
        end
      }
      found
    }
  end
end