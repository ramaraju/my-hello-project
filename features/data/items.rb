require 'yaml'

class Item
  attr_accessor :item
  attr_accessor :case_id
  attr_accessor :shop
  attr_accessor :item_name
  attr_accessor :category
  attr_accessor :description
  attr_accessor :type
  attr_accessor :order_button_string
  attr_accessor :item_id
  attr_accessor :cpc_search_keyword
  attr_accessor :cpc_item_name
  attr_accessor :web_view
  attr_accessor :price
  attr_accessor :tax
  attr_accessor :delivery_charge
  attr_accessor :x_axis_name
  attr_accessor :y_axis_name
  attr_accessor :in_stock_display
  attr_accessor :x_axis_option
  attr_accessor :y_axis_option
  attr_accessor :select
  attr_accessor :check_box
  attr_accessor :amount_in_stock
  attr_accessor :text_in_stock
  attr_accessor :text_not_in_stock
  attr_accessor :order_not_in_stock
  attr_accessor :product_directory_id
  attr_accessor :period_limit
  attr_accessor :time_specific_purchase
  attr_accessor :book_date
  attr_accessor :bidding
  attr_accessor :point_charge_rate
  attr_accessor :next_day_delivery_area
  attr_accessor :valid_period
  attr_accessor :asuraku_area
  attr_accessor :spux_rate

  def display_name
    description.nil? ? item_name : "#{description}\n#{item_name}"
  end
  def list_display_name
    if cpc_search_keyword.nil?
      item_name
    else
      "[PR]".concat(Moji.zen_to_han(item_name))
    end
  end
end

ITEMS = YAML.load(File.open("#{File.expand_path(File.dirname(__FILE__))}/items.yml"))

class << ITEMS
  def search(**filter)
    find { |item|
      found = true
      filter.each { |key, value|
        if item.instance_variable_get("@#{key}") != value
          found = false
          next
        end
      }
      found
    }
  end
end