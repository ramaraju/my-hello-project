require 'yaml'

class Card
  attr_accessor :card, :number
end

CARDS = YAML.load(File.open("#{File.expand_path(File.dirname(__FILE__))}/cards.yml"))

class << CARDS
  def search(**filter)
    find { |card|
      found = true
      filter.each { |key, value|
        if card.instance_variable_get("@#{key}") != value
          found = false
          next
        end
      }
      found
    }
  end
end