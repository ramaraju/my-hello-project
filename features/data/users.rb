require 'yaml'

class CreditCard
  attr_accessor :type, :number, :date
end

class Address
  attr_accessor :last_name, :first_name, :last_name_kana, :first_name_kana, :address
end

class User
  attr_accessor :no, :case_id, :user, :username, :last_name, :first_name, :password, :rank, :email, :birthday, :age
  attr_accessor :phone, :cards, :bank, :addresses
  attr_accessor :bookmarks, :view_history, :order_history, :search_history
  attr_accessor :payment, :orders, :points, :coupons,  :member_total_points, :member_future_granted_points, :member_timed_points, :rakuten_points
  def credentials
    [@username, @password]
  end
  def display_username
    last_name + ' ' + first_name
  end
end

USERS = YAML.load(File.open("#{File.expand_path(File.dirname(__FILE__))}/users.yml"))

class << USERS
  def user_by_name(username)
    find { |user| user.user == username }
  end
  def search(**filter)
    find { |user|
      found = true
      filter.each { |key, value|
        if user.instance_variable_get("@#{key}") != value
          found = false
          next
        end
      }
      found
    }
  end
end