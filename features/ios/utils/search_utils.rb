module SearchUtils
  def search(item)
    if item.cpc_search_keyword.nil?
      to_search = item.item_name
    else
      to_search = item.cpc_search_keyword
    end
    search = App.open_search_detail
    search.set_filter(:in_stock, true)
    search.search_bar.tap
    search.search_text_field.text = to_search
    calabash.tap_keyboard_action_key
    App.wait_progress_gone
    SearchResults.new
  end
  def goto_item_page(item)
    search_result = search(item)
    item_page = search_result.item(item).tap
  end
end