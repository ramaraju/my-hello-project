module BrowsingHistoryUtils
  def check_browsing_history_item_quantity
    home = App.goto_home
    #get number of browsing history item on home screen
    number_on_home = get_browsing_history_number
    menu = App.goto_menu
    #get number of browsing history item on home screen from More screen
    browsing_history = menu.options.browsing_history.tap
    item_number_on_bh = calabash.query('tableView', numberOfRowsInSection:0)
    #check numbers are equal
    expect(number_on_home).to eq(item_number_on_bh)

    #get number of browsing history item on home screen from member screen
    menu = App.goto_menu
    member_page = menu.options.member_service.tap
    browsing_history = member_page.options.browsing_history.tap
    item_number_on_bh = calabash.query('tableView', numberOfRowsInSection:0)
    #check numbers are equal
    expect(number_on_home).to eq(item_number_on_bh)
  end
  def check_browsing_history_first_item(item)
    first_item_query = 'RIItemListTableViewCell index:0 label index:0'
    menu = App.goto_menu
    browsing_history = menu.options.browsing_history.tap
    first_item = View.new(first_item_query)
    first_item_name = Query.new(first_item_query).first['value']
    expect(item.item_name).to eq(first_item_name)
    item_page = first_item.tap
    item_page = ItemPage.new(item)
    item_page.name_label.scroll_to
    if item.web_view
      item_page_web = item_page.goto_webview(item)
    end
    menu = App.goto_menu
    member_page = menu.options.member_service.tap
    browsing_history = member_page.options.browsing_history.tap
    first_item_name = calabash.query(first_item_query).first['value']
    expect(item.item_name).to eq(first_item_name)
    item_page = first_item.tap
    item_page = ItemPage.new(item)
    item_page.name_label.scroll_to
    if item.web_view
      item_page_web = item_page.goto_webview(item)
    end
  end
end


