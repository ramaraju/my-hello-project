module HomeUtils
  def get_browsing_history_number
    home = Home.wait
    home.browsing_button.scroll_to
    calabash.swipe(:up)
    home.browsing_button.tap
    browsing_collection_index = calabash.query('UICollectionView index:0', :numberOfSections).first.to_i
    item_number_on_home = calabash.query('UICollectionView index:0', numberOfItemsInSection:browsing_collection_index - 1)
  end
  def check_browsing_on_home_first_item(item)
    App.goto_home
    first_item_query = 'RIHomeItemCollectionViewCell index:0 label index:0'
    first_item = View.new(first_item_query)
    first_item.scroll_to
    first_item_name = Query.new(first_item_query).first['value']
    expect(first_item_name).to eq(item.item_name)
    item_page = first_item.tap
    item_page = ItemPage.new(item)
    item_page.name_label.scroll_to
    if item.web_view
      item_page_web = item_page.goto_webview(item)
    end
  end
end


