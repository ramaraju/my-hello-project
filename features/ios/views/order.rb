class WebCart < WebView
  ab_test
end

class WebCartA < WebCart
  web_view :order_button, xpath: '//input[@id="orderbutton"]', tap_target: :WebOrderStep1
  def initialize
    super(xpath: '//header/div[text()="買い物かご"]')
  end
end

class WebCartB < WebCart
  web_view :order_button, css: '.order-button', tap_target: :WebOrderStep1
  def initialize
    super(xpath: '//header/h1[text()="買い物かご"]')
  end
end

class WebOrderStep1 < NavigationBar
  def initialize
    super(id: '注文者情報')
  end
  def enter_password(password = nil)
    WebView.new(xpath: '//input[@id="p"]').enter_text(password || App.password)
    WebView.new(xpath: '//input[@id="login_button"]').tap!
    WebOrderStep2.wait
  end
end

class WebOrderStep2 < WebView
  ab_test
end

class WebOrderStep2A < WebOrderStep2
  web_view :commit_button_top, xpath: '//input[@name="commit"]', tap_target: :WebOrderStep3
  def initialize
    super(xpath: '//form/div/input[@class="button_red" and @value="注文を確定する"]')
  end
  def value(key)
    case key
    when '商品（税込）'
      xpath = '//html/body/div/form/div/div/table/tbody/tr[1]/td[@class="charge"]'
    when '送料'
      xpath = '//html/body/div/form/div/div/table/tbody/tr[2]/td[@class="charge"]'
    when '合計金額'
      xpath = '//html/body/div/form/div/div/h2/span'
    when 'ポイント'
      xpath = '//html/body/div/form/div/div/div/div/span/span'
    when 'お支払方法'
      xpath = '//html/body/div/form/div[@class="ctBoxPJ"][1]/div/div[@class="ctMaB10 ctBold"]'
    when '配送方法'
      xpath = '//html/body/div/form/div[@class="ctBoxPJ"][2]/div/div[@class="ctMaB10 ctBold"]'
    end
    result = WebView.all(xpath: xpath).query.first
    fail "element not found: #{key}" if !result
    result['textContent'].strip
  end
end

class WebOrderStep2B < WebOrderStep2
  web_view :commit_button_top, xpath: '//button[@name="commit"]', tap_target: :WebOrderStep3
  def initialize
    super(xpath: '//*[@id="step-main"]/article/header/h1[text()="買い物かご"]')
  end
  def value(key)
    case key
    when '商品（税込）'
      xpath = '//*[@id="form"]/section[2]/div[1]/div[1]/div/div[1]/div[2]'
    when '送料'
      xpath = '//*[@id="form"]/section[2]/div[1]/div[1]/div/div[2]/div[2]'
    when '合計金額'
      xpath = '//*[@id="form"]/section[2]/div[1]/div[3]/div[2]'
    when 'ポイント'
      xpath = '//*[@id="subTotal-271079"]/div[1]/span[3]'
    when 'お支払方法'
      xpath = '//*[@id="payment-info"]/div/div[2]/div[2]'
    when '配送方法'
      xpath = '//*[@id="form"]/section[3]/div[2]/div/div[2]/div[2]'
    end
    result = WebView.all(xpath: xpath).query.first
    fail "element not found: #{key}" if !result
    result['textContent'].strip
  end
end

class WebOrderStep3 < WebView
  ab_test
end

class WebOrderStep3A < WebOrderStep3
  def initialize
    super(xpath: '//header/div[text()="注文完了"]')
  end
end

class WebOrderStep3B < WebOrderStep3
  def initialize
    super(xpath: '//div[text()="Thank You"]')
  end
end
