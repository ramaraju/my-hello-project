require_relative 'genre'
require_relative 'login'
require_relative 'order'
require_relative 'tabs'

class Home < TabPage(0)
  class Search < Page('UIFieldEditor')
    view :text_field, TextField.new(placeholder: 'キーワードから探す')
    view :cancel_button, Button.new(marked: 'キャンセル')
    def suggestions
      keywords = calabash.query('RISuggestedKeywordCell', :keywordText, :text)
      genres = calabash.query('RISuggestedKeywordCell', :genreText, :text)
      keywords.zip(genres)
    end
    alias_method :history, :suggestions
    def suggest_label(keyword, genre)
      suggest_label = View.new(Query.new('UILabel', text: keyword).sibling('UILabel', text: genre), tap_target: SearchResults)
      suggest_label.wait(timeout_message: 'suggest label does not exist') {
        TableView.new.flick(y: -50)
      }
    end
  end
  class Genres < Page('UISegmentedControl', selectedSegmentIndex: 0)
    def genre(name)
      label = Label.new(text: name, tap_target: Genre.new(name))
      label.wait {
        ScrollView.new.flick(y: -50, offset: {x: 0, y: 100})
      }
      ScrollView.new.flick(y: -50, offset: {x: 0, y: 100})
      sleep(1) && label
    end
  end
  class TabList < Page('UISegmentedControl')
    def initialize(page_name)
      selection_tabs = [:genre, :bookmarks, :browsing_history, :recommend]
      super(selectedSegmentIndex: selection_tabs.index(page_name))
    end
    def tap_item(item)
      Label.new(text: item.item_name).wait {
        calabash.flick('UICollectionView', {x: 0, y: -50}, offset: {x: 0, y: 50}) && sleep(1)
      }
      calabash.flick('UICollectionView', {x: 0, y: -50}, offset: {x: 0, y: 50})
      Label.new(text: item.item_name, tap_target: ItemDetail).tap!
    end
    def list_detail
      button_label_query = Query.new('UIButtonLabel', "{text ENDSWITH 'をすべて見る'}")
      detail_button_query = button_label_query.parent('RIButton')
      Button.new(detail_button_query).wait {
        calabash.flick('UICollectionView', {x: 0, y: -50}, offset: {x: 0, y: 50})
      }
      calabash.flick('UICollectionView', {x: 0, y: -50}, offset: {x: 0, y: 50}) && sleep(0.5)
      page_name = button_label_query.first['text'].start_with?('お気に入り') ? 'お気に入りブックマーク' : '商品閲覧履歴'
      Button.new(detail_button_query, tap_target: Menu::ItemList.new(page_name))
    end
  end
  def search_keyword(keyword) # create a specific one rather than fuse with search(item) otherwise it makes the use in other functions a nightmare!!!
    home = Home.new
    home.search_bar.tap
    home.search_text_field.text = keyword
    calabash.tap_keyboard_action_key
  end
  def login(user)
    login_button.tap!.login(user)
  end
  attr_reader :points_button, :coupon_button, :info_button
  attr_reader :search_bar, :login_button, :user_name, :genres_button, :bookmarks_button
  attr_reader :recommend_button, :browsing_button, :ok_button, :search_text_field
  def initialize
    super
    @points_button = Label.new(text: '楽天ポイント')
    @coupon_button = Label.new(text: 'myクーポン')
    @info_button = Label.new(text: 'お知らせ')
    @search_bar = View.new('UISearchBarTextFieldLabel', tap_target: Search)
    @search_text_field = TextField.new('UISearchBarTextField')
    @login_button = Button.new(marked: 'ログイン', tap_target: Login.new(self))
    @genres_button = SegmentLabel.new(text: 'ジャンル', tap_target: Genres)
    @bookmarks_button = SegmentLabel.new(text: 'お気に入り', tap_target: TabList.new(:bookmarks))
    @browsing_button = SegmentLabel.new(text: '閲覧履歴', tap_target: TabList.new(:browsing_history))
    @recommend_button = SegmentLabel.new(text: 'オススメ', tap_target: TabList.new(:recommend))
    @user_name = calabash.query('RIHomeUserInfoCell', :nameLabel, :text).first
    @ok_button = Label.new(marked: 'OK')
  end
end
