require_relative 'search_results'

class Genre < Page
  class SubgenreTable < TableView
    cell :all_items, text: 'このジャンルの商品を見る', tap_target: SearchResults
  end
  attr_reader :subgenre_table
  def initialize(genre_name)
    super('UINavigationBar', id: genre_name)
    @subgenre_table = SubgenreTable.new
  end
  def go_back(page)
    View.new(marked: '戻る', tap_target: page).tap!
  end
end
