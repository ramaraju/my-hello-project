require_relative 'filters/shop_review_filter'

class ShopReview < Page('UINavigationBar', id: 'ショップレビュー')
  attr_reader :filter_navigation_button, :review_overall_score_label, :review_filter_close_button, :item_review_label
  def initialize
    super
    @filter_navigation_button = NavigationButton.new(marked: '絞込', tap_target: ShopReviewFilter)
    @review_overall_score_label = Label.new(marked: '総合評価')
    @review_filter_close_button = Button.new(marked: 'btnDel ios7')
    @item_review_label = SegmentLabel.new(marked: '商品レビュー')
  end
end
