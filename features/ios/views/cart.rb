class Cart < Page('UINavigationBar', id: '買い物かご')
  attr_reader :checkout_button
  def initialize
    super
    @checkout_button = WebView.new(xpath: '//input[@id="orderbutton"]', tap_target: Step1)
  end
  def clear
    delete_link = WebView.new(xpath: '//a[starts-with(text(),"削除")]')
    calabash.wait_for_network_indicator
    while delete_link.exist?
      delete_link.tap
      calabash.wait_for_network_indicator
    end
  end
end
