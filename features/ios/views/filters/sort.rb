require_relative 'filter_page'

class Sort < FilterPage
  attr_reader :options
  def initialize
    super('並び順')
    class << @options
      cell :default, text: '標準', tap_target: ItemReviewFilter
      cell :new, text: '新着順', tap_target: ItemReviewFilter
      cell :high_review, text: '評価が高い順', tap_target: ItemReviewFilter
    end
  end
end
