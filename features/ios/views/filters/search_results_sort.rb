require_relative 'filter_page'

class SearchResultsSort < FilterPage
  def initialize
    super('並び順')
    class << @options
      cell :default, text: '標準', tap_target: SearchResults
      cell :price_low, text: '価格が安い', tap_target: SearchResults
      cell :price_high, text: '価格が高い', tap_target: SearchResults
      cell :newest, text: '新着順', tap_target: SearchResults
      cell :review_number, text: '感想の件数が多い', tap_target: SearchResults
      cell :review_high, text: '感想の評価が高い', tap_target: SearchResults
    end
  end
end
