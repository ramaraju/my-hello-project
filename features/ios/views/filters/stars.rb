class Stars < Page('UINavigationBar', id: '★の数')
  attr_reader :navigation_back_button, :stars_list, :stars_all, :stars_5, :stars_4, :stars_3, :stars_2, :stars_1
  def initialize
    super
    @navigation_back_button = NavigationBackButton.new(marked: '戻る', tap_target: ItemReview)
    @stars_all = Label.new(text: 'すべて', tap_target: ItemReviewFilter)
    @stars_5 = Label.new(text: '5つ', tap_target: ItemReviewFilter)
    @stars_3 = Label.new(text: '4つ', tap_target: ItemReviewFilter)
    @stars_3 = Label.new(text: '3つ', tap_target: ItemReviewFilter)
    @stars_2 = Label.new(text: '2つ', tap_target: ItemReviewFilter)
    @stars_1 = Label.new(text: '1つ', tap_target: ItemReviewFilter)
  end
end
