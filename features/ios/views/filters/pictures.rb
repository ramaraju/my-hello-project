require_relative 'filter_page'

class Pictures < FilterPage
  attr_reader :options
  def initialize
    super('投稿画像')
    class << @options
      cell :all, text: 'すべて', tap_target: ItemReviewFilter
      cell :with_pic, text: '画像あり', tap_target: ItemReviewFilter
    end
  end
end
