require_relative 'filter_page'

class ItemReviewFilter < FilterPage
  attr_reader :navigation_back_button, :validate_button, :clear_button, :options
  def initialize
    super('絞込/並び順')
    @navigation_back_button = NavigationBackButton.new(marked: '戻る', tap_target: ItemReview)
    @validate_button = RIButton.new(marked: '上の条件で探す', tap_target: ItemReview)
    @clear_button = RIButton.new(marked: 'クリア')
    class << @options
      cell :stars, text: nil, tap_target: Stars
      cell :reviewer, text: 'レビュアー', tap_target: Reviewer
      cell :reviewer_age, text: 'レビュアーの年齢', tap_target: ReviewerAge
      cell :reviewer_gender, text: 'レビュアーの性別', tap_target: ReviewerGender
      cell :pictures, text: '翌日配送エリア', tap_target: Pictures
      cell :sort, text: '並び順', tap_target: Sort
    end
  end
  def clear_filter
    clear_button.tap!
    FilterPage::ClearConfirmation.new.clear_button.tap!
  end
end
