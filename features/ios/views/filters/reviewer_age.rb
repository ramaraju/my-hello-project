require_relative 'filter_page'

class ReviewerAge < FilterPage
  attr_reader :options
  def initialize
    super('レビュアーの年齢')
    class << @options
      cell :all, text: 'すべて', tap_target: ItemReviewFilter
      cell :ten, text: '10代', tap_target: ItemReviewFilter
      cell :twenty, text: '20代', tap_target: ItemReviewFilter
      cell :thirty, text: '30代', tap_target: ItemReviewFilter
      cell :fourty, text: '40代', tap_target: ItemReviewFilter
      cell :fifty, text: '50代', tap_target: ItemReviewFilter
    end
  end
end
