class FilterPage < Page
  attr_reader :cancel_button, :clear_button, :options
  class ClearConfirmation < Page("view:'_UIAlertControllerActionView'")
    view :clear_button, View.new("view:'_UIAlertControllerActionView'", marked: 'クリアする')
    view :cancel_button, View.new("view:'_UIAlertControllerActionView'", marked: 'キャンセル')
  end
  def initialize(id)
    super('UINavigationBar', id: id)
    @options = TableView.new
  end
end
