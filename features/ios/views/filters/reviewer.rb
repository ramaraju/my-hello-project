require_relative 'filter_page'

class Reviewer < FilterPage
  attr_reader :options
  def initialize
    super('レビュアー')
    class << @options
      cell :buyers_only, text: '購入者のみ', tap_target: ItemReviewFilter
      cell :including_non_buyers, text: '未購入者を含む', tap_target: ItemReviewFilter
    end
  end
end
