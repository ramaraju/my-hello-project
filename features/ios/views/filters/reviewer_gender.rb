class ReviewerGender < FilterPage
  attr_reader :options
  def initialize
    super('レビュアーの性別')
    class << @options
      cell :all, text: 'すべて', tap_target: ItemReviewFilter
      cell :male, text: '男性', tap_target: ItemReviewFilter
      cell :female, text: '女性', tap_target: ItemReviewFilter
    end
  end
end
