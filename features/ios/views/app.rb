require_relative 'filters/item_review_filter'
require_relative 'filters/pictures'
require_relative 'filters/reviewer'
require_relative 'filters/reviewer_age'
require_relative 'filters/reviewer_gender'
require_relative 'filters/sort'
require_relative 'filters/stars'
require_relative 'filters/search_results_sort'
require_relative 'web_view/web_view_page'
require_relative 'web_view/my_rakuten_web'
require_relative 'web_view/point_club_web'
require_relative 'web_view/item_page_web'
require_relative 'cart'
require_relative 'genre'
require_relative 'home'
require_relative 'order'
require_relative 'item_page'
require_relative 'item_recommendations'
require_relative 'item_review'
require_relative 'login'
require_relative 'bookmarks'
require_relative 'menu'
require_relative 'ranking'
require_relative 'shop_review'
require_relative 'search'
require_relative 'search_results'
require_relative 'tabs'
require_relative '../utils/search_utils'
require_relative '../utils/home_utils'
require_relative '../utils/browsing_history_utils'
require_relative 'browsing_history'
require_relative 'step_1'

class View
  def scroll_to(direction = :up) # to replace previous version to handle cases when control is behind Nav or Tab bar
    wait {
      calabash.swipe(direction)
    }
    # to avoid controls that are under top and bottom bars
    if TabBar.exist? && direction == :up && rect.y >= TabBar.new.rect.y - 50 || NavigationBar.exist? && direction == :down && rect.y >= NavigationBar.new.rect.y + 50
      calabash.swipe(direction)
    end
    self
  end
end

class RIButton < View('RIButton')
end

class App
  class << self; include RSpec::Matchers end
  extend Calabash::Cucumber::Operations
  def self.tabs
    @tabs ||= Tabs.new
  end
  def self.username
    @user.username
  end
  def self.password
    @user.password
  end
  def self.dismiss_alerts
    while Alert.exist?
      alert = Alert.new
      case alert.title
      when 'ようこそ楽天市場へ！'
        reply = 'いいえ'
      when '楽天市場からのお知らせを受け取りますか？'
        reply = '今は興味がない'
      end
      button = alert.buttons.find { |button| button.label == reply }
      expect(button).not_to be_nil
      button.tap
      sleep(2)
    end
  end
  def self.restart
    calabash.start_test_server_in_background
    goto_home
  end
  def self.debug?
    Bridge::RIPreferences.isDebugging
  end
  def self.stage?
    Bridge::RIPreferences.isStagingMode
  end
  def self.set_stage(stage = true)
    tabs.menu.tap!.set_stage(stage) if stage? != stage
  end
  def self.goto_home
    tabs.home.tap!
  end
  def self.open_search_detail
    tabs.search.tap!
    Search.wait
  end
  def self.goto_menu
    tabs.menu.tap!
    Menu.wait
  end
  def self.logout
    goto_menu.logout if Bridge::RIAppSession.sharedInstance.userId
  end
  def self.login(user)
    user = USERS.search(user: user) if user.is_a?(String)
    @user = user
    home = Home.new
    if !Label.new(marked: "#{user.last_name}#{user.first_name}さん").exist?
      logout if !home.login_button.exist?
      goto_home.login(user)
    end
  end
  def self.cart_item_count
    Bridge::RIAppSession.sharedInstance.cartItemNumber
  end
  def self.clear_cart
    tabs.cart.tap!.clear # if cart_item_count > 0
  end
  def self.open_bookmarks
    goto_bookmarks
  end
  def self.goto_bookmarks
    goto_menu.options.bookmarks.tap!
  end
  def self.goto_browsing
    goto_menu.options.browsing_history.tap!
  end
  def self.goto_search
    tabs.search.tap!
    Search.wait
  end
  def self.clear_search_history
    tabs.search.tap!.search_history_button.tap!.clear_history
  end
  def self.clear_bookmarks
    goto_bookmarks.clear
  end
  def self.clear_browsing
    goto_browsing.clear
  end
  def self.wait_progress_gone
    sleep 1
    View.new('MBProgressHUD').wait_gone
  end
  def self.keyboard_press_key(key, times)
    index = 0
    while index != times
      calabash.keyboard_enter_char key
      index += 1
    end
  end
end
