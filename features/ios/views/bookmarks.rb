class Bookmarks < Page('UILabel', marked:'お気に入りブックマーク')
  class Bookmark
    attr_accessor :name_label
    def initialize(item)
      @name_label = Label.new(marked: item.item_name, tap_target: ItemPage.new(item))
    end
    def shop_label
      Label.new(@name_label.query.sibling('label', index: 0))
    end
    def price_label
      Label.new(@name_label.query.sibling('label', index: 1))
    end
    def scroll_to
      @name_label.scroll_to(:up)
    end
    def tap!
      @name_label.tap!
    end
  end
  attr_reader :edit_button
  def initialize
    super
    @edit_button = View.new('UIButtonLabel', marked: '編集')
  end
  def bookmark(item)
    Bookmark.new(item)
  end
  alias_method :item, :bookmark
end
