require_relative 'web_view/my_rakuten_web'
require_relative 'web_view/point_club_web'
require_relative 'bookmarks'

class Menu < TabPage(4)
  class MemberService < NavigationBar
    attr_reader :user_info, :total_points, :time_limit_points, :future_points, :options
    def initialize
      super(id: '会員情報')
      @user_info = Label.new(marked: 'userNameAndRankLabel')
      @total_points = Label.new(marked: 'totalPointLabel')
      @time_limit_points = Label.new(marked: 'limitedPointLabel')
      @future_points = Label.new(marked: 'futurePointLabel')
      @options = TableView.new
      class << @options
        cell :my_rakuten, text: 'my Rakuten', tap_target: MyRakutenWeb
        cell :point_club, text: 'Point Club', tap_target: PointClubWeb
        cell :bookmarks, tap_target: Bookmarks
        cell :browsing_history, text: '閲覧履歴', tap_target: ItemList.new('商品閲覧履歴')
        cell :order_history, text: '購入履歴', tap_target: ItemList.new('[Rakuten]Log in')
      end
    end
    def logout
      Label.new(text: 'ログアウト').scroll_to.tap
      logout_popup = ActionSheet.wait
      logout_popup.tap_button(0)
    end
    def login(user)
      Label.new(text: 'ログイン', tap_target: Login.new(MemberService)).tap!.login(user.username, user.password)
    end
  end
  class ItemList < NavigationBar
    attr_reader :navigation_button, :sort_button, :select_button, :blank_message
    def initialize(title = 'お気に入りブックマーク')
      super(id: title)
      @navigation_button = View.new('UINavigationButton')
      @sort_button = View.new('RIButton')
      @select_button = View.new(Query.new('UITableViewCellEditControl').descendant('UIImageView'))
      @blank_message = View.new('UITableViewCell', text: '表示する商品がありません')
    end
    def clear
      until blank_message.exist?
        navigation_button.tap
        while select_button.exist?
          select_array = Query.new('UITableViewCellEditControl').descendant('UIImageView').run
          y_min = Query.new('UINavigationBar').first['rect']['y'] + Query.new('UINavigationBar').first['rect']['height']
          select_array.each { |select|
            calabash.touch(select) if select['rect']['y'] > y_min
          }
          calabash.flick('UIViewControllerWrapperView', {x: 0, y: -100})
          calabash.wait_for_animations
        end
        break if blank_message.exist?
        navigation_button.tap && action_sheet = ActionSheet.wait
        delete_button = ''
        action_sheet.buttons.each { |button|
          delete_button = button
          break if button.end_with?('を削除する')
        }
        action_sheet.tap_button(action_sheet.buttons.index(delete_button)) && sleep(2)
      end
    end
    def item(item)
      App.wait_progress_gone
      Label.new(text: item).wait {
        calabash.flick('UITableViewWrapperView', {x: 0, y: -100}) && sleep(1)
      }
      y_max = Query.new('UITabBar').first['rect']['y']
      if Label.new(text: item).query.first['rect']['center_y'] >= y_max
        calabash.flick('UITableViewWrapperView', {x: 0, y: -100}) && sleep(1)
      end
      Label.new(text: item, tap_target: ItemDetail).wait
    end
  end
  class DebugOptions < TableView
    cell :debug, text: 'Debugging'
    cell :stage, text: 'Staging Mode'
  end
  class Setting < NavigationBar
    class LoginStatus < TableView
      cell :save, text: '保存する'
      cell :not_save, text: '保存しない'
    end
    attr_reader :login_status, :notice
    def initialize
      super(id: '設定')
      @login_status = Label.new(text: '楽天会員ログイン状態', tap_target: LoginStatus)
      @notice = Label.new(text: '楽天市場からのお知らせ', tap_target: Alert)
    end
  end
  attr_reader :options
  def initialize
    super
    @options = TableView.new
    class << @options
      cell :bookmarks, text: 'お気に入りブックマーク', tap_target: Bookmarks
      cell :browsing_history, text: '閲覧履歴', tap_target: ItemList.new('商品閲覧履歴')
      cell :member_service, text: '会員情報', tap_target: MemberService
      cell :setting, text: '設定', tap_target: Setting
      cell :debug_options, text: 'Debug Options', tap_target: DebugOptions
    end
  end
  def logout
    options.member_service.tap!.logout
  end
  def set_stage(stage = true)
    debug_label = @options.debug_options
    debug_label.scroll_to
    view = debug_label.tap
    if stage && !view.debug.switch.on?
      view.debug.switch.tap
      App.restart
      return App.set_stage(stage)
    end
    if stage != view.stage.switch.on?
      view.stage.switch.tap
      App.restart
    end
  end
end

class ItemDetail < View('RIItemDetailCollectionView')
  def initialize(item_name = nil)
    return super() if item_name.nil?
    super(Query.new('RIItemViewDescriptionCell').descendant('UILabel').predicate("text ENDSWITH '#{item_name}'"))
  end
  def tap_details
    calabash.wait_for_animations
    label = Label.new(text: '詳細を見る', tap_target: WebItemDetail)
    count = 0
    begin
      scroll(:down)
      sleep(1)
      count += 1
      fail if count == 50
    end
    until label.exist?
      sleep(1)
      label.tap!
    end
  end
  def bookmark(bookmark_location = :up)
    case bookmark_location
    when :up
      View.new('RIBarButtonItemView').wait
    when :down
      View.new(Query.new('UICollectionViewCell').descendant('RIButton', marked: 'お気に入り')).wait {
        calabash.flick('RIItemDetailCollectionView', {x: 0, y: -100})
      }
    end
  end
  def assert_item_info(item_object)
    unless item_object.nil?
      unless item_object.item_name.nil?
        View.new(Query.new('RIItemViewDescriptionCell').descendant('UILabel')
                     .predicate("text ENDSWITH '#{item_object.item_name}'")).wait(timeout_message: 'item name is not correct') {
          calabash.flick('UIScrollView', {x: 0, y: -50}) && sleep(1)
        }
      end
      unless item_object.item_button.nil?
        View.new('UIButton', marked: item_object.item_button).wait(timeout_message: 'item button is not correct') {
          calabash.flick('UIScrollView', {x: 0, y: -50}) && sleep(1)
        }
      end
    end
  end
  def go_back(page)
    if View.new('UITabBar', selectedIndex: 1).exist?
      View.new('UINavigationItemButtonView', tap_target: page).tap!
    else
      View.new('UINavigationButton', tap_target: page).tap!
    end
  end
end
