class ItemReview < Page('UINavigationBar', id: '商品レビュー')
  attr_reader :filter_navigation_button, :review_overall_score_label, :review_filter_all_label, :review_filter_5_stars_label
  attr_reader :review_filter_4_stars_label, :review_filter_3_stars_label, :review_filter_2_stars_label, :review_filter_1_stars_label, :review_filter_close_button, :shop_review_label
  def initialize
    super
    @filter_sort_navigation_button = NavigationButton.new(marked: '絞込/並び順', tap_target: ItemReviewFilter)
    @review_overall_score_label = Label.new(marked: '総合評価')
    @review_filter_all_label = TableViewCellContentView.new(index: 0)
    @review_filter_5_stars_label = TableViewCellContentView.new(index: 1)
    @review_filter_4_stars_label = TableViewCellContentView.new(index: 2)
    @review_filter_3_stars_label = TableViewCellContentView.new(index: 3)
    @review_filter_2_stars_label = TableViewCellContentView.new(index: 4)
    @review_filter_1_stars_label = TableViewCellContentView.new(index: 5)
    @review_filter_close_button = Button.new(marked: 'btnDel ios7')
    @shop_review_label = SegmentLabel.new(marked: 'ショップレビュー')
  end
end
