class Tabs < TabBar
  attr_reader :home, :search, :ranking, :cart, :menu
  def initialize
    super
    @home = TabBarButton.new(index: 0, tap_target: Home)
    @search = TabBarButton.new(index: 1, tap_target: Search)
    @ranking = TabBarButton.new(index: 2, tap_target: Ranking)
    @cart = TabBarButton.new(index: 3, tap_target: Cart)
    @menu = TabBarButton.new(index: 4, tap_target: Menu)
  end
end

class TabPage < Page
end

def TabPage(index)
  page_class = Class.new(TabPage)
  page_class.send(:define_method, :trait) {
    Query.new('UITabBar', selectedIndex: index)
  }
  page_class
end

require_relative 'home'
require_relative 'search'
require_relative 'ranking'
require_relative 'cart'
require_relative 'menu'
