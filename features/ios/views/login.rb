class Login < Page
  view :username_field, TextField.new(placeholder: 'ユーザID')
  view :password_field, TextField.new(placeholder: 'パスワード')
  button :login_button, marked: 'ログイン'
  button :logout_button, marked: 'ログアウト'
  def initialize(parent)
    super('UINavigationBar', id: 'ログイン')
    login_button.tap_target = logout_button.tap_target = parent
  end
  def login(user)
    username_field.text = user.username
    password_field.text = user.password
    login_button.tap!
  end
end
