class BrowsingHistory < Page('UILabel', marked:'商品閲覧履歴')
  attr_reader :edit_button
  def initialize
    super
    @edit_button = View.new('UIButtonLabel', marked: '編集')
  end
end
