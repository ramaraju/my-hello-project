class ItemPage < Page('RIItemViewSectionTitleCell') # can't use the page id as identifier as the page is reused with different id if open from browsing history, bookmarks, etc...
  attr_reader :bookmark_navigation_icon, :back_button, :cart_button, :bookmark_button, :share_button, :line_button, :room_button
  attr_reader :cart_button, :review_button, :links, :pc_version_label, :shop_items_label, :shop_top_page_label, :shop_payment_shipping_label
  attr_reader :review_label, :see_recommendations_button, :details_label
  attr_accessor :name_label, :add_search_shop_label
  def initialize(item = nil)
    super
    @bookmark_navigation_icon = ImageView.new(id: 'icon_toolbar_favorite')
    @back_button = NavigationItemBackButton.new
    # @cart_button = ImageView.new(id: 'cart')
    @bookmark_button = ImageView.new(id: 'AddToFavorite')
    @share_button = ImageView.new(id: 'ShareShare')
    @line_button = ImageView.new(id: 'LineShare')
    @room_button = ImageView.new(id: 'ROOMShare')
    @cart_button = ImageView.new(id: 'cart', tap_target: Cart)
    @review_button = View.new("* {text BEGINSWITH 'レビュー'}", tap_target: ItemReview)
    @pc_version_label = Label.new(marked: 'PC版で見る')
    @shop_items_label = Label.new(text: 'ショップの商品一覧', tap_target: SearchResults)
    @shop_top_page_label = Label.new(text: 'ショップのトップページ')
    @shop_payment_shipping_label = Label.new(text: '会社概要・決済方法・配送方法')
    @add_search_shop_label = Label.new(text: '検索ショップリストに追加')
    @see_recommendations_button = View.new(marked: 'オススメ商品をもっと見る', tap_target: ItemRecommendations)
    description_label = ''
    if item
      @details_label = Label.new(text: '詳細を見る', tap_target: item && ItemPageWeb.new(item))
      description_label = "#{item.description} " if item && !item.description.nil?
      @name_label = Label.new(marked: "#{description_label}#{item.item_name}")
    end
  end

  def tap_details
    calabash.wait_for_animations
    @details_label.scroll_to(:up)
    @details_label.tap
  end
  def goto_webview(item)
    item_page = ItemPage.new(item)
    item_page_web = item_page.details_label.scroll_to.tap
    item_page_web.periodic_order.scroll_to
  end
end
