require_relative 'item_page'
require_relative '../utils/search_utils'

class Search < TabPage(1)
  include SearchUtils
  class SearchHistory < Page('UINavigationButton', marked: '削除')
    attr_reader :clear_button, :search_detail_button
    def initialize
      @clear_button = View.new('UINavigationButton', marked: '削除')
      @search_detail_button = Label.new(marked: '詳細検索', tap_target: Search)
    end
    def clear_history
      clear_button.tap
      as = ActionSheet.wait
      as.tap_button(as.buttons.index('削除する'))
    end
  end
  class AllGenres < Page('UINavigationBar', id: 'すべてのジャンル')
    attr_reader :cancel_button
    def initialize
      super
      @cancel_button = Button.new(marked: 'キャンセル')
    end
  end
  class PriceRange < Page('UINavigationBar', id: '価格帯')
    attr_reader :price_min_field, :price_max_field, :validate_button
    def initialize
      super
      @price_min_field = TextField.new(index: 0)
      @price_max_field = TextField.new(index: 1)
      @validate_button = Button.new(marked: '確定')
    end
    def set_range(min, max)
      @price_min_field.text = min.to_s
      @price_max_field.text = max.to_s
      @validate_button.tap!
    end
  end
  class ExcludeKeyword < Page('UINavigationBar', id: '除外キーワード')
    attr_reader :keyword_field, :validate_button, :ok_button, :back_button
    def initialize
      super
      @keyword_field = TextField.new(index: 0)
      @validate_button = Button.new(marked: '確定')
      @ok_button = Label.new(marked: 'OK')
      @back_button = View.new('UINavigationItemButtonView')
    end
    def set_keyword(keyword)
      keyword_field.clear_button.tap_if_exist
      keyword_field.enter_text(keyword.to_s)
      validate_button.tap!
      App.wait_progress_gone
    end
  end
  class SelectShop < Page('UINavigationBar', id: 'ショップ指定')
    attr_reader :edit_button, :complete_button, :delete_button
    def initialize
      super
      @keyword_field = TextField.new(index: 0)
      @validate_button = Button.new(marked: '確定')
    end
    def set_shop(shop)
      View.new(marked: shop.shop_name).tap
    end
  end
  class Asuraku < Page('UINavigationBar', id: 'エリア指定')
  end
  class Sort < Page('UINavigationBar', id: '並び順')
    class Order < TableView
      cell :default, text: '標準'
      cell :price_low, text: '価格が安い'
      cell :price_high, text: '価格が高い'
      cell :newest, text: '新着順'
      cell :reviews_number, text: '感想の件数が多い'
      cell :reviews_high, text: '感想の評価が高い'
    end
    attr_reader :order
    def initialize
      @order = Order.new
    end
  end
  class Criteria < TableView
    cell :all_genres, text: 'ジャンル', tap_target: AllGenres
    cell :price_range, text: '価格帯', tap_target: PriceRange
    cell :exclude_keyword, text: '除外キーワード', tap_target: ExcludeKeyword
    cell :shop, text: 'ショップ', tap_target: SelectShop
    cell :asuraku, text: '翌日配送エリア', tap_target: Asuraku
    cell :in_stock, text: '在庫あり・注文可能'
    cell :free_shipping, text: '送料無料'
    cell :card_ok, text: 'カードOK'
    cell :sort, text: '並び順', tap_target: Sort
  end
  def search_item(item_id)
    # taking random data from DB waiting for db refactoring
    item = ITEMS.find { |item| item.search_id == item_id }
    search_text_field.clear_button.tap_if_exist
    search_text_field.enter_text(item.case_id)
    calabash.tap_keyboard_action_key
  end
  def search_keyword(keyword) # create a specific one rather than fuse with search(item) otherwise it makes the use in other functions a nightmare!!!
    search_bar.tap
    search_text_field.text = keyword
    calabash.tap_keyboard_action_key
  end
  def label(genre)
    Label.new(text: genre)
  end
  def set_price_range(min, max)
    criteria.price_range.tap
    price_range.set_range(min.to_s, max.to_s)
  end
  def set_exclude_keyword(keyword)
    criteria.exclude_keyword.tap
    exclude_keyword.set_keyword(keyword.to_s)
  end
  def set_asuraku(area)
    criteria.asuraku.tap
    View.new('UITableViewCell', text: area).scroll_to
    View.new('UITableViewCell', text: area).tap
  end
  def set_search_shop(shop)
    criteria.shop.tap
    @select_shop.set_shop(shop)
  end
  def set_filter(filter, status)
    current_status = filter_status(filter)
    case filter
    when :in_stock
      button = criteria.in_stock
    when :free_shipping
      button = criteria.free_shipping
    when :card_ok
      button = criteria.card_ok
    end
    while status != current_status
      button.tap
      current_status = filter_status(filter)
    end
    raise "#{filter} status hasn't been set to #{status}" unless current_status == status
  end
  def filter_status(filter)
    case filter
    when :genre
      filter_string = 'ジャンル'
    when :price_range
      filter_string = '価格帯'
    when :exclude_keyword
      filter_string = '除外キーワード'
    when :shop
      filter_string = 'ショップ'
    when :asuraku
      filter_string = '翌日配送エリア'
    when :in_stock
      filter_string = '在庫あり・注文可能'
      flag = :availability
    when :free_shipping
      filter_string = '送料無料'
      flag = :postageFlag
    when :card_ok
      filter_string = 'カードOK'
      flag = :creditCardFlag
    when :sort
      filter_string = '並び順'
    end
    if filter == :in_stock || filter == :free_shipping || filter == :card_ok # for those, it's just a tick mark
      calabash.query("view text:'#{filter_string}' index:0", :_viewControllerForAncestor, :searchOptions, flag).first == 1
    else # want to get the value
      calabash.query("UILabel text:\'#{filter_string}\' sibling label").first['label']
    end
  end
  def clear_all_filters
    clear_criteria_button.tap
    clear_confirm_button.tap
  end
  attr_reader :search_bar, :search_text_field, :cancel_button, :search_history_button, :criteria, :sort
  attr_reader :price_range, :exclude_keyword, :apply_criteria_button, :clear_criteria_button, :suggestions_list
  attr_reader :clear_confirm_button, :clear_subgenre_button, :search_history_button, :ok_button, :back_button
  def initialize
    super
    @search_bar = Label.new('UISearchBar')
    @search_text_field = TextField.new('UISearchBarTextField')
    @cancel_button = Button.new(title: 'キャンセル')
    @search_history_button = SegmentLabel.new(text: '検索履歴', tap_target: Search::SearchHistory)
    @criteria = Criteria.new
    @sort = Sort.new
    @price_range = PriceRange.new
    @exclude_keyword = ExcludeKeyword.new
    @select_shop = SelectShop.new
    @apply_criteria_button = Button.new(marked: '上の条件で探す')
    @clear_criteria_button = Button.new(marked: 'クリア')
    @clear_confirm_button = View.new(text: 'クリア', index: 1)
    @clear_subgenre_button = Button.new(marked: '×')
    @ok_button = Label.new(marked: 'OK')
    @suggestions_list = View.new('RISuggestedKeywordCell')
    @back_button = View.new('UINavigationItemButtonView')
  end
end
