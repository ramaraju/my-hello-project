class Step1 < Page('UILabel', text: '注文者情報')
  attr_reader :username_field, :password_field, :login_button, :no_account_button
  def initialize
    super
    @username_field = WebView.new(xpath: '//input[@id="u"]')
    @password_field = WebView.new(xpath: '//input[@id="p"]')
    @login_button = WebView.new(xpath: '//input[@id="login_button"]')
    @no_account_button = WebView.new(xpath: '//input[@id="unlogin_button"]')
  end
  def login(user)
    username_field.tap
    App.keyboard_press_key('Delete', 10)
    calabash.keyboard_enter_text(user.username)
    password_field.tap
    calabash.keyboard_enter_text(user.password)
    login_button.tap!
  end
end
