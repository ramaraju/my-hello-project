require_relative '../item_page'

class ItemPageWeb < WebViewPage
  web_view :periodic_order, xpath: '@id=\"step0_purchase_bot\"'
  def initialize(item)
    shop = SHOPS.search(shop: item.shop)
    shop_name = shop.shop_name
    super("#{item.item_name}：#{shop_name}")
  end
  def place_in_cart
    button = WebView.new(css: '#fixedCheckOut > a', tap_target: WebCart)
    button.scroll_to
    button.tap!
  end
end
