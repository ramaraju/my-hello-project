class WebViewPage < View
  def initialize(page_name)
    super(Query.new('UINavigationBar').descendant('UILabel', text: page_name))
  end
end
