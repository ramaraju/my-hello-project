require_relative 'web_view_page'

class PointClubWeb < WebViewPage
  def initialize
    super('【楽天PointClub】：楽天スーパーポイント総合サイト')
  end
end
