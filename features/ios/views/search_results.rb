require 'moji'

class SearchResults < NavigationBar
  attr_reader :pr_item, :search_text_field, :search_bar, :cheapest_shop_button, :layout_button, :back_button
  attr_reader :search_history_button, :genre_button
  def initialize
    super(id: 'RISearchResultsView')
    @pr_item = Label.new(predicate: "text BEGINSWITH '[PR]'")
    @search_bar = Label.new('UISearchBar')
    @search_text_field = TextField.new('UISearchBarTextField')
    @cheapest_shop_button = Label.new(text: '最安ショップを見る')
    @layout_button = NavigationButton.new
    @back_button = View.new('UINavigationItemButtonView', tap_target: Search)
    @sort_button = View.new(text: '並び順', tap_target: SearchResultsSort)
    @search_history_button = View.new(text: '検索履歴', tap_target: Search::SearchHistory)
    @genre_button = Label.new(text: 'ジャンル')
  end
  def set_sort(type)
    search_results_sort = @sort_button.tap
    search_results_sort.options.send(type).tap
  end
  def label(genre_name)
    Label.new(text: genre_name)
  end
  def item(item, shop = nil)
    item_name = item.item_name
    item_name = '[PR]'.concat(Moji.zen_to_han(item.item_name)) if !item.cpc_search_keyword.nil?
    item_query = Query.new('RIResultsListCollectionViewCell').descendant('UILabel', text: item_name)
    item_query = item_query.sibling("UILabel text: #{shop}") if shop
    View.new('RIGridCollectionView').wait && sleep(1)
    View.new(item_query, tap_target: ItemDetail).wait(timeout_message: 'item does not exist') {
      calabash.flick('RIGridCollectionView', {x: 0, y: -100})
    }
  end
  def go_back(page)
    View.new(marked: '戻る', tap_target: page).tap!
  end
  def item_by_index(index)
    View.new("RIResultsListCollectionViewCell index:#{index.to_i - 1}")
  end
  def results_number
    calabash.query('RIGridCollectionView', numberOfItemsInSection: 0)
  end
  def cell_info(cell_index, type)
    case type
    when :shop_name
      type = 0
    when :item_name
      type = 1
    end
    calabash.query("RIResultsListCollectionViewCell index: #{cell_index - 1} UILabel index: #{type}").first['text']
  end
end
