class Home02 < TestCase
  def setup
    App.dismiss_alerts
    App.set_stage
    App.clear_search_history
  end
  def run
    home = App.goto_home
    genres = home.genres_button.tap!
    genre_top = genres.genre('食品').tap!
    search_result = genre_top.subgenre_table.all_items.tap!
    search_result.label('食品').wait(timeout_message: 'current genre is not correct')
    genre_top = search_result.go_back(Genre.new('食品'))
    home = genre_top.go_back(Home)
    search = home.search_bar.tap!
    search.history.include?(['検索キーワードなし', '標準/食品/在庫あり・注文可能'])
  end
end
