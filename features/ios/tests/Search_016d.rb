class Search016d < TestCase
  def setup
    App.dismiss_alerts
    App.set_stage
  end
  def run(category, item_type)
    ## check no results are found
    search = App.goto_search
    search.search_keyword('cpc')
    search_results = SearchResults.wait
    search_results.genre_button.tap
    Label.new(text: category).scroll_to.tap
    Label.new(text: "#{category}に決定する").scroll_to.tap
    search_results = SearchResults.wait
    App.wait_progress_gone
    first_item_name = search_results.cell_info(1, :item_name)
    if item_type == 'cpc'
      expect(first_item_name).to start_with('[PR]')
    else
      expect(first_item_name).not_to start_with('[PR]')
    end
  end
end
