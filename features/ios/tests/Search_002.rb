require_relative '../utils/search_utils'

class Search002 < TestCase
  include SearchUtils
  def setup
    App.dismiss_alerts
    App.set_stage
  end
  def run
    home = App.goto_home
    home.search_keyword('ca')
    SearchResults.wait
    home = App.goto_home
    home.search_keyword('c')
    View.new(marked: '漢字または2文字以上のキーワードを指定してください。
　1文字(英数ひらがなカタカナ)での検索はできません').wait(timeout_message: '漢字または2文字以上のキーワードを指定してください。
　1文字(英数ひらがなカタカナ)での検索はできません。not displayed')
    home.ok_button.tap
    expect(home.ok_button).not_to be_visible
  end
end
