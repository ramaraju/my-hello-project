class Search016b < TestCase
  def setup
    App.dismiss_alerts
    App.set_stage
  end
  def run
    ## check no results are found
    search = App.goto_search
    search.criteria.all_genres.tap
    search.search_keyword('auto')
    search_results = SearchResults.wait
    App.wait_progress_gone
    first_item_name = search_results.cell_info(1, :item_name)
    expect(first_item_name).not_to start_with('[PR]')
    expect(first_item_name).to_not be_nil
  end
end
