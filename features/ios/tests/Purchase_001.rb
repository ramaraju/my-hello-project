class Purchase001 < TestCase
  include SearchUtils
  def setup
    App.dismiss_alerts
    App.set_stage
  end
  def run(item)
    user = USERS.search(user: 'userone')
    item = ITEMS.search(item: item)
    App.login(user)
    goto_item_page(item)
    cart = ItemPage.new(item).cart_button.scroll_to.tap!
    step1 = cart.checkout_button.scroll_to.tap!
    step1.login(user)
    sleep(10)
  end
end
