class Search015 < TestCase
  def setup
    App.dismiss_alerts
    App.set_stage
  end
  def run(sort, item1, item2, item3, item4, item5)
    item = []
    item[0] = ITEMS.search(item: item1)
    item[1] = ITEMS.search(item: item2)
    item[2] = ITEMS.search(item: item3)
    item[3] = ITEMS.search(item: item4)
    item[4] = ITEMS.search(item: item5)
    search = App.goto_search
    search.search_keyword('cala_sort')
    search_results = SearchResults.new
    App.wait_progress_gone
    ## for each sort type, check the items are properly sorted
    search_results.set_sort(sort.to_sym)
    App.wait_progress_gone
    calabash.scroll_to_collection_view_item(4, 0)
    sleep 1
    for i in 0..4
      raise "item#{i} is not at the right position" if
          calabash.query('all UICollectionViewCell label {text beginswith "cala_sort"}', :text)[i] != item[i].item_name
    end
  end
end
