class Search008 < TestCase
  def setup
    App.dismiss_alerts
    App.set_stage
  end
  def run
    item = ITEMS.search(item: 'max_price')
    ## check can get results with widest range
    search = App.goto_search
    search.set_price_range(0, 999999999)
    search.search_keyword('cala')
    SearchResults.wait
    ## check filter is properly applied to search
    search = App.goto_search
    search.set_price_range(999999998, 999999999)
    search.search_keyword('cala')
    SearchResults.wait
    View.new(marked: item.item_name).wait(timeout_message: "#{item.item_name} not displayed")
    results_number = calabash.query('RIGridCollectionView', numberOfItemsInSection: 0)
    raise "#{item.item_name} is not displayed" unless View.new(marked: item.item_name).exist?
    raise 'only the max price item should be displayed' unless results_number[0].to_i == 1
    ## an error message is raised if max amount < min amount
    search = App.goto_search
    search.set_price_range(100, 50)
    View.new(marked: '価格帯の範囲指定に誤りがあります').wait(timeout_message: '価格帯の範囲指定に誤りがあります not displayed')
    search.ok_button.tap
    search.back_button.tap
    Search.wait
  end
end
