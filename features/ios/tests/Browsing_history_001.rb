class BrowsingHistory001 < TestCase
  def setup
    App.dismiss_alerts
    App.set_stage
  end
  def run
    user = USERS.search(user: 'userone')
    App.logout
    home = App.goto_home
    home.browsing_button.tap!
    View.new(marked: 'ログインすると、閲覧履歴が表示されます。').wait(timeout_message: 'ログインすると、閲覧履歴が表示されます。not displayed')
    ## View browsing history from More menu
    menu = App.goto_menu
    menu.options.browsing_history.tap_target = Login.new(Menu)
    menu.options.browsing_history.tap!.login(user)
    BrowsingHistory.wait
    ## View browsing history from rakuten service menu
    App.logout
    rakuten_service = Menu::MemberService.wait
    rakuten_service.options.browsing_history.tap_target = Login.new(Menu)
    rakuten_service.options.browsing_history.tap!.login(user)
    BrowsingHistory.wait
  end
end
