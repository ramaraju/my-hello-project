class Search011 < TestCase
  include RSpec::Matchers
  def setup
    App.dismiss_alerts
    App.set_stage
  end
  def run
    item1 = ITEMS.search(item: '通常商品(項目選択肢別がない) 51')
    item2 = ITEMS.search(item: '通常商品(項目選択肢別がない) 52')
    ## check only the 2 asuraku items are displayed
    search = App.goto_search
    search.set_asuraku('愛媛県')
    search.search_keyword('cala')
    search_results = SearchResults.new
    View.new(marked: item1.item_name).wait(timeout_message: "#{item1.item_name} not displayed")
    View.new(marked: item2.item_name).wait(timeout_message: "#{item2.item_name} not displayed")
    results_number = search_results.results_number
    expect(results_number[0].to_i).to eq(2)
    search = search_results.back_button.tap
    ## after clearing the filters, check the umber of results has increased
    search.clear_all_filters
    search.search_keyword('cala') # go to search results
    App.wait_progress_gone
    results_number = search_results.results_number
    expect(results_number[0].to_i).to be > 2
    ## check the selected area comes up first of the list
    search_results = SearchResults.new
    search = search_results.back_button.tap
    search.criteria.asuraku.tap
    expect(calabash.query('UITableViewCell index: 0').first['text']).to eq('愛媛県')
  end
end
