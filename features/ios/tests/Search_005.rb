require_relative '../utils/search_utils'

class Search005 < TestCase
  include SearchUtils
  def setup
    App.dismiss_alerts
    App.set_stage
  end
  def run
    item = ITEMS.search(item: 'regular_1')
    search_results = search(item)
    ## select list display
    while calabash.query('RIResultsListCollectionViewCell', visible: 1).length.to_i == 0 && search_results.layout_button.tap
    end
    search_results.layout_button.tap
    item_number_3x3 = calabash.query('RISearchResultsGridCollectionViewCell', visible: 1).length.to_i
    search_results.layout_button.tap
    item_number_2x2 = calabash.query('RISearchResultsGridCollectionViewCell', visible: 1).length.to_i
    search_results.layout_button.tap
    item_number_1x1 = calabash.query('RISearchResultsGridCollectionViewCell', visible: 1).length.to_i
    expect(item_number_3x3).to be > item_number_2x2
    expect(item_number_2x2).to be > item_number_1x1
    ## select list display
    while calabash.query('RIResultsListCollectionViewCell', visible: 1).length.to_i == 0 && search_results.layout_button.tap
    end
  end
end
