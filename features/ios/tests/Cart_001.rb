class Cart001 < TestCase
  def setup
    App.dismiss_alerts
    App.set_stage
  end
  def run
    item = ITEMS.search(item: 'CPC_01')
    ## check no results are found
    search = App.goto_search
    search.search_keyword('cpc')
    SearchResults.wait
    item_cell = View.new(marked: item.list_display_name)
    item_cell.wait(timeout_message: "#{item.list_display_name} not displayed")
    item_cell.tap
    ItemPage.new(item).wait
  end
end
