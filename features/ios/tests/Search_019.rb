class Search019 < TestCase
  include RSpec::Matchers
  def setup
    App.dismiss_alerts
    App.set_stage
  end
  def run
    search = App.open_search_detail
    search_history = search.search_history_button.tap
    search_history.clear_history
    search = search_history.search_detail_button.tap
    ## make a first search with some conditions
    search.set_price_range(10, 100)
    search.set_exclude_keyword('yo')
    search.search_keyword('wam')
    search_results = SearchResults.wait
    ## make a second search with new conditions
    search_results.back_button.tap
    search.set_price_range(10, 500)
    search.search_keyword('uil')
    search_results = SearchResults.wait
    search_results.back_button.tap
    ## redo the same search
    search.search_keyword('uil')
    search_results = SearchResults.wait
    search_results.back_button.tap
    search_history = search_results.search_history_button.tap
    ## check the search has been added to search history
    first_search_item = calabash.query('RISuggestedKeywordCell index:0 UILabel index:0').first['text']
    first_search_criteria = calabash.query('RISuggestedKeywordCell index:0 UILabel index:1').first['text']
    second_search_item = calabash.query('RISuggestedKeywordCell index:1 UILabel index:0').first['text']
    second_search_criteria = calabash.query('RISuggestedKeywordCell index:1 UILabel index:1').first['text']
    expect(first_search_item).to eq('uil')
    expect(first_search_criteria).to eq('標準/10円 〜 500円/在庫あり・注文可能/yo')
    expect(second_search_item).to eq('wam')
    expect(second_search_criteria).to eq('標準/10円 〜 100円/在庫あり・注文可能/yo')
    expect(calabash.query('RISuggestedKeywordCell').length).to eq(2)
    ## check filters are saved when you go back to the page
    search = search_history.search_detail_button.tap
    expect(search.filter_status(:price_range)).to eq('10円 〜 500円')
    expect(search.filter_status(:exclude_keyword)).to eq('yo')
    ## check history can be cleared
    search_history = search.search_history_button.tap
    search_history.clear_history
    expect(calabash.query('RISuggestedKeywordCell').length).to eq(0)
  end
end
