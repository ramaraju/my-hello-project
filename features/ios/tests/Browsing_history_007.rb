require_relative '../utils/search_utils'
require_relative '../utils/home_utils'
require_relative '../utils/browsing_history_utils'

class BrowsingHistory007 < TestCase
  include SearchUtils, BrowsingHistoryUtils, HomeUtils
  def setup
    App.dismiss_alerts
  end
  def run(item)
    user = USERS.search(user: 'default')
    item = ITEMS.search(item: item)
    App.login(user)
    goto_item_page(item)
    check_browsing_on_home_first_item(item)
    check_browsing_history_first_item(item)
  end
end
