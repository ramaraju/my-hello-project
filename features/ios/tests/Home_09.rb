class Home09 < TestCase
  def setup
    App.dismiss_alerts
    App.set_stage
    App.logout
  end
  def run
    user = USERS.search(user: 'userone')
    member_service = App.goto_menu.options.member_service.tap!
    member_service.login(user)
    expect(member_service.user_info.wait.text).to eq("#{user.last_name} #{user.first_name} さん\nあなたは#{user.rank}会員")
    expect(member_service.total_points.text).to eq(user.total_points.to_s)
    expect(member_service.time_limit_points.text).to eq(user.time_limit_points.to_s)
    expect(member_service.future_points.text).to eq(user.future_points.to_s)
  end
end
