class Search016g < TestCase
  def setup
    App.dismiss_alerts
    App.set_stage
  end
  def run
    ## check no results are found
    shop = SHOPS.search(shop: 'cpc_honten')
    search = App.goto_search
    search.search_keyword(shop.shop_name)
    search_results = SearchResults.wait
    search_results.item_by_index(1).tap
    item_page = ItemPage.wait
    item_page.add_search_shop_label.scroll_to.tap
    search = App.goto_search
    search.set_search_shop(shop)
    search.search_keyword('cpc')
    search_results.genre_button.tap
    Label.new(text: 'CD・DVD・楽器').scroll_to.tap
    Label.new(text: 'CD・DVD・楽器に決定する').scroll_to.tap
    search_results = SearchResults.wait
    App.wait_progress_gone
    first_item_name = search_results.cell_info(1, :item_name)
    expect(first_item_name).not_to start_with('[PR]')
  end
end
