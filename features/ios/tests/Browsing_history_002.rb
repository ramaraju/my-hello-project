class BrowsingHistory002 < TestCase
  def setup
    App.dismiss_alerts
    App.set_stage
  end
  def run
    user = USERS.search(user: 'userone')
    App.login(user)
    App.clear_browsing
    home = App.goto_home
    home.browsing_button.tap!
    View.new(marked: '閲覧した商品の履歴が表示されます。').wait(timeout_message: '閲覧した商品の履歴が表示されます。not displayed')
  end
end
