require_relative '../utils/search_utils'
require_relative '../utils/home_utils'
require_relative '../utils/browsing_history_utils'

class BrowsingHistory006 < TestCase
  include HomeUtils, SearchUtils, BrowsingHistoryUtils
  def setup
    App.dismiss_alerts
    App.set_stage
  end
  def run
    user = USERS.search(user: 'default')
    item1 = ITEMS.search(item: 'regular_1')
    item2 = ITEMS.search(item: 'regular_2')
    App.login(user)
    search_result = search(item1)
    search_result.item(item1).tap ## go to item page
    check_browsing_on_home_first_item(item1)
    check_browsing_history_first_item(item1)
    search_result = search(item2)
    search_result.item(item2).tap ## go to item page
    check_browsing_on_home_first_item(item2)
    check_browsing_history_first_item(item2)
  end
end
