require_relative '../utils/search_utils'

class Home05 < TestCase
  include SearchUtils
  def setup
    App.dismiss_alerts
    App.set_stage
  end
  def run(item)
    user = USERS.search(user: 'cala_f_12')
    item = ITEMS.search(item: item)
    App.login(user)
    App.clear_bookmarks
    item_detail = search(item).item(item).tap!
    item_detail.bookmark.tap
    bookmarks = App.goto_home.bookmarks_button.tap!
    item_detail = bookmarks.tap_item(item)
    item_detail.assert_item_info(item)
    bookmarks = item_detail.go_back(Home::TabList.new(:bookmarks))
    bookmarks_list = bookmarks.list_detail.tap!
    item_detail = bookmarks_list.item(item).tap!
    item_detail.assert_item_info(item)
  end
end
