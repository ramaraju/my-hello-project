require_relative '../utils/search_utils'

class Search004 < TestCase
  include SearchUtils
  def setup
    App.dismiss_alerts
    App.set_stage
  end
  def run
    item = ITEMS.search(item: '最安ショップを見る')
    search_results = search(item)
    search_results.cheapest_shop_button.tap
    WebViewPage.new('グリーンハウス GREEN HOUSE GH-CA-IPOD5NSK | 価格比較 - 商品価格ナビ').wait
  end
end
