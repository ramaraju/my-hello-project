class Search01 < TestCase
  def setup
    App.dismiss_alerts
    App.set_stage
  end
  def run
    # maybe can use backdoor to generate history
    # otherwise the test will most likely take longer than 10 min
    1.upto(90) { |i|
      home = App.goto_home
      search = home.search_bar.tap!
      search.text_field.text = '%03d' % i
      calabash.tap_keyboard_action_key
      calabash.wait_for_animations
    }
    home = App.goto_home
    expect {
      search = home.search_bar.tap!
      search.cancel_button.tap
    }.to perform_under(5).and_sample(10)
  end
end
