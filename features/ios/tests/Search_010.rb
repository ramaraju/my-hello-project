class Search010 < TestCase
  def setup
    App.dismiss_alerts
    App.set_stage
  end
  def run
    item1 = ITEMS.search(item: 'keyword_1')
    item2 = ITEMS.search(item: 'keyword_2')
    shop1 = SHOPS.search(shop: item1.shop)
    search = App.goto_search
    search.search_keyword('my_automation_keyword') # goto search results
    View.new(marked: item1.item_name).wait(timeout_message: "#{item1.item_name} not displayed")
    View.new(marked: item2.item_name).wait(timeout_message: "#{item2.item_name} not displayed")
    ## add shop to you favorites
    View.new(marked: item1.item_name).tap
    item_page = ItemPage.new(item1)
    item_page.add_search_shop_label.scroll_to.tap
    ## check items are filtered
    search = App.goto_search
    search.set_search_shop(shop1)
    search.search_keyword('my_automation_keyword') # goto search results
    View.new(marked: item1.item_name).wait(timeout_message: "#{item1.item_name} not displayed")
    expect(View.new(marked: item2.item_name).exist?).to be false
    search = SearchResults.new.back_button.tap
    expect(search.filter_status(:shop)).to eq(shop1.shop_name)
    search.clear_all_filters
    expect(search.filter_status(:shop)).to eq('指定なし')
  end
end
