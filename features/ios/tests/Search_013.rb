class Search013 < TestCase
  def setup
    App.dismiss_alerts
    App.set_stage
  end
  def run(item1, item2, filter)
    item1 = ITEMS.search(item: item1)
    item2 = ITEMS.search(item: item2)
    ## check 2 results are displayed (with and without free shipping)
    search = App.goto_search
    expect(search.filter_status(filter.to_sym)).to be false
    search.search_keyword(item1.cpc_search_keyword)
    search_results = SearchResults.new
    App.wait_progress_gone
    results_number = search_results.results_number
    expect(results_number[0].to_i).to eq(2)
    View.new(marked: item1.item_name).wait(timeout_message: "#{item1.item_name} not displayed")
    View.new(marked: item2.item_name).wait(timeout_message: "#{item2.item_name} not displayed")
    search = search_results.back_button.tap
    ## check only 1 result is displayed (with free shipping)
    search.set_filter(filter.to_sym, true)
    search.search_keyword(item1.cpc_search_keyword)
    View.new(marked: item1.item_name).wait(timeout_message: "#{item1.item_name} not displayed")
    expect(View.new(marked: item2.item_name).exist?).to be false
    App.wait_progress_gone
    results_number = search_results.results_number
    expect(results_number[0].to_i).to eq(1)
  end
end
