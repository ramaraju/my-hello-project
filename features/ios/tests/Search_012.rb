class Search012 < TestCase
  def setup
    App.dismiss_alerts
    App.set_stage
  end
  def run
    item = ITEMS.search(item: '通常商品(在庫なし) 37')
    ## check no results are found
    search = App.goto_search
    expect(search.filter_status(:in_stock)).to be true
    search.search_keyword(item.item_name)
    search_results = SearchResults.new
    results_number = search_results.results_number
    expect(results_number[0].to_i).to eq(0)
    search = search_results.back_button.tap
    ## after setting in stock to false, check a result can be found
    search.set_filter(:in_stock, false)
    search.search_keyword(item.item_name)
    View.new(marked: '在庫なし').wait(timeout_message: '在庫なし not displayed')
    results_number = search_results.results_number
    expect(results_number[0].to_i).to eq(1)
  end
end
