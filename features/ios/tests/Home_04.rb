class Home04 < TestCase
  def setup
    App.dismiss_alerts
    App.set_stage
    App.clear_search_history
  end
  def run
    home = App.goto_home
    search = home.search_bar.tap!
    search.text_field.enter_text('canon')
    search_result = search.suggest_label('canon', 'カメラ・光学機器').tap!
    expect(search_result.search_text_field.text).to eq('canon')
    search_result.label('カメラ・光学機器').wait(timeout_message: 'current genre is not correct')
    search_detail = App.open_search_detail
    expect(search_detail.search_text_field.text).to eq('canon')
    search_detail.label('カメラ・光学機器').wait(timeout_message: 'current genre is not correct')
  end
end
