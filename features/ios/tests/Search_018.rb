class Search018 < TestCase
  def setup
    App.dismiss_alerts
    App.set_stage
  end
  def run
    ## check no results are found
    search = App.goto_search
    search.apply_criteria_button.tap
    View.new(marked: '検索キーワードまたはジャンルを指定してください').wait(timeout_message: '検索キーワードまたはジャンルを指定してください not displayed')
    search.ok_button.tap
    search.apply_criteria_button.tap
    View.new(marked: '検索キーワードまたはジャンルを指定してください').wait(timeout_message: '検索キーワードまたはジャンルを指定してください not displayed')
  end
end
