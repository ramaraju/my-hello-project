class Search016f < TestCase
  def setup
    App.dismiss_alerts
    App.set_stage
  end
  def run
    ## check no results are found
    search = App.goto_search
    search.set_asuraku('東京都')
    search.search_keyword('auto')
    search_results = SearchResults.wait
    search_results.genre_button.tap
    Label.new(text: 'CD・DVD・楽器').scroll_to.tap
    Label.new(text: 'CD・DVD・楽器に決定する').scroll_to.tap
    search_results = SearchResults.wait
    App.wait_progress_gone
    first_item_name = search_results.cell_info(1, :item_name)
    expect(first_item_name).not_to start_with('[PR]')
  end
end
