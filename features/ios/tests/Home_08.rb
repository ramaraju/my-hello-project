class Home08 < TestCase
  def setup
    App.dismiss_alerts
    App.set_stage
  end
  def run
    user = USERS.search(user: 'userone')
    App.login(user)
    setting = App.goto_menu.options.setting.tap!
    login_status = setting.login_status.tap!
    login_status.save.tap
    home = App.restart
    expect(home.user_name).to eq('useroneさん')
    setting = App.goto_menu.options.setting.tap!
    login_status = setting.login_status.tap!
    login_status.not_save.tap
    home = App.restart
    expect(home.user_name).to be_nil
    setting = App.goto_menu.options.setting.tap!
    alert = setting.notice.tap!
    expect(alert.title).to eq('楽天市場からのお知らせを受け取りますか？')
  end
end
