require_relative '../utils/search_utils'

class Search003 < TestCase
  include SearchUtils
  def setup
    App.dismiss_alerts
    App.set_stage
  end
  def run
    item1 = ITEMS.search(item: 'regular_1')
    item2 = ITEMS.search(item: 'regular_2')
    item1_label = Label.new(marked: item1.item_name)
    item2_label = Label.new(marked: item2.item_name)
    App.clear_search_history
    ## check no suggestions list is displayed when the search history is empty
    search = App.open_search_detail
    search.search_bar.tap
    expect(search.suggestions_list).not_to be_visible
    ## check suggestions list is displayed if at least
    search.search_text_field.enter_text('a')
    expect(search.suggestions_list).to be_visible
    search.cancel_button.tap
    ## check search history shows up automatically after tapping the bar
    search(item1)
    search = App.open_search_detail
    search.search_bar.tap
    expect(item1_label).to be_visible
    search.cancel_button.tap
    ## check search history with multiple items shows up automatically after tapping the bar
    search(item2)
    search = App.open_search_detail
    search.search_bar.tap
    expect(item1_label).to be_visible
    expect(item2_label).to be_visible
    item2_label.tap
    search_results = SearchResults.wait
    search_results.item_by_index(1).tap
    ItemPage.new(item2).wait
  end
end
