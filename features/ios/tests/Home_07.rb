class Home07 < TestCase
  def setup
    App.dismiss_alerts
    App.set_stage
  end
  def run
    user = USERS.search(user: 'cala_f_12')
    App.login(user)
    Home.new.recommend_button.tap
  end
end
