class Search009 < TestCase
  include RSpec::Matchers
  def setup
    App.dismiss_alerts
    App.set_stage
  end
  def run
    item1 = ITEMS.search(item: 'keyword_1')
    item2 = ITEMS.search(item: 'keyword_2')
    search = App.goto_search
    search.search_keyword('my_automation_keyword')
    View.new(marked: item1.item_name).wait(timeout_message: "#{item1.item_name} not displayed")
    View.new(marked: item2.item_name).wait(timeout_message: "#{item2.item_name} not displayed")
    ## check the results are filtered according to the excluded keyword
    search = App.goto_search
    search.set_exclude_keyword('01')
    search.search_keyword('my_automation_keyword')
    View.new(marked: item2.item_name).wait(timeout_message: "#{item2.item_name} not displayed")
    expect(View.new(marked: item1.item_name).exist?).to be false
    ## check 128 characters keyword can be input
    search = App.goto_search
    search.set_exclude_keyword('vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv')
    search = Search.wait
    ## an error pops up when the keyword is 129 characters long
    search.criteria.exclude_keyword.tap
    search.exclude_keyword.keyword_field.enter_text('c')
    search.exclude_keyword.validate_button.tap!
    View.new(marked: '除外キーワードは128文字以内で指定してください').wait(timeout_message: '除外キーワードは128文字以内で指定してください。not displayed')
    search.ok_button.tap
    search.back_button.tap
    Search.wait
  end
end
