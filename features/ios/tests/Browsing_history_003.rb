require_relative '../utils/home_utils'
require_relative '../utils/browsing_history_utils'

class BrowsingHistory003 < TestCase
  include HomeUtils, BrowsingHistoryUtils
  def setup
    App.dismiss_alerts
    App.set_stage
  end
  def run
    user = USERS.search(user: 'history_90')
    App.login(user)
    check_browsing_history_item_quantity
  end
end
