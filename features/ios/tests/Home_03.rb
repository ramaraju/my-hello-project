class Home03 < TestCase
  def setup
    App.dismiss_alerts
    App.set_stage
  end
  def run
    home = App.goto_home
    genres = home.genres_button.tap!
    genre_top = genres.genre('メンズファッション').tap!
    search_result = genre_top.subgenre_table.all_items.tap!
    search_result.label('メンズファッション').wait(timeout_message: 'current genre is not correct')
    search_result.pr_item.wait(timeout_message: 'PR item does not exist')
  end
end
