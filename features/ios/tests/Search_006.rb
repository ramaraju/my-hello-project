class Search006 < TestCase
  include RSpec::Matchers
  def setup
    App.dismiss_alerts
    App.set_stage
  end
  def run
    search = App.open_search_detail
    ## set filters for search
    search.set_price_range(10, 100)
    search.set_exclude_keyword('yo')
    search.set_asuraku('岩手県')
    search.search_keyword('wam')
    search_results = SearchResults.wait
    ## check filters are saved when you go back to the page
    search = search_results.back_button.tap
    expect(search.filter_status(:price_range)).to eq('10円 〜 100円')
    expect(search.filter_status(:exclude_keyword)).to eq('yo')
    expect(search.filter_status(:asuraku)).to eq('岩手県')
    search.clear_all_filters
    ## set filters for search
    search.set_filter(:in_stock, true)
    search.set_filter(:free_shipping, true)
    search.set_filter(:card_ok, true)
    search.search_keyword('wam')
    search_results = SearchResults.wait
    ## check filters are saved when you go back to the page
    search = search_results.back_button.tap
    expect(search.filter_status(:price_range)).to eq('指定なし')
    expect(search.filter_status(:exclude_keyword)).to eq('指定なし')
    expect(search.filter_status(:asuraku)).to eq('指定なし')
    expect(search.filter_status(:in_stock)).to be true
    expect(search.filter_status(:free_shipping)).to be true
    expect(search.filter_status(:card_ok)).to be true
  end
end
