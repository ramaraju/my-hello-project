class Home01 < TestCase
  def setup
    App.dismiss_alerts
    App.set_stage
  end
  def run
    @user = USERS.search(user: 'userone')
    App.logout
    home = App.goto_home
    home.points_button.exist! && home.coupon_button.not_exist! && home.info_button.exist!
    App.login(@user)
    home.points_button.exist! && home.coupon_button.exist! && home.info_button.exist!
    home.points_button.tap!
  end
end
