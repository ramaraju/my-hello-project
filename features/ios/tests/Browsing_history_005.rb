class BrowsingHistory005 < TestCase
  include HomeUtils, SearchUtils, BrowsingHistoryUtils
  def setup
    App.dismiss_alerts
    App.set_stage
  end
  def run
    App.clear_browsing
    check_browsing_history_item_quantity
  end
end
