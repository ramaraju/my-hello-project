require_relative '../utils/search_utils'

class Home06 < TestCase
  include SearchUtils
  def setup
    App.dismiss_alerts
    App.set_stage
  end
  def run(item)
    user = USERS.search(user: 'cala_f_12')
    item = ITEMS.search(item: item)
    App.login(user)
    App.clear_browsing
    search(item).item(item).tap! # go to item page
    browsing = App.goto_home.browsing_button.tap!
    item_detail = browsing.tap_item(item.item_name)
    item_detail.assert_item_info(item)
    browsing = item_detail.go_back(Home::TabList.new(:browsing_history))
    bookmarks_list = browsing.list_detail.tap!
    item_detail = bookmarks_list.item(item.item_name).tap!
    item_detail.assert_item_info(item)
  end
end
