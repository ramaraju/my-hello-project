class Bookmarks006 < TestCase
  def setup
    App.dismiss_alerts
  end
  def run(user, item)
    user = USERS.search(user: user)
    item = ITEMS.search(item: item)
    shop = SHOPS.search(shop: item.shop)
    App.login(user)
    bookmarks = App.goto_bookmarks
    bookmark = bookmarks.bookmark(item)
    bookmark.scroll_to
    expect(bookmark.shop_label.text).to eq(shop.shop_name)
    item_page = bookmark.tap!
    item_page.name_label.scroll_to
    if item.web_view
      item_page_web = item_page.details_label.scroll_to.tap
      item_page_web.periodic_order.scroll_to
=begin
      item.order_button_string.each do |order_type_button|
        Label.new(marked: order_type_button).scroll_to
      end
=end
    end
  end
end
