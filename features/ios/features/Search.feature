Feature: Search
@ios @complete
  Scenario: Large history
    Given Search_01
@ios @complete @smoke
Scenario: Check suggestions list from Home
  Given Search_002
@ios @complete
Scenario: Check search history and suggestions list in search tab
  Given Search_003
@ios @complete
Scenario: Check "最安ショップを見る" link can be tapped
  Given Search_004
@ios @complete
Scenario: Check you can change search results layout
  Given Search_005
@ios @complete
Scenario: Check search criteria can be saved and cleared
  Given Search_006
@ios @complete
Scenario: Check search price range criteria
  Given Search_008
@ios @complete
Scenario: Check search exclude key criteria
  Given Search_009
@ios @complete
Scenario: Check search shop criteria
  Given Search_010
@ios @complete
Scenario: Check search asuraku criteria
  Given Search_011
@ios @complete
Scenario: Check search in_stock criteria
  Given Search_012
@ios @complete
Scenario Outline: Check search shipping fee and card criteria
  Given Search_013(<item1>,<item2>,<filter>)

  Examples:
  |item1            |item2          |filter        |
  | 通常商品 送料込   |通常商品 送料別  |free_shipping|
  | 通常商品 カードOK |通常商品 カードNG|card_ok|
@ios @complete
Scenario Outline: Check sort
  Given Search_015(<sort>, <item1>, <item2>, <item3>, <item4>, <item5>)

  Examples:
    |sort           |item1                        |item2                       |item3                      |item4                      |item5                      |
    | default       | 通常商品(項目選択肢別がない) 38 |通常商品(項目選択肢別がない) 40|通常商品(項目選択肢別がない) 42|通常商品(項目選択肢別がない) 39|通常商品(項目選択肢別がない) 41|
    | price_low     | 通常商品(項目選択肢別がない) 42 |通常商品(項目選択肢別がない) 41|通常商品(項目選択肢別がない) 40|通常商品(項目選択肢別がない) 39|通常商品(項目選択肢別がない) 38|
    | price_high    | 通常商品(項目選択肢別がない) 38 |通常商品(項目選択肢別がない) 39|通常商品(項目選択肢別がない) 40|通常商品(項目選択肢別がない) 41|通常商品(項目選択肢別がない) 42|
    | newest           | 通常商品(項目選択肢別がない) 38 |通常商品(項目選択肢別がない) 40|通常商品(項目選択肢別がない) 42|通常商品(項目選択肢別がない) 39|通常商品(項目選択肢別がない) 41|
    | review_number | 通常商品(項目選択肢別がない) 40 |通常商品(項目選択肢別がない) 42|通常商品(項目選択肢別がない) 38|通常商品(項目選択肢別がない) 39|通常商品(項目選択肢別がない) 41|
    | review_high   | 通常商品(項目選択肢別がない) 41 |通常商品(項目選択肢別がない) 39|通常商品(項目選択肢別がない) 40|通常商品(項目選択肢別がない) 42|通常商品(項目選択肢別がない) 38|
@ios @complete
Scenario: Check cpc search
  Given Search_016a
@ios @complete
Scenario: Check cpc search
  Given Search_016b
@ios
Scenario Outline: Check cpc search
  Given Search_016c(<category1>, <category2>, <item_type>)

    Examples:
    |category1     |category2   |item_type |
    |CD・DVD・楽器  |DVD         |cpc       |
    |ダイエット・健康|サプリメント  |regular   |
@ios @complete
Scenario Outline: Check cpc search
  Given Search_016d(<keyword>, <category>, <item_type>)

    Examples:
    |category     |item_type |
    |CD・DVD・楽器 |cpc       |
    |ビール・洋酒   |regular   |
@ios @complete
Scenario: Check cpc search
  Given Search_016e
@ios @complete
Scenario: Check cpc search
  Given Search_016f
@ios @complete
Scenario: Check cpc search
  Given Search_016g
@ios @complete
Scenario: Check error message when no search keyword is entered
  Given Search_018
@ios @complete
Scenario: Check search history can be saved and criteria are kept even after navigating to other screens
  Given Search_019