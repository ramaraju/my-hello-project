@ios @wip
Feature: Bookmarks
  Scenario Outline: Verify different items can be displayed from bookmarks
    Given Bookmarks_006(<user>,<item>)

  Examples:
    |user     |item|
    |cala_f_79| 通常商品(項目選択肢別がない) 31 |
    |cala_f_79| 通常商品(項目選択肢別があり) 32 |
    |cala_f_79| 通常商品(期間限定商品) 33 |
    |cala_f_79| 通常商品(あす楽商品) 34 |
    |cala_f_79| 通常商品(酒類) 35 |
    |cala_f_79| 通常商品(藥類) 36 |
    |cala_f_79| 通常商品(在庫なし) 37 |
    |cala_f_79| CPC_定期購入商品(通常ジャンル) 1 |
    |cala_f_79| CPC_頒布会商品(通常ジャンル) 2 |
    |cala_f_79| CPC_予約商品(通常ジャンル) 3 |
    |cala_f_79| CPC_通常＆定期購入商品(通常ジャンル) 4 |
    |cala_f_79| CPC_定期購入商品(酒/薬事ジャンル) 5 |
    |cala_f_79| CPC_頒布会商品(酒/薬事ジャンル) 6 |
    |cala_f_79| CPC_予約商品(酒/薬事ジャンル) 7 |
    |cala_f_79| CPC_通常＆定期購入商品(酒/薬事ジャンル) 8 |
    |bookmark_time_sale| time_sale_expired|
    |bookmark_time_sale| time_sale_current|
    |bookmark_time_sale| time_sale_future|

