@ios @complete
Feature: Browsing_history
  Scenario: Access browsing history from Home and More menu
    Given Browsing_history_001
@ios @complete
  Scenario: Check empty browsing history from Home. User is logged in
    Given Browsing_history_002
@ios @complete
  Scenario: Check browsing history from Home. User is logged in and more than 90 bookmarks
    Given Browsing_history_003
@ios @complete @smoke
  Scenario: Check browsing history from Home. User is logged in, searching items
    Given Browsing_history_004
@ios @complete
  Scenario: Check browsing history from Home. User is logged in, browsing history is empty
    Given Browsing_history_005
@ios @complete
  Scenario: Check last browsed item is on top on the list. Check from home, more menu and member's page
    Given Browsing_history_006
@ios @complete
  Scenario Outline: Check all types of items can be seen in the browsing history
    Given Browsing_history_007(<item>)

  Examples:
    |item|
    | 通常商品(項目選択肢別がない) 31 |
    | 通常商品(項目選択肢別があり) 32 |
    | 通常商品(期間限定商品) 33 |
    | 通常商品(あす楽商品) 34 |
    | 通常商品(酒類) 35 |
    | 通常商品(藥類) 36 |
    | 通常商品(在庫なし) 37 |
    | CPC_定期購入商品(通常ジャンル) 1 |
    | CPC_頒布会商品(通常ジャンル) 2 |
    | CPC_予約商品(通常ジャンル) 3 |
    | CPC_通常＆定期購入商品(通常ジャンル) 4 |
    | CPC_定期購入商品(酒/薬事ジャンル) 5 |
    | CPC_頒布会商品(酒/薬事ジャンル) 6 |
    | CPC_予約商品(酒/薬事ジャンル) 7 |
    | CPC_通常＆定期購入商品(酒/薬事ジャンル) 8 |