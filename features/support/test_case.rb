require 'goya'
require 'rspec/expectations'
require 'rspec-benchmark'

Goya.project_name = 'Japan Ichiba'

if ENV['XAMARIN_TEST_CLOUD'] != '1'
  Goya.screenshot_path = File.expand_path('../../../screenshots', __FILE__)
  Goya.delete_screenshots
end

class TestCase
  include RSpec::Benchmark::Matchers
  include RSpec::Matchers
  def screenshot(title)
    calabash.screenshot(title)
  end
  def load_data(name = nil)
    name ||= self.class.to_s
    fname = "features/#{Device.platform}/data/#{name}.yml"
    return YAML.load(File.open(fname)) if File.exist?(fname)
    fname = "features/data/#{name}.yml"
    return YAML.load(File.open(fname)) if File.exist?(fname)
    raise "data not found: #{name}"
  end
  def find_user(pattern, enum)
    user = enum.map { |i| pattern % i }.find { |u|
      calabash.request_resource("user:#{u}")
    }
    expect(user).to_not be_nil
    USERS.search(user: user)
  end
end
