#!/usr/bin/env ruby
# This script should be executed from the project root directory.
require 'term/ansicolor'
require 'openstf/client'
include Term::ANSIColor
def shell(cmd)
  puts cmd.yellow
  system(cmd) || exit($?.exitstatus)
end
def error(msg)
  puts msg.red
  exit(1)
end
apps = File.expand_path('apps', Dir.pwd)
platform = (ARGV.shift || 'auto').to_sym
case platform
when :android
  ext = 'apk'
when :ios
  error('Not supported')
else
  puts 'Usage: bin/lab ios|android [app-path]'
  exit!(1)
end
app_path = ARGV.shift || Dir.glob("#{apps}/*.#{ext}").sort_by { |f| File.mtime(f) }.last
error('Could not find app file') if app_path.nil? || !File.exist?(app_path)
shell("calabash-android resign #{app_path}")
shell("calabash-android build #{app_path}")
stf = OpenSTF::Client.init(host: ENV['STF_HOST'], token: ENV['STF_TOKEN'])
device = ENV['STF_DEVICE']
uri = nil
begin
  uri = stf.connect_device(serial: device)
  at_exit {
    stf.disconnect_device(serial: device)
  }
rescue RuntimeError
  error("Device not available: #{device}")
end
shell("adb connect #{uri}")
shell("adb -s #{uri} shell pm list packages | grep rakuten | dos2unix | sed s/package:// | xargs -n1 adb -s #{uri} shell pm uninstall")
shell("bundle exec calabash-android run #{app_path} features/features/24h.feature -b -p android --format junit --out ./ --format json -o report.json --format html -o report.html --format rerun -o rerun.txt --format pretty")
