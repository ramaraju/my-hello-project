require 'open-uri'
require 'net/http'
require 'uri'
require 'mail'
require 'net/smtp'

load 'xamarin-api.rb'

apiKey = '81802507f1629c9f9706e4d5a0c5d3f0'
androidAppId = '5a45ea96-c8dc-44d2-8cd4-410889df1a3f'


options = { :address              => "smtp.gmail.com",
            :port                 => 587,
            :domain               => 'outlook.gmail.com',
            :user_name            => 'maheshs039s',
            :password             => 'smahesha05p1330',
            :authentication       => 'plain',
            :enable_starttls_auto => true  }

Mail.defaults do
  delivery_method :smtp, options
end

def open(url)
  Net::HTTP.get(URI.parse(url))
end



#Initializing the test_cloud
test_cloud = Xamarin::TestCloud::Api::V0::Client.new(apiKey)

#Get the test runs for the Android App
test_runs = test_cloud.apps.test_runs(androidAppId)

#Getting the first test run in the list
firstTestRun = test_runs.first

print("=============")
print(firstTestRun)
print("=============")

testRunId = firstTestRun.id
print("test Run ID \n")
print(testRunId)
print("\n")
result_collection = test_cloud.test_runs.results(testRunId)
failed_testNames = Array.new
failed_testLog = ""
for result in result_collection.results
  if result.status == 'failed'
    failed_testNames.insert(0, result.test_name)
  end
  if failed_testLog == ""
    page_content = open(result_collection.logs.devices.first.test_log)
    print("Device Details ==>")
    for device in result_collection.logs.devices
      print(device)
    end
    print("<=========")
    #Converting to Utf-8 decoded String
    failed_testLog = page_content.to_s.force_encoding("UTF-8")
  end
end

if failed_testNames.count > 0
  for result in failed_testNames
    print("============")
    print(result)
    print("Exact Failure for test Case : ")
    print(result)
    print("\n")
    string1 = "Scenario: " + result
    string2 = "Duration"

    messageBody = failed_testLog[/#{string1}(.*?)#{string2}/m, 1]
    print(messageBody)

    Mail.deliver do
      to "maheshs039s@gmail.com"
      from "maheshs039s@gmail.com"
      subject "MHub :: Test Failed " + result
      body messageBody
    end
  end
end
