#!/usr/bin/env ruby
# This script should be executed from the project root directory.
require 'open-uri'
require 'term/ansicolor'
require 'open-uri'
require 'net/http'
require 'uri'
require 'mail'

load 'bin/xamarin-api.rb'

include Term::ANSIColor
def shell(cmd)
  puts cmd.yellow
  system(cmd) || exit($?.exitstatus)
end
def error(msg)
  puts msg.red
  exit(1)
end

#Initial Configuration of Options for smtp connection
options = { :address              => "smtp.gmail.com",
            :port                 => 587,
            :domain               => 'outlook.gmail.com',
            :user_name            => 'maheshs039s',
            :password             => 'smahesha05p1330',
            :authentication       => 'plain',
            :enable_starttls_auto => true  }

Mail.defaults do
  delivery_method :smtp, options
end

#method for to open and read the contents in url
def open(url)
  Net::HTTP.get(URI.parse(url))
end

#user = ENV['XAMARIN_USER']
#key = ENV['XAMARIN_KEY']
user = 'shivaiah.mahesha@rakuten.com'
key = '81802507f1629c9f9706e4d5a0c5d3f0'
error('XAMARIN_USER environment variable is not set') if !user
error('XAMARIN_KEY environment variable is not set') if !key
apps = File.expand_path('apps', Dir.pwd)
platform = (ARGV.shift || 'auto').to_sym
case platform
when :android
  ext = 'apk'
  #0480619c //LG Nexus 5, Asus Google Nexus 7, Samsung galaxy S5
  #devices = 'dd1e8868'
  devices = '0480619c'
  app_name = '楽天市場'
  app_key = '5a45ea96-c8dc-44d2-8cd4-410889df1a3f'
when :ios
  ext = 'ipa'
  devices = '290c4e38'
  app_name = 'Rakuten'
  app_key = 'f39f56ba-5249-4bc4-8c66-dc20635a33a3'
else
  puts 'Usage: bin/xamarin ios|android [app-path]'
  exit!(1)
end
app_path = ARGV.shift || Dir.glob("#{apps}/*.#{ext}").sort_by { |f| File.mtime(f) }.last
error('Could not find app file') if app_path.nil? || !File.exist?(app_path)
if platform == :android
  shell("calabash-android resign #{app_path}")
  shell("calabash-android build #{app_path}")
end
shell("bundle exec test-cloud submit #{app_path} #{key} --devices #{devices} --series \"master\" --locale \"ja_JP\" --app-name \"#{app_name}\" --user #{user} --config config/cucumber.yml --profile #{platform}-24h | tee output.txt")
# exit($?.exitstatus)
# disabled until xamarin stops failing with "500 Internal Server Error"

sed = '/^(.+)https:\/\/testcloud\.xamarin\.com\/test\/.*_(.+)\/$/{s//\2/p;q;}'
run_id = `sed -En '#{sed}' output.txt`.strip
error('Could not determine run id') if run_id.empty?
require_relative 'xamarin-api'
puts 'Fetching results...'.yellow
test_cloud = Xamarin::TestCloud::Api::V0::Client.new(key)
result_collection = test_cloud.test_runs.results(run_id)
error('Test results not found') if result_collection.nil? || result_collection.results.nil?
suites = {}
result_collection.results.each { |result|
  (suites[result.test_group] ||= []) << result
}
uri = result_collection.logs.devices.first.test_log
log = []
open(uri) { |f|
  f.each_line { |line| log << line }
}
puts 'Test log:'.yellow
log.each { |line| puts line }
puts 'End of test log'.yellow
failed = false
suites.each { |suite, results|
  puts "Test suite: #{suite}"
  results.each { |result|
    puts "Test case: #{result.test_name}"
    status = result.status
    failed = true if result.status == 'failed'
    status = status.red if status == 'failed'
    status = status.green if status == 'passed'
    status = status.green if status == 'passed'
    puts "Status: #{status}"
  }
}
#exit(failed ? 1 : 0)
if failed == true
#Get the test runs for the App
  test_runs = test_cloud.apps.test_runs(app_key)

#Getting the first test run in the list
  firstTestRun = test_runs.first

  testRunId = firstTestRun.id

  for device in result_collection.logs.devices
    failed_testLog = ""
    for result in result_collection.results
      if failed_testLog == ""
        #read the testLog from the url, where xamarin has stored(Its actually in Amazon server)
        page_content = open(device.test_log)
        #Converting to Utf-8 decoded String
        failed_testLog = page_content.to_s.force_encoding("UTF-8")
      end
      if result.status == 'failed'
        string1 = 'Scenario: ' + result.test_name
        string2 = 'Duration'
        #Based on Schema i have decided to take the string between Failed header string and the 'Duration'
        messageBody = failed_testLog[/#{string1}(.*?)#{string2}/m, 1]

        printf("Mail Delivered")
        #Send out the mail to the using gmail but ultimately it will the mail id configured should be used.
        Mail.deliver do
          #email-integration@mahesh123.pagerduty.com
          to "monitor@rakuten-india.pagerduty.com"
          from "maheshs039s@gmail.com"
          subject "MHub :: Test Failed for Device ::" + device.device_configuration_id + "Test Name:" + result.test_name
          body messageBody
        end
      end
    end
  end

  # for result in result_collection.results
  #   if failed_testLog == ""
  #     #read the testLog from the url, where xamarin has stored(Its actually in Amazon server)
  #     page_content = open(result_collection.logs.devices.first.test_log)
  #     #Converting to Utf-8 decoded String
  #     failed_testLog = page_content.to_s.force_encoding("UTF-8")
  #   end
  #   if result.status == 'failed'
  #     string1 = 'Scenario: ' + result.test_name
  #     string2 = 'Duration'
  #     #Based on Schema i have decided to take the string between Failed header string and the 'Duration'
  #     messageBody = failed_testLog[/#{string1}(.*?)#{string2}/m, 1]
  #
  #     #Send out the mail to the using gmail but ultimately it will the mail id configured should be used.
  #     Mail.deliver do
  #       to "maheshs039s@gmail.com"
  #       from "maheshs039s@gmail.com"
  #       subject "MHub :: Test Failed " + result.test_name
  #       body messageBody
  #     end
  #   end
  # end
end
