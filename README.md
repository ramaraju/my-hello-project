# Setup

## Generate keystore
We will use this keystore for calabash setup.

`keytool -genkey -v -keystore ~/.android/debug.keystore -alias androiddebugkey -storepass android -keypass android -keyalg RSA -keysize 2048 -validity 10000 -dname "CN=Android Debug,O=Android,C=US"`

## Resign app

`bundle exec calabash-android resign apps/v2.2.0.apk`

## Calabash Setup
Withn this repo, calabash has been setup (see `calabash_settings` file) and keystore has already been included. but, you can use this step to poing calabash to use different keystore.

`bundle exec calabash-android setup (keystore location: ~/.android/debug.keystore,  password: android,  alias: androiddebugkey)`

## Generate adb keys

```
rm ~/.android/key
rm ~/.android/key.pub

sudo adb kill-server
sudo adb start-
```

## Install missing gems

`bundle install`

# Environment Variable Setup

```
export ANDROID_HOME=<path to SKD>

export JAVA_HOME=<path to jdk>

export ADB_DEVICE_ARG=<adb devices sirial number>

export STAGING=<on or empty>
```

> **NOTE**
>
> If STAGING is equal to 'on', it will turn staging switch on. otherwise, automation will be in product environtment.

TODO: update this information

# Run

## Locally

`bundle exec calabash-android run apps/v2.2.0-QA-B05-DEBUG.apk  -p android`

## Run in Xamarin Test Cloud

`bundle exec test-cloud submit apps/<apk name>.apk <xamarin public key string> --devices 9ee45545 --series "master" --locale "en_US" --app-name "Rポイントカード" --user os-qinghua.a.zhang@rakuten.com config/cucumber.yml  --profile android --skip-config-check`

`bundle exec test-cloud submit yourAppFile.apk 08f49d0b0d4ac7c0266f623a3da063eb --devices 1e36a13e --series "master" --locale "ja_JP" --user dhc-kun.wang@dhc.com.cn`
